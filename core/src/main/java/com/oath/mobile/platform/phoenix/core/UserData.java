package com.oath.mobile.platform.phoenix.core;

/**
 * Created by nsoni on 12/2/17.
 */

public class UserData {
    String mEmail;
    String mGivenName;
    String mFamilyName;
    String mGuid;

    String mDisplayName;
    String mLoginId;

    public UserData(String guid, String loginId, String givenName, String familyName, String email, String displayName) {
        mGuid = guid;
        mLoginId = loginId;
        mGivenName = givenName;
        mFamilyName = familyName;
        mEmail = email;
        mDisplayName = displayName;
    }
}
