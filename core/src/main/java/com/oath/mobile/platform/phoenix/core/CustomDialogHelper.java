package com.oath.mobile.platform.phoenix.core;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by yuhongli on 10/30/17.
 */

public class CustomDialogHelper {

    private CustomDialogHelper() {
    }

    public static void generateTwoVerticalButtonDialog(Dialog dialog, String title, CharSequence description, String button1,
                                                       View.OnClickListener onClickListener1, String button2, View.OnClickListener onClickListener2) {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.phoenix_custom_dialog_two_vertical_button);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView titleTextView = dialog.findViewById(R.id.phoenix_custom_dialog_title);
        titleTextView.setText(title);
        TextView descriptionTextView = dialog.findViewById(R.id.account_custom_dialog_description);
        descriptionTextView.setText(description);
        Button dialogButton1 = dialog.findViewById(R.id.account_custom_dialog_button_one);
        dialogButton1.setText(button1);
        dialogButton1.setOnClickListener(onClickListener1);
        TextView dialogButton2 = dialog.findViewById(R.id.account_custom_dialog_button_two);
        dialogButton2.setText(button2);
        dialogButton2.setOnClickListener(onClickListener2);
    }

    public static void generateOneButtonDialog(Dialog dialog, String message, String text1, View.OnClickListener onClickListener1) {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.phoenix_custom_dialog_one_button);
        dialog.findViewById(R.id.phoenix_custom_dialog_title).setVisibility(View.GONE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView messageText = dialog.findViewById(R.id.account_custom_dialog_message);
        messageText.setText(message);
        Button dialogButton = dialog.findViewById(R.id.account_custom_dialog_button);
        dialogButton.setText(text1);
        dialogButton.setOnClickListener(onClickListener1);
    }

    public static void generateOneButtonDialogWithTitle(Dialog dialog, String title, String message, String text1, View.OnClickListener onClickListener1) {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.phoenix_custom_dialog_one_button);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView titleText = (TextView) dialog.findViewById(R.id.phoenix_custom_dialog_title);
        titleText.setText(title);
        titleText.setVisibility(View.VISIBLE);
        TextView messageText = (TextView) dialog.findViewById(R.id.account_custom_dialog_message);
        messageText.setText(message);
        Button dialogButton = (Button) dialog.findViewById(R.id.account_custom_dialog_button);
        dialogButton.setText(text1);
        dialogButton.setOnClickListener(onClickListener1);
    }

    public static Dialog generateProgressDialog(Context context) {
        Dialog dialog = new Dialog(context, R.style.Theme_Phoenix_Dialog);
        dialog.setCancelable(false);
        dialog.setTitle(null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.phoenix_progress_dialog);
        return dialog;
    }
}
