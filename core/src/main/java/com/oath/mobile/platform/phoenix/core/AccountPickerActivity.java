package com.oath.mobile.platform.phoenix.core;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

/**
 * Created by yuhongli on 12/1/17.
 */

public class AccountPickerActivity extends BasePhoenixActivity implements AccountPickerAdapter.Callback {
    static final int REQUEST_CODE_FOR_AUTH = 9001;
    Dialog mProgressDialog;
    Toolbar mToolbar;
    AccountPickerAdapter mAccountsAdapter;
    IAuthManager mAuthManager;
    int mNumOfEnabledAccounts;

    @Override
    public void onCreate(Bundle savedInstanceBundle) {
        super.onCreate(savedInstanceBundle);
        setContentView(R.layout.activity_manage_accounts);
        mToolbar = findViewById(R.id.phoenix_toolbar);
        setupToolbar();
        mAuthManager = AuthManager.getInstance(this);
        RecyclerView accountsView = findViewById(R.id.phoenix_manage_accounts_list);
        setupAccountsView(accountsView);
        EventLogger.getInstance().logUserEvent(EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_START, null);
        mNumOfEnabledAccounts = mAuthManager.getAllAccounts().size();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        TextView title = findViewById(R.id.title);
        title.setText(getResources().getString(R.string.phoenix_account_picker));
    }

    private void setupAccountsView(RecyclerView accountsList) {
        mAccountsAdapter = new AccountPickerAdapter(this, mAuthManager);
        accountsList.setAdapter(mAccountsAdapter);

        accountsList.setHasFixedSize(true);
        accountsList.setLayoutManager(new LinearLayoutManager(this));
    }

    protected void showProgressDialog() {
        if (!isFinishing()) {
            if (mProgressDialog == null) {
                mProgressDialog = CustomDialogHelper.generateProgressDialog(this);
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
            } else {
                mProgressDialog.show();
            }
        }
    }

    protected void safeDismissProgressDialog() {
        if (!isFinishing() && mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setResultForPreLollipop();
    }

    /**
     * This is a solution for issue on KITKAT:
     * When launching AuthActivity from Account Picker, AuthActivity shows up but RESULT_CANCELED is immediately returned to onActivityResult()
     * which causes Account Picker does not dismiss after account sign in successfully, this happens on KITKAT when activity launchMode is set to singleInstance or singleTask.
     * <p>
     * TODO Replace setResult with result receiver in AuthActivity, so we do not need check resultCode in onActivityResult()
     */
    void setResultForPreLollipop() {
        List<IAccount> enabledAccounts = mAuthManager.getAllAccounts();
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT && mNumOfEnabledAccounts != enabledAccounts.size() && enabledAccounts.size() > 0) {
            Intent resultIntent = AuthHelper.createResultIntentForAuth(enabledAccounts.get(0));
            setResultAndFinish(Activity.RESULT_OK, resultIntent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        notifyAccountSetChanged();
    }

    void notifyAccountSetChanged() {
        mAccountsAdapter.notifyAccountSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_FOR_AUTH) {
            if (resultCode == Activity.RESULT_OK) {
                EventLogger.getInstance().logUserEvent(EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_SIGN_IN_SUCCESS, null);
                setResultAndFinish(Activity.RESULT_OK, data);
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // When returning from sign in activity, if there are no accounts to show, dismiss Account picker
                EventLogger.getInstance().logUserEvent(EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_SIGN_IN_CANCEL, null);
                if (mAccountsAdapter.getAccountCount() == 0) {
                    setResultAndFinish(resultCode, null);
                }
            } else if (resultCode == IAuthManager.RESULT_CODE_ERROR) {
                EventLogger.getInstance().logUserEvent(EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_SIGN_IN_ERROR, null);
                // TODO show error dialog
                if (mAccountsAdapter.getAccountCount() == 0) {
                    setResultAndFinish(resultCode, null);
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void setResultAndFinish(int resultCode, Intent data) {
        EventLogger.getInstance().logUserEvent(EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_END, null);
        setResult(resultCode, data);
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        safeDismissProgressDialog();
    }

    @Override
    public void onAddAccount() {
        EventLogger.getInstance().logUserEvent(EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_SIGN_IN_START, null);
        Intent authIntent = new Auth.IntentBuilder().buildSignIn(this);
        authIntent.putExtra(EventLogger.APP_INTERNAL_ORIGIN_DATA_KEY, EventLogger.TRACKING_PARAM_EVENT_ORIGIN_ACCOUNT_PICKER);
        startActivityForResult(authIntent, REQUEST_CODE_FOR_AUTH);
    }

    @Override
    public void onNoAccountsFound() {
        finish();
    }

    @Override
    public void onAccountSelected(int position, IAccount account) {
        showProgressDialog();
        EventLogger.getInstance().logUserEvent(EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_START, null);
        account.refreshToken(this, new OnRefreshTokenResponse() {
            @Override
            public void onSuccess() {
                safeDismissProgressDialog();
                fetchUserInfo((Account) account);
                EventLogger.getInstance().logUserEvent(EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_SUCCESS, null);
                //we don't need to do anything here as the item is toggled else where to on
                Intent resultIntent = AuthHelper.createResultIntentForAuth(account);
                setResultAndFinish(Activity.RESULT_OK, resultIntent);
            }

            @Override
            public void onError(int errorCode) {
                Map<String, Object> errorInfoParams = EventLogger.getRefreshTokenErrorEventParams(null, errorCode);
                EventLogger.getInstance().logUserEvent(EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_ERROR, errorInfoParams);

                safeDismissProgressDialog();
                if (errorCode == OnRefreshTokenResponse.REAUTHORIZE_USER) {
                    runOnUiThread(() -> {
                        notifyAccountSetChanged();
                        showLoginAgainDialog();
                    });
                } else if (errorCode == OnRefreshTokenResponse.NETWORK_ERROR) {
                    runOnUiThread(() -> {
                        notifyAccountSetChanged();
                        showNoNetworkDialog();
                    });
                } else if (errorCode == OnRefreshTokenResponse.SERVER_ERROR) {
                    runOnUiThread(() -> {
                        notifyAccountSetChanged();
                        showTryAgainLaterDialog();
                    });
                } else {
                    //TODO log the exact error if possible, we should not have other errors here
                    runOnUiThread(() -> {
                        notifyAccountSetChanged();
                        showTryAgainLaterDialog();
                    });
                }
            }
        });
    }

    private void fetchUserInfo(final Account account) {
        EventLogger.getInstance().logUserEvent(EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_FETCH_USER_INFO_START, null);
        account.fetchUserInfo(AccountPickerActivity.this, new OnUserDataResponseListener() {
            @Override
            public void onSuccess(UserData userData) {
                if (account.getGUID().equals(userData.mGuid)) {
                    account.setUserName(userData.mLoginId);
                    account.setDisplayName(userData.mDisplayName);
                    account.setFirstName(userData.mGivenName);
                    account.setLastName(userData.mFamilyName);
                    account.setEmail(userData.mEmail);
                    account.setIsActive(true);
                    EventLogger.getInstance().logUserEvent(EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_FETCH_USER_INFO_SUCCESS, null);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            notifyAccountSetChanged();
                        }
                    });
                }
            }

            @Override
            public void onFailure() {
                //TODO log the failure in splunk in addition to user event
                EventLogger.getInstance().logUserEvent(EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_FETCH_USER_INFO_ERROR, null);
            }
        });
    }

    void showLoginAgainDialog() {
        final Dialog customDialog = new Dialog(this);
        CustomDialogHelper.generateTwoVerticalButtonDialog(customDialog,
                this.getString(R.string.phoenix_unable_to_use_this_account),
                this.getString(R.string.phoenix_invalid_refresh_token_error),
                this.getString(R.string.phoenix_continue),
                v -> {
                    customDialog.dismiss();
                    onAddAccount();
                },
                this.getString(R.string.phoenix_cancel),
                v -> customDialog.dismiss());
        customDialog.setCancelable(true);
        customDialog.setCanceledOnTouchOutside(true);
        customDialog.show();
    }

    void showNoNetworkDialog() {
        if (AccountNetworkAPI.isAirplaneModeOn(AccountPickerActivity.this)) {
            AlertUtils.showAirplaneModeDialog(this);
        } else {
            AlertUtils.showErrorDialogWithTitle(this,
                    this.getString(R.string.phoenix_unable_to_use_this_account),
                    this.getString(R.string.phoenix_no_internet_connection));
        }
    }

    void showTryAgainLaterDialog() {
        AlertUtils.showErrorDialogWithTitle(this,
                this.getString(R.string.phoenix_unable_to_use_this_account),
                this.getString(R.string.phoenix_try_again_error));
    }
}


