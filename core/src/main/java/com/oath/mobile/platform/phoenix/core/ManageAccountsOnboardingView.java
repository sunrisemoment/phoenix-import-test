package com.oath.mobile.platform.phoenix.core;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by yuhongli on 1/2/18.
 */

public class ManageAccountsOnboardingView extends DialogFragment {


    static final int TOGGLE_ACCOUNT_VIEW_POSITION = 0;
    static final int REMOVE_ACCOUNT_VIEW_POSITION = 1;

    Button mButton;
    ImageView mCloseButton;
    ImageView mPageOneIndicator;
    ImageView mPageTwoIndicator;
    ViewPager mViewPager;
    DialogInterface.OnDismissListener mDismissListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mDismissListener = (DialogInterface.OnDismissListener) context;
        } catch (ClassCastException exception) {
            throw new ClassCastException(context.toString()
                    + " must implement DialogInterface.OnDismissListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_TUTORIAL_SCREEN, null);
        final View view = inflater.inflate(R.layout.phoenix_manage_accounts_onboarding_layout, container, false);
        mButton = view.findViewById(R.id.button);
        mCloseButton = view.findViewById(R.id.close_action);

        mPageOneIndicator = view.findViewById(R.id.pageOneIndicator);
        mPageOneIndicator.setSelected(true);

        mPageTwoIndicator = view.findViewById(R.id.pageTwoIndicator);

        mViewPager = view.findViewById(R.id.viewpager);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case TOGGLE_ACCOUNT_VIEW_POSITION:
                        mPageOneIndicator.setSelected(true);
                        mPageTwoIndicator.setSelected(false);
                        mButton.setText(getString(R.string.phoenix_manage_accounts_toggle_acct_onboarding_button_text));
                        mCloseButton.setVisibility(View.VISIBLE);
                        break;
                    case REMOVE_ACCOUNT_VIEW_POSITION:
                        mPageTwoIndicator.setSelected(true);
                        mPageOneIndicator.setSelected(false);
                        mButton.setText(getString(R.string.phoenix_manage_accounts_remove_acct_onboarding_button_text));
                        mCloseButton.setVisibility(View.INVISIBLE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewPager.getCurrentItem() == TOGGLE_ACCOUNT_VIEW_POSITION) {
                    mViewPager.setCurrentItem(REMOVE_ACCOUNT_VIEW_POSITION, true);
                } else {
                    dismiss();
                }
            }
        });

        mCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mViewPager.setAdapter(new OnboardingViewAdapter(getChildFragmentManager()));

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return view;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        mDismissListener.onDismiss(dialog);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        mDismissListener.onDismiss(dialog);
    }

    private class OnboardingViewAdapter extends FragmentPagerAdapter {

        public OnboardingViewAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case TOGGLE_ACCOUNT_VIEW_POSITION:
                    return ToggleAccountOnboardingFragment.newInstance();
                case REMOVE_ACCOUNT_VIEW_POSITION:
                    return RemoveAccountOnboardingFragment.newInstance();
                default:
                    throw new IllegalArgumentException("Unexpected position");
            }
        }
    }

}
