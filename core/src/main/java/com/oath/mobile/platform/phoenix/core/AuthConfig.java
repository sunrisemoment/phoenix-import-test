package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.yahoo.mobile.client.share.util.Util;

import net.openid.appauth.AuthorizationServiceConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Made it a Parcelable class so that the object can be sent through an Intent's extra
 * <p>
 * Created by nsoni on 10/20/17.
 */

class AuthConfig implements Parcelable {

    public static final Creator<AuthConfig> CREATOR = new Creator<AuthConfig>() {
        @Override
        public AuthConfig createFromParcel(Parcel source) {
            return new AuthConfig(source);
        }

        @Override
        public AuthConfig[] newArray(int size) {
            return new AuthConfig[size];
        }
    };
    private static final String IDP = "idp";
    private static final String AUTH_ENDPOINT = "authendpoint";
    private static final String TOKEN_ENDPOINT = "tokenendpoint";
    private static final String CLIENT_ID = "clientid";
    private static final String CLIENT_SECRET = "clientsecret";
    private static final String REDIRECT_URI = "redirecturi";
    private static final String SCOPES = "scopes";
    static final String AOL_AUTH_CONFIG_KEY = "com.oath.mobile.platform.phoenix.core.AOL_AUTH_CONFIG_KEY";
    static final String OATH_AUTH_CONFIG_KEY = "com.oath.mobile.platform.phoenix.core.OATH_AUTH_CONFIG_KEY";
    static final String AUTH_CONFIG_EXPIRE_TIME_KEY = "com.oath.mobile.platform.phoenix.core.AUTH_CONFIG_EXPIRE_TIME_KEY";
    static final String OATH_IDP_ISSUER = "https://api.login.aol.com";
    static final String OATH_AUTH_PATH = "/oauth2/request_auth";
    static final String OATH_TOKEN_PATH = "/oauth2/get_token";
    static final String AOL_COM = "api.screenname.aol.com";
    static final String AOL_IDP_ISSUER = "https://" + AOL_COM + "/auth";
    static final String AOL_TOKEN_PATH = "/auth/access_token";
    static final String AOL_AUTH_PATH = "/auth/authorize";

    public static final long DEFAULT_TIME_INTERVAL_FOR_DISCOVERY = TimeUnit.DAYS.toMillis(7); // Retrieve openid configuration once a week.
    static final String SCHEME_HTTPS = "https";
    private static final String TAG = "AuthConfig";
    private String mIdpAuthority;
    private String mAuthPath;
    private String mTokenPath;
    private String mClientID;
    private String mClientSecret;
    private String mRedirectUri;
    private List<String> mScopes;

    AuthConfig(final String idpAuthority, final String authPath, final String tokenPath, final String clientID, final String clientSecret, final String redirectUri, final List<String> scopes) {
        mIdpAuthority = idpAuthority;
        mAuthPath = authPath;
        mTokenPath = tokenPath;
        mClientID = clientID;
        mClientSecret = clientSecret;
        mRedirectUri = redirectUri;
        mScopes = scopes;
    }

    private static AuthConfig getSavedAOLAuthConfig(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(AOL_AUTH_CONFIG_KEY, null);

        if (json != null) {
            try {
                return fromJson(json);
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
                return null;
            }
        } else {
            String clientId = context.getString(R.string.client_id);
            String clientSecret = context.getString(R.string.client_secret);
            String redirectUri = context.getString(R.string.redirect_uri);
            List<String> scopes = Arrays.asList(context.getResources().getStringArray(R.array.scopes));
            AuthConfig authConfig = new AuthConfig(AOL_COM, AOL_AUTH_PATH, AOL_TOKEN_PATH, clientId, clientSecret, redirectUri, scopes);
            authConfig.saveAuthConfig(context, AOL_AUTH_CONFIG_KEY);
            return authConfig;
        }
    }

    private static AuthConfig getSavedOathAuthConfig(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(OATH_AUTH_CONFIG_KEY, null);

        if (json != null) {
            try {
                return fromJson(json);
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
                return null;
            }
        } else {
            // Read from xml file
            String idpAuthority = Uri.parse(OATH_IDP_ISSUER).getAuthority();
            String clientId = context.getString(R.string.client_id);
            String redirectUri = context.getString(R.string.redirect_uri);
            List<String> scopes = Arrays.asList(context.getResources().getStringArray(R.array.scopes));
            AuthConfig authConfig = new AuthConfig(idpAuthority, OATH_AUTH_PATH, OATH_TOKEN_PATH, clientId, null, redirectUri, scopes);
            authConfig.saveAuthConfig(context, OATH_AUTH_CONFIG_KEY);
            return authConfig;
        }

    }


    /**
     * Return auth config for the issuer
     *
     * @param context
     * @param issuer
     * @return
     */
    static AuthConfig getAuthConfigByIssuer(Context context, String issuer) {
        if (!Util.isEmpty(issuer) && AOL_IDP_ISSUER.equals(issuer)) {
            return getSavedAOLAuthConfig(context);
        } else {
            return getSavedOathAuthConfig(context);
        }
    }

    /**
     * Discovery call for Oath IDP, which discovery response follows RFC spec https://openid.net/specs/openid-connect-discovery-1_0.html#ProviderMetadata
     *
     * @param context
     */
    static void initOathOpenIdConfiguration(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        long expireTime = sharedPreferences.getLong(AUTH_CONFIG_EXPIRE_TIME_KEY, 0);
        AuthConfig oathConfig = getSavedOathAuthConfig(context);
        if (oathConfig == null || expireTime <= System.currentTimeMillis()) {
            AuthorizationServiceConfiguration.RetrieveConfigurationCallback callback = (authorizationServiceConfiguration, e) -> {
                if (e != null) {
                    Log.e(TAG, "Failed to fetch configuration for issuer " + OATH_IDP_ISSUER);
                    return;
                } else {
                    try {
                        AuthConfig authConfig = parseDiscoveryResponse(context, authorizationServiceConfiguration);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(OATH_AUTH_CONFIG_KEY, authConfig.toJson());
                        editor.putLong(AUTH_CONFIG_EXPIRE_TIME_KEY, System.currentTimeMillis() + DEFAULT_TIME_INTERVAL_FOR_DISCOVERY); // Discovery response will be expired after 90 days, and app need fetch from server again.
                        editor.apply();
                    } catch (Exception ex) {
                        Log.e(TAG, ex.getMessage());
                    }
                }
            };
            AuthorizationServiceConfiguration.fetchFromIssuer(Uri.parse(OATH_IDP_ISSUER), callback);
        }
    }

    @NonNull
    private static AuthConfig parseDiscoveryResponse(Context context, AuthorizationServiceConfiguration authorizationServiceConfiguration) {
        String redirectUri = context.getString(R.string.redirect_uri);
        String clientId = context.getString(R.string.client_id);
        String returnedIDPAuthority = Uri.parse(authorizationServiceConfiguration.discoveryDoc.getIssuer()).getAuthority();

        String authPath = authorizationServiceConfiguration.discoveryDoc.getAuthorizationEndpoint().getPath();
        String tokenPath = authorizationServiceConfiguration.discoveryDoc.getTokenEndpoint().getPath();
        List<String> scopes = Arrays.asList(context.getResources().getStringArray(R.array.scopes));
        return new AuthConfig(returnedIDPAuthority, authPath, tokenPath, clientId, null, redirectUri, scopes);
    }

    @Nullable
    private static AuthConfig fromJson(String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);

        JSONArray array = jsonObject.getJSONArray(SCOPES);
        List<String> scopes = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            scopes.add(array.getString(i));
        }
        return new AuthConfig(jsonObject.getString(IDP),
                jsonObject.getString(AUTH_ENDPOINT),
                jsonObject.getString(TOKEN_ENDPOINT),
                jsonObject.getString(CLIENT_ID),
                jsonObject.optString(CLIENT_SECRET),
                jsonObject.getString(REDIRECT_URI),
                scopes);
    }

    public String getIdpAuthority() {
        return mIdpAuthority;
    }

    public String getAuthPath() {
        return mAuthPath;
    }

    public String getTokenPath() {
        return mTokenPath;
    }

    public String getClientID() {
        return mClientID;
    }

    public String getClientSecret() {
        return mClientSecret;
    }

    public Uri getRedirectUri() {
        return Uri.parse(mRedirectUri);
    }

    public List<String> getScopes() {
        return mScopes;
    }

    public Uri getAuthUri(Context context) {
        Uri.Builder builder = new Uri.Builder()
                .scheme(SCHEME_HTTPS)
                .authority(mIdpAuthority)
                .path(mAuthPath);
        return new BaseUri(builder)
                .Builder(context)
                .build();
    }

    public Uri getTokenUri(Context context) {
        Uri.Builder builder = new Uri.Builder()
                .scheme(SCHEME_HTTPS)
                .authority(mIdpAuthority)
                .path(mTokenPath);
        return new BaseUri(builder)
                .Builder(context)
                .build();
    }

    String toJson() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(IDP, mIdpAuthority);
        jsonObject.put(AUTH_ENDPOINT, mAuthPath);
        jsonObject.put(TOKEN_ENDPOINT, mTokenPath);
        jsonObject.put(CLIENT_ID, mClientID);
        jsonObject.put(CLIENT_SECRET, mClientSecret);
        jsonObject.put(REDIRECT_URI, mRedirectUri);
        jsonObject.put(SCOPES, new JSONArray(mScopes));
        return jsonObject.toString();
    }

    void saveAuthConfig(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        try {
            editor.putString(key, toJson());
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        editor.apply();
    }

    //------- For making this class parcelable ------//

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mIdpAuthority);
        dest.writeString(this.mAuthPath);
        dest.writeString(this.mTokenPath);
        dest.writeString(this.mClientID);
        dest.writeString(this.mClientSecret);
        dest.writeString(this.mRedirectUri);
        dest.writeStringList(this.mScopes);
    }

    protected AuthConfig(Parcel in) {
        this.mIdpAuthority = in.readString();
        this.mAuthPath = in.readString();
        this.mTokenPath = in.readString();
        this.mClientID = in.readString();
        this.mClientSecret = in.readString();
        this.mRedirectUri = in.readString();
        this.mScopes = in.createStringArrayList();
    }

}
