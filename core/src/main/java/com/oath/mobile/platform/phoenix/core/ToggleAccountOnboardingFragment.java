package com.oath.mobile.platform.phoenix.core;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.concurrent.TimeUnit;

/**
 * Created by yuhongli on 1/2/18.
 */

public class ToggleAccountOnboardingFragment extends Fragment {

    View mDisabledRow;
    View mEnabledRow;

    public static ToggleAccountOnboardingFragment newInstance() {
        return new ToggleAccountOnboardingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.phoenix_toggle_account_onboarding_layout, container, false);
        mDisabledRow = view.findViewById(R.id.toggleAccountOnboardingAnimationDisabledRow);
        mEnabledRow = view.findViewById(R.id.toggleAccountOnboardingAnimationRow);
        startAnimation();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (mDisabledRow != null && mEnabledRow != null) {
            if (isVisibleToUser) {
                startAnimation();
            } else {
                mDisabledRow.setAlpha(0f);
                mEnabledRow.setScaleX(1f);
                mEnabledRow.setScaleY(1f);
            }
        }
    }

    protected void startAnimation() {
        mEnabledRow.animate().scaleX(1.35f).scaleY(1.35f).setDuration(TimeUnit.SECONDS.toMillis(1)).withEndAction(new Runnable() {
            @Override
            public void run() {
                mDisabledRow.animate().alpha(1.0f).setDuration(TimeUnit.SECONDS.toMillis(1));
            }
        });
    }

}
