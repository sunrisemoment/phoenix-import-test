package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Layout;
import android.util.AttributeSet;
import android.util.TypedValue;

/**
 * This TextView reduces size to the minimum if text is too large to fit within the
 * bounds of the TextView. It shows ellipsis in the middle of the text if the text is still too long.
 */
public class SingleLineTextView extends android.support.v7.widget.AppCompatTextView {
    private boolean mHasReducedSize = false;
    private float mMinTextSize = 0;

    public SingleLineTextView(Context context) {
        super(context);
        initialize(null, 0);
    }

    public SingleLineTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(attrs, 0);
    }

    void initialize(AttributeSet attrs, int defStyle) {
        setSingleLine();
        final TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SingleLineTextView, defStyle, 0);
        mMinTextSize = typedArray.getDimension(R.styleable.SingleLineTextView_minTextSize, mMinTextSize);
        float currentSize = getTextSize();
        // honor the minTextSize if currentSize is smaller than minTextSize
        if (currentSize < mMinTextSize) {
            setTextSize(mMinTextSize);
        }
        typedArray.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (!mHasReducedSize) {
            final Layout layout = getLayout();
            if (layout != null) {
                if (layout.getEllipsisCount(0) > 0) {
                    reduceSize();
                }
            }
        }
    }

    void reduceSize() {
        if (mMinTextSize!=0) {
            // since getDimension returns size in px, setting it as px in TextView
            setTextSize(TypedValue.COMPLEX_UNIT_PX, mMinTextSize);
        }
        mHasReducedSize = true;
    }
}
