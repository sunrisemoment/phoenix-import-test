package com.oath.mobile.platform.phoenix.core;

import java.io.IOException;

public class HttpConnectionException extends IOException {
    private int mRespCode;
    private String mRespBody;

    /**
     * Constructor used for generated errors (i.e. login error)
     *
     * @param responseCode
     * @param msg
     * @param responseBody Body of the response
     */
    public HttpConnectionException(int responseCode, String msg, String responseBody) {
        super(msg);
        this.mRespBody = responseBody;
        this.mRespCode = responseCode;
    }

    /**
     * Constructor used for generated errors (i.E login error)
     *
     * @param responseCode
     * @param msg
     */
    public HttpConnectionException(int responseCode, String msg) {
        super(msg);
        this.mRespCode = responseCode;
    }

    /**
     * Gets the HTTP response code associated with this exception.
     *
     * @return HTTP response code from the server
     */
    public final int getRespCode() {
        return mRespCode;
    }

    /**
     * Gets the HTTP response body associated with this exception
     *
     * @return HTTP response body from server
     */
    public final String getRespBody() {
        return mRespBody;
    }

    /**
     * Gets a formatted string of this exception
     *
     * @return formatted string composed of the message, HTTP response code, HTTP response body and error type.
     */
    public final String toString() {
        StringBuilder builder = new StringBuilder();
        String msg = getMessage();
        if (msg != null) {
            builder.append(msg);
            builder.append(", ");
        }
        builder.append("response code: " + mRespCode + ", ");
        builder.append("response body: " );
        builder.append(mRespBody == null ? "null" : mRespBody);
        return builder.toString();
    }
}
