package com.oath.mobile.platform.phoenix.core;

import android.accounts.AccountManager;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.yahoo.mobile.client.share.util.ThreadPoolExecutorSingleton;
import com.yahoo.mobile.client.share.util.Util;

import net.openid.appauth.TokenResponse;

import org.json.JSONException;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CountDownLatch;

import static com.oath.mobile.platform.phoenix.core.Account.KEY_ACCESS_TOKEN;
import static com.oath.mobile.platform.phoenix.core.Account.KEY_ID_TOKEN;
import static com.oath.mobile.platform.phoenix.core.Account.KEY_REAUTHORIZE_USER;
import static com.oath.mobile.platform.phoenix.core.Account.KEY_REFRESH_TOKEN;
import static com.oath.mobile.platform.phoenix.core.Account.REAUTHORIZE_USER;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapError.AUTH_CONFIG_NULL;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapError.COUNTDOWN_LATCH_WAIT_INTERRUPTED;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapError.EVENT_BOOTSTRAP_ERROR;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_ADD_ACCOUNT_FAILURE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_FAILURE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_FETCH_USER_INFO_FAILURE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_FETCH_USER_INFO_START;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_FETCH_USER_INFO_SUCCESS;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_REFRESH_TOKEN_ERROR;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_REFRESH_TOKEN_FAILURE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_REFRESH_TOKEN_START;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_REFRESH_TOKEN_SUCCESS;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_START;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_SUCCESS;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS;

/**
 * Created by nsoni on 9/21/17.
 */

public class AuthManager implements IAuthManager {

    private static final String TAG = AuthManager.class.getSimpleName();
    private static final String INVALID_TOKEN = "invalid-token";
    private static volatile IAuthManager sSingletonInstance = null;
    private final String ACCOUNT_TYPE;
    private AccountManager mAndroidAccManager;

    //Nonce related constants
    static final String UPPER_CASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static final String LOWER_CASE = "abcdefghijklmnopqrstuvwxyz";
    static final String DIGITS = "0123456789";
    static final String DICTIONARY = UPPER_CASE + LOWER_CASE + DIGITS;
    static final int SIZE = 32;
    static final Random RANDOM = new SecureRandom();
    static final char[] SYMBOLS = DICTIONARY.toCharArray();

    //Last used verification nonce, stored as part of this singleton AuthManager
    String mVerificationNonce;

    private AuthManager(Context appContext) {
        mAndroidAccManager = AccountManager.get(appContext);
        ACCOUNT_TYPE = appContext.getString(R.string.account_type);

        AuthConfig.initOathOpenIdConfiguration(appContext);
    }

    public static IAuthManager getInstance(Context context) {
        if (sSingletonInstance == null) {
            synchronized (AuthManager.class) {
                if (sSingletonInstance == null) {
                    sSingletonInstance = new AuthManager(context.getApplicationContext());
                }
            }
        }

        return sSingletonInstance;
    }

    @Override
    public IAccount getAccount(@NonNull String username) {
        android.accounts.Account[] accountsByType = getAccountsByType();
        if (!Util.isEmpty(accountsByType) && !Util.isEmpty(username)) {
            for (android.accounts.Account account : accountsByType) {
                String knownUsername = mAndroidAccManager.getUserData(account, KEY_USERNAME);
                String accessToken = mAndroidAccManager.getUserData(account, Account.KEY_ACCESS_TOKEN);
                if (!Util.isEmpty(accessToken) && username.equals(knownUsername)) {
                    return new Account(mAndroidAccManager, account);
                }
            }
        }
        return null;
    }

    @Override
    public List<IAccount> getAllAccounts() {
        android.accounts.Account[] accountsByType = getAccountsByType();
        if (!Util.isEmpty(accountsByType)) {
            List<IAccount> accounts = new ArrayList<>();
            for (android.accounts.Account account : accountsByType) {
                Account iAccount = new Account(mAndroidAccManager, account);
                if (iAccount.isLoggedIn()) {
                    accounts.add(iAccount);
                }
            }
            return accounts;
        }
        return Collections.emptyList();
    }

    List<IAccount> getAllDisabledAccounts() {
        android.accounts.Account[] accountsByType = getAccountsByType();
        if (!Util.isEmpty(accountsByType)) {
            List<IAccount> accounts = new ArrayList<>();
            for (android.accounts.Account account : accountsByType) {
                Account iAccount = new Account(mAndroidAccManager, account);
                if (!iAccount.isLoggedIn()) {
                    accounts.add(iAccount);
                }
            }
            return accounts;
        }
        return Collections.emptyList();
    }

    // Return all the accounts in the Android Account manager
    List<IAccount> getAllAccountsInternal() {
        android.accounts.Account[] accountsByType = getAccountsByType();
        if (!Util.isEmpty(accountsByType)) {
            List<IAccount> accounts = new ArrayList<>();
            for (android.accounts.Account account : accountsByType) {
                IAccount iAccount = new Account(mAndroidAccManager, account);
                accounts.add(iAccount);
            }
            return accounts;
        }
        return Collections.emptyList();
    }

    @Nullable
    IAccount addAccount(@NonNull TokenResponse tokenResponse) {
        String idTokenValue = tokenResponse.idToken;
        String accessTokenValue = tokenResponse.accessToken;
        String refreshTokenValues = tokenResponse.refreshToken;
        return addAccount(idTokenValue, accessTokenValue, refreshTokenValues);
    }

    IAccount addAccount(BootstrapData bootStrapData) {
        if (!Util.isEmpty(bootStrapData.mGuid)) {
            android.accounts.Account account = new android.accounts.Account(bootStrapData.mGuid, ACCOUNT_TYPE);
            if (mAndroidAccManager.addAccountExplicitly(account, null, null)) {
                updateAccountWithBootStrapData(bootStrapData, account);
                return new Account(mAndroidAccManager, account);
            } else {
                android.accounts.Account existingAccount = lookupAccountByGuid(bootStrapData.mGuid);
                if (existingAccount != null) {
                    //TODO Log telemetry event
                    updateAccountWithBootStrapData(bootStrapData, existingAccount);
                    return new Account(mAndroidAccManager, existingAccount);
                }
            }
        }
        return null;
    }

    private void updateAccountWithBootStrapData(BootstrapData bootStrapData, android.accounts.Account account) {
        mAndroidAccManager.setUserData(account, Account.KEY_GUID, bootStrapData.mGuid);
        mAndroidAccManager.setUserData(account, Account.KEY_DISPLAY_NAME, bootStrapData.mDisplayName);
        mAndroidAccManager.setUserData(account, KEY_USERNAME, bootStrapData.mLoginId);
        mAndroidAccManager.setUserData(account, Account.KEY_REFRESH_TOKEN, bootStrapData.mRefreshToken);
        mAndroidAccManager.setUserData(account, Account.KEY_ISSUER, AuthConfig.AOL_IDP_ISSUER);
        // Adding invalid token so that account is accessible to the app for appropriate messaging
        mAndroidAccManager.setUserData(account, KEY_ACCESS_TOKEN, INVALID_TOKEN);
        // Making the account inactive so that it looks disabled on manage account and stays accessible to the app
        mAndroidAccManager.setUserData(account, KEY_REAUTHORIZE_USER, REAUTHORIZE_USER);
    }

    /**
     * Nonce verification using stored nonce in this singleton's stored nonce variable
     *
     * @param idToken the IdToken that was returned to us by the server
     * @return true when the nonce matches what was sent to the authorization server
     * or we are bypassing the check, false otherwise
     */
    boolean verifyNonce(IdToken idToken) {
        Uri issuer = Uri.parse(idToken.getIssuer());
        //Reset verification nonce after one use
        String verificationNonce = mVerificationNonce;
        mVerificationNonce = null;
        if (issuer != null && issuer.getAuthority() != null && !AuthHelper.isAol(issuer) && verificationNonce != null) {
            String returnedNonce = idToken.getNonce();
            if (!verificationNonce.equals(returnedNonce)) {
                return false;
            }
        }
        return true;
    }

    @Nullable
    IAccount addAccount(String idTokenValue, String accessTokenValue, String refreshTokenValues) {
        IdToken idToken;
        try {
            idToken = IdToken.fromJwt(idTokenValue);
        } catch (JSONException e) {
            Log.e(TAG, "addAccount: error parsing jwt " + e.getMessage());
            return null;
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "addAccount: error with argument " + e.getMessage());
            return null;
        }
        if (!verifyNonce(idToken)) {
            Log.e(TAG, "performTokenRequest: cannot verify nonce sent");
            return null;
        }
        String currentGuid = idToken.getSubscriber();
        android.accounts.Account existingAccount = lookupAccountByGuid(currentGuid); // try to find an existing account with the current guid
        if (existingAccount != null) {
            updateAccountProfile(existingAccount, idToken);
            updateAccountIdToken(existingAccount, idTokenValue);
            updateAccountAccessToken(existingAccount, accessTokenValue);
            updateAccountRefreshToken(existingAccount, refreshTokenValues);
            return new Account(mAndroidAccManager, existingAccount);
        }

        if (idToken.getLegacyGuid() != null) {
            android.accounts.Account existingAccountWithLegacyGuid = lookupAccountByGuid(idToken.getLegacyGuid()); // if current guid does not exist, try the legacy guid
            if (existingAccountWithLegacyGuid != null) {
                mAndroidAccManager.removeAccount(existingAccountWithLegacyGuid, null, null);
            }
        }
        android.accounts.Account account = new android.accounts.Account(currentGuid, ACCOUNT_TYPE);
        if (mAndroidAccManager.addAccountExplicitly(account, null, null)) {
            updateAccountProfile(account, idToken);
            updateAccountIdToken(account, idTokenValue);
            updateAccountAccessToken(account, accessTokenValue);
            updateAccountRefreshToken(account, refreshTokenValues);
            return new Account(mAndroidAccManager, account);
        }

        //TODO Log telemetry event
        return null;
    }

    void updateAccountProfile(android.accounts.Account account, IdToken idToken) {
        mAndroidAccManager.setUserData(account, Account.KEY_FIRST_NAME, idToken.getGivenName());
        mAndroidAccManager.setUserData(account, Account.KEY_LAST_NAME, idToken.getFamilyName());
        mAndroidAccManager.setUserData(account, Account.KEY_GUID, idToken.getSubscriber());
        mAndroidAccManager.setUserData(account, Account.KEY_LEGACY_GUID, idToken.getLegacyGuid());
        //mAndroidAccManager.setUserData(account, Account.KEY_LOCALE, yahooIdToken.getLocale());
        mAndroidAccManager.setUserData(account, Account.KEY_ISSUER, idToken.getIssuer());
        mAndroidAccManager.setUserData(account, Account.KEY_DISPLAY_NAME, idToken.getName());
        mAndroidAccManager.setUserData(account, Account.KEY_EMAIL, idToken.getEmail());
        mAndroidAccManager.setUserData(account, KEY_REAUTHORIZE_USER, null); // when the value is null, it is an active account
        if (Util.isEmpty(idToken.getLoginId())) {
            // Oath idToken does not have login_id
            mAndroidAccManager.setUserData(account, KEY_USERNAME, idToken.getEmail());
        } else {
            // AOL idToken has login_id
            mAndroidAccManager.setUserData(account, KEY_USERNAME, idToken.getLoginId());
        }
        //mAndroidAccManager.setUserData(account, Account.KEY_IMAGE_URI, idToken.getImageUri());
    }

    void updateAccountAccessToken(android.accounts.Account account, String accessTokenValue) {
        mAndroidAccManager.setUserData(account, KEY_ACCESS_TOKEN, accessTokenValue);
    }

    void updateAccountRefreshToken(android.accounts.Account account, String refreshTokenValue) {
        mAndroidAccManager.setUserData(account, KEY_REFRESH_TOKEN, refreshTokenValue);
    }

    void updateAccountIdToken(android.accounts.Account account, String idTokenValue) {
        mAndroidAccManager.setUserData(account, KEY_ID_TOKEN, idTokenValue);
    }

    @NonNull
    android.accounts.Account[] getAccountsByType() {
        return mAndroidAccManager.getAccountsByType(ACCOUNT_TYPE);
    }

    android.accounts.Account lookupAccountByGuid(@NonNull String guid) {
        android.accounts.Account[] accountsByType = getAccountsByType();
        if (!Util.isEmpty(accountsByType)) {
            for (android.accounts.Account account : accountsByType) {
                String knownGuid = mAndroidAccManager.getUserData(account, Account.KEY_GUID);
                if (guid.equals(knownGuid)) {
                    return account;
                }
            }
        }
        return null;
    }

    @VisibleForTesting
    void removeAccounts() {
        for (android.accounts.Account account : getAccountsByType()) {
            mAndroidAccManager.removeAccount(account, null, null);
        }
    }

    @Override
    public void bootstrap(@NonNull Context context, @Nullable List<BootstrapData> dataList, OnBootstrapResponse onBootStrapResponse) throws IllegalStateException {
        EventLogger eventLogger = EventLogger.getInstance();
        Map<String, Object> customParam = new HashMap<>();
        customParam.put(TRACKING_PARAM_ACCOUNT_NUMBERS, Util.isEmpty(dataList) ? 0 : dataList.size());
        eventLogger.logUserEvent(EVENT_BOOTSTRAP_START, customParam);
        AuthConfig authConfig = AuthConfig.getAuthConfigByIssuer(context, AuthConfig.AOL_IDP_ISSUER);
        if (authConfig == null) {
            eventLogger.logErrorInformationEvent(EVENT_BOOTSTRAP_ERROR, AUTH_CONFIG_NULL, "Auth config null in AuthManager class");
            eventLogger.logUserEvent(EVENT_BOOTSTRAP_FAILURE, null);
            throw new IllegalStateException("Auth config is missing!");
        }
        final int[] result = {OnBootstrapResponse.FAILURE};
        if (!Util.isEmpty(dataList)) {
            ThreadPoolExecutorSingleton.getInstance().execute(new Runnable() {
                @Override
                public void run() {

                    // Because reference must be constant or effective final
                    result[0] = OnBootstrapResponse.SUCCESS;
                    int counter = dataList.size();
                    CountDownLatch countDownLatch = new CountDownLatch(counter);
                    for (BootstrapData bootStrapData : dataList) {
                        Account account = (Account) addAccount(bootStrapData);
                        if (account != null) {
                            eventLogger.logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_START, null);
                            account.refreshToken(context, new OnRefreshTokenResponse() {
                                @Override
                                public void onSuccess() {
                                    eventLogger.logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_SUCCESS, null);
                                    eventLogger.logUserEvent(EVENT_BOOTSTRAP_FETCH_USER_INFO_START, null);
                                    account.fetchUserInfo(context, new OnUserDataResponseListener() {
                                        @Override
                                        public void onSuccess(UserData userData) {
                                            eventLogger.logUserEvent(EVENT_BOOTSTRAP_FETCH_USER_INFO_SUCCESS, null);
                                            if (account.getGUID().equals(userData.mGuid)) {
                                                account.setUserName(userData.mLoginId);
                                                account.setDisplayName(userData.mDisplayName);
                                                account.setFirstName(userData.mGivenName);
                                                account.setLastName(userData.mFamilyName);
                                                account.setEmail(userData.mEmail);
                                                account.setIsActive(true);
                                            }
                                            countDownLatch.countDown();
                                        }

                                        @Override
                                        public void onFailure() {
                                            eventLogger.logUserEvent(EVENT_BOOTSTRAP_FETCH_USER_INFO_FAILURE, null);
                                            result[0] = OnBootstrapResponse.FAILURE;
                                            countDownLatch.countDown();
                                        }
                                    });
                                }

                                @Override
                                public void onError(int errorCode) {
                                    eventLogger.logErrorInformationEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_ERROR, errorCode, "Bootstrap refresh token failure");
                                    eventLogger.logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_FAILURE, null);
                                    result[0] = OnBootstrapResponse.FAILURE;
                                    countDownLatch.countDown();
                                }
                            });
                        } else { // either no guid or could not add an account
                            eventLogger.logUserEvent(EVENT_BOOTSTRAP_ADD_ACCOUNT_FAILURE, null);
                            result[0] = OnBootstrapResponse.FAILURE;
                            countDownLatch.countDown();
                        }
                    }
                    try {
                        // wait until all the refresh token callbacks are over
                        countDownLatch.await();
                    } catch (InterruptedException e) {
                        eventLogger.logErrorInformationEvent(EVENT_BOOTSTRAP_ERROR, COUNTDOWN_LATCH_WAIT_INTERRUPTED, "bootstrap: wait on the countdown latch interrupted");
                        result[0] = OnBootstrapResponse.FAILURE;
                    }
                    if (result[0] == OnBootstrapResponse.SUCCESS) {
                        eventLogger.logUserEvent(EVENT_BOOTSTRAP_SUCCESS, null);
                    } else {
                        eventLogger.logUserEvent(EVENT_BOOTSTRAP_FAILURE, null);
                    }
                    if (onBootStrapResponse != null) {
                        onBootStrapResponse.onResponse(result[0]);
                    }
                }
            });
        } else if (onBootStrapResponse != null) {
            eventLogger.logUserEvent(EVENT_BOOTSTRAP_SUCCESS, null);
            onBootStrapResponse.onResponse(OnBootstrapResponse.SUCCESS);
        }
    }

    /**
     * Data class to hold the account data to be handed over to Phoenix during bootstrapping
     */
    public static class BootstrapData {
        final String mDisplayName;
        final String mLoginId;
        final String mGuid;
        final String mRefreshToken;

        public BootstrapData(String guid, String refreshToken, String displayName, String loginId) {
            mGuid = guid;
            mRefreshToken = refreshToken;
            mDisplayName = displayName;
            mLoginId = loginId;
        }
    }

    public interface OnBootstrapResponse {
        int SUCCESS = 0;
        int FAILURE = 1;

        /**
         * Method is invoked if refresh token completed
         *
         * @param result either SUCCESS or FAILURE
         */
        void onResponse(int result);
    }

    @NonNull
    String generateNonce() {
        final char[] buffer = new char[SIZE];
        for (int index = 0; index < buffer.length; ++index) {
            buffer[index] = SYMBOLS[RANDOM.nextInt(SYMBOLS.length)];
        }
        String nonce = new String(buffer);
        //Store this nonce for verification use, since this function is called outside of AuthManager but the verification nonce is used inside of AuthManager
        mVerificationNonce = nonce;
        return nonce;
    }
}
