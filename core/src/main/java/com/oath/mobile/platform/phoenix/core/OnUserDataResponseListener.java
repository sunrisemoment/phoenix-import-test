package com.oath.mobile.platform.phoenix.core;

/**
 * Created by nsoni on 12/1/17.
 */

public interface OnUserDataResponseListener {
    void onSuccess(UserData userData);
    void onFailure();
}
