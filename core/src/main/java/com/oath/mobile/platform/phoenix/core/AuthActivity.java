package com.oath.mobile.platform.phoenix.core;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import net.openid.appauth.AuthorizationException;

import org.json.JSONException;

import java.util.Map;

import static com.oath.mobile.platform.phoenix.core.EventLogger.TRACKING_KEY_ERROR_CODE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.TRACKING_KEY_ERROR_MESSAGE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.getCustomParamsWithOriginInformation;

/**
 * Created by nsoni on 10/9/17.
 */

public class AuthActivity extends BasePhoenixActivity {
    private static final String TAG = "AuthActivity";
    AuthHelper mAuthHelper;
    private boolean mRedirected;
    private boolean mAuthActivityStopped;
    private String mOrigin;

    @Override
    public void onCreate(Bundle savedInstanceBundle) {
        super.onCreate(savedInstanceBundle);
        setContentView(R.layout.auth_activity);

        if (savedInstanceBundle != null) {
            mOrigin = savedInstanceBundle.getString(EventLogger.APP_INTERNAL_ORIGIN_DATA_KEY);
            try {
                mAuthHelper = new AuthHelper(savedInstanceBundle);
                mAuthHelper.initAuthService(this);
            } catch (JSONException e) {
                //TODO handle error
                Log.e(TAG, "Exception white parsing auth request as a json string:" + e);
                onAuthResponse(IAuthManager.RESULT_CODE_ERROR, null, null);
            }
        } else {
            // This is the entry point to the flow
            Intent intent = getIntent();
            String origin = intent.getStringExtra(EventLogger.APP_INTERNAL_ORIGIN_DATA_KEY);
            Map<String, Object> customParams = getCustomParamsWithOriginInformation(null, origin);
            mOrigin = origin;
            EventLogger.getInstance().logUserEvent(EventLogger.SignInUserEvents.EVENT_SIGN_IN_START, customParams);
            mAuthHelper = new AuthHelper(this);
            mAuthHelper.initAuthService(this);
            // Launch the chrome custom tab from here
            Intent customTabIntent = mAuthHelper.createAuthIntent(this);
            startActivity(customTabIntent);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mAuthHelper.saveAuthRequest(outState);
        outState.putString(EventLogger.APP_INTERNAL_ORIGIN_DATA_KEY, mOrigin);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        mRedirected = true;
        // When browser/chrome custom tab redirects back to the app
        Uri data = intent.getData();
        if (mAuthHelper != null) {
            Map<String, Object> customParams = getCustomParamsWithOriginInformation(null, mOrigin);
            EventLogger.getInstance().logUserEvent(EventLogger.SignInUserEvents.EVENT_SIGN_IN_REDIRECT, customParams);
            mAuthHelper.handleAuthResponse(this, data, (resultCode, responseData, authorizationException) -> onAuthResponse(resultCode, responseData, authorizationException));
        } else {
            onAuthResponse(IAuthManager.RESULT_CODE_ERROR, null, null);
        }
        super.onNewIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // On app switch or when pressing back on browser/chrome custom tab
        if (!mRedirected && mAuthActivityStopped) {
            onAuthResponse(RESULT_CANCELED, null, null);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuthActivityStopped = true;
    }

    private void handleErrorResponse(final AuthorizationException authorizationException, final int resultCode, final Intent responseData) {
        DialogActionListener dialogActionListener = new DialogActionListener() {
            @Override
            public void onDialogDismissed() {
                setResultAndFinish(resultCode, responseData);
            }
        };
        AuthActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (authorizationException != null && AuthorizationException.GeneralErrors.NETWORK_ERROR.equals(authorizationException)) {
                    showErrorDialog(getString(R.string.phoenix_no_internet_connection), dialogActionListener);
                } else {
                    showErrorDialog(getString(R.string.phoenix_try_again_error), dialogActionListener);
                }
            }
        });
    }

    private void setResultAndFinish(int resultCode, Intent responseData) {
        setResult(resultCode, responseData);
        AuthActivity.this.finish();
    }

    void onAuthResponse(int resultCode, Intent responseData, AuthorizationException authorizationException) {
        Map<String, Object> customParams = getCustomParamsWithOriginInformation(null, mOrigin);
        if (resultCode == RESULT_OK) {
            EventLogger.getInstance().logUserEvent(EventLogger.SignInUserEvents.EVENT_SIGN_IN_SUCCESS, customParams);
            setResultAndFinish(resultCode, responseData);
        } else if (resultCode == IAuthManager.RESULT_CODE_ERROR) {

            if (authorizationException != null) {
                int code;
                String msg;
                if (AuthorizationException.AuthorizationRequestErrors.INVALID_REQUEST.equals(authorizationException)) {
                    code = EventLogger.SignInUserEvents.INVALID_REQUEST;
                    msg = EventLogger.SignInUserEvents.INVALID_REQUEST_MSG;
                } else if (AuthorizationException.AuthorizationRequestErrors.UNAUTHORIZED_CLIENT.equals(authorizationException)) {
                    code = EventLogger.SignInUserEvents.UNAUTHORIZED_CLIENT;
                    msg = EventLogger.SignInUserEvents.UNAUTHORIZED_CLIENT_MSG;
                } else if (AuthorizationException.AuthorizationRequestErrors.ACCESS_DENIED.equals(authorizationException)) {
                    code = EventLogger.SignInUserEvents.ACCESS_DENIED;
                    msg = EventLogger.SignInUserEvents.ACCESS_DENIED_MSG;
                } else if (AuthorizationException.AuthorizationRequestErrors.UNSUPPORTED_RESPONSE_TYPE.equals(authorizationException)) {
                    code = EventLogger.SignInUserEvents.UNSUPPORTED_RESPONSE_TYPE;
                    msg = EventLogger.SignInUserEvents.UNSUPPORTED_RESPONSE_TYPE_MSG;
                } else if (AuthorizationException.AuthorizationRequestErrors.INVALID_SCOPE.equals(authorizationException)) {
                    code = EventLogger.SignInUserEvents.INVALID_SCOPE;
                    msg = EventLogger.SignInUserEvents.INVALID_SCOPE_MSG;
                } else if (AuthorizationException.AuthorizationRequestErrors.SERVER_ERROR.equals(authorizationException)) {
                    code = EventLogger.SignInUserEvents.SERVER_ERROR;
                    msg = EventLogger.SignInUserEvents.SERVER_ERROR_MSG;
                } else if (AuthorizationException.AuthorizationRequestErrors.TEMPORARILY_UNAVAILABLE.equals(authorizationException)) {
                    code = EventLogger.SignInUserEvents.TEMPORARILY_UNAVAILABLE;
                    msg = EventLogger.SignInUserEvents.TEMPORARILY_UNAVAILABLE_MSG;
                } else if (AuthorizationException.AuthorizationRequestErrors.CLIENT_ERROR.equals(authorizationException)) {
                    code = EventLogger.SignInUserEvents.CLIENT_ERROR;
                    msg = EventLogger.SignInUserEvents.CLIENT_ERROR_MSG;
                } else {
                    code = EventLogger.SignInUserEvents.GENERAL_ERROR;
                    msg = EventLogger.SignInUserEvents.GENERAL_ERROR_MSG;
                }
                customParams.put(TRACKING_KEY_ERROR_CODE, code);
                customParams.put(TRACKING_KEY_ERROR_MESSAGE, msg);
            }
            EventLogger.getInstance().logUserEvent(EventLogger.SignInUserEvents.EVENT_SIGN_IN_FAILURE, customParams);
            handleErrorResponse(authorizationException, resultCode, responseData);
        } else if (resultCode == RESULT_CANCELED) {
            // TODO: define proper string toast
//            AlertUtils.showToast(this, "Operation canceled");
            EventLogger.getInstance().logUserEvent(EventLogger.SignInUserEvents.EVENT_SIGN_IN_USER_CANCELED, customParams);
            setResultAndFinish(resultCode, responseData);
        } else {
            EventLogger.getInstance().logUserEvent(EventLogger.SignInUserEvents.EVENT_SIGN_IN_FAILURE, customParams);
            setResultAndFinish(resultCode, responseData);
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    protected void onDestroy() {
        if (mAuthHelper != null) {
            mAuthHelper.disposeCustomTabService();
        }
        super.onDestroy();
    }

    private void showErrorDialog(final String msg, DialogActionListener dialogActionListener) {
        final Dialog customDialog = new Dialog(AuthActivity.this);
        CustomDialogHelper.generateOneButtonDialog(customDialog, msg, AuthActivity.this.getString(R.string.phoenix_ok),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customDialog.dismiss();
                        dialogActionListener.onDialogDismissed();
                    }
                });
        customDialog.setCancelable(false);
        customDialog.setCanceledOnTouchOutside(false);
        if (!AuthActivity.this.isFinishing()) {
            customDialog.show();
        }
    }

    private interface DialogActionListener {
        void onDialogDismissed();
    }

}
