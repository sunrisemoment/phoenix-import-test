package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.yahoo.mobile.client.share.util.Util;

/**
 * Created by nsoni on 10/18/17.
 */

public class Auth {


    /**
     * Creates an intent that would launch the chrome custom tab/external browser to authenticate a user.
     * Use the intent to start an activity.
     */
    private Auth() {
    }

    public static class IntentBuilder {

        Intent buildSignIn(Context context) {
            return createIntent(context);
        }

        public Intent build(Context context) {
            AuthManager authManager = (AuthManager) AuthManager.getInstance(context);
            if (authManager.getAllDisabledAccounts().size() != 0) {
                Intent intent = new Intent(context, AccountPickerActivity.class);
                return intent;
            } else {
                return createIntent(context);
            }
        }

        @NonNull
        Intent createIntent(Context context) {
            checkNullValues(context);
            Context appContext = context.getApplicationContext();
            Intent intent = new Intent(appContext, AuthActivity.class);
            intent.putExtra(EventLogger.APP_INTERNAL_ORIGIN_DATA_KEY, EventLogger.TRACKING_PARAM_EVENT_ORIGIN_APP);
            return intent;
        }

        void checkNullValues(Context context) {
            AuthConfig authConfig = AuthConfig.getAuthConfigByIssuer(context, null);
            if (Util.isEmpty(authConfig.getIdpAuthority())) {
                throw new IllegalArgumentException("Identity provider is missing");
            }
            if (Util.isEmpty(authConfig.getAuthPath())) {
                throw new IllegalArgumentException("Auth path is missing");
            }

            if (Util.isEmpty(authConfig.getTokenPath())) {
                throw new IllegalArgumentException("Token path is missing");
            }

            if (Util.isEmpty(authConfig.getClientID())) {
                throw new IllegalArgumentException("Client Id is missing");
            }
            if (Util.isEmpty(authConfig.getRedirectUri())) {
                throw new IllegalArgumentException("Redirect Uri is missing");
            }
        }
    }
}
