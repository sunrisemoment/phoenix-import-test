package com.oath.mobile.platform.phoenix.core;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;

class AlertUtils {

    private AlertUtils() {
    }

    private static final String DEFAULT_BROWSER_LANDING_PAGE = "https://www.yahoo.com";

    public static void showNetworkErrorDialog(Activity activity, int errorCode, String message) {
        switch (errorCode) {
            case AccountNetworkAPI.ERROR_CODE_NETWORK_UNAVAILABLE_AIRPLANE_MODE:
                AlertUtils.showAirplaneModeDialog(activity);
                break;
            case AccountNetworkAPI.ERROR_CODE_ACCOUNT_NETWORK_UNAVAILABLE:
            case AccountNetworkAPI.ERROR_CODE_NETWORK_TIME_OUT:
                AlertUtils.showErrorDialog(activity, message);
                break;
            case AccountNetworkAPI.ERROR_CODE_ACCOUNT_NETWORK_SSL_VERIFICATION_ERROR:
                AlertUtils.showNetworkAuthenticationDialog(activity);
                break;
            default:
                AlertUtils.showToast(activity, message);
                break;
        }
    }

    /**
     * display the standard Settings dialog for Airplane mode suggestion on Android,
     * display an alert message.
     *
     * @param activity
     */
    public static void showAirplaneModeDialog(final Activity activity) {

        final Dialog customDialog = new Dialog(activity);
        CustomDialogHelper.generateTwoVerticalButtonDialog(customDialog,
                activity.getString(R.string.phoenix_login_airplane_title),
                activity.getString(R.string.phoenix_login_airplane_mode),
                activity.getString(R.string.cancel),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customDialog.dismiss();
                        return;
                    }
                },
                activity.getString(R.string.phoenix_android_settings),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        activity.startActivity(intent);
                        customDialog.dismiss();
                        return;
                    }
                });
        customDialog.setCancelable(false);
        customDialog.setCanceledOnTouchOutside(false);
        if (!activity.isFinishing()) {
            customDialog.show();
        }
    }

    /**
     * display and alert for serious error like network unavailable
     */
    public static void showErrorDialog(final Activity activity, final String msg) {
        final Dialog customDialog = new Dialog(activity);
        CustomDialogHelper.generateOneButtonDialog(customDialog, msg, activity.getString(R.string.phoenix_ok),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customDialog.dismiss();
                    }
                });
        customDialog.setCancelable(false);
        customDialog.setCanceledOnTouchOutside(false);
        if (!activity.isFinishing()) {
            customDialog.show();
        }
    }

    /**
     * Display an alert dialog with only dismiss action with title and message
     * @param activity the activity that is this dialog's parent
     * @param title the title of the alert
     * @param msg the message of the alert
     */
    public static void showErrorDialogWithTitle(final Activity activity, final String title, final String msg) {
        final Dialog customDialog = new Dialog(activity);
        CustomDialogHelper.generateOneButtonDialogWithTitle(customDialog, title, msg, activity.getString(R.string.phoenix_ok), v -> customDialog.dismiss());
        customDialog.setCancelable(false);
        customDialog.setCanceledOnTouchOutside(false);
        if (!activity.isFinishing()) {
            customDialog.show();
        }
    }

    /**
     * Prompt user to do browser authentication when we get back some HTML because the wifi network
     * requires the user to put a password or accept some terms in a web page because
     * accessing the network.
     *
     * @param activity
     */
    public static void showNetworkAuthenticationDialog(final Activity activity) {
        final Dialog customDialog = new Dialog(activity);
        CustomDialogHelper.generateTwoVerticalButtonDialog(customDialog, "", activity.getString(R.string.phoenix_network_authentication_required),
                activity.getString(R.string.cancel),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customDialog.dismiss();
                    }
                },
                activity.getString(R.string.phoenix_go_to_browser),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(DEFAULT_BROWSER_LANDING_PAGE));
                        activity.startActivity(browserIntent);
                        customDialog.dismiss();
                    }
                });
        customDialog.setCancelable(true);
        customDialog.setCanceledOnTouchOutside(true);
        if (!activity.isFinishing()) {
            customDialog.show();
        }
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
