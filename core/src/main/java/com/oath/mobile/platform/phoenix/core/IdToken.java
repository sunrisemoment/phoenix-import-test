package com.oath.mobile.platform.phoenix.core;

import android.os.Build;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Created by billhuang on 11/2/17.
 */

class IdToken {
    private static final String ID_TOKEN_DELIMITER = "\\.";
    private static final String TAG = IdToken.class.getSimpleName();

    //Standard JWT items
    private static final String ISSUER = "iss";
    private static final String SUBJECT = "sub";
    private static final String AUDIENCE = "aud";
    private static final String EXPIRATION_TIME = "exp";
    private static final String ISSUED_AT = "iat";
    private static final String NONCE = "nonce";
    private static final String ACCESS_TOKEN_HASH = "at_hash";
    private static final String NAME = "name";
    private static final String GIVEN_NAME = "given_name";
    private static final String FAMILY_NAME = "family_name";
    private static final String EMAIL = "email";
    private static final String LOGIN_ID = "login_id";
    private static final String LEGACY_GUID = "aol_legacy_guid";

    //Standard JWT items
    private String mIss = null;
    private String mSub = null;
    private String mAud = null;
    private long mExp = TIME_STAMP_ERROR;
    private long mIat = TIME_STAMP_ERROR;
    private String mNonce = null;
    private String mAtHash = null;
    private String mName = null;
    private String mGivenName = null;
    private String mFamilyName = null;
    private String mEmail = null;
    private String mLoginId = null;
    private String mLegacyGuid = null;

    JSONObject mJsonObject;

    public static final long TIME_STAMP_ERROR = -1;

    /**
     * This constructs an IdToken from a IDToken JWT string.
     *
     * @param pJwt the entire IDtoken JWT string
     * @return the corresponding ID information read from the payload of the JWT
     */
    public static IdToken fromJwt(String pJwt) throws JSONException, IllegalArgumentException {
        IdToken idToken = new IdToken();
        idToken.read(pJwt);
        return idToken;
    }

    public String getIssuer() {
        return mIss;
    }

    public String getSubscriber() {
        return mSub;
    }

    public String getLegacyGuid() {
        return mLegacyGuid;
    }

    public String getAudience() {
        return mAud;
    }

    public long getExpirationTime() {
        return mExp;
    }

    public long getIssuedAtTime() {
        return mIat;
    }

    public String getNonce() {
        return mNonce;
    }

    public String getAccessTokenHash() {
        return mAtHash;
    }

    public String getName() {
        return mName;
    }

    public String getGivenName() {
        return mGivenName;
    }

    public String getFamilyName() {
        return mFamilyName;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getLoginId() {
        return mLoginId;
    }

    /**
     * This is a generic getter for any additional claims that you know is contained with this ID
     * Token.
     *
     * @param pClaimName the name of the key to be used for look up
     * @return the object that contains the value for that key you are looking for
     */
    Object getAdditionalClaim(String pClaimName) {
        if (mJsonObject == null) {
            return null;
        }
        try {
            return mJsonObject.get(pClaimName);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }

    /**
     * This will read the associated fields with this class.
     * Override if you are making a custom Id token class (IE one for Yahoo, one for AoL, etc.)
     * Should call super to retrieve the information as needed for jsonObjects
     *
     * @param pJwt the jwt ID token.
     */
    void read(String pJwt) throws JSONException, IllegalArgumentException {
        mJsonObject = getClaimPayloadAsJsonFromJwt(pJwt);
        mIss = mJsonObject.optString(ISSUER);
        mSub = mJsonObject.optString(SUBJECT);
        mLegacyGuid = mJsonObject.optString(LEGACY_GUID);
        mAud = mJsonObject.optString(AUDIENCE);
        mExp = mJsonObject.optLong(EXPIRATION_TIME);
        mIat = mJsonObject.optLong(ISSUED_AT);
        mNonce = mJsonObject.optString(NONCE, null);
        mAtHash = mJsonObject.optString(ACCESS_TOKEN_HASH, null);
        mName = mJsonObject.optString(NAME);
        mGivenName = mJsonObject.optString(GIVEN_NAME);
        mFamilyName = mJsonObject.optString(FAMILY_NAME);
        mEmail = mJsonObject.optString(EMAIL);
        mLoginId = mJsonObject.optString(LOGIN_ID);
    }

    /**
     * Convert a JWT into just the Payload as a JSONObject
     *
     * @param pJwt the entire jwt string, with 2 dots signifying the 3 different parts header.payload.signature
     * @return the jsonobject containing the user's information
     * @throws IllegalArgumentException when the argument is not a valid JWT or when it is null
     * @throws JSONException            when the json payload is not correct
     */
    JSONObject getClaimPayloadAsJsonFromJwt(String pJwt) throws IllegalArgumentException, JSONException {
        if (pJwt == null || pJwt.isEmpty()) {
            throw new IllegalArgumentException("Null or empty argument");
        }
        String[] data = pJwt.split(ID_TOKEN_DELIMITER);
        if (data.length == 3) {
            String claim = data[1];
            byte[] claimDecoded = Base64.decode(claim, Base64.URL_SAFE);
            String claimString = "";
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                claimString = new String(claimDecoded, StandardCharsets.UTF_8);
            } else {
                claimString = new String(claimDecoded, Charset.forName("UTF-8"));
            }
            return new JSONObject(claimString);
        } else {
            throw new IllegalArgumentException("Invalid data format, not a JWT header.payload.signature string");
        }
    }

}
