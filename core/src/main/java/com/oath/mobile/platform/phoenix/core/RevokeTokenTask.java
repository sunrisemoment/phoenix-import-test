package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import java.util.HashMap;
import java.util.Map;

import static com.oath.mobile.platform.phoenix.core.AuthHelper.RevokeTokenResponseListener.REVOKE_TOKEN_SUCCESS;

/**
 * Created by yuhongli on 11/2/17.
 */

public class RevokeTokenTask extends AsyncTask<Object, Void, Void> {

    private static final String HTTPS = "https";
    private static final String API_LOGIN_BASE_URL = "api.login.yahoo.com";
    private static final String REVOKE_TOKEN_PATH = "oauth2/revoke";
    private static final String KEY_TOKEN = "token";
    private static final String KEY_TOKEN_TYPE_HINT = "token_type_hint";
    private static final String KEY_CLIENT_ID = "client_id";
    private static final String VALUE_TOKEN_TYPE_REFRESH_TOKEN = "refresh_token";

    private int mErrorCode = -1;
    private String mErrorMessage = null;
    private AuthHelper.RevokeTokenResponseListener mListener;

    RevokeTokenTask(AuthHelper.RevokeTokenResponseListener listener) {
        mListener = listener;
    }

    @Override
    protected Void doInBackground(Object... params) {
        Context context = (Context) params[0];
        AccountNetworkAPI networkAPI = (AccountNetworkAPI) params[1];
        String clientId = (String) params[2];
        String refreshToken = (String) params[3];

        String url = buildRequestUri(context, API_LOGIN_BASE_URL, REVOKE_TOKEN_PATH);

        final Map<String, String> requestBody = createRequestParams(refreshToken, clientId);

        try {
            networkAPI.executeFormPost(context, url, null, requestBody);
        } catch (HttpConnectionException e) {
            mErrorCode = e.getRespCode();
            mErrorMessage = e.getMessage();
        }
        return null;
    }

    Map<String, String> createRequestParams(String refreshToken, String clientId) {
        Map<String, String> map = new HashMap<>();

        map.put(KEY_TOKEN, refreshToken);
        map.put(KEY_TOKEN_TYPE_HINT, VALUE_TOKEN_TYPE_REFRESH_TOKEN);
        map.put(KEY_CLIENT_ID, clientId);
        return map;
    }

    private String buildRequestUri(Context context, String authority, String path) {
        Uri authUri = Uri.parse(path);
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(HTTPS)
                .authority(authority)
                .appendEncodedPath(authUri.getEncodedPath())
                .encodedQuery(authUri.getQuery());

        return new BaseUri(builder).Builder(context).toString();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (mListener != null) {
            if (mErrorMessage != null) {
                mListener.onComplete(mErrorCode, mErrorMessage);
            } else {
                mListener.onComplete(REVOKE_TOKEN_SUCCESS, null);
            }
        }

    }
}
