package com.oath.mobile.platform.phoenix.core;

import android.content.Context;

/**
 * Created by nsoni on 9/21/17.
 */

public interface IAccount {

    /**
     * Provides the access token for a logged in account
     *
     * @return Access token as a String
     */
    String getToken();

    /**
     * Get first name of this account.
     *
     * @return first name
     */
    String getFirstName();

    /**
     * Get last name of this account.
     *
     * @return last name
     */
    String getLastName();

    /**
     * Get full name that is supposed to be displayed to the user
     *
     * @return full name
     */
    String getDisplayName();

    /**
     * Get the email for this account
     *
     * @return email id of this account
     */
    String getEmail();

    /**
     * GUID is the unique identifier for the account. Apps should use GUID to key their data in the system.
     *
     * @return GUID string
     */
    String getGUID();

    /**
     * After IDP switch the account will have new GUID from the new IDP.
     * This API will return the GUID from the old IDP.
     *
     * @return GUID string
     */
    String getLegacyGUID();

    /**
     * Get the user name for this account
     *
     * @return the user name.
     */
    String getUserName();

    /**
     * Get the user image uri for this account
     *
     * @return the user image uri.
     */
    String getImageUri();

    /**
     * Get the issuer for this account
     *
     * @return the issuer.
     */
    String getIssuer();

    /**
     * If the account has an invalid token and needs re-authorization.
     *
     * @return true when the account is active, or false when re-auth is required
     * before the account can be used. See {@link Account} for more information.
     */
    boolean isActive();

    /**
     * Refresh access token asynchronously, called when access token is expired or about to expire.
     * The result will be returned in onRefreshTokenResponse.
     *
     * @param {@link com.oath.mobile.platform.phoenix.core.OnRefreshTokenResponse}
     */
    void refreshToken(Context context, OnRefreshTokenResponse onRefreshTokenResponse);
}
