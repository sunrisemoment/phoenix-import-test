package com.oath.mobile.platform.phoenix.core;

import android.support.annotation.Nullable;

import com.oath.mobile.analytics.Config;
import com.oath.mobile.analytics.OathAnalytics;
import com.oath.mobile.analytics.helper.EventParamMap;

import java.util.HashMap;
import java.util.Map;

import static com.oath.mobile.platform.phoenix.core.AuthHelper.RefreshTokenResponseListener.ERROR_CODE_IDP_SWITCHED;
import static com.oath.mobile.platform.phoenix.core.OnRefreshTokenResponse.INVALID_REQUEST;
import static com.oath.mobile.platform.phoenix.core.OnRefreshTokenResponse.INVALID_SCOPE;
import static com.oath.mobile.platform.phoenix.core.OnRefreshTokenResponse.NETWORK_ERROR;
import static com.oath.mobile.platform.phoenix.core.OnRefreshTokenResponse.REAUTHORIZE_USER;
import static com.oath.mobile.platform.phoenix.core.OnRefreshTokenResponse.SERVER_ERROR;
import static com.oath.mobile.platform.phoenix.core.OnRefreshTokenResponse.UNAUTHORIZED_CLIENT;

/**
 * Created by billhuang on 12/5/17.
 */

class EventLogger {
    static final String TRACKING_KEY_SDK_OS = "p_os";
    static final String TRACKING_PARAM_SDK_OS = "p_android";
    static final String TRACKING_KEY_SDK_VERSION = "p_ver";
    static final String TRACKING_PARAM_SDK_VERSION = BuildConfig.VERSION_NAME;
    static final String APP_INTERNAL_DATA_PREFIX = "com.oath.mobile.platform.phoenix.core";

    static final String TRACKING_KEY_ERROR_CODE = "p_e_code";
    static final String TRACKING_KEY_ERROR_MESSAGE = "p_e_msg";

    static final String APP_INTERNAL_ORIGIN_DATA_KEY = APP_INTERNAL_DATA_PREFIX + ".OriginData";
    static final String TRACKING_KEY_EVENT_ORIGIN = "p_e_origin";
    static final String TRACKING_PARAM_EVENT_ORIGIN_MANAGE_ACCOUNT = "manage_accounts";
    static final String TRACKING_PARAM_EVENT_ORIGIN_APP = "app";
    static final String TRACKING_PARAM_EVENT_ORIGIN_ACCOUNT_PICKER = "account_picker";

    //Max overall size is 7k worth of payload, but we should stay below it as much as we can
    static final int TRACKING_PARAM_MAX_SIZE = 1000;
    volatile static EventLogger sEventLoggerSingleton;

    private EventLogger() {

    }

    public static EventLogger getInstance() {
        if (sEventLoggerSingleton == null) {
            synchronized (EventLogger.class) {
                if (sEventLoggerSingleton == null) {
                    sEventLoggerSingleton = new EventLogger();
                }
            }
        }
        return sEventLoggerSingleton;
    }

    void logUserEvent(String eventName, Map<String, Object> customParams) {
        EventParamMap eventParamMap = EventParamMap.withDefaults();
        eventParamMap.sdkName(TRACKING_PARAM_SDK_OS);
        if (customParams == null) {
            customParams = new HashMap<>();
        }
        customParams.put(TRACKING_KEY_SDK_VERSION, TRACKING_PARAM_SDK_VERSION);
        customParams.put(TRACKING_KEY_SDK_OS, TRACKING_PARAM_SDK_OS);
        eventParamMap.customParams(customParams);
        OathAnalytics.logEvent(eventName, Config.EventType.STANDARD, Config.EventTrigger.UNCATEGORIZED, eventParamMap);
    }

    void logErrorInformationEvent(String eventName, int statusCode, String msg) {
        EventParamMap eventParamMap = EventParamMap.withDefaults();
        eventParamMap.sdkName(TRACKING_PARAM_SDK_OS);
        HashMap<String, Object> customParams = new HashMap<>();
        customParams.put(TRACKING_KEY_SDK_VERSION, TRACKING_PARAM_SDK_VERSION);
        customParams.put(TRACKING_KEY_SDK_OS, TRACKING_PARAM_SDK_OS);
        customParams.put(TRACKING_KEY_ERROR_CODE, statusCode);
        customParams.put(TRACKING_KEY_ERROR_MESSAGE, msg);
        eventParamMap.customParams(customParams);
        OathAnalytics.logEvent(eventName, Config.EventType.STANDARD, Config.EventTrigger.UNCATEGORIZED, eventParamMap);
    }

    /**
     * Use this method to insert origin information to an existing custom parameter map, or create
     * a new one from scratch with just the origin information included.
     * <p>
     * The origin information indicates who launched the activity that we are tracking.
     *
     * @param customParams the existing custom param map, or null if you wish to get a new map
     * @param origin       the origin information
     * @return an Map that contains the origin information for logging to flurry
     */
    static protected Map<String, Object> getCustomParamsWithOriginInformation(@Nullable Map<String, Object> customParams, String origin) {
        if (customParams != null) {
            customParams.put(EventLogger.TRACKING_KEY_EVENT_ORIGIN, origin);
            return customParams;
        } else {
            HashMap<String, Object> params = new HashMap<>();
            params.put(EventLogger.TRACKING_KEY_EVENT_ORIGIN, origin);
            return params;
        }
    }

    /**
     * This method is to translate our internal error code into event logger error messages for
     * refresh token error information
     *
     * @param customParams a nullable map where you want the custom event to be inserted in,
     *                     if null we will make a new one and return it to you
     * @param errorCode    the error code itself, it would be an {@link OnRefreshTokenResponse} error
     *                     code.
     * @return the custom param map to be used with our flurry / benzene logger
     */
    static protected Map<String, Object> getRefreshTokenErrorEventParams(@Nullable Map<String, Object> customParams, int errorCode) {
        int eventErrorCode;
        String eventErrorMsg;
        if (INVALID_REQUEST == errorCode) {
            eventErrorCode = EventLogger.RefreshTokenUserEvents.INVALID_REQUEST;
            eventErrorMsg = EventLogger.RefreshTokenUserEvents.INVALID_REQUEST_MSG;
        } else if (REAUTHORIZE_USER == errorCode) {
            eventErrorCode = EventLogger.RefreshTokenUserEvents.REAUTHORIZE_USER;
            eventErrorMsg = EventLogger.RefreshTokenUserEvents.REAUTHORIZE_USER_MSG;
        } else if (ERROR_CODE_IDP_SWITCHED == errorCode) {
            eventErrorCode = RefreshTokenUserEvents.IDP_SWITCHED;
            eventErrorMsg = RefreshTokenUserEvents.IDP_SWITCHED_MSG;
        } else if (UNAUTHORIZED_CLIENT == errorCode) {
            eventErrorCode = EventLogger.RefreshTokenUserEvents.UNAUTHORIZED_CLIENT;
            eventErrorMsg = EventLogger.RefreshTokenUserEvents.UNAUTHORIZED_CLIENT_MSG;
        } else if (INVALID_SCOPE == errorCode) {
            eventErrorCode = EventLogger.RefreshTokenUserEvents.INVALID_SCOPE;
            eventErrorMsg = EventLogger.RefreshTokenUserEvents.INVALID_SCOPE_MSG;
        } else if (NETWORK_ERROR == errorCode) {
            eventErrorCode = EventLogger.RefreshTokenUserEvents.NETWORK_ERROR;
            eventErrorMsg = EventLogger.RefreshTokenUserEvents.NETWORK_ERROR_MSG;
        } else if (SERVER_ERROR == errorCode) {
            eventErrorCode = EventLogger.RefreshTokenUserEvents.SERVER_ERROR;
            eventErrorMsg = EventLogger.RefreshTokenUserEvents.SERVER_ERROR_MSG;
        } else {
            eventErrorCode = EventLogger.RefreshTokenUserEvents.GENERAL_ERROR;
            eventErrorMsg = EventLogger.RefreshTokenUserEvents.GENERAL_ERROR_MSG;
        }

        if (customParams != null) {
            customParams.put(TRACKING_KEY_ERROR_CODE, eventErrorCode);
            customParams.put(TRACKING_KEY_ERROR_MESSAGE, eventErrorMsg);
            return customParams;
        } else {
            HashMap<String, Object> resultParams = new HashMap<>();
            resultParams.put(TRACKING_KEY_ERROR_CODE, eventErrorCode);
            resultParams.put(TRACKING_KEY_ERROR_MESSAGE, eventErrorMsg);
            return resultParams;
        }
    }

    static class RefreshTokenInternalError {
        static final String EVENT_REFRESH_TOKEN_CLIENT_ERROR = "phnx_refresh_token_client_error";
        static final int AUTH_CONFIG_NULL = 1;
        static final int AUTH_CONFIG_ERROR = 2;
        static final int OKHTTP_ON_ERROR_WITH_NETWORK_CONNECTION = 3;
        static final int MISSING_EXCEPTION_INFORMATION = 4;
        //we should never have this since the SDK controls authentication process and client protocols
        static final int INVALID_CLIENT = 5;
        //We control the grant type, we should never have this error
        static final int UNSUPPORTED_GRANT_TYPE = 6;
        static final int NETWORK_ISSUE = 7;

        private RefreshTokenInternalError() {

        }
    }

    static class RefreshTokenServerError {
        static final String EVENT_REFRESH_TOKEN_SERVER_ERROR = "phnx_refresh_token_server_error";
        static final int SERVER_EMPTY_RESPONSE = 1;
        static final int RESPONSE_MISSING_ACCESS_TOKEN = 2;
        static final int RESPONSE_PARSE_FAILURE = 3;
        static final int RESPONSE_UNRECOGNIZED_ERROR_REASON = 4;
        static final int RESPONSE_MISSING_ERROR_FIELD = 5;
        static final int UNRECOGNIZED_STATUS_CODE_AND_ERROR = 6;
        static final int INVALID_REQUEST = 7;
        static final int INVALID_GRANT = 8;
        static final int UNAUTHORIZED_CLIENT = 9;
        static final int INVALID_SCOPE = 10;
        //Corresponds to http status codes between 500 and 599
        static final int RETRY_LATER = 11;
        static final int IDP_SWITCHED = 12;

        private RefreshTokenServerError() {

        }
    }

    static class RefreshTokenUserEvents {
        static final String EVENT_REFRESH_TOKEN_FAILURE = "phnx_refresh_token_failure";
        static final String EVENT_REFRESH_TOKEN_SUCCESS = "phnx_refresh_token_success";
        static final String EVENT_REFRESH_TOKEN_START = "phnx_refresh_token_start";
        static final int INVALID_REQUEST = 1;
        static final int REAUTHORIZE_USER = 2;
        static final int UNAUTHORIZED_CLIENT = 3;
        static final int INVALID_SCOPE = 4;
        static final int NETWORK_ERROR = 5;
        static final int SERVER_ERROR = 6;
        static final int GENERAL_ERROR = 7;
        static final int IDP_SWITCHED = 8;
        static final String INVALID_REQUEST_MSG = "Invalid request";
        static final String REAUTHORIZE_USER_MSG = "Reauthorize user";
        static final String UNAUTHORIZED_CLIENT_MSG = "Unauthorized client";
        static final String INVALID_SCOPE_MSG = "Invalid Scope";
        static final String NETWORK_ERROR_MSG = "Network error";
        static final String SERVER_ERROR_MSG = "Server error";
        static final String GENERAL_ERROR_MSG = "General error";
        static final String IDP_SWITCHED_MSG = "IDP switched";

        private RefreshTokenUserEvents() {

        }
    }

    static class SignInUserEvents {
        static final String EVENT_SIGN_IN_START = "phnx_sign_in_start";
        static final String EVENT_SIGN_IN_REDIRECT = "phnx_sign_in_redirect";
        static final String EVENT_SIGN_IN_SUCCESS = "phnx_sign_in_success";
        static final String EVENT_SIGN_IN_FAILURE = "phnx_sign_in_failure";
        static final String EVENT_SIGN_IN_USER_CANCELED = "phnx_sign_in_user_canceled";
        static final int INVALID_REQUEST = 1;
        static final int UNAUTHORIZED_CLIENT = 2;
        static final int ACCESS_DENIED = 3;
        static final int UNSUPPORTED_RESPONSE_TYPE = 4;
        static final int INVALID_SCOPE = 5;
        static final int SERVER_ERROR = 6;
        static final int TEMPORARILY_UNAVAILABLE = 7;
        static final int CLIENT_ERROR = 8;
        static final int GENERAL_ERROR = 9;
        static final String INVALID_REQUEST_MSG = "Invalid request";
        static final String UNAUTHORIZED_CLIENT_MSG = "Unauthorized client";
        static final String ACCESS_DENIED_MSG = "Access denied";
        static final String UNSUPPORTED_RESPONSE_TYPE_MSG = "Unsupported response type";
        static final String INVALID_SCOPE_MSG = "Invalid scope";
        static final String SERVER_ERROR_MSG = "Server error";
        static final String TEMPORARILY_UNAVAILABLE_MSG = "Temporarily unavailable";
        static final String CLIENT_ERROR_MSG = "Client error";
        static final String GENERAL_ERROR_MSG = "General error";

        private SignInUserEvents() {

        }
    }

    static class ManageAccountsUserEvents {
        static final String EVENT_MANAGE_ACCOUNTS_TUTORIAL_SCREEN = "phnx_manage_accounts_tutorial_screen";
        static final String EVENT_MANAGE_ACCOUNTS_START = "phnx_manage_accounts_start";
        static final String EVENT_MANAGE_ACCOUNTS_END = "phnx_manage_accounts_end";

        static final String EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_START = "phnx_manage_accounts_edit_accounts_start";
        static final String EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_END = "phnx_manage_accounts_edit_accounts_end";

        static final String EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_REMOVE_START = "phnx_manage_accounts_edit_accounts_remove_start";
        static final String EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_REMOVE_CANCEL = "phnx_manage_accounts_edit_accounts_remove_cancel";
        static final String EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_REMOVE_SUCCESS = "phnx_manage_accounts_edit_accounts_remove_success";
        static final String EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_REMOVE_FAILURE = "phnx_manage_accounts_edit_accounts_remove_failure";

        static final String EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_START = "phnx_manage_accounts_toggle_on_account_start";
        static final String EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_SUCCESS = "phnx_manage_accounts_toggle_on_account_success";
        static final String EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_FAILURE = "phnx_manage_accounts_toggle_on_account_failure";

        static final String EVENT_MANAGE_ACCOUNTS_TOGGLE_OFF_ACCOUNT_START = "phnx_manage_accounts_toggle_off_account_start";
        static final String EVENT_MANAGE_ACCOUNTS_TOGGLE_OFF_ACCOUNT_CANCEL = "phnx_manage_accounts_toggle_off_cancel";
        static final String EVENT_MANAGE_ACCOUNTS_TOGGLE_OFF_ACCOUNT_SUCCESS = "phnx_manage_accounts_toggle_off_success";

        static final String EVENT_MANAGE_ACCOUNTS_SIGN_IN_START = "phnx_manage_accounts_sign_in_start";
        static final String EVENT_MANAGE_ACCOUNTS_SIGN_IN_SUCCESS = "phnx_manage_accounts_sign_in_success";
        static final String EVENT_MANAGE_ACCOUNTS_SIGN_IN_CANCEL = "phnx_manage_accounts_sign_in_cancel";
        static final String EVENT_MANAGE_ACCOUNTS_SIGN_IN_ERROR = "phnx_manage_accounts_sign_in_error";

        private ManageAccountsUserEvents() {

        }
    }

    static class AccountPickerUserEvents {
        static final String EVENT_ACCOUNT_PICKER_START = "phnx_account_picker_start";
        static final String EVENT_ACCOUNT_PICKER_END = "phnx_account_picker_end";

        static final String EVENT_ACCOUNT_PICKER_SIGN_IN_START = "phnx_account_picker_sign_in_start";
        static final String EVENT_ACCOUNT_PICKER_SIGN_IN_SUCCESS = "phnx_account_picker_sign_in_success";
        static final String EVENT_ACCOUNT_PICKER_SIGN_IN_CANCEL = "phnx_account_picker_sign_in_cancel";
        static final String EVENT_ACCOUNT_PICKER_SIGN_IN_ERROR = "phnx_account_picker_sign_in_error";

        static final String EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_START = "phnx_account_picker_select_account_start";
        static final String EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_SUCCESS = "phnx_account_picker_select_account_success";
        static final String EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_ERROR = "phnx_account_picker_select_account_error";

        static final String EVENT_ACCOUNT_PICKER_FETCH_USER_INFO_START = "phnx_account_picker_fetch_user_info_start";
        static final String EVENT_ACCOUNT_PICKER_FETCH_USER_INFO_SUCCESS = "phnx_account_picker_fetch_user_info_success";
        static final String EVENT_ACCOUNT_PICKER_FETCH_USER_INFO_ERROR = "phnx_account_picker_fetch_user_info_error";

        private AccountPickerUserEvents() {

        }
    }

    static class BootstrapUserEvents {
        static final String EVENT_BOOTSTRAP_START = "phnx_bootstrap_start";
        static final String TRACKING_PARAM_ACCOUNT_NUMBERS = "p_acc_num";
        static final String EVENT_BOOTSTRAP_FAILURE = "phnx_bootstrap_failure";

        static final String EVENT_BOOTSTRAP_SUCCESS = "phnx_bootstrap_success";

        static final String EVENT_BOOTSTRAP_ADD_ACCOUNT_FAILURE = "phnx_bootstrap_add_account_failure";

        static final String EVENT_BOOTSTRAP_REFRESH_TOKEN_START = "phnx_bootstrap_refresh_token_start";
        static final String EVENT_BOOTSTRAP_REFRESH_TOKEN_ERROR = "phnx_bootstrap_refresh_token_error";
        static final String EVENT_BOOTSTRAP_REFRESH_TOKEN_FAILURE = "phnx_bootstrap_refresh_token_failure";
        static final String EVENT_BOOTSTRAP_REFRESH_TOKEN_SUCCESS = "phnx_bootstrap_refresh_token_success";

        static final String EVENT_BOOTSTRAP_FETCH_USER_INFO_START = "phnx_bootstrap_fetch_user_info_start";
        static final String EVENT_BOOTSTRAP_FETCH_USER_INFO_FAILURE = "phnx_bootstrap_fetch_user_info_failure";
        static final String EVENT_BOOTSTRAP_FETCH_USER_INFO_SUCCESS = "phnx_bootstrap_fetch_user_info_success";

        private BootstrapUserEvents() {

        }
    }

    static class BootstrapError {
        static final String EVENT_BOOTSTRAP_ERROR = "phnx_bootstrap_error";
        static final int AUTH_CONFIG_NULL = 1;
        static final int COUNTDOWN_LATCH_WAIT_INTERRUPTED = 2;

        private BootstrapError() {

        }
    }
}
