package com.oath.mobile.platform.phoenix.core;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by billhuang on 10/24/17.
 */

class BasePhoenixActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceBundle) {
        int themResId = ThemeManager.getThemeResId();
        if (themResId != 0) {
            this.setTheme(themResId);
        }
        super.onCreate(savedInstanceBundle);
    }
}
