package com.oath.mobile.platform.phoenix.core;

import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;
import java.util.Map;

import static com.oath.mobile.platform.phoenix.core.AuthHelper.revokeToken;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenInternalError.EVENT_REFRESH_TOKEN_CLIENT_ERROR;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenUserEvents.EVENT_REFRESH_TOKEN_FAILURE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenUserEvents.EVENT_REFRESH_TOKEN_START;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenUserEvents.EVENT_REFRESH_TOKEN_SUCCESS;

/**
 * The account class represents the internal representation of an Account that identifies a user.
 * The account could have several states like enabled, disabled, active or inactive.
 * Here is the description:
 * <p>
 * ----------------------------------------------------------
 * isLoggedIn()    | isActive()    | Description
 * ----------------------------------------------------------
 * True            | True          | Account is accessible and logged in
 * True            | False         | Account is accessible from outside of the sdk, but in logged out state because it requires re-authorization(token invalid)
 * False           | True          | Account is not accessible from outside of the sdk, but can be restored through token refresh possible only through manage account screen by toggling on the account
 * False           | False         | Needs re-authorization
 * ----------------------------------------------------------
 * <p>
 * Created by nsoni on 9/21/17.
 */

class Account implements IAccount {

    private static final String TAG = Account.class.getSimpleName();
    static final String KEY_FIRST_NAME = "first_name";
    static final String KEY_LAST_NAME = "last_name";
    static final String KEY_GUID = "guid";
    static final String KEY_LEGACY_GUID = "legacy_guid";
    static final String KEY_LOCALE = "locale";
    static final String KEY_ISSUER = "issuer";
    static final String KEY_DISPLAY_NAME = "full_name";
    static final String KEY_ID_TOKEN = "id_token";
    static final String KEY_ACCESS_TOKEN = "access_token";
    static final String KEY_REFRESH_TOKEN = "refresh_token";
    static final String KEY_EMAIL = "email";
    static final String KEY_IMAGE_URI = "image_uri";
    static final String KEY_REAUTHORIZE_USER = "reauthorize_user";
    static final String REAUTHORIZE_USER = "reauthorize_user_required";
    private final android.accounts.Account mAndroidAccount;
    private String mAccessToken;
    private AccountManager mAndroidAccManager;

    Account(AccountManager androidAccManager, android.accounts.Account androidAccount) {
        mAndroidAccount = androidAccount;
        mAndroidAccManager = androidAccManager;
    }

    @Override
    public String getToken() {
        return mAndroidAccManager.getUserData(mAndroidAccount, KEY_ACCESS_TOKEN);
    }

    @Override
    public String getFirstName() {
        return mAndroidAccManager.getUserData(mAndroidAccount, KEY_FIRST_NAME);
    }

    @Override
    public String getLastName() {
        return mAndroidAccManager.getUserData(mAndroidAccount, KEY_LAST_NAME);
    }

    @Override
    public String getDisplayName() {
        return mAndroidAccManager.getUserData(mAndroidAccount, KEY_DISPLAY_NAME);
    }

    @Override
    public String getEmail() {
        return mAndroidAccManager.getUserData(mAndroidAccount, KEY_EMAIL);
    }

    @Override
    public String getGUID() {
        return mAndroidAccManager.getUserData(mAndroidAccount, KEY_GUID);
    }

    @Override
    public String getLegacyGUID() {
        return mAndroidAccManager.getUserData(mAndroidAccount, KEY_LEGACY_GUID);
    }

    @Override
    public String getIssuer() {
        return mAndroidAccManager.getUserData(mAndroidAccount, KEY_ISSUER);
    }

    @Override
    public String getUserName() {
        return mAndroidAccManager.getUserData(mAndroidAccount, IAuthManager.KEY_USERNAME);
    }

    @Override
    public boolean isActive() {
        return mAndroidAccManager.getUserData(mAndroidAccount, KEY_REAUTHORIZE_USER) == null;
    }

    void setIsActive(boolean isActive) {
        if (isActive) {
            setUserData(KEY_REAUTHORIZE_USER, null);
        } else {
            setUserData(KEY_REAUTHORIZE_USER, REAUTHORIZE_USER);
        }
    }

    void setEmail(String email) {
        setUserData(KEY_EMAIL, email);
    }

    void setDisplayName(String displayName) {
        setUserData(KEY_DISPLAY_NAME, displayName);
    }

    void setFirstName(String firstName) {
        setUserData(KEY_FIRST_NAME, firstName);
    }

    void setLastName(String lastName) {
        setUserData(KEY_LAST_NAME, lastName);
    }

    void setUserName(String userName) {
        setUserData(IAuthManager.KEY_USERNAME, userName);
    }

    private void setUserData(String key, String value) {
        mAndroidAccManager.setUserData(mAndroidAccount, key, value);
    }

    boolean isLoggedIn() {
        return getToken() != null;
    }

    @Override
    public void refreshToken(Context context, OnRefreshTokenResponse onRefreshTokenResponse) {
        EventLogger eventLogger = EventLogger.getInstance();
        eventLogger.logUserEvent(EVENT_REFRESH_TOKEN_START, null);
        AuthConfig authConfig = AuthConfig.getAuthConfigByIssuer(context, getIssuer());
        if (authConfig == null) {
            eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_CLIENT_ERROR, EventLogger.RefreshTokenInternalError.AUTH_CONFIG_NULL, "Auth config null in Account class");
            onRefreshTokenResponse.onError(OnRefreshTokenResponse.GENERAL_ERROR);
            return;
        }

        AuthHelper.refreshAccessToken(context, getRefreshToken(), getIssuer(), new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(@NonNull String accessToken, String refreshToken) {
                setAccessToken(accessToken);
                if (refreshToken != null && !refreshToken.isEmpty()) {
                    setRefreshToken(refreshToken);
                }
                eventLogger.logUserEvent(EVENT_REFRESH_TOKEN_SUCCESS, null);
                onRefreshTokenResponse.onSuccess();
            }

            @Override
            public void onFailure(int errorCode) {
                Map<String, Object> errorInfo = EventLogger.getRefreshTokenErrorEventParams(null, errorCode);
                eventLogger.logUserEvent(EVENT_REFRESH_TOKEN_FAILURE, errorInfo);
                if (errorCode == OnRefreshTokenResponse.REAUTHORIZE_USER || errorCode == AuthHelper.RefreshTokenResponseListener.ERROR_CODE_IDP_SWITCHED) {
                    setIsActive(false);
                    onRefreshTokenResponse.onError(OnRefreshTokenResponse.REAUTHORIZE_USER);
                    return;
                }
                onRefreshTokenResponse.onError(errorCode);
            }
        });
    }

    void setRefreshToken(String refreshToken) {
        mAndroidAccManager.setUserData(mAndroidAccount, KEY_REFRESH_TOKEN, refreshToken);
    }

    void setAccessToken(String accessToken) {
        mAndroidAccManager.setUserData(mAndroidAccount, KEY_ACCESS_TOKEN, accessToken);
    }

    String getRefreshToken() {
        return mAndroidAccManager.getUserData(mAndroidAccount, KEY_REFRESH_TOKEN);
    }

    @Override
    public String getImageUri() {
        return mAndroidAccManager.getUserData(mAndroidAccount, KEY_IMAGE_URI);
    }

    void fetchUserInfo(@NonNull Context context, OnUserDataResponseListener listener) {
        AuthHelper.getUserInfoAsync(context, getToken(), getIssuer(), listener);
    }

    void disable(AccountChangeListener listener) {
        setAccessToken(null);
        listener.onAccountChanged(0);
    }

    void remove(Activity activity, AccountChangeListener listener) {
        if (activity == null) {
            return;
        }
        AuthHelper.RevokeTokenResponseListener responseListener = (resultCode, errorMessage) -> {
            switch (resultCode) {
                case AccountNetworkAPI.ERROR_CODE_NETWORK_UNAVAILABLE_AIRPLANE_MODE:
                case AccountNetworkAPI.ERROR_CODE_ACCOUNT_NETWORK_UNAVAILABLE:
                case AccountNetworkAPI.ERROR_CODE_ACCOUNT_NETWORK_SSL_VERIFICATION_ERROR:
                    if (activity != null && !activity.isFinishing()) {
                        AlertUtils.showNetworkErrorDialog(activity, resultCode, errorMessage);
                        if (listener != null) {
                            listener.onAccountChanged(AccountChangeListener.FAIL);
                        }
                    }
                    break;
                default:
                    // Ignore the error of revoke token fail
                    AccountManagerCallback callback = new AccountManagerCallback<Boolean>() {
                        @Override
                        public void run(AccountManagerFuture<Boolean> future) {
                            try {
                                future.getResult();
                                if (listener != null) {
                                    listener.onAccountChanged(AccountChangeListener.SUCCESS);
                                }
                            } catch (OperationCanceledException | IOException | AuthenticatorException e) {
                                AlertUtils.showErrorDialog(activity, activity.getResources().getString(R.string.phoenix_try_again_error));
                                if (listener != null) {
                                    listener.onAccountChanged(AccountChangeListener.FAIL);
                                }
                                Log.e(TAG, "Error when removing account from Android AccountManager");
                            }
                        }
                    };

                    mAndroidAccManager.removeAccount(mAndroidAccount, callback, null);
                    break;
            }
        };
        revokeToken(activity, getRefreshToken(), getIssuer(), responseListener);
    }

}
