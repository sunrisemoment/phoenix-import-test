package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;

import com.yahoo.mobile.client.share.util.Util;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AccountUtils {

    public static final int CJK_CHAR_START = 0x4E00;
    public static final int CJK_CHAR_END = 0x9FFF;
    public static final int JAPANESE_CHAR_START = 0x3040;
    public static final int JAPANESE_CHAR_END = 0x30FF;
    public static final int KOREAN_HANGUL_CHAR_END = 0xD7A3;
    public static final int KOREAN_HANGUL_CHAR_START = 0xAC00;

    private AccountUtils() {
    }

    /**
     * Get account's display name
     * <p>
     * //TODO , once backend could return localized full name in id_token, we can remove this method
     *
     * @param pAccount
     * @return
     */
    public static String getDisplayName(IAccount pAccount) {
        String firstName = pAccount.getFirstName();
        String lastName = pAccount.getLastName();

        if (!Util.isEmpty(firstName) && !Util.isEmpty(lastName)) {
            return formatDisplayNameByScript(firstName, lastName);
        } else if (!Util.isEmpty(lastName)) {
            return lastName;
        } else if (!Util.isEmpty(firstName)) {
            return firstName;
        } else {
            return pAccount.getUserName();
        }
    }

    private static String formatDisplayNameByScript(String firstName, String lastName) {
        char firstNameChar = firstName.charAt(0);
        char lastNameChar = lastName.charAt(0);
        String displayName;

        if (isChineseOrJapaneseOrKorean(firstNameChar) && isChineseOrJapaneseOrKorean(lastNameChar)) {
            displayName = String.format("%s%s", lastName, firstName);
        } else {
            displayName = String.format("%s %s", firstName, lastName);
        }

        return displayName;
    }

    private static boolean isChineseOrJapaneseOrKorean(char firstChar) {
        return (firstChar >= CJK_CHAR_START && firstChar <= CJK_CHAR_END) || // CJK - Chinese
                (firstChar >= JAPANESE_CHAR_START && firstChar <= JAPANESE_CHAR_END) || // Hiragana/Katakana - Japanese
                (firstChar >= KOREAN_HANGUL_CHAR_START && firstChar <= KOREAN_HANGUL_CHAR_END); // Hangul - Korean
    }

    /**
     * Get application name
     *
     * @param ctx
     * @return application label that show on app list, null if fail
     */
    public static CharSequence getApplicationName(Context ctx) {
        ApplicationInfo ai = ctx.getApplicationInfo();

        int resId = ai.labelRes;

        if (resId != 0) {
            try {
                return ctx.getString(resId);
            } catch (Resources.NotFoundException e) {
                return "";
            }
        } else {
            if (ctx.getPackageManager() != null) {
                CharSequence appNameCharSequence = ctx.getPackageManager().getApplicationLabel(ai);
                if (appNameCharSequence != null) {
                    return appNameCharSequence;
                }
            }
        }
        return "";
    }

    public static void sortAccounts(List<IAccount> accountList) {
        if (accountList != null && accountList.size() > 0) {
            Collections.sort(accountList, new Comparator<IAccount>() {

                @Override
                public int compare(IAccount lhs, IAccount rhs) {
                    if (lhs.getDisplayName() == null && rhs.getDisplayName() != null) return -1;
                    if (lhs.getDisplayName() != null && rhs.getDisplayName() == null) return 1;
                    if (lhs.getDisplayName() == null && rhs.getDisplayName() == null) return 0;
                    return lhs.getDisplayName().compareToIgnoreCase(rhs.getDisplayName());
                }
            });
        }
    }
}
