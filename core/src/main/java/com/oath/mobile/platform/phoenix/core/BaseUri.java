package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.yahoo.mobile.client.share.logging.Log;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by nsoni on 1/17/18.
 */

class BaseUri {

    public static final String TAG = "BaseUri";
    static final String QUERY_PARAM_KEY_APP_ID = "appid";
    static final String QUERY_PARAM_KEY_APP_VERSION = "appsrcv";
    static final String QUERY_PARAM_KEY_SRC_SDK = "src";
    static final String QUERY_PARAM_VAL_SRC_SDK_ANDROID_PHNX = "androidphnx";
    static final String QUERY_PARAM_KEY_SRC_SDK_VERSION = "srcv";
    static final String QUERY_PARAM_KEY_ASDK_EMBEDDED = ".asdk_embedded";
    static final String QUERY_PARAM_VAL_ASDK_EMBEDDED_1 = "1";
    static final String QUERY_PARAM_KEY_INTL = "intl";
    static final String QUERY_PARAM_KEY_LANG = "lang";

    Uri.Builder mUriBuilder;

    BaseUri(Uri.Builder builder) {
        mUriBuilder = builder;
    }

    Uri.Builder Builder(Context context) {
        mUriBuilder
                .appendQueryParameter(QUERY_PARAM_KEY_APP_ID, context.getPackageName())
                .appendQueryParameter(QUERY_PARAM_KEY_APP_VERSION, getAppVersion(context))
                .appendQueryParameter(QUERY_PARAM_KEY_SRC_SDK, QUERY_PARAM_VAL_SRC_SDK_ANDROID_PHNX)
                .appendQueryParameter(QUERY_PARAM_KEY_SRC_SDK_VERSION, BuildConfig.VERSION_NAME)
                .appendQueryParameter(QUERY_PARAM_KEY_ASDK_EMBEDDED, QUERY_PARAM_VAL_ASDK_EMBEDDED_1)
                .appendQueryParameter(QUERY_PARAM_KEY_INTL, IntlLang.getIntlLang(Locale.getDefault()).getIntl())
                .appendQueryParameter(QUERY_PARAM_KEY_LANG, IntlLang.getIntlLang(Locale.getDefault()).getLang());
        return mUriBuilder;

    }

    String getAppVersion(@NonNull Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "getAppVersion(): Package name not found");
        } catch (Exception e) {
            // Catching all exceptions here as we do know this source is fine in prod but
            // has occasional problems with the package manager which we need to just ignore.
            Log.e(TAG, "getAppVersion(): Exception while getting package info", e);
        }
        return "";
    }

    public static class IntlLang {
        /**
         * yala l10n_map is different from Android default locale. Need a
         * mapping for localization
         */
        private static Map<String, String> sLocaleToLang;
        private static Map<String, String> sLocaleToIntl;

        static {
            sLocaleToLang = new HashMap<String, String>();
            sLocaleToLang.put("zh-CN", "zh-Hans-CN");
            sLocaleToLang.put("zh-TW", "zh-Hant-TW");
            sLocaleToLang.put("zh-HK", "zh-Hant-HK");

            sLocaleToIntl = new HashMap<String, String>();
            sLocaleToIntl.put("ar-JO", "xa");
            sLocaleToIntl.put("en-GB", "uk");
            sLocaleToIntl.put("en-JO", "xe");
            sLocaleToIntl.put("es-US", "e1");
            sLocaleToIntl.put("fr-CA", "cf");
            sLocaleToIntl.put("ko-KR", "us");
            sLocaleToIntl.put("pt-PT", "xp");
            sLocaleToIntl.put("zh-CN", "us");
        }

        private String intl;
        private String lang;

        public static IntlLang getIntlLang(Locale currentLocale) {
            String intl = "us";
            String langTag = "en-US";

            if (currentLocale != null) {
                String country = currentLocale.getCountry();
                if (country != null) {
                    intl = country;
                }

                String lang = currentLocale.getLanguage();
                if (lang != null) {
                    lang = convertOldLangToNew(lang);
                }
                if (lang != null && country != null) {
                    String locale = lang.toLowerCase() + "-" + country.toUpperCase();
                    langTag = getLangTag(locale);

                    if (sLocaleToIntl.containsKey(locale)) {
                        intl = sLocaleToIntl.get(locale);
                    }
                }
                if ("ar".equalsIgnoreCase(lang)) {
                    intl = "xa";
                }
            }

            return new IntlLang(intl, langTag);
        }

        public String getIntl() {
            return this.intl.toLowerCase();
        }

        public String getLang() {
            return this.lang;
        }

        private IntlLang(final String intl, final String lang) {
            this.intl = intl;
            this.lang = lang;
        }

        // For backward compatibility, java converts new ISO language code to old.
        // We need to convert old back to new because backend uses new language.
        // reference: http://stackoverflow.com/questions/13974169/jdk-locale-class-handling-of-iso-language-codes-for-hebrew-he-yiddish-yi-an
        private static String convertOldLangToNew(String language) {
            if (language.equals("iw")) {
                return "he";
            } else if (language.equals("ji")) {
                return "yi";
            } else if (language.equals("in")) {
                return "id";
            } else {
                return language;
            }
        }

        /**
         * Get Lang tag from locale (language+country) It's for some special
         * l10n_map case
         *
         * @param locale
         * @return
         */
        private static String getLangTag(String locale) {
            if (sLocaleToLang.containsKey(locale)) {
                return sLocaleToLang.get(locale);
            }

            return locale;
        }
    }
}
