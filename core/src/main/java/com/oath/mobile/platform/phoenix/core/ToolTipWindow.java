package com.oath.mobile.platform.phoenix.core;

/**
 * Created by yuhongli on 12/18/17.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;

/**
 * Created by nsoni on 8/31/16.
 */
public class ToolTipWindow implements View.OnClickListener, PopupWindow.OnDismissListener {
    static final int MAX_COUNT = 1;
    static final String SHARED_PREF_PREFIX = "YAHOO_ACCOUNT_SHARED_PREF_" + "ToolTipWindow";
    public static final String REMOVE = "Remove";
    public static final String EDIT = "Edit";
    private final BubbleLayout mBubbleLayout;
    final PopupWindow mPopupWindow;
    private final TextView mBubbleLayoutText;
    private final ImageButton mBubbleDismissButton;
    private String mViewLabel;

    public ToolTipWindow(Context mContext) {
        mBubbleLayout = (BubbleLayout) LayoutInflater.from(mContext).inflate(R.layout.phoenix_tooltip_layout, null);
        mBubbleLayoutText = (TextView) mBubbleLayout.findViewById(R.id.tooltip_text);
        mBubbleDismissButton = (ImageButton) mBubbleLayout.findViewById(R.id.tooltip_dismiss);
        mBubbleDismissButton.setOnClickListener(this);
        mPopupWindow = BubblePopupHelper.create(mContext, mBubbleLayout);
        mPopupWindow.setOutsideTouchable(true);
        mPopupWindow.setOnDismissListener(this);
    }

    public void showToolTip(final View anchorView, String viewLabel, Spanned toolTipText) {
        showToolTip(anchorView, viewLabel, toolTipText, 0);
    }

    public void showToolTip(final View anchorView, String viewLabel, final Spanned toolTipText, final int offsetForArrowFromRight) {
        if (getToolTipCount(anchorView.getContext(), viewLabel) < MAX_COUNT) {
            mViewLabel = viewLabel;
            anchorView.post(new Runnable() {
                @Override
                public void run() {
                    int[] location = new int[2];
                    anchorView.getLocationOnScreen(location); // Extract x and y position of the anchor view on the screen
                    mBubbleLayout.setArrowDirection(ArrowDirection.TOP);
                    DisplayMetrics dm = anchorView.getContext().getResources().getDisplayMetrics();
                    int toolTipTextViewWidth = anchorView.getContext().getResources().getDimensionPixelSize(R.dimen.phoenix_tooltip_width);
                    int dismissImageSize = anchorView.getContext().getResources().getDimensionPixelSize(R.dimen.phoenix_tooltip_dismiss_button_size);
                    int dismissImagePadding = anchorView.getContext().getResources().getDimensionPixelSize(R.dimen.phoenix_tooltip_padding);
                    int totalTooltipWidth = toolTipTextViewWidth + dismissImageSize + 2 * dismissImagePadding;
                    if (offsetForArrowFromRight > 0) {
                        mBubbleLayout.setArrowPosition(totalTooltipWidth - (int) (offsetForArrowFromRight * dm.density)); // Arrow position is measured from x position of the tooltip view
                    } else {
                        mBubbleLayout.setArrowPosition(totalTooltipWidth - (anchorView.getWidth() - anchorView.getPaddingLeft()) / 2);
                    }
                    mBubbleLayoutText.setText(toolTipText);
                    int tooltipMargin = anchorView.getContext().getResources().getDimensionPixelSize(R.dimen.phoenix_tooltip_margin_right);
                    if (isAnchorViewVisible(anchorView)) {
                        mPopupWindow.showAtLocation(anchorView, Gravity.TOP, (dm.widthPixels - totalTooltipWidth) / 2 - tooltipMargin, anchorView.getHeight() * 3 / 4 + location[1] /* y position of the anchor view */);
                    }
                }
            });
        }
    }

    public boolean isAnchorViewVisible(View anchorView) {
        return anchorView.getWindowToken() != null && anchorView.getWindowVisibility() == View.VISIBLE;
    }

    private int getToolTipCount(Context context, String viewLabel) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(SHARED_PREF_PREFIX + viewLabel, 0);
    }

    @Override
    public void onClick(View v) {
        if (v == mBubbleDismissButton) {
            dismiss();
        }
    }

    public void dismiss() {
        if (mPopupWindow != null && mPopupWindow.isShowing()) {
            mPopupWindow.dismiss();
        }
    }

    private void incrementCount(Context context, String viewLabel) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        int count = sharedPreferences.getInt(SHARED_PREF_PREFIX + viewLabel, 0);
        if (count < MAX_COUNT) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(SHARED_PREF_PREFIX + viewLabel, count + 1);
            editor.apply();
        }
    }

    @Override
    public void onDismiss() {
        incrementCount(mPopupWindow.getContentView().getContext(), mViewLabel);
    }
}
