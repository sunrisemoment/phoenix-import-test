package com.oath.mobile.platform.phoenix.core;


import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.yahoo.mobile.client.android.fuji.widget.OrbImageView;
import com.yahoo.mobile.client.share.util.Util;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;

/**
 * Created by yuhongli on 3/11/17.
 */
class ManageAccountsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected static final int TOGGLE_ACCOUNT_CONFIRMATION_DIALOG_MAX_COUNT = 5;
    public static final String SPACE = " ";
    public static final String KEY_TOGGLE_ACCOUNT_CONFIRMATION_DIALOG_COUNTER = "toggle_account_dialog_confirmation_counter";

    protected static final int TYPE_HEADER_ITEM = 0;
    protected static final int TYPE_ACCOUNT_ITEM = 1;
    protected static final int TYPE_ADD_ACCOUNT_ITEM = 2;
    protected static final int HEADER_POSITION = 0;
    public static final int HEADER_OFFSET = 1;

    public static final float DISABLED_ACCOUNT_ITEM_ALPHA = 0.5f;
    public static final float ENABLED_ACCOUNT_ITEM_ALPHA = 1f;

    private Callback mListener;

    private List<IAccount> mAccounts;
    private AuthManager mAuthManager;
    private boolean inEditMode;
    private boolean isToolTipShown = false;
    public ToolTipWindow mRemoveButtonToolTipWindow;

    public ManageAccountsAdapter(@NonNull Callback listener, @NonNull IAuthManager authManager) {
        mListener = listener;
        mAuthManager = (AuthManager) authManager;
        setAccounts();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mRemoveButtonToolTipWindow == null) {
            mRemoveButtonToolTipWindow = new ToolTipWindow(parent.getContext());
        }
        switch (viewType) {
            case TYPE_ACCOUNT_ITEM:
                return new AccountItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.manage_accounts_list_item_account, parent, false), mListener);
            case TYPE_ADD_ACCOUNT_ITEM:
                return new AddAccountItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.manage_accounts_list_item_add_account, parent, false), mListener);
            case TYPE_HEADER_ITEM:
                return new HeaderItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.manage_accounts_list_item_header, parent, false));
            default:
                throw new IllegalArgumentException("view type not defined");
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AccountItemViewHolder) {
            ((AccountItemViewHolder) holder).bindData(getAccountAtPosition(position), inEditMode);
        } else if (holder instanceof HeaderItemViewHolder) {
            ((HeaderItemViewHolder) holder).bindData(inEditMode);
        }
    }

    private IAccount getAccountAtPosition(int position) {
        return mAccounts.get(position - HEADER_OFFSET);
    }

    public void removeAccountAtPosition(int position) {
        int index = position - HEADER_OFFSET;
        if (mAccounts.size() > 0 && index >= 0 && index < mAccounts.size()) {
            mAccounts.remove(index);
            if (mAccounts.size() > 0) {
                notifyItemRemoved(position);
            } else {
                mListener.onNoAccountsFound();
            }
        }
    }

    public int getAccountCount() {
        if (!Util.isEmpty(mAccounts)) {
            return mAccounts.size();
        }
        return 0;
    }

    @Override
    public int getItemCount() {
        int count = getAccountCount();
        if (!inEditMode) {
            count += 1; // Add Account
        }
        return count + HEADER_OFFSET; // Header
    }

    @Override
    public int getItemViewType(int position) {
        if (position == HEADER_POSITION) {
            return TYPE_HEADER_ITEM;
        } else if (position == mAccounts.size() + HEADER_OFFSET) {
            return TYPE_ADD_ACCOUNT_ITEM;
        } else {
            return TYPE_ACCOUNT_ITEM;
        }
    }

    public void notifyAccountSetChanged() {
        setAccounts();
    }

    protected void setAccounts() {
        List<IAccount> allAccounts = mAuthManager.getAllAccountsInternal();
        mAccounts = new ArrayList<>();
        if (Util.isEmpty(allAccounts)) {
            mListener.onNoAccountsFound();
        } else {
            mAccounts.addAll(allAccounts);
            // Accounts are in alphabetical order
            AccountUtils.sortAccounts(mAccounts);
        }
        notifyDataSetChanged();
    }

    public void enableEditMode() {
        if (inEditMode) {
            return;
        }
        inEditMode = true;
        isToolTipShown = false;
        notifyDataSetChanged();
    }

    public void disableEditMode() {
        if (!inEditMode) {
            return;
        }

        inEditMode = false;
        mRemoveButtonToolTipWindow.dismiss();
        notifyDataSetChanged();
    }

    class AccountItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

        private final TextView mName;
        private final TextView mEmail;
        private final OrbImageView mImage;
        private final SwitchCompat mStateToggle;
        private final TextView mRemove;
        private final CoordinatorLayout mCoordinator;

        protected Callback mListener;
        protected Context mContext;

        private Account mAccount;

        public AccountItemViewHolder(View itemView, Callback listener) {
            super(itemView);
            mContext = itemView.getContext();
            mName = itemView.findViewById(R.id.account_display_name);
            mEmail = itemView.findViewById(R.id.account_email);
            mImage = itemView.findViewById(R.id.account_profile_image);
            mStateToggle = itemView.findViewById(R.id.account_state_toggle);
            mRemove = itemView.findViewById(R.id.account_remove);
            mCoordinator = itemView.findViewById(R.id.account_coordinator);
            mListener = listener;
        }

        public void bindData(IAccount account, boolean inEditMode) {
            mAccount = (Account) account;
            setAccountNameAndEmail(account);
            OkHttpClient client = AccountNetworkAPI.getInstance(mContext).getOkHttpClient();
            ImageLoader.loadImageIntoView(client, mAccount.getImageUri(), mImage);

            mStateToggle.setChecked(mAccount.isLoggedIn() && mAccount.isActive());
            if (!inEditMode) {
                mStateToggle.setVisibility(View.VISIBLE);
                mRemove.setVisibility(View.INVISIBLE);
            } else {
                mStateToggle.setVisibility(View.INVISIBLE);
                mRemove.setVisibility(View.VISIBLE);
                if (!isToolTipShown) { // Tooltip should be shown only once for the screen
                    isToolTipShown = true;
                    mRemoveButtonToolTipWindow.showToolTip(mRemove,
                            ToolTipWindow.REMOVE,
                            Html.fromHtml(mContext.getResources().getString(R.string.phoenix_manage_accounts_remove_tooltip)));
                }
            }
            setItemViewAlpha(mStateToggle.isChecked());

            mRemove.setOnClickListener(this);
            mRemove.setContentDescription(itemView.getContext().getString(R.string.phoenix_accessibility_account_remove_manage_account, mAccount.getUserName()));
            mStateToggle.setOnCheckedChangeListener(this);
        }

        private void setAccountNameAndEmail(IAccount account) {
            String userName = account.getUserName();
            String displayName = account.getDisplayName();
            mName.setText(displayName);
            updateRootViewContentDesc();
            if (!Util.isEqual(displayName, userName)) {
                mEmail.setText(userName);
                mEmail.setVisibility(View.VISIBLE);
            } else {
                mEmail.setVisibility(View.GONE);
            }
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.account_remove) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    mListener.onRemoveAccount(getAdapterPosition(), mAccount);
                    mRemoveButtonToolTipWindow.dismiss();
                } // else ignore the click since we don't know which item was actually clicked
            }
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
            if (isChecked) {
                //turn off start
                EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_TOGGLE_OFF_ACCOUNT_START, null);
            } else {
                //turn on start
                EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_START, null);
            }
            if (buttonView.getId() == R.id.account_state_toggle) {
                AccountChangeListener toggleCompleteListener = new AccountChangeListener() {
                    @Override
                    public void onAccountChanged(int result) {
                        if (!isChecked) {
                            EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_TOGGLE_OFF_ACCOUNT_SUCCESS, null);
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    showAccountDisabledSnackbar();
                                }
                            });
                        }
                        mStateToggle.setChecked(mAccount.isLoggedIn());
                        updateRootViewContentDesc();
                    }
                };
                if (isChecked != (mAccount.isLoggedIn() && mAccount.isActive())) {
                    SharedPreferences sharedPreferences = mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
                    int toggleConfirmationCounter = sharedPreferences.getInt(KEY_TOGGLE_ACCOUNT_CONFIRMATION_DIALOG_COUNTER, 1);
                    if (toggleConfirmationCounter > TOGGLE_ACCOUNT_CONFIRMATION_DIALOG_MAX_COUNT
                            || isChecked) {
                        mListener.onAccountToggled(getAdapterPosition(), mAccount, toggleCompleteListener);
                    } else {
                        showToggleOffAccountDialog(getAdapterPosition(), toggleCompleteListener);
                        sharedPreferences.edit().putInt(KEY_TOGGLE_ACCOUNT_CONFIRMATION_DIALOG_COUNTER, ++toggleConfirmationCounter).apply();
                    }
                    setItemViewAlpha(isChecked);
                }
            }
        }

        protected void updateRootViewContentDesc() {
            String accountName = mAccount.getUserName();
            mStateToggle.setContentDescription(itemView.getContext().getString(R.string.phoenix_accessibility_account_switch_in_manage_account, accountName));

            if (mAccount.isLoggedIn() && mAccount.isActive()) {
                itemView.setContentDescription(accountName + SPACE + itemView.getContext().getString(R.string.phoenix_accessibility_account_enabled));
            } else {
                itemView.setContentDescription(accountName + SPACE + itemView.getContext().getString(R.string.phoenix_accessibility_account_disabled));
            }
        }

        void showAccountDisabledSnackbar() {
            Snackbar snackbar = Snackbar.make(mCoordinator, R.string.phoenix_manage_accounts_disable_message, Snackbar.LENGTH_SHORT);
            snackbar.getView().setBackground(itemView.getContext().getResources().getDrawable(R.drawable.phoenix_disable_account_snackbar_bg_));
            snackbar.show();
        }

        private void setItemViewAlpha(boolean isChecked) {
            float alpha = isChecked ? ENABLED_ACCOUNT_ITEM_ALPHA : DISABLED_ACCOUNT_ITEM_ALPHA;

            mName.setAlpha(alpha);
            mImage.setAlpha(alpha);
            mEmail.setAlpha(alpha);
        }

        protected void showToggleOffAccountDialog(final int position, final AccountChangeListener listener) {
            final Dialog dialog = new Dialog(mContext);
            String title = mContext.getResources().getString(R.string.phoenix_toggle_off_account_dialog_title);
            String description = mContext.getResources().getString(R.string.phoenix_toggle_off_account_dialog_desc);
            String button1 = mContext.getResources().getString(R.string.phoenix_toggle_off_account_dialog_button);
            String button2 = mContext.getResources().getString(R.string.cancel);
            OnClickListener listener1 = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!((ManageAccountsActivity) mContext).isFinishing()) {
                        dialog.dismiss();
                        mListener.onAccountToggled(position, mAccount, listener);
                    }
                }
            };
            OnClickListener listener2 = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!((ManageAccountsActivity) mContext).isFinishing()) {
                        dialog.dismiss();
                        mStateToggle.setChecked(true);
                        setItemViewAlpha(mStateToggle.isChecked());
                        EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_TOGGLE_OFF_ACCOUNT_CANCEL, null);
                    }
                }
            };

            CustomDialogHelper.generateTwoVerticalButtonDialog(dialog, title, description, button1, listener1, button2, listener2);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
    }

    class AddAccountItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final Callback mListener;

        public AddAccountItemViewHolder(View itemView, Callback listener) {
            super(itemView);
            mListener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onAddAccount();
        }
    }

    static class HeaderItemViewHolder extends RecyclerView.ViewHolder {

        private final TextView mHeaderText;

        public HeaderItemViewHolder(View itemView) {
            super(itemView);
            mHeaderText = itemView.findViewById(R.id.account_manage_accounts_header);
        }

        public void bindData(boolean inEditMode) {
            if (inEditMode) {
                mHeaderText.setText(itemView.getResources().getString(R.string.phoenix_manage_accounts_edit_mode_header));
            } else {
                mHeaderText.setText(itemView.getResources().getString(R.string.phoenix_manage_accounts_header, AccountUtils.getApplicationName(itemView.getContext())));
            }
        }
    }

    /* Callback Interface */
    public interface Callback {
        void onAddAccount();

        void onNoAccountsFound();

        void onRemoveAccount(int position, IAccount account);

        void onAccountToggled(int position, IAccount account, AccountChangeListener listener);
    }
}
