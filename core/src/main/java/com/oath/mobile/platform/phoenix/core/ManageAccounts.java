package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.content.Intent;
import android.os.ResultReceiver;

import static com.oath.mobile.platform.phoenix.core.ManageAccountsActivity.ARG_DISMISS_ON_ADD_ACCOUNT;

/**
 * Created by yuhongli on 11/6/17.
 */

public class ManageAccounts {

    public static final String KEY_ACCOUNT_CHANGE_RESULT = "account_change_result";

    /**
     * Use this key with the returned bundle data to retrieve an ArrayList with String elements
     * {@link Intent#getStringArrayListExtra(String)}. Each element is the guid of the account that
     * was removed or toggled off from the app
     */
    public static final String KEY_REMOVED_ACCOUNTS_LIST = "removed_accounts_list";

    /**
     * Use this key with the returned bundle data to retrieve an ArrayList with String elements
     * {@link Intent#getStringArrayListExtra(String)}. Each element is the guid of the account that
     * was added to the app
     */
    public static final String KEY_ADDED_ACCOUNTS_LIST = "added_accounts_list";

    public static final int RESULT_ACCOUNT_CHANGED = 1;

    private ManageAccounts() {
    }

    public static class IntentBuilder {
        private Intent mIntent;

        public IntentBuilder() {
            mIntent = new Intent();
        }

        public IntentBuilder enableDismissOnAddAccount() {
            mIntent.putExtra(ARG_DISMISS_ON_ADD_ACCOUNT, true);
            return this;
        }

        public IntentBuilder setResultReceiver(ResultReceiver resultReceiver) {
            mIntent.putExtra(ManageAccountsActivity.RESULT_RECEIVER_EXTRA, resultReceiver);
            return this;
        }

        public Intent build(Context context) {
            mIntent.setClass(context, ManageAccountsActivity.class);
            mIntent.putExtra(ManageAccountsActivity.INTERNAL_LAUNCH_GATE, true);
            return mIntent;
        }
    }


}
