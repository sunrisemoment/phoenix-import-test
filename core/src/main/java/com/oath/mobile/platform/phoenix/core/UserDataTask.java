package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import okhttp3.Headers;

/**
 * Created by nsoni on 12/1/17.
 */

public class UserDataTask extends AsyncTask<Object, Void, Void> {
    private static final String HTTPS = "https";
    private static final String USER_DATA_PATH_AOL = "auth/getUserData";
    private static final String PARAM_KEY_F = "f";
    private static final String PARAM_VALUE_F = "json";
    private static final String PARAM_KEY_ATTR = "attribute";
    private static final String PARAM_VALUE_ATTR = "guid,given_name,family_name,email";

    private static final String HEADER_KEY_AUTH = "authorization";
    private static final String BEARER = "Bearer ";

    AuthHelper.UserDataResponseListener mListener;

    public UserDataTask(AuthHelper.UserDataResponseListener userDataResponseListener) {
        mListener = userDataResponseListener;
    }

    @Override
    protected Void doInBackground(Object... params) {
        Context context = (Context) params[0];
        AccountNetworkAPI networkAPI = (AccountNetworkAPI) params[1];
        String accessToken = (String) params[2];

        try {
            String response = networkAPI.executeGet(context,
                    buildRequestUri(context, AuthConfig.AOL_COM, USER_DATA_PATH_AOL),
                    getHeaders(accessToken));
            mListener.onResponse(response);
        } catch (HttpConnectionException e) {
            mListener.onError(e.getRespCode(), e.getMessage());
        }
        return null;
    }

    private String buildRequestUri(Context context, String authority, String path) {
        Uri authUri = Uri.parse(path);
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(HTTPS)
                .authority(authority)
                .appendEncodedPath(authUri.getEncodedPath())
                .appendQueryParameter(PARAM_KEY_F, PARAM_VALUE_F)
                .appendQueryParameter(PARAM_KEY_ATTR, PARAM_VALUE_ATTR);

        return new BaseUri(builder).Builder(context).toString();
    }

    private Headers getHeaders(String accessToken) {
        Headers.Builder builder = new Headers.Builder();
        builder.add(HEADER_KEY_AUTH, BEARER + accessToken);
        return builder.build();
    }
}
