package com.oath.mobile.platform.phoenix.core;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.util.Log;
import android.util.TypedValue;

import com.yahoo.mobile.client.share.util.Util;
import com.yahoo.uda.yi13n.internal.Utils;

import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationRequest;
import net.openid.appauth.AuthorizationResponse;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.AuthorizationServiceConfiguration;
import net.openid.appauth.ResponseTypeValues;
import net.openid.appauth.TokenRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.oath.mobile.platform.phoenix.core.AuthConfig.AOL_AUTH_PATH;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.AOL_COM;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.AOL_IDP_ISSUER;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.AOL_TOKEN_PATH;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenInternalError.EVENT_REFRESH_TOKEN_CLIENT_ERROR;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenServerError.EVENT_REFRESH_TOKEN_SERVER_ERROR;

/**
 * Created by nsoni on 10/9/17.
 */

class AuthHelper {
    static final String SHARED_PREF_PHOENIX = "phoenix_preferences";
    static final String QUERY_PARAM_VALUE_PROMPT_SELECT_ACCOUNT = "select_account";
    static final String QUERY_PARAM_VALUE_PROMPT_LOGIN = "login";
    static final String QUERY_PARAM_VALUE_SCOPE_AOL_TOKEN_EXCHANGE = "mobilesdk";
    static final String QUERY_PARAM_VALUE_SCOPE_OATH = "sdpp-w";
    static final String QUERY_PARAM_VALUE_SCOPE_OPENID = "openid";
    private static final String QUERY_PARAM_KEY_NONCE = "nonce";
    private static final String QUERY_PARAM_KEY_CLIENT_SECRET = "client_secret";
    private static final String QUERY_PARAM_KEY_GRANT_TYPE = "grant_type";
    private static final String QUERY_PARAM_VALUE_GRANT_TYPE_AUTH_CODE = "authorization_code";
    private static final String QUERY_PARAM_VALUE_GRANT_TYPE_REFRESH_TOKEN = "refresh_token";
    private static final String QUERY_PARAM_KEY_SCOPE = "scope";
    private static final String QUERY_PARAM_KEY_CLIENT_ID = "client_id";
    private static final String QUERY_PARAM_KEY_REFRESH_TOKEN = "refresh_token";
    private static final String SAVED_AUTH_REQUEST_KEY = "SAVED_AUTH_REQUEST_KEY";
    private static final String TAG = AuthHelper.class.getSimpleName();
    static final String KEY_ERROR_DESCRIPTION = "error_description";
    static final String KEY_ERROR_URI = "error_uri";
    static final String KEY_ERROR = "error";
    static final String ERROR_VALUE_INVALID_REQUEST = "invalid_request";
    static final String ERROR_VALUE_INVALID_CLIENT = "invalid_client";
    static final String ERROR_VALUE_INVALID_GRANT = "invalid_grant";
    static final String ERROR_VALUE_UNAUTHORIZED_CLIENT = "unauthorized_client";
    static final String ERROR_VALUE_UNSUPPORTED_GRANT_TYPE = "unsupported_grant_type";
    static final String ERROR_VALUE_INVALID_SCOPE = "invalid_scope";
    static final String ERROR_VALUE_IDP_SWITCHED = "refresh_token_invalid_idp_switched";
    static final String KEY_GRANT_TYPE = "grant_type";
    static final String VALUE_GRANT_TYPE = "refresh_token";
    static final String KEY_SCOPE = "scope";
    static final String KEY_CLIENT_ID = "client_id";
    static final String KEY_REFRESH_TOKEN = "refresh_token";
    static final char SPACE = ' ';
    static final String USER_IDP_AOL = "aol";
    static final String USER_IDP_KEY = "user_idp";

    AuthorizationService mAuthorizationService;
    private AuthorizationRequest mAuthRequest;

    AuthHelper(@NonNull Context context) {
        mAuthRequest = createAuthRequest(context);
    }

    AuthHelper(@NonNull Bundle savedInstanceBundle) throws JSONException {
        mAuthRequest = AuthorizationRequest.jsonDeserialize(savedInstanceBundle.getString(SAVED_AUTH_REQUEST_KEY));
    }

    void initAuthService(@NonNull Context context) {
        mAuthorizationService = new AuthorizationService(context);
    }

    Intent createAuthIntent(@NonNull Context context) {
        Uri requestUri = mAuthRequest.toUri();

        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.phoenixToolbarColor, typedValue, true);
        int customTabColour = typedValue.data;

        CustomTabsIntent customTabsIntent = mAuthorizationService.createCustomTabsIntentBuilder()
                .setToolbarColor(customTabColour)
                .build();
        Intent intent = customTabsIntent.intent;
        intent.setData(requestUri);
        intent.putExtra(CustomTabsIntent.EXTRA_TITLE_VISIBILITY_STATE, 0);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        return intent;
    }

    void disposeCustomTabService() {
        if (mAuthorizationService != null) {
            mAuthorizationService.dispose();
        }
    }

    private AuthorizationRequest createAuthRequest(@NonNull Context context) {
        AuthConfig authConfig = AuthConfig.getAuthConfigByIssuer(context, null);
        AuthorizationServiceConfiguration serviceConfiguration = new AuthorizationServiceConfiguration(
                authConfig.getAuthUri(context) /* auth endpoint */,
                authConfig.getTokenUri(context) /* token endpoint */,
                null  /* registration endpoint */
        );

        AuthorizationRequest.Builder builder = new AuthorizationRequest.Builder(
                serviceConfiguration,
                authConfig.getClientID(),
                ResponseTypeValues.CODE,
                authConfig.getRedirectUri()
        );
        List<String> scopes = new ArrayList();
        scopes.addAll(authConfig.getScopes());
        scopes.add(QUERY_PARAM_VALUE_SCOPE_OPENID);
        scopes.add(QUERY_PARAM_VALUE_SCOPE_OATH);
        builder.setScopes(scopes);

        AuthManager authManager = (AuthManager) AuthManager.getInstance(context);
        String prompt = authManager.getAllAccountsInternal().size() == 0 ? QUERY_PARAM_VALUE_PROMPT_SELECT_ACCOUNT : QUERY_PARAM_VALUE_PROMPT_LOGIN;

        Map<String, String> additionalParamMap = new HashMap<>();
        additionalParamMap.put(QUERY_PARAM_KEY_NONCE, authManager.generateNonce());
        builder.setPrompt(prompt);
        builder.setAdditionalParameters(additionalParamMap);

        return builder.build();
    }

    void saveAuthRequest(Bundle outState) {
        outState.putString(SAVED_AUTH_REQUEST_KEY, mAuthRequest.jsonSerializeString());
    }

    void handleAuthResponse(@NonNull Context context, Uri data, @NonNull ResponseListener listener) {
        final int resultCode;
        if (data == null) {
            // In theory, it should never come here, but writing it for edge cases
            Log.e(TAG, "handleAuthResponse: Uri is null");
            listener.onComplete(IAuthManager.RESULT_CODE_ERROR, null, null);
        } else {
            if (data.getQueryParameterNames().contains(AuthorizationException.PARAM_ERROR)) {
                String error = data.getQueryParameter(AuthorizationException.PARAM_ERROR);
                AuthorizationException ex = AuthorizationException
                        .fromOAuthTemplate(AuthorizationException.AuthorizationRequestErrors.byString(error),
                                error,
                                data.getQueryParameter(AuthorizationException.PARAM_ERROR_DESCRIPTION),
                                parseUriIfAvailable(data.getQueryParameter(AuthorizationException.PARAM_ERROR_URI)));

                resultCode = IAuthManager.RESULT_CODE_ERROR;
                Log.e(TAG, "handleAuthResponse: " + "Error code: " + ex.code + " Error message: " + ex.errorDescription);
                listener.onComplete(resultCode, null, ex);
            } else {
                AuthorizationResponse response = (new AuthorizationResponse.Builder(mAuthRequest)).fromUri(data).build();
                // In error case the exchange token call will return
                exchangeCodeForTokens(context, response, listener);
            }
        }
    }

    Uri parseUriIfAvailable(@Nullable String uri) {
        return uri == null ? null : Uri.parse(uri);
    }

    void exchangeCodeForTokens(@NonNull final Context context, @NonNull AuthorizationResponse response, @NonNull final ResponseListener listener) {
        if (response.authorizationCode == null) {
            throw new IllegalStateException("authorizationCode not available for exchange request");
        }

        Map<String, String> additionalParamMap = new HashMap<>();

        TokenRequest request;
        // response.additionalParameters is always not null
        if (isAol(response.request.toUri()) || USER_IDP_AOL.equals(response.additionalParameters.get(USER_IDP_KEY))) {
            AuthConfig authConfig = AuthConfig.getAuthConfigByIssuer(context, AOL_IDP_ISSUER);
            if (!Util.isEmpty(authConfig.getClientSecret())) {
                additionalParamMap.put(QUERY_PARAM_KEY_CLIENT_SECRET, authConfig.getClientSecret());
            }
            request = createTokenExchangeRequestForAol(response, additionalParamMap);
        } else {
            request = response.createTokenExchangeRequest(additionalParamMap);
        }


        mAuthorizationService.performTokenRequest(request, (tokenResponse, exception) -> {
            if (exception != null) {
                Log.e(TAG, "performTokenRequest: " + "Error code: " + exception.code + " Error message: " + exception.errorDescription);
                listener.onComplete(IAuthManager.RESULT_CODE_ERROR, null, exception);
            } else if (tokenResponse != null) {
                IAccount account = ((AuthManager) AuthManager.getInstance(context)).addAccount(tokenResponse);

                if (account != null) {
                    Intent resultIntent = createResultIntentForAuth(account);
                    listener.onComplete(Activity.RESULT_OK, resultIntent, null);
                } else {
                    Log.e(TAG, "performTokenRequest: " + "Account could not be added");
                    listener.onComplete(IAuthManager.RESULT_CODE_ERROR, null, null);
                }
            } else {
                Log.e(TAG, "performTokenRequest: " + "AuthorizationException and TokenResponse are null");
                listener.onComplete(IAuthManager.RESULT_CODE_ERROR, null, null);
            }

        });
    }

    static Intent createResultIntentForAuth(IAccount account) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(IAuthManager.KEY_USERNAME, account.getUserName());
        return resultIntent;
    }

    @NonNull
    private TokenRequest createTokenExchangeRequestForAol(@NonNull AuthorizationResponse response, Map<String, String> additionalParamMap) {
        TokenRequest request;// scope = mobilesdk is required in token exchange call for AOL, and response.createTokenExchangeRequest does not allow override scope value, so use builder to build TokenRequest
        Uri aolTokenUri = new Uri.Builder()
                .scheme(AuthConfig.SCHEME_HTTPS)
                .authority(AOL_COM)
                .path(AOL_TOKEN_PATH)
                .build();
        Uri aolAuthUri = new Uri.Builder()
                .scheme(AuthConfig.SCHEME_HTTPS)
                .authority(AOL_COM)
                .path(AOL_AUTH_PATH)
                .build();
        AuthorizationServiceConfiguration configuration = new AuthorizationServiceConfiguration(aolAuthUri, aolTokenUri, null);
        TokenRequest.Builder builder = new TokenRequest.Builder(configuration, response.request.clientId);
        request = builder.setGrantType(QUERY_PARAM_VALUE_GRANT_TYPE_AUTH_CODE).setRedirectUri(response.request.redirectUri.buildUpon().appendQueryParameter(USER_IDP_KEY, USER_IDP_AOL).build())
                .setScope(QUERY_PARAM_VALUE_SCOPE_AOL_TOKEN_EXCHANGE)
                .setCodeVerifier(response.request.codeVerifier)
                .setAuthorizationCode(response.authorizationCode).setAdditionalParameters(additionalParamMap)
                .build();
        return request;
    }

    /**
     * Our internal function to refresh the access token as AppAuth does not support just refresh the token by itself.
     * <p>
     *
     * @param context          the context to get the saved AuthConfig and get string resources
     * @param refreshToken     the refresh token that we have with the account
     * @param issuer           the issuer to get AuthConfig
     * @param responseListener the listener for us to notify the result of the refresh to
     */
    static void refreshAccessToken(@NonNull final Context context, @NonNull final String refreshToken, final String issuer, RefreshTokenResponseListener responseListener) {
        AuthConfig authConfig = AuthConfig.getAuthConfigByIssuer(context, issuer);
        EventLogger eventLogger = EventLogger.getInstance();

        if (authConfig == null) {
            eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_CLIENT_ERROR, EventLogger.RefreshTokenInternalError.AUTH_CONFIG_NULL, "Auth config null in AuthHelper class");
            responseListener.onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
            return;
        }

        //Creating the body of the request with the values needed
        //Namely, we need refresh_token, grant_type set to refresh_token,
        // scope (for yahoo "openid sdpp-w") and client_id for the refresh call to succeed.
        HashMap<String, String> requestParams = new HashMap<>();
        requestParams.put(QUERY_PARAM_KEY_REFRESH_TOKEN, refreshToken);
        requestParams.put(QUERY_PARAM_KEY_GRANT_TYPE, QUERY_PARAM_VALUE_GRANT_TYPE_REFRESH_TOKEN);
        String clientId = authConfig.getClientID();
        if (clientId == null || clientId.isEmpty()) {
            eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_CLIENT_ERROR,
                    EventLogger.RefreshTokenInternalError.AUTH_CONFIG_ERROR,
                    "clientId is empty or null");
            responseListener.onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
            return;
        }
        requestParams.put(KEY_CLIENT_ID, clientId);
        if (isAol(authConfig.getAuthUri(context))) {
            requestParams.put(QUERY_PARAM_KEY_CLIENT_SECRET, authConfig.getClientSecret());
        }
        String encodedRequestBody = AccountNetworkAPI.getFormUrlEncodedRequestBody(requestParams);

        //creating the headers, IE we accept json return values and not say XML or any other return type
        HashMap<String, String> headers = new HashMap<>();
        headers.put(AccountNetworkAPI.ACCEPT, AccountNetworkAPI.CONTENT_TYPE_JSON);

        AccountNetworkAPI.getInstance(context).executeFormPostAsync(context, authConfig.getTokenUri(context).toString(), headers, encodedRequestBody, new AccountNetworkAPI.FormPostAsyncCallback() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String accessToken = jsonObject.getString(Account.KEY_ACCESS_TOKEN);
                    if (Utils.isEmpty(accessToken)) {
                        eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_SERVER_ERROR,
                                EventLogger.RefreshTokenServerError.RESPONSE_MISSING_ACCESS_TOKEN,
                                "Server response contains access token key but value is empty");
                        responseListener.onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
                        return;
                    }
                    String returnedRefreshToken = null;
                    if (!isAol(authConfig.getTokenUri(context))) {
                        returnedRefreshToken = jsonObject.getString(Account.KEY_REFRESH_TOKEN);
                    }
                    responseListener.onSuccess(accessToken, returnedRefreshToken);
                } catch (JSONException e) {
                    eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_SERVER_ERROR,
                            EventLogger.RefreshTokenServerError.RESPONSE_PARSE_FAILURE,
                            "Server response missing required fields " + e.getMessage());
                    responseListener.onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
                }
            }

            @Override
            public void onFailure(int code, HttpConnectionException exception) {
                /*
                  we are going to do some additional error handling here for specific IDP (namely
                  Oath and AoL IDPs) as to translate some errors into a simpler error that the client
                  can deal with
                */
                if (AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR == code) {
                    if (exception == null) {
                        eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_CLIENT_ERROR,
                                EventLogger.RefreshTokenInternalError.MISSING_EXCEPTION_INFORMATION,
                                "An error from the server was encountered but no exception information was captured");
                        responseListener.onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
                    } else {
                        String trimmedRespBody = exception.getRespBody();
                        if (trimmedRespBody != null) {
                            trimmedRespBody = trimmedRespBody.substring(0, Math.min(trimmedRespBody.length(), EventLogger.TRACKING_PARAM_MAX_SIZE));
                        }
                        if (exception.getRespCode() == 400) {
                            try {
                                JSONObject jsonObject = new JSONObject(exception.getRespBody());
                                String errorMsg = jsonObject.getString(KEY_ERROR);
                                if (ERROR_VALUE_INVALID_REQUEST.equals(errorMsg)) {
                                    eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_SERVER_ERROR,
                                            EventLogger.RefreshTokenServerError.INVALID_REQUEST,
                                            "Invalid request error");
                                    responseListener.onFailure(OnRefreshTokenResponse.INVALID_REQUEST);

                                } else if (ERROR_VALUE_INVALID_CLIENT.equals(errorMsg)) {
                                    eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_CLIENT_ERROR,
                                            EventLogger.RefreshTokenInternalError.INVALID_CLIENT,
                                            "Invalid client error");
                                    responseListener.onFailure(OnRefreshTokenResponse.GENERAL_ERROR);

                                } else if (ERROR_VALUE_INVALID_GRANT.equals(errorMsg)) {
                                    eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_SERVER_ERROR,
                                            EventLogger.RefreshTokenServerError.INVALID_GRANT,
                                            "Invalid grant error");
                                    responseListener.onFailure(OnRefreshTokenResponse.REAUTHORIZE_USER);

                                } else if (ERROR_VALUE_IDP_SWITCHED.equals(errorMsg)) {
                                    eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_SERVER_ERROR,
                                            EventLogger.RefreshTokenServerError.IDP_SWITCHED,
                                            "refresh token is invalid because user idp is switched to oath");
                                    responseListener.onFailure(RefreshTokenResponseListener.ERROR_CODE_IDP_SWITCHED); // special error code for internal purpose

                                } else if (ERROR_VALUE_UNAUTHORIZED_CLIENT.equals(errorMsg)) {
                                    eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_SERVER_ERROR,
                                            EventLogger.RefreshTokenServerError.UNAUTHORIZED_CLIENT,
                                            "Unauthorized client error");
                                    responseListener.onFailure(OnRefreshTokenResponse.UNAUTHORIZED_CLIENT);

                                } else if (ERROR_VALUE_UNSUPPORTED_GRANT_TYPE.equals(errorMsg)) {
                                    eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_CLIENT_ERROR,
                                            EventLogger.RefreshTokenInternalError.UNSUPPORTED_GRANT_TYPE,
                                            "Unsupported grant type error");
                                    responseListener.onFailure(OnRefreshTokenResponse.GENERAL_ERROR);

                                } else if (ERROR_VALUE_INVALID_SCOPE.equals(errorMsg)) {
                                    eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_SERVER_ERROR,
                                            EventLogger.RefreshTokenServerError.INVALID_SCOPE,
                                            "Invalid scope error");
                                    responseListener.onFailure(OnRefreshTokenResponse.INVALID_SCOPE);

                                } else {
                                    eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_SERVER_ERROR,
                                            EventLogger.RefreshTokenServerError.RESPONSE_UNRECOGNIZED_ERROR_REASON,
                                            "Unrecognized error. Http status: "
                                                    + exception.getRespCode() + " Response Body: " + trimmedRespBody);
                                    responseListener.onFailure(OnRefreshTokenResponse.GENERAL_ERROR);

                                }
                            } catch (JSONException e) {
                                eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_SERVER_ERROR,
                                        EventLogger.RefreshTokenServerError.RESPONSE_MISSING_ERROR_FIELD,
                                        "No error field. Http status: "
                                                + exception.getRespCode() + " Response Body: " + trimmedRespBody);
                                responseListener.onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
                            }
                        } else if (exception.getRespCode() >= 500 && exception.getRespCode() < 600) {
                            eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_SERVER_ERROR,
                                    EventLogger.RefreshTokenServerError.RETRY_LATER,
                                    "Http 5xx code (retry later) encountered. Http status: " +
                                            exception.getRespCode() + " Response Body: " + trimmedRespBody);
                            responseListener.onFailure(OnRefreshTokenResponse.SERVER_ERROR);
                        } else {
                            eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_SERVER_ERROR,
                                    EventLogger.RefreshTokenServerError.UNRECOGNIZED_STATUS_CODE_AND_ERROR,
                                    "Unrecognized http status code. Http status: "
                                            + exception.getRespCode() + " Response Body: " + trimmedRespBody);
                            responseListener.onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
                        }
                    }

                } else {
                    responseListener.onFailure(code);
                }
            }
        });
    }

    static void revokeToken(@NonNull final Context context, @NonNull final String refreshToken, @NonNull final String issuer, @NonNull final RevokeTokenResponseListener listener) {
        AuthConfig config = AuthConfig.getAuthConfigByIssuer(context, issuer);
        if (config != null && !isAol(config.getAuthUri(context))) {
            AccountNetworkAPI networkAPI = AccountNetworkAPI.getInstance(context);
            new RevokeTokenTask(listener).execute(context, networkAPI, config.getClientID(), refreshToken);
        } else {
            listener.onComplete(RevokeTokenResponseListener.REVOKE_TOKEN_SUCCESS, null);
        }
    }

    static void getUserInfoAsync(@NonNull final Context context, @NonNull final String accessToken, final String issuer, @NonNull final OnUserDataResponseListener listener) {
        AuthConfig config = AuthConfig.getAuthConfigByIssuer(context, issuer);
        if (config != null && isAol(config.getAuthUri(context))) {
            AccountNetworkAPI networkAPI = AccountNetworkAPI.getInstance(context);
            new UserDataTask(new UserDataResponseListener() {
                @Override
                public void onResponse(String responseJson) {
                    UserDataResponse response = UserDataResponse.fromJson(responseJson);
                    if (response != null && 200 == response.mStatusCode) {
                        listener.onSuccess(response.mUserData);
                    } else {
                        //TODO: log it properly with telemetry
                        Log.e(TAG + " Error code: ", (response != null) ? Integer.toString(response.mStatusCode) : "No response received");
                        listener.onFailure();
                    }
                }

                @Override
                public void onError(int errorCode, String errorMessage) {
                    listener.onFailure();
                }
            }).execute(context, networkAPI, accessToken);
        } else {
            listener.onFailure();
        }
    }

    static boolean isAol(@NonNull Uri uri) {
        return uri.getAuthority().endsWith(AOL_COM);
    }

    interface ResponseListener {
        void onComplete(int resultCode, Intent responseIntent, AuthorizationException authorizationException);
    }

    interface RefreshTokenResponseListener {
        // It is a special case that will be internally handled and not available externally.
        int ERROR_CODE_IDP_SWITCHED = 9099;

        void onSuccess(@NonNull String accessToken, @Nullable String refreshToken);

        void onFailure(int code);
    }

    interface RevokeTokenResponseListener {
        int REVOKE_TOKEN_SUCCESS = 0;

        void onComplete(int resultCode, String errorMessage);
    }

    interface UserDataResponseListener {
        void onResponse(String response);

        void onError(int errorCode, String errorMessage);
    }
}
