package com.oath.mobile.platform.phoenix.core;

/**
 * Created by yuhongli on 10/31/17.
 */

public interface OnRefreshTokenResponse {
    /**
     * The request includes an unsupported parameter value (other than grant type), or have repeated
     * parameters, or includes multiple credentials, or is otherwise malformed
     */
    int INVALID_REQUEST = -20;

    /**
     * The provided authorization grant (e.g., authorization code, resource owner credentials)
     * or refresh token is invalid, expired or revoked. Please prompt the user to log in
     * again to obtain a new refresh token.
     */
    int REAUTHORIZE_USER = -21;

    /**
     * The authenticated client is not authorized to use this grant type, check client_id and
     * client_secret
     */
    int UNAUTHORIZED_CLIENT = -22;

    /**
     * The requested scope is invalid, unknown, malformed or exceeds the scope granted by the server
     */
    int INVALID_SCOPE = -23;

    /**
     * We were unable to reach the identity server, please check the network connection and prompt
     * the user for action if required.
     */
    int NETWORK_ERROR = -24;

    /**
     * We have received a 5xx Server Error code, please try again later.
     */
    int SERVER_ERROR = -25;

    /**
     * Something has went wrong with the authorization process, please ensure your configuration is
     * correct. Contact the phoenix team if the problem persists.
     */
    int GENERAL_ERROR = -50;

    /**
     * Method is invoked if refresh token completed
     */
    void onSuccess();

    /**
     * This will be called whenever we encounter an error in getting the refresh token.
     * We define an error to be a response from the server with Http code other than 200 or if there
     * was some error encountered during the processing of this request before it was sent to the server.
     *
     * This roughly follows the standards defined by the Oauth 2.0 Authorization Framework.
     * For more information you can look at IETF RFC 6749 section 5.2 and section 6
     *
     * @see <a href="https://tools.ietf.org/html/rfc6749#section-5.2">https://tools.ietf.org/html/rfc6749#section-5.2</a>
     * @see <a href="https://tools.ietf.org/html/rfc6749#section-6">https://tools.ietf.org/html/rfc6749#section-6</a>
     *
     * @param errorCode the error code thrown by the SDK.
     */

    void onError(int errorCode);
}
