package com.oath.mobile.platform.phoenix.core;

/**
 * Created by yuhongli on 10/30/17.
 */

interface AccountChangeListener {
    int SUCCESS = 0;
    int FAIL = 1;
    void onAccountChanged(int result);
}
