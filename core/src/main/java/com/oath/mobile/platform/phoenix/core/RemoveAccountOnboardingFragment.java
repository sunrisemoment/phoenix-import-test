package com.oath.mobile.platform.phoenix.core;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.concurrent.TimeUnit;

/**
 * Created by yuhongli on 1/2/18.
 */

public class RemoveAccountOnboardingFragment extends Fragment {


    View mRemoveRow;

    public static RemoveAccountOnboardingFragment newInstance() {
        return new RemoveAccountOnboardingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.phoenix_remove_account_onboarding_layout, container, false);
        mRemoveRow = view.findViewById(R.id.removeAccountOnboardingAnimationRow);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (mRemoveRow != null) {
            if (isVisibleToUser) {
                startAnimation();
            } else {
                mRemoveRow.setScaleX(1f);
                mRemoveRow.setScaleY(1f);
            }
        }
    }

    protected void startAnimation() {
        mRemoveRow.animate().scaleX(0f).scaleY(0f).setStartDelay(500).setDuration(TimeUnit.SECONDS.toMillis(1));
    }

}
