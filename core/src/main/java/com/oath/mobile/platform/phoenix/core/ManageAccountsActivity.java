package com.oath.mobile.platform.phoenix.core;

import android.app.Activity;
import android.app.Dialog;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.provider.Settings;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.yahoo.mobile.client.share.util.ThreadPoolExecutorSingleton;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by yuhongli on 10/26/17.
 */
public class ManageAccountsActivity extends BasePhoenixActivity implements ManageAccountsAdapter.Callback, DialogInterface.OnDismissListener {

    static final int REQUEST_CODE_FOR_AUTH = 9000;
    static final String ARG_DISMISS_ON_ADD_ACCOUNT = "dismiss_when_new_account_added";
    static final String RESULT_RECEIVER_EXTRA = "com.oath.mobile.platform.phoenix.core.ManageAccountsActivity.RESULT_RECEIVER_EXTRA";
    static final String KEY_SHOW_MANAGE_ACCOUNTS_ONBOARDING = "show_manage_accounts_onboarding";
    protected MenuItem mEditMenuItem;
    Toolbar mToolbar;
    ManageAccountsAdapter mAccountsAdapter;
    IAuthManager mAuthManager;
    ManageAccountsViewModel mViewModel;
    private boolean inEditMode;
    Dialog mProgressDialog;
    //Make them specifically a ArrayList and not a List to make use of serialization for instance states
    ArrayList<String> mRemovedAccounts;
    ArrayList<String> mAddedAccounts;
    ToolTipWindow mEditButtonToolTipWindow;
    static final String INTERNAL_LAUNCH_GATE = "internal_launch_gate";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean isLaunchedFromBuilder = false;
        if (savedInstanceState != null) {
            isLaunchedFromBuilder = savedInstanceState.getBoolean(INTERNAL_LAUNCH_GATE);
            mRemovedAccounts = savedInstanceState.getStringArrayList(ManageAccounts.KEY_REMOVED_ACCOUNTS_LIST);
            mAddedAccounts = savedInstanceState.getStringArrayList(ManageAccounts.KEY_ADDED_ACCOUNTS_LIST);
            //Do this check here because even if the saved instance contains the right key, the value
            //may fail to be mapped to a ArrayList and return null.
            if (mRemovedAccounts == null) {
                mRemovedAccounts = new ArrayList<>();
            }
            if (mAddedAccounts == null) {
                mAddedAccounts = new ArrayList<>();
            }
        } else {
            mRemovedAccounts = new ArrayList<>();
            mAddedAccounts = new ArrayList<>();
            isLaunchedFromBuilder = getIntent().getBooleanExtra(INTERNAL_LAUNCH_GATE, false);
        }

        if (!isLaunchedFromBuilder) {
            throw new UnsupportedOperationException("Please initialize ManageAccountsActivity via ManageAccounts.IntentBuilder");
        }
        EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_START, null);

        setContentView(R.layout.activity_manage_accounts);
        mViewModel = ViewModelProviders.of(this).get(ManageAccountsViewModel.class);
        mViewModel.setShouldDismissOnAddAccount(getIntent().getBooleanExtra(ARG_DISMISS_ON_ADD_ACCOUNT, false));
        mViewModel.setResultReceiver(getIntent().getParcelableExtra(RESULT_RECEIVER_EXTRA));

        mToolbar = findViewById(R.id.phoenix_toolbar);
        setupToolbar();
        mAuthManager = AuthManager.getInstance(this);
        mEditButtonToolTipWindow = new ToolTipWindow(this);

        RecyclerView accountsView = findViewById(R.id.phoenix_manage_accounts_list);
        setupAccountsView(accountsView);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (shouldShowOnboardingFlow()) {
            showManageAccountsOnboardingFlow();
        } else {
            showEditToolTip();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList(ManageAccounts.KEY_REMOVED_ACCOUNTS_LIST, mRemovedAccounts);
        outState.putStringArrayList(ManageAccounts.KEY_ADDED_ACCOUNTS_LIST, mAddedAccounts);
        outState.putBoolean(INTERNAL_LAUNCH_GATE, true);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mToolbar.setNavigationOnClickListener(v -> finish());
    }

    void showEditToolTip() {
        mEditButtonToolTipWindow.showToolTip(mToolbar, ToolTipWindow.EDIT,
                Html.fromHtml(getResources().getString(R.string.phoenix_manage_accounts_edit_tooltip)),
                (getResources().getInteger(R.integer.phoenix_manage_account_edit_tooltip_offset)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.manage_accounts_menu, menu);
        mEditMenuItem = menu.findItem(R.id.account_edit_accounts);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.account_edit_accounts) {
            if (!inEditMode) {
                enableEditMode();
            } else {
                disableEditMode();
            }
            return true;
        }

        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        notifyAccountSetChanged();
    }

    void notifyAccountSetChanged() {
        mAccountsAdapter.notifyAccountSetChanged();
    }

    boolean shouldShowOnboardingFlow() {
        return getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE).getBoolean(KEY_SHOW_MANAGE_ACCOUNTS_ONBOARDING, true);
    }

    private void showManageAccountsOnboardingFlow() {
        getManageAccountsOnboardingView().show(getSupportFragmentManager(), "");
        getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE).edit().putBoolean(KEY_SHOW_MANAGE_ACCOUNTS_ONBOARDING, false).apply();
    }

    protected ManageAccountsOnboardingView getManageAccountsOnboardingView() {
        return new ManageAccountsOnboardingView();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_FOR_AUTH) {
            if (resultCode == Activity.RESULT_OK) {
                mViewModel.setAccountChanged(true);
                String username = data.getStringExtra(IAuthManager.KEY_USERNAME);
                if (username != null) {
                    markAccountAdded(username);
                    invokeOnReceiveResult(IAuthManager.RESULT_CODE_ACCOUNT_LOGGED_IN, data.getStringExtra(IAuthManager.KEY_USERNAME));
                }
                EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_SIGN_IN_SUCCESS, null);
                if (mViewModel.shouldDismissOnAddAccount()) {
                    finish();
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_SIGN_IN_CANCEL, null);
                // When returning from sign in activity, if there are no accounts to show, dismiss ManageAccounts
                if (mAccountsAdapter.getAccountCount() == 0) {
                    mViewModel.setAccountChanged(true);
                    finish();
                }
            } else if (resultCode == IAuthManager.RESULT_CODE_ERROR) {
                // TODO show error dialog
                EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_SIGN_IN_ERROR, null);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    void enableEditMode() {
        EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_START, null);

        inEditMode = true;
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        mEditMenuItem.setTitle(getString(R.string.phoenix_manage_accounts_done));
        mAccountsAdapter.enableEditMode();
        mEditButtonToolTipWindow.dismiss();
    }

    void disableEditMode() {
        EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_END, null);
        inEditMode = false;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mEditMenuItem.setTitle(getString(R.string.phoenix_manage_accounts_edit));
        mAccountsAdapter.disableEditMode();
    }

    private void setupAccountsView(RecyclerView accountsList) {
        mAccountsAdapter = new ManageAccountsAdapter(this, mAuthManager);
        accountsList.setAdapter(mAccountsAdapter);

        accountsList.setHasFixedSize(true);
        accountsList.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onAddAccount() {
        EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_SIGN_IN_START, null);
        Intent authIntent = new Auth.IntentBuilder().buildSignIn(this);
        authIntent.putExtra(EventLogger.APP_INTERNAL_ORIGIN_DATA_KEY, EventLogger.TRACKING_PARAM_EVENT_ORIGIN_MANAGE_ACCOUNT);
        startActivityForResult(authIntent, REQUEST_CODE_FOR_AUTH);
    }

    @Override
    public void onNoAccountsFound() {
        mViewModel.setAccountChanged(true);
        finish();
    }

    @Override
    public void onRemoveAccount(final int position, final IAccount account) {
        EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_REMOVE_START, null);

        final Dialog dialog = new Dialog(this);
        //Save account information

        CustomDialogHelper.generateTwoVerticalButtonDialog(dialog,
                getString(R.string.phoenix_remove_account_dialog_title),
                Html.fromHtml(getString(R.string.phoenix_remove_account_dialog, account.getUserName())),
                getString(R.string.phoenix_remove_account),
                (View v) -> removeAccount(position, account, dialog),
                getString(R.string.cancel),
                v -> {
                    EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_REMOVE_CANCEL, null);
                    dialog.dismiss();
                }
        );

        showConfirmationDialog(dialog);
    }

    private void removeAccount(final int position, final IAccount account, final Dialog dialog) {
        dialog.dismiss();
        AccountChangeListener listener = result -> {
            safeDismissProgressDialog();
            if (result == AccountChangeListener.SUCCESS) {
                mViewModel.setAccountChanged(true);
                //By this point, the account is already gone, used saved information
                if (((Account) account).isLoggedIn()) {
                    markAccountRemoved(account.getUserName());
                }
                if (!isFinishing()) {
                    mAccountsAdapter.removeAccountAtPosition(position);
                }
                EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_REMOVE_SUCCESS, null);
            } else {
                EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_REMOVE_FAILURE, null);
            }
        };
        showProgressDialog();
        // send the synchronous message back to the caller before account is removed

        ThreadPoolExecutorSingleton.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                invokeOnReceiveResult(IAuthManager.RESULT_CODE_ACCOUNT_LOGGED_OUT, account.getUserName());
                ((Account) account).remove(ManageAccountsActivity.this, listener);
            }
        });
    }

    void markAccountRemoved(String username) {
        if (mAddedAccounts.contains(username)) {
            mAddedAccounts.remove(username);
        }

        if (!mRemovedAccounts.contains(username)) {
            mRemovedAccounts.add(username);
        }
    }

    void markAccountAdded(String username) {
        if (mRemovedAccounts.contains(username)) {
            mRemovedAccounts.remove(username);
        }

        if (!mAddedAccounts.contains(username)) {
            mAddedAccounts.add(username);
        }
    }

    private void showConfirmationDialog(Dialog dialog) {
        if (dialog != null && !dialog.isShowing()) {
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
    }

    @Override
    public void onAccountToggled(int position, IAccount account, AccountChangeListener listener) {
        mViewModel.setAccountChanged(true);
        if (((Account) account).isLoggedIn() && account.isActive()) {
            // toggle off
            showProgressDialog();
            ThreadPoolExecutorSingleton.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    invokeOnReceiveResult(IAuthManager.RESULT_CODE_ACCOUNT_LOGGED_OUT, account.getUserName());
                    markAccountRemoved(account.getUserName());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((Account) account).disable(listener);
                            safeDismissProgressDialog();
                        }
                    });
                }
            });
        } else {
            // toggle on
            showProgressDialog();
            account.refreshToken(this, new OnRefreshTokenResponse() {
                @Override
                public void onSuccess() {
                    //we don't need to do anything here as the item is toggled else where to on
                    //toggle on success
                    markAccountAdded(account.getUserName());
                    fetchUserInfo((Account) account);
                    invokeOnReceiveResult(IAuthManager.RESULT_CODE_ACCOUNT_LOGGED_IN, account.getUserName());
                    EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_SUCCESS, null);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            safeDismissProgressDialog();
                        }
                    });
                }

                @Override
                public void onError(int errorCode) {
                    Map<String, Object> errorInfoParams = EventLogger.getRefreshTokenErrorEventParams(null, errorCode);
                    EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_FAILURE, errorInfoParams);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            safeDismissProgressDialog();
                        }
                    });
                    if (errorCode == OnRefreshTokenResponse.REAUTHORIZE_USER) {
                        runOnUiThread(() -> {
                            notifyAccountSetChanged();
                            showLoginAgainDialog();
                        });
                    } else if (errorCode == OnRefreshTokenResponse.NETWORK_ERROR) {
                        runOnUiThread(() -> {
                            notifyAccountSetChanged();
                            showNoNetworkDialog();
                        });
                    } else {
                        runOnUiThread(() -> {
                            notifyAccountSetChanged();
                            showTryAgainLaterDialog();
                        });
                    }
                }
            });

        }
    }

    private void fetchUserInfo(final Account account) {
        account.fetchUserInfo(ManageAccountsActivity.this, new OnUserDataResponseListener() {
            @Override
            public void onSuccess(UserData userData) {
                if (account.getGUID().equals(userData.mGuid)) {
                    account.setUserName(userData.mLoginId);
                    account.setDisplayName(userData.mDisplayName);
                    account.setFirstName(userData.mGivenName);
                    account.setLastName(userData.mFamilyName);
                    account.setEmail(userData.mEmail);
                    account.setIsActive(true);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            notifyAccountSetChanged();
                        }
                    });
                }
            }

            @Override
            public void onFailure() {
                //TODO log the failure
            }
        });
    }

    void invokeOnReceiveResult(int resultCodeAccountLoggedIn, String userName) {
        if (mViewModel.getResultReceiver() != null) {
            Bundle bundle = new Bundle();
            bundle.putString(IAuthManager.KEY_USERNAME, userName);
            mViewModel.getResultReceiver().send(resultCodeAccountLoggedIn, bundle);
        }
    }

    void showLoginAgainDialog() {
        final Dialog customDialog = new Dialog(this);
        CustomDialogHelper.generateTwoVerticalButtonDialog(customDialog,
                this.getString(R.string.phoenix_unable_to_turn_on_account),
                this.getString(R.string.phoenix_invalid_refresh_token_error),
                this.getString(R.string.phoenix_continue),
                v -> {
                    customDialog.dismiss();
                    onAddAccount();
                },
                this.getString(R.string.phoenix_cancel),
                v -> customDialog.dismiss());
        customDialog.setCancelable(true);
        customDialog.setCanceledOnTouchOutside(true);
        customDialog.show();
    }

    void showNoNetworkDialog() {
        if (isAirplaneModeOn()) {
            AlertUtils.showAirplaneModeDialog(this);
        } else {
            AlertUtils.showErrorDialogWithTitle(this,
                    this.getString(R.string.phoenix_unable_to_turn_on_account),
                    this.getString(R.string.phoenix_no_internet_connection));
        }
    }

    void showTryAgainLaterDialog() {
        AlertUtils.showErrorDialogWithTitle(this,
                this.getString(R.string.phoenix_unable_to_turn_on_account),
                this.getString(R.string.phoenix_try_again_error));
    }

    boolean isAirplaneModeOn() {
        return Settings.Global.getInt(this.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }

    @Override
    public void finish() {
        EventLogger.getInstance().logUserEvent(EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_END, null);
        if (mViewModel.hasAccountChanged()) {
            Intent resultIntent = new Intent();
            resultIntent.putExtra(ManageAccounts.KEY_ACCOUNT_CHANGE_RESULT, ManageAccounts.RESULT_ACCOUNT_CHANGED);
            resultIntent.putStringArrayListExtra(ManageAccounts.KEY_REMOVED_ACCOUNTS_LIST, mRemovedAccounts);
            resultIntent.putStringArrayListExtra(ManageAccounts.KEY_ADDED_ACCOUNTS_LIST, mAddedAccounts);
            setResult(Activity.RESULT_OK, resultIntent);
        }
        super.finish();
    }

    protected void showProgressDialog() {
        if (!isFinishing()) {
            if (mProgressDialog == null) {
                mProgressDialog = CustomDialogHelper.generateProgressDialog(this);
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
            } else {
                mProgressDialog.show();
            }
        }
    }

    protected void safeDismissProgressDialog() {
        if (!isFinishing() && mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        safeDismissProgressDialog();
        mEditButtonToolTipWindow.dismiss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        showEditToolTip();
    }

    public static class ManageAccountsViewModel extends ViewModel {
        private boolean shouldDismissActivity;
        private boolean isAccountChanged;
        private ResultReceiver resultReceiver;

        public void setShouldDismissOnAddAccount(boolean shouldDismissActivity) {
            this.shouldDismissActivity = shouldDismissActivity;
        }

        public void setAccountChanged(boolean accountChanged) {
            isAccountChanged = accountChanged;
        }

        public boolean shouldDismissOnAddAccount() {
            return shouldDismissActivity;
        }

        public boolean hasAccountChanged() {
            return isAccountChanged;
        }

        void setResultReceiver(ResultReceiver resultReceiver) {
            this.resultReceiver = resultReceiver;
        }

        ResultReceiver getResultReceiver() {
            return resultReceiver;
        }

    }
}
