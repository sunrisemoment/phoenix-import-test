package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yahoo.mobile.client.android.fuji.widget.OrbImageView;
import com.yahoo.mobile.client.share.util.Util;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;

/**
 * Created by yuhongli on 12/1/17.
 */

public class AccountPickerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Callback mListener;
    protected static final int TYPE_ACCOUNT_ITEM = 0;
    protected static final int TYPE_ADD_ACCOUNT_ITEM = 1;
    private List<IAccount> mAccounts;
    private AuthManager mAuthManager;

    public AccountPickerAdapter(@NonNull Callback listener, @NonNull IAuthManager authManager) {
        mListener = listener;
        mAuthManager = (AuthManager) authManager;
        setAccounts();
    }


    protected void setAccounts() {
        List<IAccount> allAccounts = mAuthManager.getAllDisabledAccounts();
        mAccounts = new ArrayList<>();
        if (Util.isEmpty(allAccounts)) {
            mListener.onNoAccountsFound();
        } else {
            mAccounts.addAll(allAccounts);
            // Accounts are in alphabetical order
            AccountUtils.sortAccounts(mAccounts);
        }
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_ACCOUNT_ITEM:
                return new AccountPickerAdapter.AccountItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.account_picker_list_item_account, parent, false), mListener);
            case TYPE_ADD_ACCOUNT_ITEM:
                return new AccountPickerAdapter.AddAccountItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.manage_accounts_list_item_add_account, parent, false), mListener);
            default:
                throw new IllegalArgumentException("view type not defined");
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AccountItemViewHolder) {
            ((AccountItemViewHolder) holder).bindData(getAccountAtPosition(position));
        }
    }

    private IAccount getAccountAtPosition(int position) {
        return mAccounts.get(position);
    }

    public int getAccountCount() {
        if (!Util.isEmpty(mAccounts)) {
            return mAccounts.size();
        }
        return 0;
    }

    @Override
    public int getItemCount() {
        int count = getAccountCount();
        count += 1; // Add Account
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == mAccounts.size()) {
            return TYPE_ADD_ACCOUNT_ITEM;
        } else {
            return TYPE_ACCOUNT_ITEM;
        }
    }

    public void notifyAccountSetChanged() {
        setAccounts();
    }

    class AccountItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView mName;
        private final TextView mEmail;
        private final OrbImageView mImage;

        protected Callback mListener;
        protected Context mContext;

        private Account mAccount;
        private View mItemView;

        public AccountItemViewHolder(View itemView, Callback listener) {
            super(itemView);
            mContext = itemView.getContext();
            mName = itemView.findViewById(R.id.account_display_name);
            mEmail = itemView.findViewById(R.id.account_email);
            mImage = itemView.findViewById(R.id.account_profile_image);
            mListener = listener;
            mItemView = itemView;
        }

        public void bindData(IAccount account) {
            mAccount = (Account) account;
            setAccountNameAndEmail(account);
            OkHttpClient client = AccountNetworkAPI.getInstance(mContext).getOkHttpClient();
            ImageLoader.loadImageIntoView(client, mAccount.getImageUri(), mImage);
            mItemView.setOnClickListener(this);
        }

        private void setAccountNameAndEmail(IAccount account) {
            String userName = account.getUserName();
            String displayName = account.getDisplayName();
            mName.setText(displayName);
            if (!Util.isEqual(displayName, userName)) {
                mEmail.setText(userName);
                mEmail.setVisibility(View.VISIBLE);
            } else {
                mEmail.setVisibility(View.GONE);
            }
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                mListener.onAccountSelected(getAdapterPosition(), mAccount);
            } // else ignore the click since we don't know which item was actually clicked
        }
    }

    class AddAccountItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final Callback mListener;

        public AddAccountItemViewHolder(View itemView, Callback listener) {
            super(itemView);
            mListener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onAddAccount();
        }
    }

    public interface Callback {
        void onAddAccount();

        void onNoAccountsFound();

        void onAccountSelected(int position, IAccount account);
    }
}
