package com.oath.mobile.platform.phoenix.core;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by nsoni on 12/1/17.
 */

public class UserDataResponse {

    private static final String RESPONSE = "response";
    private static final String STATUS_CODE = "statusCode";
    private static final String DATA = "data";
    private static final String USER_DATA = "userData";
    private static final String DISPLAY_NAME = "displayName";
    private static final String LOGIN_ID = "loginId";
    private static final String ATTRIBUTES = "attributes";
    private static final String GIVEN_NAME = "given_name";
    private static final String FAMILY_NAME = "family_name";
    private static final String GUID = "guid";
    private static final String EMAIL = "email";
    int mStatusCode;
    UserData mUserData;


    // TODO: Write another data response parser for Oath
    private UserDataResponse(int statusCode, UserData userData) {
        mStatusCode = statusCode;
        mUserData = userData;
    }

    static UserDataResponse fromJson(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject responseObject = jsonObject.getJSONObject(RESPONSE);
            int statusCode = responseObject.getInt(STATUS_CODE);
            JSONObject userData = responseObject.optJSONObject(DATA).optJSONObject(USER_DATA);
            if (userData != null) {
                String displayName = userData.optString(DISPLAY_NAME);
                String loginId = userData.optString(LOGIN_ID);
                JSONObject attributes = userData.optJSONObject(ATTRIBUTES);
                if (attributes != null) {
                    String givenName = attributes.optString(GIVEN_NAME);
                    String familyName = attributes.optString(FAMILY_NAME);
                    String guid = attributes.optString(GUID);
                    String email = attributes.optString(EMAIL);
                    return new UserDataResponse(statusCode, new UserData(guid, loginId, givenName, familyName, email, displayName));
                }
            }
            return new UserDataResponse(statusCode, null);
        } catch (JSONException e) {
            //TODO add logging
        }
        return null;
    }

}
