package com.oath.mobile.platform.phoenix.core;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;

import com.yahoo.mobile.client.share.util.Util;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ImageLoader {
    private static String TAG = "ImageLoader";


    public static void loadImageIntoView(OkHttpClient client, String url, final ImageView imageView) {
        if (imageView == null) {
            throw new NullPointerException("ImageView should not be null");
        }
        loadImage(client, url, bitmap -> {
            if (bitmap != null) {
                imageView.setImageBitmap(bitmap);
            }
        });
    }

    public static void loadImage(OkHttpClient client, String url, @NonNull final IAccountImageLoaderListener loaderListener) {
        if (Util.isEmpty(url) || HttpUrl.parse(url) == null) {
            return;
        }
        Request request = new Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                postOnMainThread(null, loaderListener);
                Log.e(TAG, "Image load failed");
            }

            @Override
            public void onResponse(Call call, final Response response) {
                if (response != null) {
                    if (response.isSuccessful()) {
                        try {
                            final Bitmap bitmap = BitmapFactory.decodeStream(response.body().byteStream());
                            postOnMainThread(bitmap, loaderListener);
                        } catch (Exception e) {
                            postOnMainThread(null, loaderListener);
                            Log.e(TAG, e.getMessage());
                        } finally {
                            response.body().close();
                        }
                    } else {
                        response.body().close();
                        postOnMainThread(null, loaderListener);
                        Log.e(TAG, "Image load failed");
                    }
                } else {
                    postOnMainThread(null, loaderListener);
                    Log.e(TAG, "Failed to get network response");
                }
            }
        });
    }

    private static void postOnMainThread(final Bitmap bitmap, final IAccountImageLoaderListener loaderListener) {
        Handler mainHandler = new Handler(Looper.getMainLooper());// for getting main thread
        mainHandler.post(() -> loaderListener.complete(bitmap));
    }

    public interface IAccountImageLoaderListener {

        void complete(Bitmap bm);

    }
}
