package com.oath.mobile.platform.phoenix.core;

import android.content.Context;

import java.util.List;

/**
 * Created by nsoni on 9/21/17.
 */

public interface IAuthManager {
    /**
     * The activity that started the Auth intent,
     * must use the following action as part of it's intent filter after prepending the application id
     */
    int RESULT_CODE_ERROR = 9001;
    int RESULT_CODE_ACCOUNT_LOGGED_IN = 9002;
    int RESULT_CODE_ACCOUNT_LOGGED_OUT = 9003;

    /**
     * Key to set/retrieve user name into the result intent
     */
    String KEY_USERNAME = "username";

    /**
     * Return the account with the given username if it exists in the Android AccountManager
     * else return null
     *
     * NOTE: It will return new object with the same values, every time it is called
     *
     * @param username login id entered by the user during the sign in as a username
     * @return
     */
    IAccount getAccount(String username);

    /**
     * Provide all the logged in accounts within the app
     *
     * NOTE: It will return new objects with the same values, every time it is called
     *
     * @return List of IAccount objects or an empty list
     */
    List<IAccount> getAllAccounts();

    /**
     * This API is used when an app bootstraps existing user to work with phoenix sdk during the first launch.
     * This measure needs to be taken by the app so that accounts stay logged in after the app upgrade (where the newer version of the app uses phoenix sdk).
     * Essentially, it hands over credential management and storage of each account to phoenix sdk.
     *
     * @param context
     * @param dataList List of BootstrapData objects for containing GUID, Display Name, Login id and Refresh token for each account being handed over
     * @param onBootStrapResponse Callback to get list of successfully added IAccount objects
     * @throws IllegalStateException If the Auth Config is missing
     */
    void bootstrap(Context context, List<AuthManager.BootstrapData> dataList, AuthManager.OnBootstrapResponse onBootStrapResponse) throws IllegalStateException;
}
