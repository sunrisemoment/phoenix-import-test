package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.webkit.URLUtil;

import com.yahoo.mobile.client.share.telemetry.TelemetryLog;
import com.yahoo.mobile.client.share.util.Util;
import com.yahoo.mobile.client.share.yokhttp.TelemetryLogInterceptor;
import com.yahoo.mobile.client.share.yokhttp.YOkHttp;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;

import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static com.oath.mobile.platform.phoenix.core.AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenInternalError.EVENT_REFRESH_TOKEN_CLIENT_ERROR;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenServerError.EVENT_REFRESH_TOKEN_SERVER_ERROR;

/**
 * Created by yuhongli on 11/2/17.
 */

public class AccountNetworkAPI {

    /**
     * Http scheme and domain validation failed
     */
    public static final int ERROR_CODE_HTTP_VALIDATION_ERROR = 2400;

    public static final int ERROR_CODE_ACCOUNT_DATA_TRANSPORT_ERROR = 2200;
    /**
     * Network related issues
     */
    public static final int ERROR_CODE_ACCOUNT_NETWORK_UNAVAILABLE = 2300;
    public static final int ERROR_CODE_ACCOUNT_NETWORK_SSL_VERIFICATION_ERROR = ERROR_CODE_ACCOUNT_NETWORK_UNAVAILABLE + 1;
    public static final int ERROR_CODE_ACCOUNT_NETWORK_SSL_VERIFICATION_ERROR_DUE_TO_DATE_TIME =
            ERROR_CODE_ACCOUNT_NETWORK_UNAVAILABLE + 2;
    public static final int ERROR_CODE_NETWORK_UNAVAILABLE_AIRPLANE_MODE = ERROR_CODE_ACCOUNT_NETWORK_UNAVAILABLE
            + 3;
    public static final int ERROR_CODE_NETWORK_TIME_OUT = ERROR_CODE_ACCOUNT_NETWORK_UNAVAILABLE + 4;
    public static final int ERROR_CODE_CANCELLED = ERROR_CODE_ACCOUNT_NETWORK_UNAVAILABLE + 5;

    public static final String CONTENT_TYPE_JSON = "application/json";
    public static final String CONTENT_TYPE_X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";
    public static final String ELEM_HEADER_CONTENT_TYPE = "Content-Type";
    public static final String ACCEPT = "Accept";
    public static final String INVALID_URL_MSG = "Input url is invalid."; //Internal / debug use only
    private static final String UTF8 = "charset=utf-8";
    private static final String MEDIA_TYPE_SEPARATOR = ";";
    private static final String MEDIA_TYPE_JSON = CONTENT_TYPE_JSON + MEDIA_TYPE_SEPARATOR + UTF8;
    private static final String MEDIA_TYPE_X_WWW_FORM_URLENCODED = CONTENT_TYPE_X_WWW_FORM_URLENCODED;
    private static final String CONTENT_TYPE = "content-type";
    private static final String TAG = AccountNetworkAPI.class.getSimpleName();

    private static volatile AccountNetworkAPI sSingletonInstance = null;

    private volatile OkHttpClient mOkHttpClient;

    private AccountNetworkAPI(Context context) {
        mOkHttpClient = createOkHttpClient(context);
    }

    public static AccountNetworkAPI getInstance(Context context) {
        if (sSingletonInstance == null) {
            synchronized (AccountNetworkAPI.class) {
                if (sSingletonInstance == null) {
                    sSingletonInstance = new AccountNetworkAPI(context);
                }
            }
        }

        return sSingletonInstance;
    }

    static boolean validateUrl(String url) {
        if (Util.isEmpty(url)) {
            return false;
        }
        if (!URLUtil.isHttpsUrl(url)) {
            return false;
        }

        if (!validateDomain(url)) {
            return false;
        }

        return true;
    }

    private static boolean validateDomain(String url) {
        try {
            URI uri = new URI(url);
            String host = uri.getHost();

            if (Util.isEmpty(host)) {
                return false;
            }
            return true; // TODO Add domain validation
        } catch (URISyntaxException e) {
            return false;
        }
    }

    /**
     * Checks the availability of a network.
     *
     * @param context A Context object.
     * @return True if a network is available, false otherwise.
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
        }

        return false;
    }

    /**
     * Check airplane mode is on or off
     *
     * @param context
     * @return
     */
    public static boolean isAirplaneModeOn(Context context) {
        return Settings.System.getInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) == 1;
    }

    @VisibleForTesting
    static void resetInstance() {
        sSingletonInstance = null;
    }

    private String getResponseBody(Response response) throws HttpConnectionException {
        ResponseBody body = response.body();
        try {
            return body.string();
        } catch (IOException e) {
            throw new HttpConnectionException(
                    ERROR_CODE_ACCOUNT_DATA_TRANSPORT_ERROR,
                    null);
        } finally {
            if (body != null) {
                body.close();
            }
        }
    }

    private OkHttpClient createOkHttpClient(Context context) {
        List<Interceptor> interceptors = new ArrayList<>();
        interceptors.add(TelemetryLogInterceptor.create(context, TelemetryLog.getInstance(), 0));
        long cacheSize = context.getResources().getInteger(R.integer.phoenix_okhttp_cache_size);
        Cache cache = new Cache(context.getCacheDir(), cacheSize);
        return YOkHttp.create(interceptors).newBuilder().cache(cache).build();
    }

    Response executeRequest(Context context, Request request) throws HttpConnectionException {
        if (!isNetworkAvailable(context)) {
            if (isAirplaneModeOn(context)) {
                throw new HttpConnectionException(ERROR_CODE_NETWORK_UNAVAILABLE_AIRPLANE_MODE, context.getString(R.string.phoenix_login_airplane_mode));
            } else {
                throw new HttpConnectionException(ERROR_CODE_ACCOUNT_NETWORK_UNAVAILABLE, context.getString(R.string.phoenix_no_internet_connection));
            }
        }

        Response response;
        try {
            response = mOkHttpClient.newCall(request).execute();
        } catch (SSLHandshakeException e) {
            throw new HttpConnectionException(
                    ERROR_CODE_ACCOUNT_NETWORK_SSL_VERIFICATION_ERROR_DUE_TO_DATE_TIME,
                    context.getString(R.string.phoenix_error_check_date_time));
        } catch (SSLPeerUnverifiedException e) {
            throw new HttpConnectionException(
                    ERROR_CODE_ACCOUNT_NETWORK_SSL_VERIFICATION_ERROR,
                    context.getString(R.string.phoenix_network_authentication_required));
        } catch (SocketTimeoutException | SocketException ste) {
            throw new HttpConnectionException(ERROR_CODE_NETWORK_TIME_OUT, context
                    .getString(R.string.phoenix_no_internet_connection));
        } catch (IOException e) {
            throw new HttpConnectionException(
                    ERROR_CODE_ACCOUNT_DATA_TRANSPORT_ERROR,
                    context.getString(R.string.phoenix_login_transport_error));
        }

        if (!response.isSuccessful()) {
            int code = response.code();
            switch (code) {
                case HttpURLConnection.HTTP_CLIENT_TIMEOUT:
                case HttpURLConnection.HTTP_GATEWAY_TIMEOUT:
                    throw new HttpConnectionException(code, context
                            .getString(R.string.phoenix_no_internet_connection));
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    break; // return response to caller to handler the message
                default:
                    throw new HttpConnectionException(
                            ERROR_CODE_ACCOUNT_DATA_TRANSPORT_ERROR,
                            context.getString(R.string.phoenix_login_transport_error));
            }
        }
        return response;
    }

    void executeFormPostAsync(Context context, String url, Map<String, String> headers, String formUrlEncodedData, FormPostAsyncCallback callback) {
        EventLogger eventLogger = EventLogger.getInstance();
        if (!validateUrl(url)) {
            eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_CLIENT_ERROR,
                    EventLogger.RefreshTokenInternalError.AUTH_CONFIG_ERROR,
                    "Invalid url supplied " + url);
            callback.onFailure(OnRefreshTokenResponse.GENERAL_ERROR, null);
            return;
        }

        if (!isNetworkAvailable(context)) {
            eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_CLIENT_ERROR,
                    EventLogger.RefreshTokenInternalError.NETWORK_ISSUE,
                    "No network");
            callback.onFailure(OnRefreshTokenResponse.NETWORK_ERROR, null);
            return;
        }

        Headers.Builder builder = new Headers.Builder();
        if (!Util.isEmpty(headers)) {
            for (Map.Entry<String, String> header : headers.entrySet()) {
                builder.add(header.getKey(), header.getValue());
            }
        }

        Request request = new Request.Builder()
                .url(url)
                .headers(builder.build())
                .post(RequestBody.create(MediaType.parse(MEDIA_TYPE_X_WWW_FORM_URLENCODED), formUrlEncodedData))
                .build();

        mOkHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if (isNetworkAvailable(context)) {
                    if (e != null) {
                        eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_CLIENT_ERROR,
                                EventLogger.RefreshTokenInternalError.OKHTTP_ON_ERROR_WITH_NETWORK_CONNECTION,
                                "Cannot connect to server, even with network connected " + e.getMessage());
                    } else {
                        eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_CLIENT_ERROR,
                                EventLogger.RefreshTokenInternalError.OKHTTP_ON_ERROR_WITH_NETWORK_CONNECTION,
                                "Cannot connect to server, even with network connected");
                    }
                } else {
                    eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_CLIENT_ERROR,
                            EventLogger.RefreshTokenInternalError.NETWORK_ISSUE,
                            "No network");
                }
                callback.onFailure(OnRefreshTokenResponse.NETWORK_ERROR, null);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                int responseCode = response.code();
                ResponseBody responseBody = response.body();
                if (responseBody == null) {
                    eventLogger.logErrorInformationEvent(EVENT_REFRESH_TOKEN_SERVER_ERROR,
                            EventLogger.RefreshTokenServerError.SERVER_EMPTY_RESPONSE,
                            "We received empty response body");
                    callback.onFailure(OnRefreshTokenResponse.GENERAL_ERROR, null);
                    return;
                }
                String responseString = responseBody.string();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    /**
                     * Reminder, if you change this behavior of only calling
                     * {@link FormPostAsyncCallback#onSuccess(String)} when it is a HTTP_OK code,
                     * change the error handling in all instances where this method is called!
                     */
                    callback.onSuccess(responseString);
                } else {
                    /**
                     * We have encountered a non 200 code, give it back to the caller for them
                     * to determine what to best do with the result, this error is logged in the
                     * AuthHelper and not here
                     */
                    callback.onFailure(IDP_SPECIFIC_ERROR, new HttpConnectionException(responseCode, "Non 200 response from server", responseString));
                }
            }
        });
    }

    String executeGet(Context context, String url, Headers headers) throws HttpConnectionException {
        if (!validateUrl(url)) {
            throw new HttpConnectionException(ERROR_CODE_HTTP_VALIDATION_ERROR, "Input url is invalid.", null);
        }

        headers = headers.newBuilder().add(CONTENT_TYPE, CONTENT_TYPE_X_WWW_FORM_URLENCODED).build();

        Request request = new Request.Builder()
                .url(url)
                .headers(headers)
                .build();

        Response response = executeRequest(context, request);
        return getResponseBody(response);
    }

    String executeJSONPost(Context context, String url, @Nullable Map<String, String> headers, String data) throws HttpConnectionException {
        if (!validateUrl(url)) {
            throw new HttpConnectionException(ERROR_CODE_HTTP_VALIDATION_ERROR, "Input url is invalid.", null);
        }

        Headers.Builder builder = new Headers.Builder();
        if (!Util.isEmpty(headers)) {
            for (Map.Entry<String, String> header : headers.entrySet()) {
                builder.add(header.getKey(), header.getValue());
            }
        }

        Request request = new Request.Builder()
                .url(url)
                .headers(builder.build())
                .post(RequestBody.create(MediaType.parse(MEDIA_TYPE_JSON), data))
                .build();

        Response response = executeRequest(context, request);
        String contentType = response.header(ELEM_HEADER_CONTENT_TYPE).toLowerCase();

        if (Util.isEmpty(contentType) || (contentType.indexOf(CONTENT_TYPE_JSON) != 0)) {
            throw new HttpConnectionException(
                    ERROR_CODE_ACCOUNT_DATA_TRANSPORT_ERROR,
                    context.getString(R.string.phoenix_login_transport_error));
        }
        return getResponseBody(response);
    }

    String executeFormPost(Context context, String url, @Nullable Map<String, String> headers, @NonNull Map<String, String> requestBodyQueryParams) throws HttpConnectionException {
        if (!validateUrl(url)) {
            throw new HttpConnectionException(ERROR_CODE_HTTP_VALIDATION_ERROR, "Input url is invalid.", null);
        }

        Headers.Builder builder = new Headers.Builder();
        if (!Util.isEmpty(headers)) {
            for (Map.Entry<String, String> header : headers.entrySet()) {
                builder.add(header.getKey(), header.getValue());
            }
        }

        String requestBody = getFormUrlEncodedRequestBody(requestBodyQueryParams);
        if (requestBody == null) {
            throw new HttpConnectionException(
                    ERROR_CODE_ACCOUNT_DATA_TRANSPORT_ERROR,
                    context.getString(R.string.phoenix_login_transport_error));
        }

        Request request = new Request.Builder()
                .url(url)
                .headers(builder.build())
                .post(RequestBody.create(MediaType.parse(MEDIA_TYPE_X_WWW_FORM_URLENCODED), requestBody))
                .build();

        Response response = executeRequest(context, request);
        return getResponseBody(response);
    }

    public OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }

    @VisibleForTesting
    void setOkHttpClient(OkHttpClient httpClient) {
        mOkHttpClient = httpClient;
    }

    public interface FormPostAsyncCallback {
        int IDP_SPECIFIC_ERROR = -40;
        void onSuccess(String response);

        void onFailure(int code, HttpConnectionException httpConnectionException);
    }

    /**
     * Encode the request parameters into a single payload to be sent as part of a post request
     * This is in the type of "application/x-www-form-urlencoded"
     *
     * @param requestParams the necessary items to build the refresh token
     * @return the x-www-form-urlencoded string to be sent
     */
    @NonNull
    static String getFormUrlEncodedRequestBody(@NonNull Map<String, String> requestParams) {
        android.net.Uri.Builder encodingUri = new android.net.Uri.Builder();
        Iterator iterator = requestParams.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry param = (Map.Entry) iterator.next();
            encodingUri.appendQueryParameter((String) param.getKey(), (String) param.getValue());
        }

        return encodingUri.build().getEncodedQuery();
    }
}
