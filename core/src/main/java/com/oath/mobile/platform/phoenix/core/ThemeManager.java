package com.oath.mobile.platform.phoenix.core;

/**
 * Use this class to set the theme of phoenix sdk managed screens.
 *
 * Created by billhuang on 10/20/17.
 */

public class ThemeManager {
    private static int sThemeResId = 0;
    
    //Disable init
    private ThemeManager () {
    }
    /**
     * Get the stored theme id
     * @return the stored theme id
     */
    public static int getThemeResId() {
        return sThemeResId;
    }

    /**
     * Set the theme used by phoenix sdk
     * @param pThemeResId the resource ID associated with the theme you wish to use with phoenix SDK, see the README for more details.
     */
    public static void setThemeResId(int pThemeResId) {
        sThemeResId = pThemeResId;
    }
}
