package com.oath.mobile.testutils;

import org.junit.runners.model.InitializationError;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.manifest.AndroidManifest;
import org.robolectric.res.Fs;
import org.robolectric.res.ResourcePath;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nsoni on 11/14/17.
 */

public class TestRoboTestRunnerValidRes extends RobolectricTestRunner {

    /**
     * Creates a runner to run {@code testClass}. Looks in your working directory for your AndroidManifest.xml file
     * and res directory by default. Use the {@link Config} annotation to configure.
     *
     * @param testClass the test class to be run
     * @throws InitializationError if junit says so
     */
    public TestRoboTestRunnerValidRes(Class<?> testClass) throws InitializationError {
        super(testClass);
    }

    @Override
    protected AndroidManifest getAppManifest(Config config) {
        final AndroidManifest newAM = super.getAppManifest(config);
        AndroidManifest newAM2 = new AndroidManifest(newAM.getAndroidManifestFile(), newAM.getResDirectory(), newAM.getAssetsDirectory()){
            @Override
            public List<ResourcePath> getIncludedResourcePaths() {
                List<ResourcePath> lrp = new ArrayList<>();
                lrp.add(
                        new ResourcePath(
                                newAM.getRClass(),
                                Fs.fileFromPath("src/test/res-valid"),
                                null,
                                null
                        )
                );
                lrp.addAll(newAM.getIncludedResourcePaths());
                return lrp;
            }
        };
        return newAM2;
    }
}
