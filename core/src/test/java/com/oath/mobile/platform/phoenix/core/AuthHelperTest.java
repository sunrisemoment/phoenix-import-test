package com.oath.mobile.platform.phoenix.core;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.customtabs.CustomTabsServiceConnection;
import android.widget.Button;

import com.oath.mobile.testutils.TestRoboTestRunnerValidRes;

import junit.framework.Assert;

import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationRequest;
import net.openid.appauth.AuthorizationResponse;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.TokenRequest;
import net.openid.appauth.TokenResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowDialog;

import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.List;

import okhttp3.Headers;

import static com.oath.mobile.platform.phoenix.core.AuthConfig.AOL_AUTH_CONFIG_KEY;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.AOL_COM;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.AOL_IDP_ISSUER;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.AOL_TOKEN_PATH;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.OATH_AUTH_CONFIG_KEY;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.OATH_IDP_ISSUER;
import static com.oath.mobile.platform.phoenix.core.AuthHelper.RefreshTokenResponseListener.ERROR_CODE_IDP_SWITCHED;
import static com.oath.mobile.platform.phoenix.core.AuthHelper.USER_IDP_AOL;
import static com.oath.mobile.platform.phoenix.core.AuthHelper.USER_IDP_KEY;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenInternalError.EVENT_REFRESH_TOKEN_CLIENT_ERROR;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenServerError.EVENT_REFRESH_TOKEN_SERVER_ERROR;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_APP_REDIRECT_URI;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_APP_SCHEME;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_AUTH_CODE;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_INVALID_ERROR;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_INVALID_OAUTH_ERROR;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_INVALID_RESPONSE_BODY_EMPTY_ACCESS_TOKEN;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_STATE;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_INTERNAL_SERVER_ERROR_RESPONSE_BODY;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_OAUTH_IDP_SWITCH_ERROR;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_OAUTH_INVALID_CLIENT_ERROR;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_OAUTH_INVALID_GRANT_ERROR;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_OAUTH_INVALID_REQUEST_ERROR;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_OAUTH_INVALID_SCOPE_ERROR;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_OAUTH_UNAUTHORIZED_CLIENT_ERROR;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_OAUTH_UNSUPPORTED_GRANT_TYPE_ERROR;
import static com.oath.mobile.platform.phoenix.core.TestValues.getTestAuthCodeExchangeResponseNonce;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.contains;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by nsoni on 10/19/17.
 */
@RunWith(TestRoboTestRunnerValidRes.class)
public class AuthHelperTest extends BaseTest {
    final String KEY_AUTHORIZATION_CODE = "code";
    final String KEY_STATE = "state";

    public static final String GET_USER_INFO_VALID_RESPONSE = "{\n" +
            "\t\"response\": {\n" +
            "\t\t\"data\": {\n" +
            "\t\t\t\"userData\": {\n" +
            "\t\t\t\t\"loginId\": \"try.myemail\",\n" +
            "\t\t\t\t\"md5Verified\": false,\n" +
            "\t\t\t\t\"lastAuth\": \"1512363832000\",\n" +
            "\t\t\t\t\"displayName\": \"Frank Underwood\",\n" +
            "\t\t\t\t\"OPENAUTH_LOGIN_TYPE\": \"OAUTH\",\n" +
            "\t\t\t\t\"attributes\": {\n" +
            "\t\t\t\t\t\"guid\": \"test@example.com\",\n" +
            "\t\t\t\t\t\"given_name\": \"Frank\",\n" +
            "\t\t\t\t\t\"family_name\": \"Underwood\",\n" +
            "\t\t\t\t\t\"email\": \"try.myemail@aol.com\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t}\n" +
            "\t\t},\n" +
            "\t\t\"statusText\": \"OK\",\n" +
            "\t\t\"statusCode\": 200\n" +
            "\t}\n" +
            "}";

    final String GET_USER_INFO_NON_200_RESPONSE = "{\n" +
            "\t\"response\": {\n" +
            "\t\t\"data\": {\n" +
            "\t\t},\n" +
            "\t\t\"statusText\": \"Bad Request\",\n" +
            "\t\t\"statusCode\": 400\n" +
            "\t}\n" +
            "}";

    private Uri mCodeUri;

    private Intent mCodeIntent;

    private AuthConfig mAuthConfig;
    private Intent mAuthConfigIntent;
    private Context mContext;

    @Before
    public void setUp() {
        mContext = RuntimeEnvironment.application.getApplicationContext();
        mCodeUri = new Uri.Builder()
                .scheme(TEST_APP_SCHEME)
                .appendQueryParameter(KEY_STATE, TEST_STATE)
                .appendQueryParameter(KEY_AUTHORIZATION_CODE, TEST_AUTH_CODE)
                .build();
        mCodeIntent = new Intent();
        mCodeIntent.setData(mCodeUri);

        mAuthConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                null,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        mAuthConfigIntent = new Intent();
        mAuthConfigIntent.putExtra(AuthConfig.OATH_AUTH_CONFIG_KEY, mAuthConfig);
    }

    @Test
    public void createAuthIntent_WhenHasDefaultValidAuthConfigAndNoAccountOnDevice_ShouldCreateTheIntent() throws Exception {
        mAuthConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        Activity activity = spy(Robolectric.setupActivity(Activity.class));
        activity.setTheme(R.style.Theme_Phoenix_Default);
        activity.recreate();
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());

        AuthHelper authService = new AuthHelper(activity);
        authService.initAuthService(activity);
        Intent actualIntent = authService.createAuthIntent(activity);
        assertEquals("Should be present", TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(), actualIntent.getData().getAuthority());
        assertEquals("Should be present", TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(), actualIntent.getData().getPath());
        assertEquals("Should be present", TestValues.TEST_APP_REDIRECT_URI.toString(), actualIntent.getData().getQueryParameter("redirect_uri"));
        assertEquals("Should be present", TestValues.TEST_CLIENT_ID, actualIntent.getData().getQueryParameter("client_id"));
        assertNull("Should be null", actualIntent.getData().getQueryParameter("client_secret"));
        assertEquals("Should be present", AuthHelper.QUERY_PARAM_VALUE_PROMPT_SELECT_ACCOUNT, actualIntent.getData().getQueryParameter("prompt"));
        assertEquals("Should be present", "code", actualIntent.getData().getQueryParameter("response_type"));
        assertFalse("Should be present", actualIntent.getData().getQueryParameters("state").isEmpty());
        assertEquals("Should be present", "openid email sdpp-w", actualIntent.getData().getQueryParameter("scope"));

        assertEquals(BuildConfig.APPLICATION_ID, actualIntent.getData().getQueryParameter(BaseUri.QUERY_PARAM_KEY_APP_ID));
        assertEquals(BuildConfig.VERSION_NAME, actualIntent.getData().getQueryParameter(BaseUri.QUERY_PARAM_KEY_APP_VERSION));
        assertEquals(BaseUri.QUERY_PARAM_VAL_ASDK_EMBEDDED_1, actualIntent.getData().getQueryParameter(BaseUri.QUERY_PARAM_KEY_ASDK_EMBEDDED));
        assertEquals("us", actualIntent.getData().getQueryParameter(BaseUri.QUERY_PARAM_KEY_INTL));
        assertEquals("en-US", actualIntent.getData().getQueryParameter(BaseUri.QUERY_PARAM_KEY_LANG));
        assertEquals(BaseUri.QUERY_PARAM_VAL_SRC_SDK_ANDROID_PHNX, actualIntent.getData().getQueryParameter(BaseUri.QUERY_PARAM_KEY_SRC_SDK));
        assertEquals(BuildConfig.VERSION_NAME, actualIntent.getData().getQueryParameter(BaseUri.QUERY_PARAM_KEY_SRC_SDK_VERSION));
        assertEquals(Intent.FLAG_ACTIVITY_NO_HISTORY, actualIntent.getFlags());
    }

    @Test
    public void createAuthIntent_WhenHasDefaultValidAuthConfigAndHasLoggedInAccount_ShouldCreateTheIntent() throws Exception {
        mAuthConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        Activity activity = spy(Robolectric.setupActivity(Activity.class));
        activity.setTheme(R.style.Theme_Phoenix_Default);
        activity.recreate();
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());

        AuthHelper authService = new AuthHelper(activity);
        authService.initAuthService(activity);
        Intent actualIntent = authService.createAuthIntent(activity);
        assertEquals("Should be present", TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(), actualIntent.getData().getAuthority());
        assertEquals("Should be present", TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(), actualIntent.getData().getPath());
        assertEquals("Should be present", TestValues.TEST_APP_REDIRECT_URI.toString(), actualIntent.getData().getQueryParameter("redirect_uri"));
        assertEquals("Should be present", TestValues.TEST_CLIENT_ID, actualIntent.getData().getQueryParameter("client_id"));
        assertNull("Should be null", actualIntent.getData().getQueryParameter("client_secret"));
        assertEquals("Should be present", AuthHelper.QUERY_PARAM_VALUE_PROMPT_LOGIN, actualIntent.getData().getQueryParameter("prompt"));
        assertEquals("Should be present", "code", actualIntent.getData().getQueryParameter("response_type"));
        assertFalse("Should be present", actualIntent.getData().getQueryParameters("state").isEmpty());
        assertEquals("Should be present", "openid email sdpp-w", actualIntent.getData().getQueryParameter("scope"));
    }

    @Test
    public void createAuthIntent_WhenHasDefaultValidAuthConfigAndHasLoggedOutAccount_ShouldCreateTheIntent() throws Exception {
        mAuthConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        assertFalse(account.isLoggedIn());
        Activity activity = spy(Robolectric.setupActivity(Activity.class));
        activity.setTheme(R.style.Theme_Phoenix_Default);
        activity.recreate();
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());

        AuthHelper authService = new AuthHelper(activity);
        authService.initAuthService(activity);
        Intent actualIntent = authService.createAuthIntent(activity);
        assertEquals("Should be present", TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(), actualIntent.getData().getAuthority());
        assertEquals("Should be present", TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(), actualIntent.getData().getPath());
        assertEquals("Should be present", TestValues.TEST_APP_REDIRECT_URI.toString(), actualIntent.getData().getQueryParameter("redirect_uri"));
        assertEquals("Should be present", TestValues.TEST_CLIENT_ID, actualIntent.getData().getQueryParameter("client_id"));
        assertNull("Should be null", actualIntent.getData().getQueryParameter("client_secret"));
        assertEquals("Should be present", AuthHelper.QUERY_PARAM_VALUE_PROMPT_LOGIN, actualIntent.getData().getQueryParameter("prompt"));
        assertEquals("Should be present", "code", actualIntent.getData().getQueryParameter("response_type"));
        assertFalse("Should be present", actualIntent.getData().getQueryParameters("state").isEmpty());
        assertEquals("Should be present", "openid email sdpp-w", actualIntent.getData().getQueryParameter("scope"));
    }


    @Test
    public void createAuthIntent_WhenPassedValidAuthConfigTwoTimes_ShouldCreateTheIntentWithDifferentPKCE() throws Exception {
        Activity activity = spy(Robolectric.setupActivity(Activity.class));
        activity.setTheme(R.style.Theme_Phoenix_Default);
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());

        AuthHelper authService = new AuthHelper(activity);
        authService.initAuthService(activity);
        Intent actualIntent = authService.createAuthIntent(activity);
        assertFalse("Should be present", actualIntent.getData().getQueryParameters("code_challenge").isEmpty());
        assertFalse("Should be present", actualIntent.getData().getQueryParameters("code_challenge_method").isEmpty());

        Activity activityTwo = spy(Robolectric.setupActivity(Activity.class));
        activityTwo.setTheme(R.style.Theme_Phoenix_Default);
        doReturn(true).when(((Context) activityTwo)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());

        AuthHelper authServiceTwo = new AuthHelper(activityTwo);
        authServiceTwo.initAuthService(activity);
        Intent actualIntentTwo = authServiceTwo.createAuthIntent(activityTwo);
        assertFalse("Should be present", actualIntentTwo.getData().getQueryParameters("code_challenge").isEmpty());
        assertFalse("Should be present", actualIntentTwo.getData().getQueryParameters("code_challenge_method").isEmpty());

        assertTrue("Code Challenge should not be the same", !actualIntentTwo.getData().getQueryParameters("code_challenge").equals(actualIntent.getData().getQueryParameters("code_challenge")));
        assertTrue("Code Challenge Method should be the same", actualIntentTwo.getData().getQueryParameters("code_challenge_method").equals(actualIntent.getData().getQueryParameters("code_challenge_method")));
    }


    @Test
    public void disposeCustomTabService_WhenCalled_ShouldCallDisposeOnAuthService() throws Exception {
        Activity activity = spy(Robolectric.setupActivity(Activity.class));
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        AuthHelper authService = new AuthHelper(activity);
        authService.mAuthorizationService = spy(new AuthorizationService(activity));
        doNothing().when(authService.mAuthorizationService).dispose();

        authService.disposeCustomTabService();

        verify(authService.mAuthorizationService).dispose();

    }

    @Test
    public void handleAuthResponse_WhenValidAuthResponseForNonAOL_ShouldCallPerformTokenRequest() throws Exception {
        Activity activity = spy(Robolectric.setupActivity(Activity.class));
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        AuthHelper authHelper = spy(new AuthHelper(activity));
        authHelper.mAuthorizationService = spy(new AuthorizationService(activity));
        doNothing().when(authHelper.mAuthorizationService).performTokenRequest(any(TokenRequest.class), any(AuthorizationService.TokenResponseCallback.class));

        AuthorizationResponse response = TestValues.getTestAuthResponse();
        AuthHelper.ResponseListener callback = new AuthHelper.ResponseListener() {
            @Override
            public void onComplete(int resultCode, Intent responseIntent, AuthorizationException authorizationException) {

            }
        };
        authHelper.handleAuthResponse(activity,
                TestValues.TEST_APP_REDIRECT_URI
                        .buildUpon()
                        .appendQueryParameter("code", TestValues.TEST_AUTH_CODE)
                        .appendQueryParameter("state", TestValues.TEST_STATE)
                        .build(), callback);

        ArgumentCaptor<TokenRequest> tokenResponseArgumentCaptor = ArgumentCaptor.forClass(TokenRequest.class);
        ArgumentCaptor<AuthorizationService.TokenResponseCallback> tokenResponseCallbackArgumentCaptor = ArgumentCaptor.forClass(AuthorizationService.TokenResponseCallback.class);

        verify(authHelper.mAuthorizationService).performTokenRequest(tokenResponseArgumentCaptor.capture(), tokenResponseCallbackArgumentCaptor.capture());

        assertEquals(response.authorizationCode, tokenResponseArgumentCaptor.getValue().authorizationCode);
        assertEquals(TestValues.TEST_CLIENT_ID, tokenResponseArgumentCaptor.getValue().clientId);
        assertFalse(TestValues.TEST_CLIENT_SECRET, tokenResponseArgumentCaptor.getValue().additionalParameters.containsKey("client_secret"));
        assertEquals(TestValues.TEST_SCOPE + " " + AuthHelper.QUERY_PARAM_VALUE_SCOPE_OATH, tokenResponseArgumentCaptor.getValue().scope);
    }


    @Test
    public void handleAuthResponse_WhenSecretNotPresentForNonAOLRequest_ShouldCallPerformTokenRequestWithNoClientSecretParam() throws Exception {
        Activity activity = spy(Robolectric.setupActivity(Activity.class));
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        mAuthConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                "",
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));

        AuthHelper authHelper = spy(new AuthHelper(activity));
        authHelper.mAuthorizationService = spy(new AuthorizationService(activity));
        doNothing().when(authHelper.mAuthorizationService).performTokenRequest(any(TokenRequest.class), any(AuthorizationService.TokenResponseCallback.class));

        AuthorizationResponse response = TestValues.getTestAuthResponse();
        AuthHelper.ResponseListener callback = new AuthHelper.ResponseListener() {
            @Override
            public void onComplete(int resultCode, Intent responseIntent, AuthorizationException authorizationException) {

            }
        };
        authHelper.handleAuthResponse(activity,
                TestValues.TEST_APP_REDIRECT_URI
                        .buildUpon()
                        .appendQueryParameter("code", TestValues.TEST_AUTH_CODE)
                        .appendQueryParameter("state", TestValues.TEST_STATE)
                        .build(), callback);

        ArgumentCaptor<TokenRequest> tokenResponseArgumentCaptor = ArgumentCaptor.forClass(TokenRequest.class);
        ArgumentCaptor<AuthorizationService.TokenResponseCallback> tokenResponseCallbackArgumentCaptor = ArgumentCaptor.forClass(AuthorizationService.TokenResponseCallback.class);

        verify(authHelper.mAuthorizationService).performTokenRequest(tokenResponseArgumentCaptor.capture(), tokenResponseCallbackArgumentCaptor.capture());

        assertEquals(response.authorizationCode, tokenResponseArgumentCaptor.getValue().authorizationCode);
        assertEquals(TestValues.TEST_CLIENT_ID, tokenResponseArgumentCaptor.getValue().clientId);
        assertFalse(tokenResponseArgumentCaptor.getValue().getRequestParameters().containsKey("client_secret"));
        assertEquals(TestValues.TEST_SCOPE + " " + AuthHelper.QUERY_PARAM_VALUE_SCOPE_OATH, tokenResponseArgumentCaptor.getValue().scope);
    }

    @Test
    public void handleAuthResponse_WhenValidAuthResponseForAOL_ShouldCallPerformTokenRequest() throws Exception {
        Activity activity = spy(Robolectric.setupActivity(Activity.class));
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        mAuthConfig = new AuthConfig(TestValues.TEST_AOL_IDP_TOKEN_ENDPOINT.getAuthority(),
                TestValues.TEST_AOL_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_AOL_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        mAuthConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        AuthHelper authHelper = spy(new AuthHelper(activity));
        authHelper.mAuthorizationService = spy(new AuthorizationService(activity));
        doNothing().when(authHelper.mAuthorizationService).performTokenRequest(any(TokenRequest.class), any(AuthorizationService.TokenResponseCallback.class));

        mockAolRequest(authHelper);

        AuthorizationResponse response = TestValues.getTestAuthResponse();
        AuthHelper.ResponseListener callback = new AuthHelper.ResponseListener() {
            @Override
            public void onComplete(int resultCode, Intent responseIntent, AuthorizationException authorizationException) {

            }
        };
        authHelper.handleAuthResponse(activity,
                TestValues.TEST_APP_REDIRECT_URI
                        .buildUpon()
                        .appendQueryParameter("code", TestValues.TEST_AUTH_CODE)
                        .appendQueryParameter("state", TestValues.TEST_STATE)
                        .build(), callback);

        ArgumentCaptor<TokenRequest> tokenResponseArgumentCaptor = ArgumentCaptor.forClass(TokenRequest.class);
        ArgumentCaptor<AuthorizationService.TokenResponseCallback> tokenResponseCallbackArgumentCaptor = ArgumentCaptor.forClass(AuthorizationService.TokenResponseCallback.class);

        verify(authHelper.mAuthorizationService).performTokenRequest(tokenResponseArgumentCaptor.capture(), tokenResponseCallbackArgumentCaptor.capture());

        assertEquals(response.authorizationCode, tokenResponseArgumentCaptor.getValue().authorizationCode);
        assertEquals(TestValues.TEST_CLIENT_ID, tokenResponseArgumentCaptor.getValue().clientId);
        assertEquals(TestValues.TEST_CLIENT_SECRET, tokenResponseArgumentCaptor.getValue().getRequestParameters().get("client_secret"));
        assertEquals(AuthHelper.QUERY_PARAM_VALUE_SCOPE_AOL_TOKEN_EXCHANGE, tokenResponseArgumentCaptor.getValue().scope);
    }


    @Test
    public void handleAuthResponse_WhenValidAuthResponseForOathWithAolIssuer_ShouldCallPerformTokenRequestWithAolTokenEndPoint() throws Exception {
        Activity activity = spy(Robolectric.setupActivity(Activity.class));
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        AuthHelper authHelper = spy(new AuthHelper(activity));
        authHelper.mAuthorizationService = spy(new AuthorizationService(activity));
        doNothing().when(authHelper.mAuthorizationService).performTokenRequest(any(TokenRequest.class), any(AuthorizationService.TokenResponseCallback.class));

        AuthorizationResponse response = TestValues.getTestAuthResponse();
        AuthHelper.ResponseListener callback = new AuthHelper.ResponseListener() {
            @Override
            public void onComplete(int resultCode, Intent responseIntent, AuthorizationException authorizationException) {

            }
        };
        authHelper.handleAuthResponse(activity,
                TestValues.TEST_APP_REDIRECT_URI
                        .buildUpon()
                        .appendQueryParameter("code", TestValues.TEST_AUTH_CODE)
                        .appendQueryParameter("state", TestValues.TEST_STATE)
                        .appendQueryParameter(USER_IDP_KEY, USER_IDP_AOL)
                        .build(), callback);

        ArgumentCaptor<TokenRequest> tokenResponseArgumentCaptor = ArgumentCaptor.forClass(TokenRequest.class);
        ArgumentCaptor<AuthorizationService.TokenResponseCallback> tokenResponseCallbackArgumentCaptor = ArgumentCaptor.forClass(AuthorizationService.TokenResponseCallback.class);

        verify(authHelper.mAuthorizationService).performTokenRequest(tokenResponseArgumentCaptor.capture(), tokenResponseCallbackArgumentCaptor.capture());

        assertEquals(response.authorizationCode, tokenResponseArgumentCaptor.getValue().authorizationCode);
        Uri aolTokenUri = new Uri.Builder()
                .scheme(AuthConfig.SCHEME_HTTPS)
                .authority(AOL_COM)
                .path(AOL_TOKEN_PATH)
                .build();
        assertEquals(aolTokenUri, tokenResponseArgumentCaptor.getValue().configuration.tokenEndpoint);
        assertEquals(TestValues.TEST_CLIENT_ID, tokenResponseArgumentCaptor.getValue().clientId);
        assertEquals(TestValues.TEST_CLIENT_SECRET, tokenResponseArgumentCaptor.getValue().getRequestParameters().get("client_secret"));
        assertEquals(AuthHelper.QUERY_PARAM_VALUE_SCOPE_AOL_TOKEN_EXCHANGE, tokenResponseArgumentCaptor.getValue().scope);
    }

    private void mockAolRequest(AuthHelper authHelper) throws NoSuchFieldException, IllegalAccessException {
        AuthorizationRequest request = spy(TestValues.getTestAuthRequest());
        doReturn(new Uri.Builder().encodedAuthority("http://api.screenname.aol.com").build()).when(request).toUri();
        Field field = AuthHelper.class.getDeclaredField("mAuthRequest");
        field.setAccessible(true);
        field.set(authHelper, request);
    }

    @Test(expected = IllegalStateException.class)
    public void exchangeToken_WhenNoAuthorizationCode_ShouldThrowException() throws Exception {
        Activity activity = Robolectric.setupActivity(Activity.class);
        mAuthConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                "",
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        AuthHelper authHelper = new AuthHelper(activity);
        AuthorizationResponse response = mock(AuthorizationResponse.class);
        authHelper.exchangeCodeForTokens(activity,
                response, mock(AuthHelper.ResponseListener.class));
    }

    @Test
    public void handleAuthResponse_WhenIntentHasAuthCodeData_ShouldCallPerformTokenRequestToGetValidResponse() throws Exception {
        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);
        activity.mAuthHelper = spy(new AuthHelper(activity));
        activity.mAuthHelper.mAuthorizationService = spy(new AuthorizationService(activity));

        doAnswer(invocation -> {
            AuthorizationService.TokenResponseCallback callback = (AuthorizationService.TokenResponseCallback) invocation.getArguments()[1];
            callback.onTokenRequestCompleted(TestValues.getTestAuthCodeExchangeResponse(), null);
            return null;
        }).when(activity.mAuthHelper.mAuthorizationService).performTokenRequest(any(TokenRequest.class), any(AuthorizationService.TokenResponseCallback.class));

        AuthManager authManager = (AuthManager) AuthManager.getInstance(activity);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();

        AuthHelper.ResponseListener callback = new AuthHelper.ResponseListener() {
            @Override
            public void onComplete(int resultCode, Intent responseIntent, AuthorizationException authorizationException) {

            }
        };
        activity.mAuthHelper.handleAuthResponse(activity,
                TestValues.TEST_APP_REDIRECT_URI
                        .buildUpon()
                        .appendQueryParameter("code", TestValues.TEST_AUTH_CODE)
                        .appendQueryParameter("state", TestValues.TEST_STATE)
                        .build(), callback);

        ArgumentCaptor<TokenRequest> tokenRequestArgumentCaptor = ArgumentCaptor.forClass(TokenRequest.class);
        verify(activity.mAuthHelper.mAuthorizationService).performTokenRequest(tokenRequestArgumentCaptor.capture(), any(AuthorizationService.TokenResponseCallback.class));

        Uri tokenEndpoint = tokenRequestArgumentCaptor.getValue().configuration.tokenEndpoint;
        assertEquals(BuildConfig.APPLICATION_ID, tokenEndpoint.getQueryParameter(BaseUri.QUERY_PARAM_KEY_APP_ID));
        assertEquals(BuildConfig.VERSION_NAME, tokenEndpoint.getQueryParameter(BaseUri.QUERY_PARAM_KEY_APP_VERSION));
        assertEquals(BaseUri.QUERY_PARAM_VAL_ASDK_EMBEDDED_1, tokenEndpoint.getQueryParameter(BaseUri.QUERY_PARAM_KEY_ASDK_EMBEDDED));
        assertEquals("us", tokenEndpoint.getQueryParameter(BaseUri.QUERY_PARAM_KEY_INTL));
        assertEquals("en-US", tokenEndpoint.getQueryParameter(BaseUri.QUERY_PARAM_KEY_LANG));
        assertEquals(BaseUri.QUERY_PARAM_VAL_SRC_SDK_ANDROID_PHNX, tokenEndpoint.getQueryParameter(BaseUri.QUERY_PARAM_KEY_SRC_SDK));
        assertEquals(BuildConfig.VERSION_NAME, tokenEndpoint.getQueryParameter(BaseUri.QUERY_PARAM_KEY_SRC_SDK_VERSION));

        List<IAccount> accountList = AuthManager.getInstance(activity).getAllAccounts();
        assertEquals(1, accountList.size());
        assertEquals(TestValues.TEST_ACCESS_TOKEN, accountList.get(0).getToken());
    }

    @Test
    public void handleAuthResponse_WhenNullUri_ShouldCallResponseListenerWithErrorResponse() throws Exception {
        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);
        activity.mAuthHelper = spy(new AuthHelper(activity));
        activity.mAuthHelper.mAuthorizationService = spy(new AuthorizationService(activity));
        doNothing().when(activity.mAuthHelper.mAuthorizationService).performTokenRequest(any(TokenRequest.class), any(AuthorizationService.TokenResponseCallback.class));

        AuthorizationResponse response = TestValues.getTestAuthResponse();
        AuthHelper.ResponseListener callback = new AuthHelper.ResponseListener() {
            @Override
            public void onComplete(int resultCode, Intent responseIntent, AuthorizationException authorizationException) {

            }
        };
        activity.onNewIntent(new Intent());


        verify(activity.mAuthHelper.mAuthorizationService, never()).performTokenRequest(any(TokenRequest.class), any(AuthorizationService.TokenResponseCallback.class));

        ShadowActivity shadowActivity = shadowOf(activity);
        Assert.assertNotSame(IAuthManager.RESULT_CODE_ERROR, shadowActivity.getResultCode());
        //dismiss error dialog
        Dialog latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        Button confirmButton = latestDialog.findViewById(R.id.account_custom_dialog_button);
        confirmButton.performClick();
        //verify result code
        Assert.assertEquals(IAuthManager.RESULT_CODE_ERROR, shadowActivity.getResultCode());
        Intent resultIntent = shadowActivity.getResultIntent();
        Assert.assertNull(resultIntent);
    }

    @Test
    public void parseUriIfAvailable_whenNull_ShouldReturnNull() {
        Activity activity = spy(Robolectric.setupActivity(Activity.class));
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());

        AuthHelper authService = new AuthHelper(activity);
        Uri test = authService.parseUriIfAvailable(null);

        assertNull("The uri should be null as we passed null in", test);
    }

    @Test
    public void parseUriIfAvailable_whenNotNull_ShouldReturnNotNull() {
        Activity activity = spy(Robolectric.setupActivity(Activity.class));
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());

        AuthHelper authService = new AuthHelper(activity);
        Uri test = authService.parseUriIfAvailable(TEST_APP_REDIRECT_URI.toString());

        assertNotNull("The uri should not be null as we passed a valid uri in", test);
    }

    @Test
    public void revokeToken_WhenIsCalledWithYahooIdp_ShouldStartAsyncTask() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doReturn("test").when(accountNetworkAPIMock).executeJSONPost(any(Context.class), anyString(), anyMap(), anyString());
        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RevokeTokenResponseListener listener = mock(AuthHelper.RevokeTokenResponseListener.class);
        AuthHelper.revokeToken(RuntimeEnvironment.application, "refresh_token", OATH_IDP_ISSUER, listener);
        Robolectric.flushBackgroundThreadScheduler();
        verify(accountNetworkAPIMock).executeFormPost(any(Context.class), anyString(), anyMap(), anyMap());
        verify(listener).onComplete(anyInt(), anyString());
    }

    @Test
    public void revokeToken_WhenIsCalledWithNonYahooIdp_ShouldNotStartAsyncTask() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_AOL_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_AOL_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_AOL_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(RuntimeEnvironment.application.getApplicationContext(), AOL_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doReturn("test").when(accountNetworkAPIMock).executeJSONPost(any(Context.class), anyString(), anyMap(), anyString());
        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RevokeTokenResponseListener listener = mock(AuthHelper.RevokeTokenResponseListener.class);
        AuthHelper.revokeToken(RuntimeEnvironment.application, "refresh_token", AOL_IDP_ISSUER, listener);
        Robolectric.flushBackgroundThreadScheduler();
        verify(accountNetworkAPIMock, never()).executeFormPost(any(Context.class), anyString(), anyMap(), anyMap());
        verify(listener).onComplete(anyInt(), anyString());
    }

    @Test
    public void refreshAccessToken_WhenAuthConfigIsNull_ShouldNotCallExecutePost() throws Exception {
        AuthConfig authConfig = new AuthConfig(null, null, null, null, null, null, null);
        authConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        doNothing().when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("code is not the same", OnRefreshTokenResponse.GENERAL_ERROR, code);
            }
        });

        // Test
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        // Verify
        verify(accountNetworkAPIMock, never()).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
        verify(eventLogger, times(1)).logErrorInformationEvent(eq(EVENT_REFRESH_TOKEN_CLIENT_ERROR), eq(EventLogger.RefreshTokenInternalError.AUTH_CONFIG_NULL), contains("null"));
    }

    @Test
    public void refreshAccessToken_WhenScopeStringsNull_ShouldCallExecutePost() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), null);
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                asyncCallback.onSuccess(TestValues.getTestAuthCodeExchangeResponse().jsonSerializeString());
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("code is not the same", OnRefreshTokenResponse.GENERAL_ERROR, code);
            }
        });

        // Test
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        // Verify
        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onSuccess(anyString(), anyString());
    }

    @Test
    public void refreshAccessToken_WhenClientIdsEmpty_ShouldNotCallExecutePos() throws Exception {
        Context context = Robolectric.setupActivity(Activity.class);
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                "",
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doNothing().when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("code is not the same", OnRefreshTokenResponse.GENERAL_ERROR, code);
            }
        });

        // Test
        AuthHelper.refreshAccessToken(context, TestValues.TEST_REFRESH_TOKEN, null, responseListener);
        // Verify
        verify(accountNetworkAPIMock, never()).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
        verify(eventLogger, times(1)).logErrorInformationEvent(eq(EVENT_REFRESH_TOKEN_CLIENT_ERROR), eq(EventLogger.RefreshTokenInternalError.AUTH_CONFIG_ERROR), Matchers.contains("clientId"));
    }

    @Test
    public void refreshAccessToken_WhenExecutePostSucceedsWithJsonException_ShouldCallResponseListenerOnCompleteWithFailure() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                asyncCallback.onSuccess("bad json");
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("code is not the same", OnRefreshTokenResponse.GENERAL_ERROR, code);
            }
        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
        verify(eventLogger, times(1)).logErrorInformationEvent(eq(EVENT_REFRESH_TOKEN_SERVER_ERROR), eq(EventLogger.RefreshTokenServerError.RESPONSE_PARSE_FAILURE), Matchers.contains("missing required fields"));
    }

    @Test
    public void refreshAccessToken_WhenExecutePostSucceedsWithEmptyAccessToken_ShouldCallResponseListenerOnCompleteWithFailure() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                asyncCallback.onSuccess(TEST_INVALID_RESPONSE_BODY_EMPTY_ACCESS_TOKEN);
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("code is not the same", OnRefreshTokenResponse.GENERAL_ERROR, code);
            }
        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
        verify(eventLogger, times(1)).logErrorInformationEvent(eq(EVENT_REFRESH_TOKEN_SERVER_ERROR), eq(EventLogger.RefreshTokenServerError.RESPONSE_MISSING_ACCESS_TOKEN), Matchers.contains("empty"));
    }

    @Test
    public void refreshAccessToken_WhenExecutePostFailureWithClientError_ShouldCallResponseListenerOnCompleteWithTokenInvalid() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                HttpConnectionException exception = new HttpConnectionException(HttpURLConnection.HTTP_BAD_REQUEST, "Non 200 error", TEST_VALID_OAUTH_INVALID_GRANT_ERROR);
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, exception);
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("Code is not the same", OnRefreshTokenResponse.REAUTHORIZE_USER, code);
            }

        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onFailure(OnRefreshTokenResponse.REAUTHORIZE_USER);
        verify(eventLogger, times(1)).logErrorInformationEvent(eq(EVENT_REFRESH_TOKEN_SERVER_ERROR), eq(EventLogger.RefreshTokenServerError.INVALID_GRANT), Matchers.contains("Invalid grant"));
    }

    @Test
    public void refreshAccessToken_WhenExecutePostFailureWhenIdpSwitch_ShouldCallResponseListenerOnCompleteWithTokenInvalid() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                HttpConnectionException exception = new HttpConnectionException(HttpURLConnection.HTTP_BAD_REQUEST, "Non 200 error", TEST_VALID_OAUTH_IDP_SWITCH_ERROR);
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, exception);
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("Code is not the same", ERROR_CODE_IDP_SWITCHED, code);
            }

        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onFailure(ERROR_CODE_IDP_SWITCHED);
        verify(eventLogger, times(1)).logErrorInformationEvent(eq(EVENT_REFRESH_TOKEN_SERVER_ERROR), eq(EventLogger.RefreshTokenServerError.IDP_SWITCHED), Matchers.contains("refresh token is invalid because user idp is switched to oath"));
    }

    @Test
    public void refreshAccessToken_WhenExecutePostFailureWithClientErrorInvalidClient_ShouldCallResponseListenerOnErrorWithInvalidRequest() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                HttpConnectionException exception = new HttpConnectionException(HttpURLConnection.HTTP_BAD_REQUEST, "Non 200 error", TEST_VALID_OAUTH_INVALID_CLIENT_ERROR);
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, exception);
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("Code is not the same", OnRefreshTokenResponse.GENERAL_ERROR, code);
            }

        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
        verify(eventLogger, times(1)).logErrorInformationEvent(eq(EVENT_REFRESH_TOKEN_CLIENT_ERROR), eq(EventLogger.RefreshTokenInternalError.INVALID_CLIENT), Matchers.contains("Invalid client"));
    }

    @Test
    public void refreshAccessToken_WhenExecutePostFailureWithClientErrorInvalidRequest_ShouldCallResponseListenerOnErrorWithInvalidRequest() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                HttpConnectionException exception = new HttpConnectionException(HttpURLConnection.HTTP_BAD_REQUEST, "Non 200 error", TEST_VALID_OAUTH_INVALID_REQUEST_ERROR);
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, exception);
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("Code is not the same", OnRefreshTokenResponse.INVALID_REQUEST, code);
            }

        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onFailure(OnRefreshTokenResponse.INVALID_REQUEST);
        verify(eventLogger, times(1)).logErrorInformationEvent(eq(EVENT_REFRESH_TOKEN_SERVER_ERROR), eq(EventLogger.RefreshTokenServerError.INVALID_REQUEST), Matchers.contains("Invalid request"));
    }

    @Test
    public void refreshAccessToken_WhenExecutePostFailureWithClientErrorInvalidClient_ShouldCallResponseListenerOnErrorWithGeneralError() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                HttpConnectionException exception = new HttpConnectionException(HttpURLConnection.HTTP_BAD_REQUEST, "Non 200 error", TEST_VALID_OAUTH_INVALID_CLIENT_ERROR);
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, exception);
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("Code is not the same", OnRefreshTokenResponse.GENERAL_ERROR, code);
            }

        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
        verify(eventLogger, times(1)).logErrorInformationEvent(eq(EVENT_REFRESH_TOKEN_CLIENT_ERROR), eq(EventLogger.RefreshTokenInternalError.INVALID_CLIENT), Matchers.contains("Invalid client"));
    }

    @Test
    public void refreshAccessToken_WhenExecutePostFailureWithClientErrorUnauthorizedClient_ShouldCallResponseListenerOnErrorWithUnauthorizedClient() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                HttpConnectionException exception = new HttpConnectionException(HttpURLConnection.HTTP_BAD_REQUEST, "Non 200 error", TEST_VALID_OAUTH_UNAUTHORIZED_CLIENT_ERROR);
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, exception);
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("Code is not the same", OnRefreshTokenResponse.UNAUTHORIZED_CLIENT, code);
            }

        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onFailure(OnRefreshTokenResponse.UNAUTHORIZED_CLIENT);
        verify(eventLogger, times(1)).logErrorInformationEvent(eq(EVENT_REFRESH_TOKEN_SERVER_ERROR), eq(EventLogger.RefreshTokenServerError.UNAUTHORIZED_CLIENT), Matchers.contains("Unauthorized client"));
    }

    @Test
    public void refreshAccessToken_WhenExecutePostFailureWithClientErrorUnsupportedGrantType_ShouldCallResponseListenerOnErrorWithGeneralError() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                HttpConnectionException exception = new HttpConnectionException(HttpURLConnection.HTTP_BAD_REQUEST, "Non 200 error", TEST_VALID_OAUTH_UNSUPPORTED_GRANT_TYPE_ERROR);
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, exception);
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("Code is not the same", OnRefreshTokenResponse.GENERAL_ERROR, code);
            }

        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
        verify(eventLogger, times(1)).logErrorInformationEvent(eq(EVENT_REFRESH_TOKEN_CLIENT_ERROR), eq(EventLogger.RefreshTokenInternalError.UNSUPPORTED_GRANT_TYPE), Matchers.contains("Unsupported grant type"));
    }

    @Test
    public void refreshAccessToken_WhenExecutePostFailureWithClientErrorInvalidScope_ShouldCallResponseListenerOnErrorWithInvalidScope() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                HttpConnectionException exception = new HttpConnectionException(HttpURLConnection.HTTP_BAD_REQUEST, "Non 200 error", TEST_VALID_OAUTH_INVALID_SCOPE_ERROR);
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, exception);
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("Code is not the same", OnRefreshTokenResponse.INVALID_SCOPE, code);
            }

        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onFailure(OnRefreshTokenResponse.INVALID_SCOPE);
        verify(eventLogger, times(1)).logErrorInformationEvent(eq(EVENT_REFRESH_TOKEN_SERVER_ERROR), eq(EventLogger.RefreshTokenServerError.INVALID_SCOPE), Matchers.contains("Invalid scope"));
    }

    @Test
    public void refreshAccessToken_WhenExecutePostFailureWithClientErrorRandomError_ShouldCallResponseListenerOnErrorWithGeneralError() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                HttpConnectionException exception = new HttpConnectionException(HttpURLConnection.HTTP_BAD_REQUEST, "Non 200 error", TEST_INVALID_OAUTH_ERROR);
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, exception);
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("Code is not the same", OnRefreshTokenResponse.GENERAL_ERROR, code);
            }

        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
        verify(eventLogger, times(1)).logErrorInformationEvent(eq(EVENT_REFRESH_TOKEN_SERVER_ERROR), eq(EventLogger.RefreshTokenServerError.RESPONSE_UNRECOGNIZED_ERROR_REASON), Matchers.contains("Unrecognized"));
    }


    @Test
    public void refreshAccessToken_WhenExecutePostFailureWithClientErrorBadJson_ShouldCallResponseListenerOnErrorWithGeneralError() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                HttpConnectionException exception = new HttpConnectionException(HttpURLConnection.HTTP_BAD_REQUEST, "Non 200 error", TEST_INVALID_ERROR);
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, exception);
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("Code is not the same", OnRefreshTokenResponse.GENERAL_ERROR, code);
            }

        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
        verify(eventLogger, times(1)).logErrorInformationEvent(eq(EVENT_REFRESH_TOKEN_SERVER_ERROR), eq(EventLogger.RefreshTokenServerError.RESPONSE_MISSING_ERROR_FIELD), Matchers.contains("No error field"));
    }

    @Test
    public void refreshAccessToken_WhenExecutePostFailureWithServerError_ShouldCallResponseListenerOnErrorWithServerError() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                HttpConnectionException exception = new HttpConnectionException(HttpURLConnection.HTTP_INTERNAL_ERROR, "Non 200 error", TEST_VALID_INTERNAL_SERVER_ERROR_RESPONSE_BODY);
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, exception);
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("Code is not the same", OnRefreshTokenResponse.SERVER_ERROR, code);
            }

        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onFailure(OnRefreshTokenResponse.SERVER_ERROR);
        verify(eventLogger, times(1)).logErrorInformationEvent(eq(EVENT_REFRESH_TOKEN_SERVER_ERROR), eq(EventLogger.RefreshTokenServerError.RETRY_LATER), Matchers.contains("Http 5xx code"));
    }

    @Test
    public void refreshAccessToken_WhenExecutePostFailureWithNoClientError_ShouldCallResponseListenerOnCompleteWithFailure() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, new HttpConnectionException(1234, "random bad error"));
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("code is not the same", OnRefreshTokenResponse.GENERAL_ERROR, code);
            }
        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
        verify(eventLogger, times(1)).logErrorInformationEvent(eq(EVENT_REFRESH_TOKEN_SERVER_ERROR), eq(EventLogger.RefreshTokenServerError.UNRECOGNIZED_STATUS_CODE_AND_ERROR), Matchers.contains("Unrecognized http status code"));
    }

    @Test
    public void refreshAccessToken_WhenExecutePostFailureIDPErrorButNoException_ShouldCallGeneralError() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, null);
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);

        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {

            }

            @Override
            public void onFailure(int code) {
                assertEquals("code is not the same", OnRefreshTokenResponse.GENERAL_ERROR, code);
            }
        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(responseListener).onFailure(OnRefreshTokenResponse.GENERAL_ERROR);
        verify(eventLogger, times(1)).logErrorInformationEvent(eq(EVENT_REFRESH_TOKEN_CLIENT_ERROR), eq(EventLogger.RefreshTokenInternalError.MISSING_EXCEPTION_INFORMATION), Matchers.contains("no exception information"));
    }

    @Test
    public void refreshAccessToken_WhenExecutePostSucceeds_ShouldCallResponseListenerOnComplete() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                asyncCallback.onSuccess(TestValues.getTestAuthCodeExchangeResponse().jsonSerializeString());
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);
        ArgumentCaptor<String> formDataArgumentCaptor = ArgumentCaptor.forClass(String.class);
        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {
                assertEquals("AccessToken is not correct", TestValues.TEST_ACCESS_TOKEN, accessToken);
                assertEquals("RefreshToken is not correct", TestValues.TEST_REFRESH_TOKEN, refreshToken);
            }

            @Override
            public void onFailure(int code) {

            }
        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);
        ArgumentCaptor<String> tokenUrl = ArgumentCaptor.forClass(String.class);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), tokenUrl.capture(), anyMap(), formDataArgumentCaptor.capture(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        assertFalse(formDataArgumentCaptor.getValue().contains("client_secret=" + TestValues.TEST_CLIENT_SECRET));
        Uri uriRefreshToken = Uri.parse(tokenUrl.getValue());

        assertEquals(BuildConfig.APPLICATION_ID, uriRefreshToken.getQueryParameter(BaseUri.QUERY_PARAM_KEY_APP_ID));
        assertEquals(BuildConfig.VERSION_NAME, uriRefreshToken.getQueryParameter(BaseUri.QUERY_PARAM_KEY_APP_VERSION));
        assertEquals(BaseUri.QUERY_PARAM_VAL_ASDK_EMBEDDED_1, uriRefreshToken.getQueryParameter(BaseUri.QUERY_PARAM_KEY_ASDK_EMBEDDED));
        assertEquals("us", uriRefreshToken.getQueryParameter(BaseUri.QUERY_PARAM_KEY_INTL));
        assertEquals("en-US", uriRefreshToken.getQueryParameter(BaseUri.QUERY_PARAM_KEY_LANG));
        assertEquals(BaseUri.QUERY_PARAM_VAL_SRC_SDK_ANDROID_PHNX, uriRefreshToken.getQueryParameter(BaseUri.QUERY_PARAM_KEY_SRC_SDK));
        assertEquals(BuildConfig.VERSION_NAME, uriRefreshToken.getQueryParameter(BaseUri.QUERY_PARAM_KEY_SRC_SDK_VERSION));
        verify(responseListener).onSuccess(anyString(), anyString());
    }

    @Test
    public void refreshAccessToken_WhenRefreshForAol_ShouldSentClientSecretAndRefreshTokenIsNull() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_AOL_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_AOL_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_AOL_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                asyncCallback.onSuccess(TestValues.getTestAuthCodeExchangeResponse().jsonSerializeString());
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);
        ArgumentCaptor<String> formDataArgumentCaptor = ArgumentCaptor.forClass(String.class);
        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {
                assertNull(refreshToken);
                assertEquals("AccessToken is not correct", TestValues.TEST_ACCESS_TOKEN, accessToken);
            }

            @Override
            public void onFailure(int code) {

            }
        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, null, responseListener);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), formDataArgumentCaptor.capture(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        assertTrue(formDataArgumentCaptor.getValue().contains("client_secret=" + TestValues.TEST_CLIENT_SECRET));
        verify(responseListener).onSuccess(any(), any());
    }

    @Test
    public void refreshAccessToken_WhenRefreshForOath_ShouldNotSentClientSecretAndRefreshTokenIsNotNull() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                asyncCallback.onSuccess(TestValues.getTestAuthCodeExchangeResponse().jsonSerializeString());
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);
        ArgumentCaptor<String> formDataArgumentCaptor = ArgumentCaptor.forClass(String.class);
        AuthHelper.RefreshTokenResponseListener responseListener = spy(new AuthHelper.RefreshTokenResponseListener() {
            @Override
            public void onSuccess(String accessToken, String refreshToken) {
                assertNotNull(refreshToken);
                assertEquals("AccessToken is not correct", TestValues.TEST_ACCESS_TOKEN, accessToken);
            }

            @Override
            public void onFailure(int code) {

            }
        });
        AuthHelper.refreshAccessToken(mContext, TestValues.TEST_REFRESH_TOKEN, OATH_IDP_ISSUER, responseListener);

        verify(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), formDataArgumentCaptor.capture(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        assertFalse(formDataArgumentCaptor.getValue().contains("client_secret="));
        verify(responseListener).onSuccess(any(), any());
    }


    @Test
    public void isAol_WhenUriIsAol_ShouldReturnTrue() throws Exception {
        Uri uri = new Uri.Builder().encodedAuthority("https://api.screenname.aol.com").build();
        assertTrue("Unexpected value", AuthHelper.isAol(uri));
    }

    @Test
    public void isAol_WhenUriIsNotAol_ShouldReturnFalse() throws Exception {
        Uri uri = new Uri.Builder().encodedAuthority("https://test.sample.com").build();
        assertFalse("Unexpected value", AuthHelper.isAol(uri));
    }

    @Test
    public void isAol_WhenUriIsYahoo_ShouldReturnFalse() throws Exception {
        Uri uri = new Uri.Builder().encodedAuthority("https://test.yahoo.com").build();
        assertTrue("Unexpected value", !AuthHelper.isAol(uri));
    }

    @Test
    public void getUserDataAsync_WhenConfigIsNull_ShouldCallOnFailure() throws Exception {
        OnUserDataResponseListener listener = mock(OnUserDataResponseListener.class);
        doNothing().when(listener).onFailure();
        doNothing().when(listener).onSuccess(any(UserData.class));

        AuthConfig config = new AuthConfig(null, null, null, null, null, null, null);
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AuthHelper.getUserInfoAsync(mContext, TestValues.TEST_ACCESS_TOKEN, null, listener);

        verify(listener).onFailure();
    }

    @Test
    public void getUserDataAsync_WhenUrlIsNotAOL_ShouldCallOnFailure() throws Exception {
        OnUserDataResponseListener listener = mock(OnUserDataResponseListener.class);
        doNothing().when(listener).onFailure();
        doNothing().when(listener).onSuccess(any(UserData.class));
        mAuthConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        AuthHelper.getUserInfoAsync(mContext, TestValues.TEST_ACCESS_TOKEN, null, listener);

        verify(listener).onFailure();
    }

    @Test
    public void getUserDataAsync_WhenValidRequest_ShouldCallOnSuccessWithUserData() throws Exception {
        OnUserDataResponseListener listener = mock(OnUserDataResponseListener.class);
        doNothing().when(listener).onFailure();
        doNothing().when(listener).onSuccess(any(UserData.class));
        AuthConfig authConfig = new AuthConfig(TestValues.TEST_AOL_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_AOL_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_AOL_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        authConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPI = spy(AccountNetworkAPI.getInstance(mContext));

        doReturn(GET_USER_INFO_VALID_RESPONSE).when(accountNetworkAPI).executeGet(any(Context.class), anyString(), any(Headers.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPI);

        AuthHelper.getUserInfoAsync(mContext, TestValues.TEST_ACCESS_TOKEN, null, listener);

        ArgumentCaptor<UserData> userDataArgumentCaptor = ArgumentCaptor.forClass(UserData.class);
        verify(listener).onSuccess(userDataArgumentCaptor.capture());

        UserData userData = userDataArgumentCaptor.getValue();

        Assert.assertEquals("Display name is not correct", "Frank Underwood", userData.mDisplayName);
        Assert.assertEquals("Email is not correct", "try.myemail@aol.com", userData.mEmail);
        Assert.assertEquals("Last name is not correct", "Underwood", userData.mFamilyName);
        Assert.assertEquals("First name is not correct", "Frank", userData.mGivenName);
        Assert.assertEquals("Guid is not correct", "test@example.com", userData.mGuid);

        ArgumentCaptor<String> url = ArgumentCaptor.forClass(String.class);
        verify(accountNetworkAPI).executeGet(any(Context.class), url.capture(), any(Headers.class));

        Uri uriUserData = Uri.parse(url.getValue());
        assertEquals(BuildConfig.APPLICATION_ID, uriUserData.getQueryParameter(BaseUri.QUERY_PARAM_KEY_APP_ID));
        assertEquals(BuildConfig.VERSION_NAME, uriUserData.getQueryParameter(BaseUri.QUERY_PARAM_KEY_APP_VERSION));
        assertEquals(BaseUri.QUERY_PARAM_VAL_ASDK_EMBEDDED_1, uriUserData.getQueryParameter(BaseUri.QUERY_PARAM_KEY_ASDK_EMBEDDED));
        assertEquals("us", uriUserData.getQueryParameter(BaseUri.QUERY_PARAM_KEY_INTL));
        assertEquals("en-US", uriUserData.getQueryParameter(BaseUri.QUERY_PARAM_KEY_LANG));
        assertEquals(BaseUri.QUERY_PARAM_VAL_SRC_SDK_ANDROID_PHNX, uriUserData.getQueryParameter(BaseUri.QUERY_PARAM_KEY_SRC_SDK));
        assertEquals(BuildConfig.VERSION_NAME, uriUserData.getQueryParameter(BaseUri.QUERY_PARAM_KEY_SRC_SDK_VERSION));
    }

    @Test
    public void getUserDataAsync_WhenResponseIsNot200_ShouldCallOnFailure() throws Exception {
        OnUserDataResponseListener listener = mock(OnUserDataResponseListener.class);
        doNothing().when(listener).onFailure();
        doNothing().when(listener).onSuccess(any(UserData.class));
        AuthConfig authConfig = new AuthConfig(TestValues.TEST_AOL_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_AOL_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_AOL_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        authConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPI = spy(AccountNetworkAPI.getInstance(mContext));

        doReturn(GET_USER_INFO_NON_200_RESPONSE).when(accountNetworkAPI).executeGet(any(Context.class), anyString(), any(Headers.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPI);

        AuthHelper.getUserInfoAsync(mContext, TestValues.TEST_ACCESS_TOKEN, null, listener);

        verify(listener).onFailure();
    }


    @Test
    public void getUserDataAsync_WhenResponseNotJson_ShouldCallOnFailure() throws Exception {
        OnUserDataResponseListener listener = mock(OnUserDataResponseListener.class);
        doNothing().when(listener).onFailure();
        doNothing().when(listener).onSuccess(any(UserData.class));
        AuthConfig authConfig = new AuthConfig(TestValues.TEST_AOL_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_AOL_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_AOL_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        authConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPI = spy(AccountNetworkAPI.getInstance(mContext));

        doReturn("some non json response").when(accountNetworkAPI).executeGet(any(Context.class), anyString(), any(Headers.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPI);

        AuthHelper.getUserInfoAsync(mContext, TestValues.TEST_ACCESS_TOKEN, null, listener);

        verify(listener).onFailure();
    }

    @Test
    public void getUserDataAsync_WhenExecuteGetThrowsException_ShouldCallOnFailure() throws Exception {
        OnUserDataResponseListener listener = mock(OnUserDataResponseListener.class);
        doNothing().when(listener).onFailure();
        doNothing().when(listener).onSuccess(any(UserData.class));
        AuthConfig authConfig = new AuthConfig(TestValues.TEST_AOL_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_AOL_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_AOL_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        authConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPI = spy(AccountNetworkAPI.getInstance(mContext));

        doThrow(new HttpConnectionException(500, "server error")).when(accountNetworkAPI).executeGet(any(Context.class), anyString(), any(Headers.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPI);

        AuthHelper.getUserInfoAsync(mContext, TestValues.TEST_ACCESS_TOKEN, null, listener);

        verify(listener).onFailure();
    }

    @Test
    public void getUserDataAsync_WhenIssuerIsOath_ShouldNotSendRequest() throws Exception {
        AccountNetworkAPI accountNetworkAPI = spy(AccountNetworkAPI.getInstance(mContext));
        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPI);
        OnUserDataResponseListener listener = mock(OnUserDataResponseListener.class);
        AuthHelper.getUserInfoAsync(mContext, TestValues.TEST_ACCESS_TOKEN, OATH_IDP_ISSUER, listener);

        verify(accountNetworkAPI, never()).executeGet(eq(mContext), anyString(), any(Headers.class));
    }
}
