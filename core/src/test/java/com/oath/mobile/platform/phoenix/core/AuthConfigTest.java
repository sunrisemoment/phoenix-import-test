package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Parcel;

import com.oath.mobile.testutils.TestRoboTestRunnerValidRes;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RuntimeEnvironment;

import java.util.Arrays;
import java.util.Collections;

import static com.oath.mobile.platform.phoenix.core.AuthConfig.AOL_IDP_ISSUER;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.OATH_AUTH_CONFIG_KEY;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.OATH_IDP_ISSUER;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;

/**
 * Created by nsoni on 10/23/17.
 */
@RunWith(TestRoboTestRunnerValidRes.class)
public class AuthConfigTest extends BaseTest {

    private AuthConfig mConfig;
    private Context mContext;

    @Before
    public void setUp() throws Exception {
        mContext = RuntimeEnvironment.application.getApplicationContext();
        mConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
    }

    @Test
    public void getConfigAttributes_WhenValidParams_ShouldReturnCorrectValues() throws Exception {
        Assert.assertEquals(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(), mConfig.getIdpAuthority());
        Assert.assertEquals(TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(), mConfig.getAuthPath());
        Assert.assertEquals(TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(), mConfig.getTokenPath());
        Assert.assertEquals(TestValues.TEST_CLIENT_ID, mConfig.getClientID());
        Assert.assertEquals(TestValues.TEST_CLIENT_SECRET, mConfig.getClientSecret());
        Assert.assertEquals(TestValues.TEST_APP_REDIRECT_URI, mConfig.getRedirectUri());
        Assert.assertEquals(Arrays.asList(TestValues.TEST_SCOPE.split(" ")), mConfig.getScopes());
        Uri authUri = mConfig.getAuthUri(mContext);
        Assert.assertEquals(TestValues.TEST_IDP_AUTH_ENDPOINT.toString(), authUri.getScheme() + "://" + authUri.getAuthority() + authUri.getPath());
        Uri tokenUri = mConfig.getTokenUri(mContext);
        Assert.assertEquals(TestValues.TEST_IDP_TOKEN_ENDPOINT.toString(), tokenUri.getScheme() + "://" + tokenUri.getAuthority() + tokenUri.getPath());

    }

    @Test
    public void describeContents_WhenCalled_ShouldReturn0() throws Exception {
        Assert.assertEquals(0, mConfig.describeContents());
    }

    @Test
    public void writeToParcel_WhenUsed_ShouldWriteToAParcelObj() throws Exception {
        Parcel parcel = Parcel.obtain();
        // Obtain a Parcel object and write the parcelable object to it
        mConfig.writeToParcel(parcel, 0);

        // After you're done with writing, you need to reset the parcel for reading
        parcel.setDataPosition(0);

        // Reconstruct object from parcel and asserts:
        AuthConfig createdFromParcel = AuthConfig.CREATOR.createFromParcel(parcel);

        // Test
        Assert.assertEquals(mConfig.getIdpAuthority(), createdFromParcel.getIdpAuthority());
        Assert.assertEquals(mConfig.getAuthPath(), createdFromParcel.getAuthPath());
        Assert.assertEquals(mConfig.getTokenPath(), createdFromParcel.getTokenPath());
        Assert.assertEquals(mConfig.getClientID(), createdFromParcel.getClientID());
        Assert.assertEquals(mConfig.getClientSecret(), createdFromParcel.getClientSecret());
        Assert.assertEquals(mConfig.getRedirectUri(), createdFromParcel.getRedirectUri());
        Assert.assertEquals(mConfig.getScopes(), createdFromParcel.getScopes());
        TestUtils.assertEquals(mConfig.getAuthUri(mContext), createdFromParcel.getAuthUri(mContext));
        TestUtils.assertEquals(mConfig.getTokenUri(mContext), createdFromParcel.getTokenUri(mContext));
    }

    @Test
    public void saveAuthConfig_WhenValidJson_ShouldSaveToSharedPreference() throws Exception {
        mConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        AuthConfig createdFromJson = AuthConfig.getAuthConfigByIssuer(mContext, null);

        // Test
        Assert.assertEquals(mConfig.getIdpAuthority(), createdFromJson.getIdpAuthority());
        Assert.assertEquals(mConfig.getAuthPath(), createdFromJson.getAuthPath());
        Assert.assertEquals(mConfig.getTokenPath(), createdFromJson.getTokenPath());
        Assert.assertEquals(mConfig.getClientID(), createdFromJson.getClientID());
        Assert.assertEquals(mConfig.getClientSecret(), createdFromJson.getClientSecret());
        Assert.assertEquals(mConfig.getRedirectUri(), createdFromJson.getRedirectUri());
        Collections.sort(mConfig.getScopes());
        Collections.sort(createdFromJson.getScopes());
        Assert.assertEquals(mConfig.getScopes(), createdFromJson.getScopes());
        TestUtils.assertEquals(mConfig.getAuthUri(mContext), createdFromJson.getAuthUri(mContext));
        TestUtils.assertEquals(mConfig.getTokenUri(mContext), createdFromJson.getTokenUri(mContext));
    }

    @Test
    public void getSavedConfig_WhenNotJson_ShouldReturnNull() throws Exception {
        AuthConfig authConfig = new AuthConfig(null, null, null, null, null, null, null);
        authConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        AuthConfig createdFromJson = AuthConfig.getAuthConfigByIssuer(mContext, null);
        Assert.assertNull(createdFromJson);
    }

    @Test
    public void getSavedAuthConfig_WhenToJsonThrowsException_ShouldTakeDefaultConfigFromResourceStrings() throws Exception {
        AuthConfig authConfig = spy(new AuthConfig("123", null, null, null, null, null, null));
        doThrow(JSONException.class).when(authConfig).toJson();
        authConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        AuthConfig createdFromJson = AuthConfig.getAuthConfigByIssuer(mContext, null);
        Assert.assertNotNull(createdFromJson);
        Assert.assertEquals(Uri.parse(OATH_IDP_ISSUER).getAuthority(), createdFromJson.getIdpAuthority());
        Assert.assertEquals(AuthConfig.OATH_AUTH_PATH, createdFromJson.getAuthPath());
        Assert.assertEquals(AuthConfig.OATH_TOKEN_PATH, createdFromJson.getTokenPath());
        Assert.assertEquals(TestValues.TEST_CLIENT_ID, createdFromJson.getClientID());
        Assert.assertEquals(null, createdFromJson.getClientSecret());
        Assert.assertEquals(TestValues.TEST_APP_REDIRECT_URI, createdFromJson.getRedirectUri());
        Assert.assertEquals(Arrays.asList(TestValues.TEST_SCOPE.split(" ")), createdFromJson.getScopes());
        Uri authUri = Uri.parse(OATH_IDP_ISSUER).buildUpon().path(AuthConfig.OATH_AUTH_PATH).build();
        Uri tokenUri = Uri.parse(OATH_IDP_ISSUER).buildUpon().path(AuthConfig.OATH_TOKEN_PATH).build();
        Assert.assertEquals(authUri.toString(), authUri.getScheme() + "://" + authUri.getAuthority() + authUri.getPath());
        Assert.assertEquals(tokenUri.toString(), tokenUri.getScheme() + "://" + tokenUri.getAuthority() + tokenUri.getPath());

    }

    @Test
    public void getAuthConfigByIssuer_WhenIssuerIsOathAndHasConfigSavedForOath_ShouldReturnOathConfig() throws Exception {
        mConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AuthConfig createdFromJson = AuthConfig.getAuthConfigByIssuer(mContext, OATH_IDP_ISSUER);
        Assert.assertNotNull(createdFromJson);
        Assert.assertEquals(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(), createdFromJson.getIdpAuthority());
        Assert.assertEquals(TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(), createdFromJson.getAuthPath());
        Assert.assertEquals(TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(), createdFromJson.getTokenPath());
        Assert.assertEquals(TestValues.TEST_CLIENT_ID, createdFromJson.getClientID());
        Assert.assertEquals(TestValues.TEST_CLIENT_SECRET, createdFromJson.getClientSecret());
        Assert.assertEquals(TestValues.TEST_APP_REDIRECT_URI, createdFromJson.getRedirectUri());
        Assert.assertEquals(Arrays.asList(TestValues.TEST_SCOPE.split(" ")), createdFromJson.getScopes());
        Uri authUri = createdFromJson.getAuthUri(mContext);
        Assert.assertEquals(TestValues.TEST_IDP_AUTH_ENDPOINT.toString(), authUri.getScheme() + "://" + authUri.getAuthority() + authUri.getPath());
        Uri tokenUri = createdFromJson.getTokenUri(mContext);
        Assert.assertEquals(TestValues.TEST_IDP_TOKEN_ENDPOINT.toString(), tokenUri.getScheme() + "://" + tokenUri.getAuthority() + tokenUri.getPath());
    }

    @Test
    public void getAuthConfigByIssuer_WhenIssuerIsOathAndNoConfigSavedForOath_ShouldReturnOathDefaultConfig() throws Exception {
        AuthConfig createdFromJson = AuthConfig.getAuthConfigByIssuer(mContext, OATH_IDP_ISSUER);
        Assert.assertNotNull(createdFromJson);
        Assert.assertEquals(Uri.parse(OATH_IDP_ISSUER).getAuthority(), createdFromJson.getIdpAuthority());
        Assert.assertEquals(AuthConfig.OATH_AUTH_PATH, createdFromJson.getAuthPath());
        Assert.assertEquals(AuthConfig.OATH_TOKEN_PATH, createdFromJson.getTokenPath());
        Assert.assertEquals(TestValues.TEST_CLIENT_ID, createdFromJson.getClientID());
        Assert.assertEquals(null, createdFromJson.getClientSecret());
        Assert.assertEquals(TestValues.TEST_APP_REDIRECT_URI, createdFromJson.getRedirectUri());
        Assert.assertEquals(Arrays.asList(TestValues.TEST_SCOPE.split(" ")), createdFromJson.getScopes());
        Uri authUri = Uri.parse(OATH_IDP_ISSUER).buildUpon().path(AuthConfig.OATH_AUTH_PATH).build();
        Uri tokenUri = Uri.parse(OATH_IDP_ISSUER).buildUpon().path(AuthConfig.OATH_TOKEN_PATH).build();
        TestUtils.assertEquals(new BaseUri(authUri.buildUpon()).Builder(mContext).build(), createdFromJson.getAuthUri(mContext));
        TestUtils.assertEquals(new BaseUri(tokenUri.buildUpon()).Builder(mContext).build(), createdFromJson.getTokenUri(mContext));
    }

    @Test
    public void getAuthConfigByIssuer_WhenIssuerIsAol_ShouldReturnAolConfig() throws Exception {
        AuthConfig createdFromJson = AuthConfig.getAuthConfigByIssuer(mContext, AOL_IDP_ISSUER);
        Assert.assertNotNull(createdFromJson);
        Assert.assertEquals(TestValues.TEST_AOL_IDP_AUTH_ENDPOINT.getAuthority(), createdFromJson.getIdpAuthority());
        Assert.assertEquals(TestValues.TEST_AOL_IDP_AUTH_ENDPOINT.getPath(), createdFromJson.getAuthPath());
        Assert.assertEquals(TestValues.TEST_AOL_IDP_TOKEN_ENDPOINT.getPath(), createdFromJson.getTokenPath());
        Assert.assertEquals(TestValues.TEST_CLIENT_ID, createdFromJson.getClientID());
        Assert.assertEquals(TestValues.TEST_CLIENT_SECRET, createdFromJson.getClientSecret());
        Assert.assertEquals(TestValues.TEST_APP_REDIRECT_URI, createdFromJson.getRedirectUri());
        Assert.assertEquals(Arrays.asList(TestValues.TEST_SCOPE.split(" ")), createdFromJson.getScopes());
        Uri authUri = createdFromJson.getAuthUri(mContext);
        Assert.assertEquals(TestValues.TEST_AOL_IDP_AUTH_ENDPOINT.toString(), authUri.getScheme() + "://" + authUri.getAuthority() + authUri.getPath());
        Uri tokenUri = createdFromJson.getTokenUri(mContext);
        Assert.assertEquals(TestValues.TEST_AOL_IDP_TOKEN_ENDPOINT.toString(), tokenUri.getScheme() + "://" + tokenUri.getAuthority() + tokenUri.getPath());
    }

    @Test
    public void getAuthConfigByIssuer_WhenIssuerIsNull_ShouldReturnSavedPrimaryConfig() throws Exception {
        mConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        AuthConfig createdFromJson = AuthConfig.getAuthConfigByIssuer(mContext, null);
        Assert.assertNotNull(createdFromJson);
        Assert.assertEquals(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(), createdFromJson.getIdpAuthority());
        Assert.assertEquals(TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(), createdFromJson.getAuthPath());
        Assert.assertEquals(TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(), createdFromJson.getTokenPath());
        Assert.assertEquals(TestValues.TEST_CLIENT_ID, createdFromJson.getClientID());
        Assert.assertEquals(TestValues.TEST_CLIENT_SECRET, createdFromJson.getClientSecret());
        Assert.assertEquals(TestValues.TEST_APP_REDIRECT_URI, createdFromJson.getRedirectUri());
        Assert.assertEquals(Arrays.asList(TestValues.TEST_SCOPE.split(" ")), createdFromJson.getScopes());
        Uri authUri = createdFromJson.getAuthUri(mContext);
        Assert.assertEquals(TestValues.TEST_IDP_AUTH_ENDPOINT.toString(), authUri.getScheme() + "://" + authUri.getAuthority() + authUri.getPath());
        Uri tokenUri = createdFromJson.getTokenUri(mContext);
        Assert.assertEquals(TestValues.TEST_IDP_TOKEN_ENDPOINT.toString(), tokenUri.getScheme() + "://" + tokenUri.getAuthority() + tokenUri.getPath());
    }

    @Test
    public void getAuthConfigByIssuer_WhenIssuerIsEmpty_ShouldReturnSavedPrimaryConfig() throws Exception {
        mConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        AuthConfig createdFromJson = AuthConfig.getAuthConfigByIssuer(mContext, "");
        Assert.assertNotNull(createdFromJson);
        Assert.assertEquals(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(), createdFromJson.getIdpAuthority());
        Assert.assertEquals(TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(), createdFromJson.getAuthPath());
        Assert.assertEquals(TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(), createdFromJson.getTokenPath());
        Assert.assertEquals(TestValues.TEST_CLIENT_ID, createdFromJson.getClientID());
        Assert.assertEquals(TestValues.TEST_CLIENT_SECRET, createdFromJson.getClientSecret());
        Assert.assertEquals(TestValues.TEST_APP_REDIRECT_URI, createdFromJson.getRedirectUri());
        Assert.assertEquals(Arrays.asList(TestValues.TEST_SCOPE.split(" ")), createdFromJson.getScopes());
        Uri authUri = createdFromJson.getAuthUri(mContext);
        Assert.assertEquals(TestValues.TEST_IDP_AUTH_ENDPOINT.toString(), authUri.getScheme() + "://" + authUri.getAuthority() + authUri.getPath());
        Uri tokenUri = createdFromJson.getTokenUri(mContext);
        Assert.assertEquals(TestValues.TEST_IDP_TOKEN_ENDPOINT.toString(), tokenUri.getScheme() + "://" + tokenUri.getAuthority() + tokenUri.getPath());
    }

    @Test
    public void getAuthConfigByIssuer_WhenIssuerIsOathAndHasExceptionWhenParseSavedOathConfig_ShouldReturnDefaultOathConfig() throws Exception {
        mConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);// saved primary config
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(OATH_AUTH_CONFIG_KEY, "{test}");
        editor.apply(); // saved oath config

        AuthConfig createdFromJson = AuthConfig.getAuthConfigByIssuer(mContext, OATH_IDP_ISSUER);
        Assert.assertNull(createdFromJson);
    }

    @Test
    public void newArray_WhenIsCalled_ShouldReturnArray() throws Exception {
        AuthConfig[] authConfigs = AuthConfig.CREATOR.newArray(2);
        assertEquals("Unexpected size of array", 2, authConfigs.length);
    }

    @Test
    public void initOathOpenIdConfiguration_WhenCalled_ShouldRetrieveOathConfigFromDiscoveryAndSave() throws Exception {
        AuthConfig.initOathOpenIdConfiguration(mContext);
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(OATH_AUTH_CONFIG_KEY, null);
        AuthConfig expectedConfig = new AuthConfig("api.login.aol.com",
                AuthConfig.OATH_AUTH_PATH,
                AuthConfig.OATH_TOKEN_PATH,
                TestValues.TEST_CLIENT_ID,
                null,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        assertEquals("Unexpected config", expectedConfig.toJson(), json);
    }
}
