package com.oath.mobile.platform.phoenix.core;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import static com.oath.mobile.platform.phoenix.core.EventLogger.TRACKING_KEY_ERROR_CODE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.TRACKING_KEY_ERROR_MESSAGE;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_ORIGIN;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by billhuang on 12/7/17.
 */

@RunWith(RobolectricTestRunner.class)
public class EventLoggerTest extends BaseTest {
    @Test
    public void constructor_WhenInvoked_ShouldThrowException() throws Exception {
        Class<?> eventLogger = Class.forName("com.oath.mobile.platform.phoenix.core.EventLogger");
        try {
            eventLogger.newInstance();
        } catch (IllegalAccessException e) {
            assertEquals("class java.lang.IllegalAccessException", e.getClass().toString());
        }
        Constructor<?> constructor = eventLogger.getDeclaredConstructor();
        constructor.setAccessible(true); // set it public
        constructor.newInstance(); // invoke it for coverage
    }

    @Test
    public void constructorRefreshTokenInternalErrorCode_WhenInvoked_ShouldThrowException() throws Exception {
        Class<?> errorCodeClass = Class.forName("com.oath.mobile.platform.phoenix.core.EventLogger$RefreshTokenInternalError");
        try {
            errorCodeClass.newInstance();
        } catch (IllegalAccessException e) {
            assertEquals("class java.lang.IllegalAccessException", e.getClass().toString());
        }
        Constructor<?> constructor = errorCodeClass.getDeclaredConstructor();
        constructor.setAccessible(true); // set it public
        constructor.newInstance(); // invoke it for coverage
    }

    @Test
    public void constructorRefreshTokenServerErrorCode_WhenInvoked_ShouldThrowException() throws Exception {
        Class<?> errorCodeClass = Class.forName("com.oath.mobile.platform.phoenix.core.EventLogger$RefreshTokenServerError");
        try {
            errorCodeClass.newInstance();
        } catch (IllegalAccessException e) {
            assertEquals("class java.lang.IllegalAccessException", e.getClass().toString());
        }
        Constructor<?> constructor = errorCodeClass.getDeclaredConstructor();
        constructor.setAccessible(true); // set it public
        constructor.newInstance(); // invoke it for coverage
    }

    @Test
    public void constructorRefreshTokenUserEvents_WhenInvoked_ShouldThrowException() throws Exception {
        Class<?> errorCodeClass = Class.forName("com.oath.mobile.platform.phoenix.core.EventLogger$RefreshTokenUserEvents");
        try {
            errorCodeClass.newInstance();
        } catch (IllegalAccessException e) {
            assertEquals("class java.lang.IllegalAccessException", e.getClass().toString());
        }
        Constructor<?> constructor = errorCodeClass.getDeclaredConstructor();
        constructor.setAccessible(true); // set it public
        constructor.newInstance(); // invoke it for coverage
    }

    @Test
    public void constructorSignInUserEvents_WhenInvoked_ShouldThrowException() throws Exception {
        Class<?> errorCodeClass = Class.forName("com.oath.mobile.platform.phoenix.core.EventLogger$SignInUserEvents");
        try {
            errorCodeClass.newInstance();
        } catch (IllegalAccessException e) {
            assertEquals("class java.lang.IllegalAccessException", e.getClass().toString());
        }
        Constructor<?> constructor = errorCodeClass.getDeclaredConstructor();
        constructor.setAccessible(true); // set it public
        constructor.newInstance(); // invoke it for coverage
    }

    @Test
    public void constructorManageAccountUserEvents_WhenInvoked_ShouldThrowException() throws Exception {
        Class<?> errorCodeClass = Class.forName("com.oath.mobile.platform.phoenix.core.EventLogger$ManageAccountsUserEvents");
        try {
            errorCodeClass.newInstance();
        } catch (IllegalAccessException e) {
            assertEquals("class java.lang.IllegalAccessException", e.getClass().toString());
        }
        Constructor<?> constructor = errorCodeClass.getDeclaredConstructor();
        constructor.setAccessible(true); // set it public
        constructor.newInstance(); // invoke it for coverage
    }

    @Test
    public void constructorAccountPickerUserEvents_WhenInvoked_ShouldThrowException() throws Exception {
        Class<?> errorCodeClass = Class.forName("com.oath.mobile.platform.phoenix.core.EventLogger$AccountPickerUserEvents");
        try {
            errorCodeClass.newInstance();
        } catch (IllegalAccessException e) {
            assertEquals("class java.lang.IllegalAccessException", e.getClass().toString());
        }
        Constructor<?> constructor = errorCodeClass.getDeclaredConstructor();
        constructor.setAccessible(true); // set it public
        constructor.newInstance(); // invoke it for coverage
    }

    @Test
    public void constructorBootStrapUserEvents_WhenInvoked_ShouldThrowException() throws Exception {
        Class<?> errorCodeClass = Class.forName("com.oath.mobile.platform.phoenix.core.EventLogger$BootstrapUserEvents");
        try {
            errorCodeClass.newInstance();
        } catch (IllegalAccessException e) {
            assertEquals("class java.lang.IllegalAccessException", e.getClass().toString());
        }
        Constructor<?> constructor = errorCodeClass.getDeclaredConstructor();
        constructor.setAccessible(true); // set it public
        constructor.newInstance(); // invoke it for coverage
    }

    @Test
    public void constructorBootStrapError_WhenInvoked_ShouldThrowException() throws Exception {
        Class<?> errorCodeClass = Class.forName("com.oath.mobile.platform.phoenix.core.EventLogger$BootstrapError");
        try {
            errorCodeClass.newInstance();
        } catch (IllegalAccessException e) {
            assertEquals("class java.lang.IllegalAccessException", e.getClass().toString());
        }
        Constructor<?> constructor = errorCodeClass.getDeclaredConstructor();
        constructor.setAccessible(true); // set it public
        constructor.newInstance(); // invoke it for coverage
    }

    @Test
    public void getCustomParamsWithOriginInformation_whenNullMap_ShouldReturnNewMap() {
        Map<String, Object> result = EventLogger.getCustomParamsWithOriginInformation(null, TEST_ORIGIN);
        assertNotNull("The param result is null", result);
        assertEquals("The Origin information is wrong", TEST_ORIGIN, result.get(EventLogger.TRACKING_KEY_EVENT_ORIGIN));
    }

    @Test
    public void getCustomParamsWithOriginInformation_whenNonNullMap_ShouldReturnSameMapWithNewInformation() {
        HashMap<String, Object> testMap = new HashMap<>();
        String testKey = "Random";
        String testVal = "Random";
        testMap.put(testKey, testVal);
        Map<String, Object> result = EventLogger.getCustomParamsWithOriginInformation(testMap, TEST_ORIGIN);
        assertNotNull("The param result is null", result);
        assertEquals("The Origin information is wrong", TEST_ORIGIN, result.get(EventLogger.TRACKING_KEY_EVENT_ORIGIN));
        assertEquals("The Prefilled value is still there", testVal, result.get(testKey));
    }

    @Test
    public void getRefreshTokenErrorEventParams_whenNullMap_ShouldReturnNewMap() {
        Map<String, Object> result = EventLogger.getRefreshTokenErrorEventParams(null, OnRefreshTokenResponse.GENERAL_ERROR);
        assertNotNull("The param is null", result);
        assertEquals("The Error Code is wrong", EventLogger.RefreshTokenUserEvents.GENERAL_ERROR, result.get(TRACKING_KEY_ERROR_CODE));
        assertEquals("The Error Msg is wrong", EventLogger.RefreshTokenUserEvents.GENERAL_ERROR_MSG, result.get(TRACKING_KEY_ERROR_MESSAGE));
    }


    @Test
    public void getRefreshTokenErrorEventParams_whenNonNullMap_ShouldReturnSameMapWithNewInformation() {
        HashMap<String, Object> testMap = new HashMap<>();
        String testKey = "Random";
        String testVal = "Random";
        testMap.put(testKey, testVal);
        Map<String, Object> result = EventLogger.getRefreshTokenErrorEventParams(testMap, OnRefreshTokenResponse.GENERAL_ERROR);
        assertNotNull("The param result is null", result);
        assertEquals("The Error Code is wrong", EventLogger.RefreshTokenUserEvents.GENERAL_ERROR, result.get(TRACKING_KEY_ERROR_CODE));
        assertEquals("The Error Msg is wrong", EventLogger.RefreshTokenUserEvents.GENERAL_ERROR_MSG, result.get(TRACKING_KEY_ERROR_MESSAGE));
        assertEquals("The Prefilled value is still there", testVal, result.get(testKey));
    }

}
