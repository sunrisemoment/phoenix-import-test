package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.content.Intent;

import com.oath.mobile.testutils.TestRoboTestRunnerValidRes;

import net.openid.appauth.TokenResponse;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RuntimeEnvironment;

import java.lang.reflect.Constructor;
import java.util.Arrays;

import static com.oath.mobile.platform.phoenix.core.AuthConfig.OATH_AUTH_CONFIG_KEY;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

/**
 * Created by nsoni on 10/19/17.
 */
@RunWith(TestRoboTestRunnerValidRes.class)
public class AuthTest extends BaseTest {

    private Context mContext;
    private AuthConfig mAuthConfig;

    @Before
    public void setUp() throws Exception {
        mContext = RuntimeEnvironment.application;
        mAuthConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                null,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
    }

    @Test
    public void constructor_WhenInvoked_ShouldThrowException() throws Exception {
        Class<?> authClass = Class.forName("com.oath.mobile.platform.phoenix.core.Auth");
        try {
            authClass.newInstance();
        } catch (IllegalAccessException e) {
            assertEquals("class java.lang.IllegalAccessException", e.getClass().toString());
        }
        Constructor<?> constructor = authClass.getDeclaredConstructor();
        constructor.setAccessible(true); // set it public
        constructor.newInstance(); // invoke it for coverage
    }

    @Test
    public void build_WhenNoDisabledAccountsAndCalled_ShouldCreateTheIntentForAuthActivity() throws Exception {
        mAuthConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                null,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        Intent intent = new Auth.IntentBuilder().build(mContext);

        Assert.assertEquals(BuildConfig.APPLICATION_ID + ".AuthActivity", intent.getComponent().getClassName());
    }

    @Test
    public void build_WhenHasDisabledAccountsAndCalled_ShouldCreateTheIntentForAccountPickerActivity() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = TestValues.getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        Intent intent = new Auth.IntentBuilder().build(mContext);

        Assert.assertEquals(BuildConfig.APPLICATION_ID + ".AccountPickerActivity", intent.getComponent().getClassName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void build_WhenIdpMissing_ShouldThrowException() throws Exception {
        mAuthConfig = new AuthConfig("",
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                null,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        mAuthConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        try {
            Intent intent = new Auth.IntentBuilder()
                    .build(mContext);
        } catch (Exception e) {
            assertEquals("Identity provider is missing", e.getMessage());
            throw e;
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void build_WhenAuthEndpointMissing_ShouldThrowException() throws Exception {
        AuthConfig authConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                "",
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                null,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        authConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        try {
            Intent intent = new Auth.IntentBuilder()
                    .build(mContext);
        } catch (Exception e) {
            assertEquals("Auth path is missing", e.getMessage());
            throw e;
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void build_WhenTokenEndpointMissing_ShouldThrowException() throws Exception {
        mAuthConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                "",
                TestValues.TEST_CLIENT_ID,
                null,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        mAuthConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        try {
            Intent intent = new Auth.IntentBuilder()
                    .build(mContext);

        } catch (Exception e) {
            assertEquals("Token path is missing", e.getMessage());
            throw e;
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void build_WhenClientIdMissing_ShouldThrowException() throws Exception {
        mAuthConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                "",
                null,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        mAuthConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        try {
            Intent intent = new Auth.IntentBuilder()
                    .build(mContext);
        } catch (Exception e) {
            assertEquals("Client Id is missing", e.getMessage());
            throw e;
        }
    }

    @Test
    public void build_WhenClientSecretMissing_ShouldSetItEmptyString() throws Exception {

        mAuthConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                null,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        mAuthConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        Intent intent = new Auth.IntentBuilder()
                .build(mContext);

        Assert.assertEquals(BuildConfig.APPLICATION_ID + ".AuthActivity", intent.getComponent().getClassName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void build_WhenRedirectUriMissing_ShouldThrowException() throws Exception {
        mAuthConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                null,
                "", Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        mAuthConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        try {
            Intent intent = new Auth.IntentBuilder()
                    .build(mContext);
        } catch (Exception e) {
            assertEquals("Redirect Uri is missing", e.getMessage());
            throw e;
        }
    }

    @Test
    public void build_WhenScopesMissing_ShouldNotThrowException() throws Exception {

        Intent intent = new Auth.IntentBuilder()
                .build(mContext);
        assertNotNull(intent);
    }

    @Test
    public void buildSignIn_WhenIsCalled_ShouldCreateTheIntentForAuthActivity() throws Exception {
        mAuthConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                null,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        mAuthConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        Intent intent = new Auth.IntentBuilder()
                .buildSignIn(mContext);

        Assert.assertEquals(BuildConfig.APPLICATION_ID + ".AuthActivity", intent.getComponent().getClassName());
    }
}
