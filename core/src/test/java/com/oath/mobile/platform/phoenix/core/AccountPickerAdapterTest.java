package com.oath.mobile.platform.phoenix.core;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.oath.mobile.testutils.TestRoboTestRunnerValidRes;

import net.openid.appauth.TokenResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RuntimeEnvironment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by yuhongli on 12/4/17.
 */
@RunWith(TestRoboTestRunnerValidRes.class)
public class AccountPickerAdapterTest extends BaseTest {


    private Context mContext;

    @Before
    public void setUp() {
        mContext = RuntimeEnvironment.application;
    }

    @Test
    public void constructor_WhenCalled_Success() {
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        doReturn(Collections.emptyList()).when(authManager).getAllDisabledAccounts();
        AccountPickerAdapter.Callback listener = mock(AccountPickerAdapter.Callback.class);
        AccountPickerAdapter adapter = new AccountPickerAdapter(listener, authManager);
        verify(listener).onNoAccountsFound();
    }

    @Test
    public void onCreateViewHolder_WhenTypeAccountItem_ShouldReturnAccountItemViewHolder() {
        AccountPickerActivity activity = Robolectric.setupActivity(AccountPickerActivity.class);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);

        AccountPickerAdapter.Callback listener = mock(AccountPickerAdapter.Callback.class);
        AccountPickerAdapter adapter = new AccountPickerAdapter(listener, authManager);
        RecyclerView.ViewHolder viewHolder = adapter.createViewHolder(new LinearLayout(activity), AccountPickerAdapter.TYPE_ACCOUNT_ITEM);
        assertNotNull("View holder should not be null", viewHolder);
        assertTrue("View Holder type does not match", viewHolder instanceof AccountPickerAdapter.AccountItemViewHolder);
    }

    @Test
    public void onCreateViewHolder_WhenTypeAddAccountItem_ShouldReturnAddAccountItemViewHolder() {
        Activity activity = Robolectric.setupActivity(AccountPickerActivity.class);
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        doReturn(Collections.emptyList()).when(authManager).getAllDisabledAccounts();
        AccountPickerAdapter.Callback listener = mock(AccountPickerAdapter.Callback.class);

        AccountPickerAdapter adapter = new AccountPickerAdapter(listener, authManager);
        RecyclerView.ViewHolder viewHolder = adapter.createViewHolder(new LinearLayout(activity), AccountPickerAdapter.TYPE_ADD_ACCOUNT_ITEM);
        assertNotNull("View holder should not be null", viewHolder);
        assertTrue("View Holder type does not match", viewHolder instanceof AccountPickerAdapter.AddAccountItemViewHolder);
    }

    @Test(expected = IllegalArgumentException.class)
    public void onCreateViewHolder_WhenUnknownType_ShouldThrowAnException() {
        Activity activity = Robolectric.setupActivity(AccountPickerActivity.class);
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        doReturn(Collections.emptyList()).when(authManager).getAllDisabledAccounts();

        AccountPickerAdapter.Callback listener = mock(AccountPickerAdapter.Callback.class);

        AccountPickerAdapter adapter = new AccountPickerAdapter(listener, authManager);
        adapter.createViewHolder(new LinearLayout(activity), 3);
        fail("IllegalArgumentException expected for creating an unknown view type");
    }

    @Test
    public void onBindViewHolder_WhenGiveAccountItemViewHolder_ShouldCallBindData() {
        Activity activity = Robolectric.setupActivity(AccountPickerActivity.class);
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<IAccount> accounts = new ArrayList<>();
        IAccount accountMock = mock(IAccount.class);
        accounts.add(accountMock);
        doReturn(accounts).when(authManager).getAllDisabledAccounts();

        AccountPickerAdapter.Callback listener = mock(AccountPickerAdapter.Callback.class);

        AccountPickerAdapter adapter = new AccountPickerAdapter(listener, authManager);
        AccountPickerAdapter.AccountItemViewHolder viewHolder = mock(AccountPickerAdapter.AccountItemViewHolder.class);
        adapter.onBindViewHolder(viewHolder, 0);
        verify(viewHolder).bindData(accountMock);
    }

    @Test
    public void getItemCount_WhenAccountSetIsEmpty_ShouldReturnOne() {
        Activity activity = Robolectric.setupActivity(AccountPickerActivity.class);
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        doReturn(Collections.emptyList()).when(authManager).getAllDisabledAccounts();
        AccountPickerAdapter.Callback listener = mock(AccountPickerAdapter.Callback.class);

        AccountPickerAdapter adapter = new AccountPickerAdapter(listener, authManager);
        assertEquals("Unexpected account count", 0, adapter.getAccountCount());
        assertEquals("Unexpected item count", 1, adapter.getItemCount()); // header and add account
    }

    @Test
    public void getItemCount_WhenAccountSetIsNotEmpty_Success() {
        Activity activity = Robolectric.setupActivity(AccountPickerActivity.class);
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        IAccount accountMock = mock(IAccount.class);
        List<IAccount> accounts = new ArrayList<>();
        accounts.add(accountMock);
        doReturn(accounts).when(authManager).getAllDisabledAccounts();
        AccountPickerAdapter.Callback listener = mock(AccountPickerAdapter.Callback.class);
        AccountPickerAdapter adapter = new AccountPickerAdapter(listener, authManager);

        assertEquals("Unexpected account count", 1, adapter.getAccountCount());
        assertEquals("Unexpected item count", 2, adapter.getItemCount());
    }

    @Test
    public void getItemCount_WhenHasTwoAccountsButOneAccountIsDisabled_Success() {
        Activity activity = Robolectric.setupActivity(AccountPickerActivity.class);
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        IAccount accountMock1 = mock(IAccount.class);
        IAccount accountMock2 = mock(IAccount.class);
        List<IAccount> accounts = new ArrayList<>();
        List<IAccount> disabledAccounts = new ArrayList<>();
        accounts.add(accountMock1);
        accounts.add(accountMock2);
        disabledAccounts.add(accountMock1);
        doReturn(disabledAccounts).when(authManager).getAllDisabledAccounts();
        doReturn(accounts).when(authManager).getAllAccountsInternal();
        AccountPickerAdapter.Callback listener = mock(AccountPickerAdapter.Callback.class);
        AccountPickerAdapter adapter = new AccountPickerAdapter(listener, authManager);

        assertEquals("Unexpected account count", 1, adapter.getAccountCount());
        assertEquals("Unexpected item count", 2, adapter.getItemCount());
    }

    @Test
    public void accountItemViewHolder_OnClickAccount_ShouldNotifyListener() {
        Activity activity = Robolectric.setupActivity(AccountPickerActivity.class);
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<Account> accounts = new ArrayList<>();
        Account accountMock1 = mock(Account.class);
        accounts.add(accountMock1);
        doReturn(accounts).when(authManager).getAllDisabledAccounts();
        AccountPickerAdapter.Callback listener = mock(AccountPickerAdapter.Callback.class);

        AccountPickerAdapter adapter = new AccountPickerAdapter(listener, authManager);
        RecyclerView.ViewHolder viewHolder = spy(adapter.createViewHolder(new LinearLayout(activity), AccountPickerAdapter.TYPE_ACCOUNT_ITEM));
        adapter.bindViewHolder(viewHolder, 0);
        doReturn(0).when(viewHolder).getAdapterPosition();
        assertNotNull("View holder should not be null", viewHolder);
        viewHolder.itemView.getRootView().callOnClick();

        verify(listener).onAccountSelected(anyInt(), any(IAccount.class));
    }

    @Test
    public void getItemViewType_WhenAddAccountItemViewType_Success() {
        Activity activity = Robolectric.setupActivity(AccountPickerActivity.class);
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<IAccount> accounts = new ArrayList<>();
        IAccount accountMock = mock(IAccount.class);
        accounts.add(accountMock);
        doReturn(accounts).when(authManager).getAllDisabledAccounts();
        AccountPickerAdapter.Callback listener = mock(AccountPickerAdapter.Callback.class);
        AccountPickerAdapter adapter = new AccountPickerAdapter(listener, authManager);
        int itemViewType = adapter.getItemViewType(1);

        assertEquals("Unexpected item view type", AccountPickerAdapter.TYPE_ADD_ACCOUNT_ITEM, itemViewType);
    }

    @Test
    public void getItemViewType_WhenAccountItemViewType_Success() {
        Activity activity = Robolectric.setupActivity(AccountPickerActivity.class);
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<IAccount> accounts = new ArrayList<>();
        IAccount accountMock = mock(IAccount.class);
        accounts.add(accountMock);
        doReturn(accounts).when(authManager).getAllDisabledAccounts();
        AccountPickerAdapter.Callback listener = mock(AccountPickerAdapter.Callback.class);

        AccountPickerAdapter adapter = new AccountPickerAdapter(listener, authManager);
        int itemViewType = adapter.getItemViewType(0);

        assertEquals("Unexpected item view type", AccountPickerAdapter.TYPE_ACCOUNT_ITEM, itemViewType);
    }

    @Test
    public void notifyAccountSetChanged_WhenAccountAdded_ShouldUpdateAccountSet() {
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<IAccount> accounts = new ArrayList<>();
        IAccount accountMock1 = mock(IAccount.class);
        when(accountMock1.getUserName()).thenReturn("test1@xx.com");
        accounts.add(accountMock1);
        IAccount accountMock2 = mock(IAccount.class);
        when(accountMock2.getUserName()).thenReturn("test2@xx.com");
        doReturn(accounts).when(authManager).getAllDisabledAccounts();

        AccountPickerAdapter.Callback listener = mock(AccountPickerAdapter.Callback.class);
        AccountPickerAdapter adapter = new AccountPickerAdapter(listener, authManager);
        AccountPickerAdapter adapterSpy = spy(adapter);

        assertEquals("Unexpected item count", 2, adapterSpy.getItemCount());

        accounts.add(accountMock2);
        doReturn(accounts).when(authManager).getAllDisabledAccounts();

        adapterSpy.notifyAccountSetChanged();

        assertEquals("Unexpected item count", 3, adapterSpy.getItemCount());

        verify(adapterSpy).notifyDataSetChanged();
    }

    @Test
    public void addAccountItemViewHolder_OnClick_ShouldNotifyListener() throws Exception {
        Activity activity = Robolectric.setupActivity(AccountPickerActivity.class);
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<IAccount> accounts = new ArrayList<>();
        IAccount accountMock1 = mock(IAccount.class);
        accounts.add(accountMock1);
        doReturn(accounts).when(authManager).getAllDisabledAccounts();
        AccountPickerAdapter.Callback listener = mock(AccountPickerAdapter.Callback.class);

        AccountPickerAdapter adapter = new AccountPickerAdapter(listener, authManager);
        RecyclerView.ViewHolder viewHolder = adapter.createViewHolder(new LinearLayout(activity), AccountPickerAdapter.TYPE_ADD_ACCOUNT_ITEM);
        assertNotNull("View holder should not be null", viewHolder);
        viewHolder.itemView.callOnClick();

        verify(listener).onAddAccount();
    }

    @Test
    public void accountItemViewHolder_BindData_WhenUserNameEmpty_ShouldUseEmail() {
        Activity activity = Robolectric.setupActivity(AccountPickerActivity.class);
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<Account> accounts = new ArrayList<>();
        Account accountMock1 = mock(Account.class);
        when(accountMock1.getUserName()).thenReturn("test1@xx.com");
        accounts.add(accountMock1);
        doReturn(accounts).when(authManager).getAllDisabledAccounts();
        AccountPickerAdapter.Callback listener = mock(AccountPickerAdapter.Callback.class);

        AccountPickerAdapter adapter = new AccountPickerAdapter(listener, authManager);

        AccountPickerAdapter.AccountItemViewHolder viewHolder = (AccountPickerAdapter.AccountItemViewHolder) adapter.createViewHolder(new LinearLayout(activity), AccountPickerAdapter.TYPE_ACCOUNT_ITEM);
        assertNotNull("View holder should not be null", viewHolder);
        viewHolder.bindData(accountMock1);
        TextView displayName = (TextView) viewHolder.itemView.findViewById(R.id.account_display_name);
        TextView email = (TextView) viewHolder.itemView.findViewById(R.id.account_email);
        assertEquals("Display name should be account name", "", displayName.getText().toString());
        assertEquals("Email field should be hidden", View.VISIBLE, email.getVisibility());
    }

    @Test
    public void accountItemViewHolder_BindData_WhenUserNameAndDisplayNameDifferent_ShouldDisplayBoth() {
        Activity activity = Robolectric.setupActivity(AccountPickerActivity.class);
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<Account> accounts = new ArrayList<>();
        Account accountMock1 = mock(Account.class);
        when(accountMock1.getUserName()).thenReturn("test1@xx.com");
        when(accountMock1.getFirstName()).thenReturn("Test");
        when(accountMock1.getLastName()).thenReturn("Unit");
        when(accountMock1.getDisplayName()).thenReturn("Test Unit");
        accounts.add(accountMock1);
        doReturn(accounts).when(authManager).getAllDisabledAccounts();

        AccountPickerAdapter.Callback listener = mock(AccountPickerAdapter.Callback.class);

        AccountPickerAdapter adapter = new AccountPickerAdapter(listener, authManager);

        AccountPickerAdapter.AccountItemViewHolder viewHolder = (AccountPickerAdapter.AccountItemViewHolder) adapter.createViewHolder(new LinearLayout(activity), AccountPickerAdapter.TYPE_ACCOUNT_ITEM);
        assertNotNull("View holder should not be null", viewHolder);
        viewHolder.bindData(accountMock1);
        TextView displayName = (TextView) viewHolder.itemView.findViewById(R.id.account_display_name);
        TextView email = (TextView) viewHolder.itemView.findViewById(R.id.account_email);
        assertEquals("Unexpected value returned", accountMock1.getFirstName() + " " + accountMock1.getLastName(), displayName.getText().toString());
        assertEquals("Email field shoud be visible", View.VISIBLE, email.getVisibility());
        assertEquals("Unexpected value returned", accountMock1.getUserName(), email.getText().toString());
    }
}
