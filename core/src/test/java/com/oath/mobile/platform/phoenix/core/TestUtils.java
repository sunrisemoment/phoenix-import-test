package com.oath.mobile.platform.phoenix.core;

import android.net.ConnectivityManager;
import android.net.Uri;

import junit.framework.Assert;

import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowConnectivityManager;
import org.robolectric.shadows.ShadowNetworkInfo;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import static org.robolectric.Shadows.shadowOf;

/**
 * Created by yuhongli on 11/28/17.
 */

public class TestUtils {
    static void setNetworkConnectivity(boolean isConnected) {
        ConnectivityManager cm = (ConnectivityManager)
                RuntimeEnvironment.application.getSystemService("connectivity");

        ShadowConnectivityManager shadowCM = shadowOf(cm);
        ShadowNetworkInfo shadowNetworkInfo = shadowOf(shadowCM.getActiveNetworkInfo());
        shadowNetworkInfo.setConnectionStatus(isConnected);
    }

    public static void assertEquals(Uri expectedUrl, Uri actualUrl) throws UnsupportedEncodingException {
        Assert.assertEquals("Schemes don't match", expectedUrl.getScheme(), actualUrl.getScheme());
        Assert.assertEquals("Authorities don't match", expectedUrl.getAuthority(), actualUrl.getAuthority());
        Assert.assertEquals("Hosts don't match", expectedUrl.getHost(), actualUrl.getHost());
        Assert.assertEquals("Paths don't match", expectedUrl.getPath(), actualUrl.getPath());

        String[] expectedQueryParameters = expectedUrl.getQuery().split("&");
        String[] actualQueryParameters = actualUrl.getQuery().split("&");

        Assert.assertEquals("Total number of query parameters do not match", expectedQueryParameters.length, actualQueryParameters.length);
        Map<String, String> expectedQueryParams = extractQueryParamsMap(expectedQueryParameters);
        Map<String, String> actualQueryParams = extractQueryParamsMap(actualQueryParameters);

        Assert.assertEquals("Number of query parameters do not match", expectedQueryParams.size(), actualQueryParams.size());
        for (Map.Entry<String, String> pair : actualQueryParams.entrySet()) {
            Assert.assertEquals("Query parameter doesn't match for key " + pair.getKey(), expectedQueryParams.get(pair.getKey()), pair.getValue());
        }
    }

    private static Map<String, String> extractQueryParamsMap(String[] queryPairs) throws UnsupportedEncodingException {
        Map<String, String> queryParamsMap = new HashMap<>();
        for (String pair : queryPairs) {
            final int index = pair.indexOf("=");
            final String key = index > 0 ? URLDecoder.decode(pair.substring(0, index), "UTF-8") : pair;
            final String value = (index > 0 && pair.length() > index + 1) ? URLDecoder.decode(pair.substring(index + 1), "UTF-8") : null;
            queryParamsMap.put(key, value);
        }
        return queryParamsMap;
    }
}
