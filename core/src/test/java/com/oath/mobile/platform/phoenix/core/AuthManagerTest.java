
package com.oath.mobile.platform.phoenix.core;

import android.accounts.AccountManager;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import com.oath.mobile.testutils.TestRoboTestRunnerValidRes;
import com.yahoo.mobile.client.share.util.ThreadPoolExecutorSingleton;

import net.openid.appauth.TokenResponse;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;
import org.robolectric.RuntimeEnvironment;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Headers;

import static com.oath.mobile.platform.phoenix.core.AuthConfig.AOL_AUTH_CONFIG_KEY;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.AUTH_CONFIG_EXPIRE_TIME_KEY;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.OATH_AUTH_CONFIG_KEY;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.OATH_AUTH_PATH;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.OATH_IDP_ISSUER;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.OATH_TOKEN_PATH;
import static com.oath.mobile.platform.phoenix.core.AuthHelperTest.GET_USER_INFO_VALID_RESPONSE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapError.COUNTDOWN_LATCH_WAIT_INTERRUPTED;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapError.EVENT_BOOTSTRAP_ERROR;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_ADD_ACCOUNT_FAILURE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_FAILURE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_FETCH_USER_INFO_FAILURE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_FETCH_USER_INFO_START;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_FETCH_USER_INFO_SUCCESS;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_REFRESH_TOKEN_FAILURE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_REFRESH_TOKEN_START;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_REFRESH_TOKEN_SUCCESS;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_START;
import static com.oath.mobile.platform.phoenix.core.EventLogger.BootstrapUserEvents.EVENT_BOOTSTRAP_SUCCESS;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenInternalError.EVENT_REFRESH_TOKEN_CLIENT_ERROR;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenInternalError.NETWORK_ISSUE;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_ACCESS_TOKEN;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_ACCESS_TOKEN_EXPIRATION_TIME;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_ID_TOKEN_WITH_LOGIN_ID_JWT;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_JWT_CLAIM_ISS_VAL;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_JWT_EMAIL_VALUE;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_JWT_EMAIL_VALUE2;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_JWT_LOGIN_ID_VALUE;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_JWT_TOKEN;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_JWT_TOKEN_YAHOO;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_JWT_TOKEN_YAHOO2;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_JWT_TOKEN_YAHOO_WITH_LEGACY_GUID;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_JWT_TOKEN_YAHOO_WITH_UPDATED_NANE;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_JWT_UPDATED_GIVEN_NAME_VALUE;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_REFRESH_TOKEN;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_REFRESH_TOKEN2;
import static com.oath.mobile.platform.phoenix.core.TestValues.getTestAuthCodeExchangeRequest;
import static com.oath.mobile.platform.phoenix.core.TestValues.getTestAuthCodeExchangeResponseNonce;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by nsoni on 10/13/17.
 */
@RunWith(TestRoboTestRunnerValidRes.class)
public class AuthManagerTest extends BaseTest {

    private final Application mContext = RuntimeEnvironment.application;
    private EventLogger mEventLoggerSpy;

    @Before
    public void setUp() throws Exception {
        AuthManager actual = (AuthManager) AuthManager.getInstance(mContext);
        actual.removeAccounts();
        mEventLoggerSpy = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = mEventLoggerSpy;
    }

    @Test
    public void getInstance_WhenCalledTwice_ShouldReturnSameObject() throws Exception {
        IAuthManager authManager1 = AuthManager.getInstance(mContext);
        IAuthManager authManager2 = AuthManager.getInstance(mContext);

        Assert.assertNotNull("Should not be null", authManager1);
        Assert.assertNotNull("Should not be null", authManager2);
        Assert.assertEquals("Objects should be equal", authManager1, authManager2);
    }

    @Test
    public void testNonce_ShouldBeValidNotDuplicate() {
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);

        String nonce = authManager.generateNonce();

        String lastNonce = authManager.mVerificationNonce;

        assertEquals("The nonce do not match", nonce, lastNonce);

        String nonce2 = authManager.generateNonce();

        assertFalse("The nonce are the same across 2 generations", nonce.equals(nonce2));

        String lastNonce2 = authManager.mVerificationNonce;

        assertEquals("The second nonce do not match", nonce2, lastNonce2);
        assertFalse("The stored nonce is not the new nonce generated", lastNonce.equals(nonce2));

        AuthManager authManageDupe = (AuthManager) AuthManager.getInstance(mContext);

        String lastNonce2Verification = authManageDupe.mVerificationNonce;

        assertEquals("The verification nonce changed between instances of auth manager", lastNonce2, lastNonce2Verification);
    }

    @Test
    public void constructor_WhenIsCalled_ShouldInitOathAuthConfig() throws Exception {
        Field field = AuthManager.class.getDeclaredField("mAndroidAccManager");
        field.setAccessible(true);
        field = AuthManager.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(AuthManager.getInstance(mContext), null);
        SharedPreferences sharedPreferences = RuntimeEnvironment.application.getApplicationContext().getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(AUTH_CONFIG_EXPIRE_TIME_KEY, System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1));
        editor.apply();
        AuthManager.getInstance(mContext);
        Robolectric.flushBackgroundThreadScheduler();
        AuthConfig config = AuthConfig.getAuthConfigByIssuer(mContext, OATH_IDP_ISSUER);
        assertEquals("Should be the same", OATH_AUTH_PATH, config.getAuthPath());
        assertEquals("Should be the same", OATH_TOKEN_PATH, config.getTokenPath());
        assertEquals(Uri.parse(OATH_IDP_ISSUER).getAuthority(), config.getIdpAuthority());
        Uri authUri = config.getAuthUri(mContext);
        assertEquals(Uri.parse(OATH_IDP_ISSUER + OATH_AUTH_PATH).toString(), authUri.getScheme() + "://" + authUri.getAuthority() + authUri.getPath());
        Uri tokenUri = config.getTokenUri(mContext);
        assertEquals(Uri.parse(OATH_IDP_ISSUER + OATH_TOKEN_PATH).toString(), tokenUri.getScheme() + "://" + tokenUri.getAuthority() + tokenUri.getPath());
        assertEquals(TestValues.TEST_CLIENT_ID, config.getClientID());
        assertEquals("", config.getClientSecret());
        assertEquals(TestValues.TEST_APP_REDIRECT_URI, config.getRedirectUri());
        assertEquals(Arrays.asList(TestValues.TEST_SCOPE.split(" ")), config.getScopes());
    }

    @Test
    public void getAllAccounts_WhenAccountsOnDevice_ShouldReturnListOfLoggedInAccounts() throws Exception {

        TokenResponse tokenResponseWithAccessToken = TestValues.getTestAuthCodeExchangeResponse();
        TokenResponse tokenResponseWithNoAccessToken = new TokenResponse.Builder(getTestAuthCodeExchangeRequest())
                .setTokenType(TokenResponse.TOKEN_TYPE_BEARER)
                .setRefreshToken(TEST_REFRESH_TOKEN2)
                .setIdToken(TEST_JWT_TOKEN_YAHOO2)
                .setAccessTokenExpirationTime(TEST_ACCESS_TOKEN_EXPIRATION_TIME).build();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponseWithAccessToken);
        authManager.addAccount(tokenResponseWithNoAccessToken);

        List<IAccount> accountList = authManager.getAllAccounts();

        Assert.assertEquals("Should be one account", 1, accountList.size());
        Assert.assertEquals(TEST_ACCESS_TOKEN, accountList.get(0).getToken());
        android.accounts.Account[] androidAccounts = authManager.getAccountsByType();
        Assert.assertEquals("Android account manager should have 2 accounts", 2, androidAccounts.length);
    }

    @Test
    public void getAllAccountsInternal_WhenAccountsOnDevice_ShouldReturnListOfAllAccounts() throws Exception {

        TokenResponse tokenResponseWithAccessToken = TestValues.getTestAuthCodeExchangeResponse();
        TokenResponse tokenResponseWithNoAccessToken = new TokenResponse.Builder(getTestAuthCodeExchangeRequest())
                .setTokenType(TokenResponse.TOKEN_TYPE_BEARER)
                .setRefreshToken(TEST_REFRESH_TOKEN2)
                .setIdToken(TEST_JWT_TOKEN_YAHOO2)
                .setAccessTokenExpirationTime(TEST_ACCESS_TOKEN_EXPIRATION_TIME).build();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponseWithAccessToken);
        authManager.addAccount(tokenResponseWithNoAccessToken);

        List<IAccount> accountList = authManager.getAllAccountsInternal();

        Assert.assertEquals("Should be 2 accounts", 2, accountList.size());
        Assert.assertEquals(TEST_ACCESS_TOKEN, accountList.get(0).getToken());
        Assert.assertNull("should be null", accountList.get(1).getToken());
        android.accounts.Account[] androidAccounts = authManager.getAccountsByType();
        Assert.assertEquals("Android account manager should have 2 accounts", 2, androidAccounts.length);
    }

    @Test
    public void getAllAccounts_WhenNoAccountsOnDevice_ShouldReturnEmptyList() throws Exception {
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);

        List<IAccount> accountList = authManager.getAllAccounts();

        Assert.assertEquals("Should return empty list", 0, accountList.size());
    }

    @Test
    public void addAccount_WhenAccountAlreadyAdded_ShouldReturnLastAddedAccountAndUpdatedInfo() throws Exception {
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        IAccount account = authManager.addAccount(tokenResponse);

        assertEquals("Should be same", TEST_REFRESH_TOKEN, ((Account)account).getRefreshToken());

        //Added again
        TokenResponse newTokenResponse = new TokenResponse.Builder(getTestAuthCodeExchangeRequest())
                .setTokenType(TokenResponse.TOKEN_TYPE_BEARER)
                .setRefreshToken(TEST_REFRESH_TOKEN + "new")
                .setAccessToken(TEST_ACCESS_TOKEN + "new")
                .setIdToken(TEST_JWT_TOKEN_YAHOO_WITH_UPDATED_NANE)
                .setAccessTokenExpirationTime(TEST_ACCESS_TOKEN_EXPIRATION_TIME).build();
        IAccount account1 = authManager.addAccount(newTokenResponse);

        Assert.assertNotEquals("Objects should be different", account, account1);
        Assert.assertEquals("should be same", TEST_ACCESS_TOKEN + "new", account1.getToken());
        Assert.assertEquals("should be same", TEST_JWT_UPDATED_GIVEN_NAME_VALUE, account1.getFirstName());
        Assert.assertEquals("should be same", TEST_ACCESS_TOKEN + "new", account.getToken());
        Assert.assertEquals("should be same", TEST_JWT_UPDATED_GIVEN_NAME_VALUE, account.getFirstName());
        assertEquals("Should be same", TEST_REFRESH_TOKEN + "new", ((Account)account1).getRefreshToken());
        Account account2 = (Account) authManager.getAccount(account.getUserName());
        assertEquals(TEST_REFRESH_TOKEN + "new", account2.getRefreshToken());
    }

    @Test
    public void addAccount_WhenAccountAlreadyAddedWithLegacyGuid_ShouldRemoveAccountWithLegacyGuidAndReturnAccountWithNewGuid() throws Exception {
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        authManager.mVerificationNonce = TestValues.getTestAuthCodeExchangeResponseNonce();
        IAccount account = authManager.addAccount(tokenResponse);

        //Added again
        TokenResponse newTokenResponse = new TokenResponse.Builder(getTestAuthCodeExchangeRequest())
                .setTokenType(TokenResponse.TOKEN_TYPE_BEARER)
                .setRefreshToken(TEST_REFRESH_TOKEN + "new")
                .setAccessToken(TEST_ACCESS_TOKEN + "new")
                .setIdToken(TEST_JWT_TOKEN_YAHOO_WITH_LEGACY_GUID) // idToken with legacy guid
                .setAccessTokenExpirationTime(TEST_ACCESS_TOKEN_EXPIRATION_TIME).build();
        IAccount account1 = authManager.addAccount(newTokenResponse);

        Assert.assertNotEquals("Objects should be different", account, account1);
        Assert.assertEquals("should be same", TEST_ACCESS_TOKEN + "new", account1.getToken());
        Assert.assertNull("should be null", account.getToken());
        Assert.assertNull("should be null", authManager.getAccount("test@example.com"));
        Assert.assertEquals("should be same", "random_current_guid", account1.getGUID());
        Assert.assertEquals("should be same", "test@example.com", account1.getLegacyGUID());
    }


    @Test
    public void addAccount_WhenNewAccount_ShouldReturnNewlyAddedAccount() throws Exception {
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);

        //New account added
        TokenResponse newTokenResponse = new TokenResponse.Builder(getTestAuthCodeExchangeRequest())
                .setTokenType(TokenResponse.TOKEN_TYPE_BEARER)
                .setRefreshToken(TEST_REFRESH_TOKEN + "new")
                .setAccessToken(TEST_ACCESS_TOKEN + "new")
                .setIdToken(TEST_JWT_TOKEN_YAHOO_WITH_LEGACY_GUID) // idToken with legacy guid
                .setAccessTokenExpirationTime(TEST_ACCESS_TOKEN_EXPIRATION_TIME).build();
        IAccount account1 = authManager.addAccount(newTokenResponse);

        Assert.assertEquals("should be same", TEST_ACCESS_TOKEN + "new", account1.getToken());
        Assert.assertEquals("should be same", "random_current_guid", account1.getGUID());
        Assert.assertEquals("should be same", "test@example.com", account1.getLegacyGUID());
    }

    @Test
    public void addAccount_WhenAddAccountFail_ShouldReturnNull() throws Exception {
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        android.accounts.AccountManager accountManagerMock = mock(AccountManager.class);
        when(accountManagerMock.addAccountExplicitly(any(), any(), any())).thenReturn(false);
        when(accountManagerMock.getUserData(any(), any())).thenReturn(null);
        Field field = AuthManager.class.getDeclaredField("mAndroidAccManager");
        field.setAccessible(true);
        android.accounts.AccountManager accountManagerSpy = spy((android.accounts.AccountManager) field.get(authManager));
        doReturn(false).when(accountManagerSpy).addAccountExplicitly(any(), any(), any());
        doReturn(null).when(accountManagerSpy).getUserData(any(), any());
        field.set(authManager, accountManagerSpy);

        IAccount account = authManager.addAccount(TestValues.getTestAuthCodeExchangeResponse());
        assertNull(account);
        field = AuthManager.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(authManager, null);
    }

    @Test
    public void addAccount_WhenIdTokenIsBad_ShouldReturnNullObject() throws Exception {
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);

        String idToken = Base64.getEncoder().encodeToString("somebadidtoken".getBytes());
        idToken = "blah." + idToken + ".blah";
        TokenResponse tokenResponse = new TokenResponse.Builder(getTestAuthCodeExchangeRequest())
                .setTokenType(TokenResponse.TOKEN_TYPE_BEARER)
                .setRefreshToken(TEST_REFRESH_TOKEN)
                .setAccessToken(TEST_ACCESS_TOKEN)
                .setIdToken(idToken)
                .setAccessTokenExpirationTime(TEST_ACCESS_TOKEN_EXPIRATION_TIME).build();

        IAccount account = authManager.addAccount(tokenResponse);
        Assert.assertNull("Should be null", account);

    }

    @Test
    public void addAccount_WhenIdTokenIsEmpty_ShouldReturnNullObject() throws Exception {
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);

        TokenResponse tokenResponse = new TokenResponse.Builder(getTestAuthCodeExchangeRequest())
                .setTokenType(TokenResponse.TOKEN_TYPE_BEARER)
                .setRefreshToken(TEST_REFRESH_TOKEN)
                .setAccessToken(TEST_ACCESS_TOKEN)
                .setAccessTokenExpirationTime(TEST_ACCESS_TOKEN_EXPIRATION_TIME).build();

        IAccount account = authManager.addAccount(tokenResponse);
        Assert.assertNull("Should be null", account);

    }

    @Test
    public void getAccount_WhenAccountAddedWithIdTokenWhichDoesNotHaveLoginId_ShouldReturnTheAccount() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);

        IAccount account = authManager.getAccount(TEST_JWT_EMAIL_VALUE);

        Assert.assertNotNull(account);
        Assert.assertEquals("email should match", account.getEmail(), TEST_JWT_EMAIL_VALUE);
        Assert.assertEquals("username should match", account.getUserName(), TEST_JWT_EMAIL_VALUE);
        //Assert.assertEquals("Image should match", account.getImageUri(), TestValues.TEST_JWT_IMAGE192);

        android.accounts.Account[] androidAccounts = authManager.getAccountsByType();

        Assert.assertEquals("GUID must be correct", "test@example.com", account.getGUID());
        Assert.assertEquals("User name must be correct", "test@yahoo.com", account.getUserName());
        Assert.assertEquals("Account name should be the guid/subscriber", "test@example.com", androidAccounts[0].name);
    }

    @Test
    public void getAccount_WhenAccountAddedWithIdTokenWhichDoesNotHaveLoginIdWithCorrectNonce_ShouldReturnTheAccountAndClearNonce() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = TestValues.getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);

        IAccount account = authManager.getAccount(TEST_JWT_EMAIL_VALUE);

        assertNull("The nonce was not cleared", authManager.mVerificationNonce);

        Assert.assertNotNull(account);
        Assert.assertEquals("email should match", account.getEmail(), TEST_JWT_EMAIL_VALUE);
        Assert.assertEquals("username should match", account.getUserName(), TEST_JWT_EMAIL_VALUE);
        //Assert.assertEquals("Image should match", account.getImageUri(), TestValues.TEST_JWT_IMAGE192);

        android.accounts.Account[] androidAccounts = authManager.getAccountsByType();

        Assert.assertEquals("GUID must be correct", "test@example.com", account.getGUID());
        Assert.assertEquals("User name must be correct", "test@yahoo.com", account.getUserName());
        Assert.assertEquals("Account name should be the guid/subscriber", "test@example.com", androidAccounts[0].name);
    }

    @Test
    public void verifyNonce_WhenIssuerIsBad_ShouldSkipNonceVerification() throws Exception {
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);

        IdToken idToken = IdToken.fromJwt(TEST_JWT_TOKEN);

        assertEquals("The IDToken Issuer is wrong", TEST_JWT_CLAIM_ISS_VAL, idToken.getIssuer());

        assertTrue("The nonce verification should be skipped with bad issuer", authManager.verifyNonce(idToken));
    }



    @Test
    public void getAccount_WhenAccountAddedWithIdTokenWhichDoesNotHaveLoginIdWithCorrectNonce_ShouldReturnNullAndClearNonce() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = "BadNonce";
        IAccount account = authManager.addAccount(tokenResponse);

        assertNull("The nonce was not cleared", authManager.mVerificationNonce);
        assertNull("The account was not null", account);
    }

    @Test
    public void getAccount_WhenAccountAddedWithIdTokenWhichHasLoginId_ShouldReturnTheAccount() throws Exception {
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        TokenResponse tokenResponse = new TokenResponse.Builder(getTestAuthCodeExchangeRequest())
                .setTokenType(TokenResponse.TOKEN_TYPE_BEARER)
                .setRefreshToken(TEST_REFRESH_TOKEN)
                .setAccessToken(TEST_ACCESS_TOKEN)
                .setIdToken(TEST_ID_TOKEN_WITH_LOGIN_ID_JWT)
                .setAccessTokenExpirationTime(TEST_ACCESS_TOKEN_EXPIRATION_TIME).build();
        authManager.addAccount(tokenResponse);

        IAccount account = authManager.getAccount(TEST_JWT_LOGIN_ID_VALUE);

        Assert.assertNotNull(account);

        android.accounts.Account[] androidAccounts = authManager.getAccountsByType();

        Assert.assertEquals("GUID must be correct", "test@example.com", account.getGUID());
        Assert.assertEquals("User name must be correct", "test", account.getUserName());
        Assert.assertEquals("Account name should be the guid/subscriber", "test@example.com", androidAccounts[0].name);
    }

    @Test
    public void getAccount_WhenAccountAddedWithNoAccessToken_ShouldNotReturnTheAccount() throws Exception {

        TokenResponse tokenResponseWithNoAccessToken = new TokenResponse.Builder(getTestAuthCodeExchangeRequest())
                .setTokenType(TokenResponse.TOKEN_TYPE_BEARER)
                .setRefreshToken(TEST_REFRESH_TOKEN2)
                .setIdToken(TEST_JWT_TOKEN_YAHOO2)
                .setAccessTokenExpirationTime(TEST_ACCESS_TOKEN_EXPIRATION_TIME).build();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponseWithNoAccessToken);

        IAccount account = authManager.getAccount(TEST_JWT_EMAIL_VALUE2);

        Assert.assertNull("Should return null", account);
        android.accounts.Account[] androidAccounts = authManager.getAccountsByType();

        Assert.assertEquals("Account name should be the guid/subscriber", "test_account2@example.com", androidAccounts[0].name);
    }


    @Test
    public void getAccount_WhenAccountNotAdded_ShouldReturnNullObject() throws Exception {
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        IAccount account = authManager.getAccount("trymyemail@yahoo.com");
        Assert.assertNull(account);
    }

    @Test
    public void lookupAccountByGuid_WhenNoAccount_ShouldReturnNull() throws Exception {
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        Assert.assertNull(authManager.lookupAccountByGuid("someguid"));
    }

    @Test(expected = IllegalStateException.class)
    public void bootstrap_WhenConfigIsNull_ShouldThrowExceptionAndLogFailure() throws Exception {
        AuthConfig authConfig = new AuthConfig(null, null, null, null, null, null, null);
        authConfig.saveAuthConfig(mContext, AOL_AUTH_CONFIG_KEY);
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        try {
            authManager.bootstrap(mContext, new ArrayList<>(), new AuthManager.OnBootstrapResponse() {
                @Override
                public void onResponse(int result) {

                }
            });
        } catch (Exception e) {
            Assert.assertEquals("Auth config is missing!", e.getMessage());
            Assert.assertEquals("java.lang.IllegalStateException", e.getClass().getName());
            ArgumentCaptor<Map> customParam = ArgumentCaptor.forClass(Map.class);
            verify(mEventLoggerSpy, times(1)).logUserEvent(Matchers.eq(EVENT_BOOTSTRAP_START), customParam.capture());
            Map<String, Object> realMap = (Map<String, Object>) customParam.getValue();
            assertTrue("Must have the key", realMap.containsKey(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));
            assertEquals(0, realMap.get(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));
            verify(mEventLoggerSpy, times(1)).logErrorInformationEvent(Matchers.eq(EVENT_BOOTSTRAP_ERROR), Matchers.eq(EventLogger.BootstrapError.AUTH_CONFIG_NULL), Matchers.contains("Auth config null in AuthManager class"));
            throw e;
        }
    }

    void initAuthConfig() {
        AuthConfig authConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        authConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
    }

    @Test
    public void bootstrap_WhenDataIsNull_ShouldReturnSuccess() throws Exception {
        initAuthConfig();

        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.bootstrap(mContext, null, new AuthManager.OnBootstrapResponse() {
            @Override
            public void onResponse(int result) {
                Assert.assertEquals("Should succeed", AuthManager.OnBootstrapResponse.SUCCESS, result);
                ArgumentCaptor<Map> customParam = ArgumentCaptor.forClass(Map.class);
                verify(mEventLoggerSpy, times(1)).logUserEvent(Matchers.eq(EVENT_BOOTSTRAP_START), customParam.capture());
                Map<String, Object> realMap = (Map<String, Object>) customParam.getValue();
                assertTrue("Must have the key", realMap.containsKey(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));
                assertEquals(0, realMap.get(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));
                verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_SUCCESS, null);
            }
        });
    }

    @Test
    public void bootstrap_WhenDataIsEmpty_ReturnSuccess() throws Exception {
        initAuthConfig();

        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.bootstrap(mContext, new ArrayList<>(), new AuthManager.OnBootstrapResponse() {
            @Override
            public void onResponse(int result) {
                Assert.assertEquals("Should succeed", AuthManager.OnBootstrapResponse.SUCCESS, result);
                ArgumentCaptor<Map> customParam = ArgumentCaptor.forClass(Map.class);
                verify(mEventLoggerSpy, times(1)).logUserEvent(Matchers.eq(EVENT_BOOTSTRAP_START), customParam.capture());
                Map<String, Object> realMap = (Map<String, Object>) customParam.getValue();
                assertTrue("Must have the key", realMap.containsKey(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));
                assertEquals(0, realMap.get(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));
                verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_SUCCESS, null);
            }
        });
    }

    @Test
    public void bootstrap_WhenCallBackIsNull_ShouldNotThrowException() throws Exception {
        initAuthConfig();

        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.bootstrap(mContext, new ArrayList<>(), null);
    }

    void mockRefreshTokenResponse(Answer mockedAnswer) throws HttpConnectionException, NoSuchFieldException, IllegalAccessException {
        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(mockedAnswer).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);
    }

    void mockSingletonExecutor() throws NoSuchFieldException, IllegalAccessException {
        ThreadPoolExecutorSingleton mockExecutor = mock(ThreadPoolExecutorSingleton.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Runnable runnable = (Runnable) invocation.getArguments()[0];
                runnable.run();
                return null;
            }
        }).when(mockExecutor).execute(any(Runnable.class));

        Field field = ThreadPoolExecutorSingleton.class.getDeclaredField("sInstance");
        field.setAccessible(true);
        field.set(null, mockExecutor);
    }

    @Test
    public void bootstrap_WhenRefreshTokenSucceeds_ShouldReturnSuccess() throws Exception {
        initAuthConfig();

        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        AuthManager.BootstrapData bootStrapData = new AuthManager.BootstrapData(TestValues.TEST_JWT_CLAIM_SUB_VAL, TestValues.TEST_REFRESH_TOKEN, TestValues.TEST_JWT_NAME_VALUE, TestValues.TEST_JWT_EMAIL_VALUE);
        List<AuthManager.BootstrapData> dataList = new ArrayList<>();
        dataList.add(bootStrapData);

        mockRefreshTokenResponse(invocation -> {
            AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
            asyncCallback.onSuccess(TestValues.getTestAuthCodeExchangeResponse().jsonSerializeString());
            return null;
        });

        mockSingletonExecutor();

        AuthManager.OnBootstrapResponse onBootStrapResponse = spy(new AuthManager.OnBootstrapResponse() {
            @Override
            public void onResponse(int result) {
                Assert.assertEquals("Should succeed", AuthManager.OnBootstrapResponse.SUCCESS, result);
                List<IAccount> accountList = authManager.getAllAccounts();
                Assert.assertEquals(TestValues.TEST_JWT_CLAIM_SUB_VAL, accountList.get(0).getGUID());
                Assert.assertTrue("Account should be logged in", ((Account) accountList.get(0)).isLoggedIn());
            }
        });
        AccountNetworkAPI accountNetworkAPI = AccountNetworkAPI.getInstance(mContext);

        doReturn(GET_USER_INFO_VALID_RESPONSE).when(accountNetworkAPI).executeGet(any(Context.class), anyString(), any(Headers.class));
        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPI);

        // Test
        authManager.bootstrap(mContext, dataList, onBootStrapResponse);

        verify(onBootStrapResponse).onResponse(AuthManager.OnBootstrapResponse.SUCCESS);
        ArgumentCaptor<Map> customParam = ArgumentCaptor.forClass(Map.class);
        verify(mEventLoggerSpy, times(1)).logUserEvent(Matchers.eq(EVENT_BOOTSTRAP_START), customParam.capture());
        Map<String, Object> realMap = (Map<String, Object>) customParam.getValue();
        assertTrue("Must have the key", realMap.containsKey(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));
        assertEquals(1, realMap.get(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));

        List<IAccount> allAccounts = authManager.getAllAccounts();
        assertEquals("Account should still be accessible", 1, allAccounts.size());
        IAccount account = allAccounts.get(0);
        assertTrue("The account should be in active state", account.isActive());

        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_START, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_SUCCESS, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_SUCCESS, null);
    }

    @Test
    public void bootstrap_WhenRefreshTokenReturnDifferentRefreshToken_ShouldNotUpdateRefreshToken() throws Exception {
        initAuthConfig();

        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        AuthManager.BootstrapData bootStrapData = new AuthManager.BootstrapData(TestValues.TEST_JWT_CLAIM_SUB_VAL, TestValues.TEST_REFRESH_TOKEN, TestValues.TEST_JWT_NAME_VALUE, TestValues.TEST_JWT_EMAIL_VALUE);
        List<AuthManager.BootstrapData> dataList = new ArrayList<>();
        dataList.add(bootStrapData);

        String updatedRefreshToken = "someRandomToken";

        mockRefreshTokenResponse(invocation -> {
            AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
            asyncCallback.onSuccess(new TokenResponse.Builder(getTestAuthCodeExchangeRequest())
                    .setTokenType(TokenResponse.TOKEN_TYPE_BEARER)
                    .setRefreshToken(updatedRefreshToken)
                    .setAccessToken(TEST_ACCESS_TOKEN)
                    .setAccessTokenExpirationTime(TEST_ACCESS_TOKEN_EXPIRATION_TIME)
                    .build()
                    .jsonSerializeString());
            return null;
        });

        mockSingletonExecutor();

        AuthManager.OnBootstrapResponse onBootStrapResponse = spy(new AuthManager.OnBootstrapResponse() {
            @Override
            public void onResponse(int result) {
                // Verify
                Assert.assertEquals("Should succeed", AuthManager.OnBootstrapResponse.SUCCESS, result);
                android.accounts.Account[] accountList = authManager.getAccountsByType();
                Assert.assertEquals(TestValues.TEST_JWT_CLAIM_SUB_VAL, accountList[0].name);
                Account account = (Account) authManager.getAllAccounts().get(0);
                Assert.assertNotEquals(updatedRefreshToken, account.getRefreshToken());
                Assert.assertEquals(TestValues.TEST_REFRESH_TOKEN, account.getRefreshToken());
            }
        });

        AccountNetworkAPI accountNetworkAPI = AccountNetworkAPI.getInstance(mContext);

        doReturn(GET_USER_INFO_VALID_RESPONSE).when(accountNetworkAPI).executeGet(any(Context.class), anyString(), any(Headers.class));

        // Test
        authManager.bootstrap(mContext, dataList, onBootStrapResponse);

        // Verify
        verify(onBootStrapResponse).onResponse(AuthManager.OnBootstrapResponse.SUCCESS);
        ArgumentCaptor<Map> customParam = ArgumentCaptor.forClass(Map.class);
        verify(mEventLoggerSpy, times(1)).logUserEvent(Matchers.eq(EVENT_BOOTSTRAP_START), customParam.capture());
        Map<String, Object> realMap = (Map<String, Object>) customParam.getValue();
        assertTrue("Must have the key", realMap.containsKey(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));
        assertEquals(1, realMap.get(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));

        List<IAccount> allAccounts = authManager.getAllAccounts();
        assertEquals("Account should still be accessible", 1, allAccounts.size());
        IAccount account = allAccounts.get(0);
        assertEquals("Refresh token must not be updated", TestValues.TEST_REFRESH_TOKEN, ((Account)account).getRefreshToken());
        assertTrue("The account should be in active state", account.isActive());

        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_START, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_SUCCESS, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_SUCCESS, null);
    }


    @Test
    public void bootstrap_WhenRefreshTokenReturnEmptyAccessToken_ShouldAddDisabledAccountAndReturnFailure() throws Exception {
        initAuthConfig();

        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        AuthManager.BootstrapData bootStrapData = new AuthManager.BootstrapData(TestValues.TEST_JWT_CLAIM_SUB_VAL, TestValues.TEST_REFRESH_TOKEN, TestValues.TEST_JWT_NAME_VALUE, TestValues.TEST_JWT_EMAIL_VALUE);
        List<AuthManager.BootstrapData> dataList = new ArrayList<>();
        dataList.add(bootStrapData);

        mockRefreshTokenResponse(invocation -> {
            AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
            asyncCallback.onSuccess(new TokenResponse.Builder(getTestAuthCodeExchangeRequest())
                    .setTokenType(TokenResponse.TOKEN_TYPE_BEARER)
                    .setRefreshToken(TEST_REFRESH_TOKEN)
                    // Access Token is missing
                    .setIdToken(TEST_JWT_TOKEN_YAHOO)
                    .setAccessTokenExpirationTime(TEST_ACCESS_TOKEN_EXPIRATION_TIME)
                    .build()
                    .jsonSerializeString());
            return null;
        });

        mockSingletonExecutor();

        AuthManager.OnBootstrapResponse onBootStrapResponse = spy(new AuthManager.OnBootstrapResponse() {
            @Override
            public void onResponse(int result) {
                // Verify
                Assert.assertEquals("Should fail", AuthManager.OnBootstrapResponse.FAILURE, result);
                android.accounts.Account[] accountList = authManager.getAccountsByType();
                Assert.assertEquals(TestValues.TEST_JWT_CLAIM_SUB_VAL, accountList[0].name);
            }
        });

        // Test
        authManager.bootstrap(mContext, dataList, onBootStrapResponse);

        // Verify
        verify(onBootStrapResponse).onResponse(AuthManager.OnBootstrapResponse.FAILURE);
        ArgumentCaptor<Map> customParam = ArgumentCaptor.forClass(Map.class);
        verify(mEventLoggerSpy, times(1)).logUserEvent(Matchers.eq(EVENT_BOOTSTRAP_START), customParam.capture());
        Map<String, Object> realMap = (Map<String, Object>) customParam.getValue();
        assertTrue("Must have the key", realMap.containsKey(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));
        assertEquals(1, realMap.get(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));

        List<IAccount> allAccounts = authManager.getAllAccounts();
        assertEquals("Account should still be accessible", 1, allAccounts.size());
        IAccount account = allAccounts.get(0);
        assertFalse("The account should be inactive state", account.isActive());

        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_START, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_FAILURE, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_FAILURE, null);
    }

    @Test
    public void bootstrap_WhenRefreshTokenReturnEmptyRefreshToken_ShouldReturnSuccess() throws Exception {

        initAuthConfig();

        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        AuthManager.BootstrapData bootStrapData = new AuthManager.BootstrapData(TestValues.TEST_JWT_CLAIM_SUB_VAL, TestValues.TEST_REFRESH_TOKEN, TestValues.TEST_JWT_NAME_VALUE, TestValues.TEST_JWT_EMAIL_VALUE);
        List<AuthManager.BootstrapData> dataList = new ArrayList<>();
        dataList.add(bootStrapData);

        mockRefreshTokenResponse(invocation -> {
            AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
            asyncCallback.onSuccess(new TokenResponse.Builder(getTestAuthCodeExchangeRequest())
                    .setTokenType(TokenResponse.TOKEN_TYPE_BEARER)
                    // Refresh Token is missing
                    .setAccessToken(TEST_ACCESS_TOKEN)
                    .setIdToken(TEST_JWT_TOKEN_YAHOO)
                    .setAccessTokenExpirationTime(TEST_ACCESS_TOKEN_EXPIRATION_TIME)
                    .build()
                    .jsonSerializeString());
            return null;
        });

        mockSingletonExecutor();

        AuthManager.OnBootstrapResponse onBootStrapResponse = spy(new AuthManager.OnBootstrapResponse() {
            @Override
            public void onResponse(int result) {
                // Verify
                Assert.assertEquals("Should success", AuthManager.OnBootstrapResponse.SUCCESS, result);
                android.accounts.Account[] accountList = authManager.getAccountsByType();
                Assert.assertEquals(TestValues.TEST_JWT_CLAIM_SUB_VAL, accountList[0].name);
            }
        });
        AccountNetworkAPI accountNetworkAPI = AccountNetworkAPI.getInstance(mContext);

        doReturn(GET_USER_INFO_VALID_RESPONSE).when(accountNetworkAPI).executeGet(any(Context.class), anyString(), any(Headers.class));

        // Test
        authManager.bootstrap(mContext, dataList, onBootStrapResponse);

        // Verify
        verify(onBootStrapResponse).onResponse(AuthManager.OnBootstrapResponse.SUCCESS);

        ArgumentCaptor<Map> customParam = ArgumentCaptor.forClass(Map.class);
        verify(mEventLoggerSpy, times(1)).logUserEvent(Matchers.eq(EVENT_BOOTSTRAP_START), customParam.capture());
        Map<String, Object> realMap = (Map<String, Object>) customParam.getValue();
        assertTrue("Must have the key", realMap.containsKey(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));
        assertEquals(1, realMap.get(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));

        List<IAccount> allAccounts = authManager.getAllAccounts();
        assertEquals("Account should still be accessible", 1, allAccounts.size());
        IAccount account = allAccounts.get(0);
        assertTrue("The account should be in active state", account.isActive());

        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_START, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_FETCH_USER_INFO_START, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_FETCH_USER_INFO_SUCCESS, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_SUCCESS, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_SUCCESS, null);
    }

    @Test
    public void bootstrap_WhenThreadInterrupted_ShouldReturnFailure() throws Exception {

        initAuthConfig();

        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        AuthManager.BootstrapData bootStrapData = new AuthManager.BootstrapData(TestValues.TEST_JWT_CLAIM_SUB_VAL, TestValues.TEST_REFRESH_TOKEN, TestValues.TEST_JWT_NAME_VALUE, TestValues.TEST_JWT_EMAIL_VALUE);
        List<AuthManager.BootstrapData> dataList = new ArrayList<>();
        dataList.add(bootStrapData);

        mockRefreshTokenResponse(invocation -> {
            Thread.currentThread().interrupt();
            return null;
        });

        mockSingletonExecutor();

        AuthManager.OnBootstrapResponse onBootStrapResponse = spy(new AuthManager.OnBootstrapResponse() {
            @Override
            public void onResponse(int result) {
                // Verify
                Assert.assertEquals("Should fail", AuthManager.OnBootstrapResponse.FAILURE, result);
                android.accounts.Account[] accountList = authManager.getAccountsByType();
                Assert.assertEquals(TestValues.TEST_JWT_CLAIM_SUB_VAL, accountList[0].name);
            }
        });

        // Test
        authManager.bootstrap(mContext, dataList, onBootStrapResponse);

        // Verify
        verify(onBootStrapResponse).onResponse(AuthManager.OnBootstrapResponse.FAILURE);

        ArgumentCaptor<Map> customParam = ArgumentCaptor.forClass(Map.class);
        verify(mEventLoggerSpy, times(1)).logUserEvent(Matchers.eq(EVENT_BOOTSTRAP_START), customParam.capture());
        Map<String, Object> realMap = (Map<String, Object>) customParam.getValue();
        assertTrue("Must have the key", realMap.containsKey(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));
        assertEquals(1, realMap.get(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));

        List<IAccount> allAccounts = authManager.getAllAccounts();
        assertEquals("Account should still be accessible", 1, allAccounts.size());
        IAccount account = allAccounts.get(0);
        assertFalse("The account should be in inactive state", account.isActive());

        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_START, null);
        verify(mEventLoggerSpy, times(1)).logErrorInformationEvent(EVENT_BOOTSTRAP_ERROR, COUNTDOWN_LATCH_WAIT_INTERRUPTED, "bootstrap: wait on the countdown latch interrupted");
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_FAILURE, null);
    }


    @Test
    public void bootstrap_WhenNoNetwork_ShouldReturnFailure() throws Exception {

        initAuthConfig();

        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        AuthManager.BootstrapData bootStrapData = new AuthManager.BootstrapData(TestValues.TEST_JWT_CLAIM_SUB_VAL, TestValues.TEST_REFRESH_TOKEN, TestValues.TEST_JWT_NAME_VALUE, TestValues.TEST_JWT_EMAIL_VALUE);
        List<AuthManager.BootstrapData> dataList = new ArrayList<>();
        dataList.add(bootStrapData);

        mockSingletonExecutor();

        AuthManager.OnBootstrapResponse onBootStrapResponse = spy(new AuthManager.OnBootstrapResponse() {
            @Override
            public void onResponse(int result) {
                // Verify
                Assert.assertEquals("Should fail", AuthManager.OnBootstrapResponse.FAILURE, result);
                android.accounts.Account[] accountList = authManager.getAccountsByType();
                Assert.assertEquals(TestValues.TEST_JWT_CLAIM_SUB_VAL, accountList[0].name);
            }
        });
        TestUtils.setNetworkConnectivity(false);
        // Test
        authManager.bootstrap(mContext, dataList, onBootStrapResponse);

        // Verify
        verify(onBootStrapResponse).onResponse(AuthManager.OnBootstrapResponse.FAILURE);

        ArgumentCaptor<Map> customParam = ArgumentCaptor.forClass(Map.class);
        verify(mEventLoggerSpy, times(1)).logUserEvent(Matchers.eq(EVENT_BOOTSTRAP_START), customParam.capture());
        Map<String, Object> realMap = (Map<String, Object>) customParam.getValue();
        assertTrue("Must have the key", realMap.containsKey(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));
        assertEquals(1, realMap.get(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));

        List<IAccount> allAccounts = authManager.getAllAccounts();
        assertEquals("Account should still be accessible", 1, allAccounts.size());
        IAccount account = allAccounts.get(0);
        assertFalse("The account should be in inactive state", account.isActive());

        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_START, null);
        verify(mEventLoggerSpy, times(1)).logErrorInformationEvent(EVENT_REFRESH_TOKEN_CLIENT_ERROR, NETWORK_ISSUE, "No network");
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_FAILURE, null);

        TestUtils.setNetworkConnectivity(true);
    }

    @Test
    public void bootstrap_WhenBootstrapDataHasNoGuid_ShouldReturnFailure() throws Exception {

        initAuthConfig();

        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        AuthManager.BootstrapData bootStrapData1 = new AuthManager.BootstrapData(null, TestValues.TEST_REFRESH_TOKEN, TestValues.TEST_JWT_NAME_VALUE, TestValues.TEST_JWT_EMAIL_VALUE);
        AuthManager.BootstrapData bootStrapData2 = new AuthManager.BootstrapData(TestValues.TEST_JWT_CLAIM_SUB_VAL, TestValues.TEST_REFRESH_TOKEN, TestValues.TEST_JWT_NAME_VALUE, TestValues.TEST_JWT_EMAIL_VALUE);
        List<AuthManager.BootstrapData> dataList = new ArrayList<>();
        dataList.add(bootStrapData1);
        dataList.add(bootStrapData2);

        mockRefreshTokenResponse(invocation -> {
            AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
            asyncCallback.onSuccess(TestValues.getTestAuthCodeExchangeResponse().jsonSerializeString());
            return null;
        });

        mockSingletonExecutor();

        AuthManager.OnBootstrapResponse onBootStrapResponse = spy(new AuthManager.OnBootstrapResponse() {
            @Override
            public void onResponse(int result) {
                // Verify
                Assert.assertEquals("Should fail", AuthManager.OnBootstrapResponse.FAILURE, result);
                List<IAccount> accountList = authManager.getAllAccounts();
                Assert.assertFalse("Should be not be empty list", accountList.isEmpty());
                Assert.assertEquals("Should have 1 account", 1, accountList.size());
            }
        });
        AccountNetworkAPI accountNetworkAPI = AccountNetworkAPI.getInstance(mContext);

        doReturn(GET_USER_INFO_VALID_RESPONSE).when(accountNetworkAPI).executeGet(any(Context.class), anyString(), any(Headers.class));

        // Test
        authManager.bootstrap(mContext, dataList, onBootStrapResponse);

        // Verify
        verify(onBootStrapResponse).onResponse(AuthManager.OnBootstrapResponse.FAILURE);

        ArgumentCaptor<Map> customParam = ArgumentCaptor.forClass(Map.class);
        verify(mEventLoggerSpy, times(1)).logUserEvent(Matchers.eq(EVENT_BOOTSTRAP_START), customParam.capture());
        Map<String, Object> realMap = (Map<String, Object>) customParam.getValue();
        assertTrue("Must have the key", realMap.containsKey(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));
        assertEquals(2, realMap.get(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));

        List<IAccount> allAccounts = authManager.getAllAccounts();
        assertEquals("Account should still be accessible", 1, allAccounts.size());
        IAccount account = allAccounts.get(0);
        assertTrue("The account should be in active state", account.isActive());

        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_START, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_ADD_ACCOUNT_FAILURE, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_FAILURE, null);
    }

    @Test
    public void bootstrap_WhenAddExistingAccount_ShouldReturnExistingAccountAndUpdateUserData() throws Exception {
        initAuthConfig();

        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        AuthManager.BootstrapData bootStrapData = new AuthManager.BootstrapData(TestValues.TEST_JWT_CLAIM_SUB_VAL, TestValues.TEST_REFRESH_TOKEN, TestValues.TEST_JWT_NAME_VALUE, TestValues.TEST_JWT_EMAIL_VALUE);
        List<AuthManager.BootstrapData> dataList = new ArrayList<>();
        dataList.add(bootStrapData);

        mockRefreshTokenResponse(invocation -> {
            AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
            asyncCallback.onSuccess(TestValues.getTestAuthCodeExchangeResponse().jsonSerializeString());
            return null;
        });

        mockSingletonExecutor();

        AuthManager.OnBootstrapResponse onBootStrapResponseFirst = spy(new AuthManager.OnBootstrapResponse() {
            @Override
            public void onResponse(int result) {
                Assert.assertEquals("Should succeed", AuthManager.OnBootstrapResponse.SUCCESS, result);
                List<IAccount> accountList = authManager.getAllAccounts();
                Assert.assertEquals(TestValues.TEST_JWT_CLAIM_SUB_VAL, accountList.get(0).getGUID());
                Assert.assertEquals("Frank Underwood", accountList.get(0).getDisplayName());
                Assert.assertEquals("try.myemail", accountList.get(0).getUserName());
            }
        });

        AccountNetworkAPI accountNetworkAPI = AccountNetworkAPI.getInstance(mContext);

        doReturn(GET_USER_INFO_VALID_RESPONSE).when(accountNetworkAPI).executeGet(any(Context.class), anyString(), any(Headers.class));

        // First
        authManager.bootstrap(mContext, dataList, onBootStrapResponseFirst);
        dataList.clear();

         final String GET_USER_INFO_VALID_RESPONSE_NEW = "{\n" +
                "\t\"response\": {\n" +
                "\t\t\"data\": {\n" +
                "\t\t\t\"userData\": {\n" +
                "\t\t\t\t\"loginId\": \"try.myemailnew\",\n" +
                "\t\t\t\t\"md5Verified\": false,\n" +
                "\t\t\t\t\"lastAuth\": \"1512363832000\",\n" +
                "\t\t\t\t\"displayName\": \"TestG TestFnew\",\n" +
                "\t\t\t\t\"OPENAUTH_LOGIN_TYPE\": \"OAUTH\",\n" +
                "\t\t\t\t\"attributes\": {\n" +
                "\t\t\t\t\t\"guid\": \"test@example.com\",\n" +
                "\t\t\t\t\t\"given_name\": \"TestG\",\n" +
                "\t\t\t\t\t\"family_name\": \"TestFnew\",\n" +
                "\t\t\t\t\t\"email\": \"" + TestValues.TEST_JWT_EMAIL_VALUE + "new" + "\"\n" +
                "\t\t\t\t}\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t\"statusText\": \"OK\",\n" +
                "\t\t\"statusCode\": 200\n" +
                "\t}\n" +
                "}";



        mockRefreshTokenResponse(invocation -> {
            AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
            asyncCallback.onSuccess(TestValues.getTestAuthCodeExchangeResponse().jsonSerializeString());
            return null;
        });
        AccountNetworkAPI accountNetworkAPI2 = AccountNetworkAPI.getInstance(mContext);

        doReturn(GET_USER_INFO_VALID_RESPONSE_NEW).when(accountNetworkAPI2).executeGet(any(Context.class), anyString(), any(Headers.class));

        String newName = TestValues.TEST_JWT_NAME_VALUE + "new";
        String loginId = "try.myemail" + "new";
        bootStrapData = new AuthManager.BootstrapData(TestValues.TEST_JWT_CLAIM_SUB_VAL, TestValues.TEST_REFRESH_TOKEN, newName, loginId);
        dataList.add(bootStrapData);

        AuthManager.OnBootstrapResponse onBootStrapResponseSecond = spy(new AuthManager.OnBootstrapResponse() {
            @Override
            public void onResponse(int result) {
                Assert.assertEquals("Should succeed", AuthManager.OnBootstrapResponse.SUCCESS, result);
                List<IAccount> accountList = authManager.getAllAccounts();
                Assert.assertEquals(TestValues.TEST_JWT_CLAIM_SUB_VAL, accountList.get(0).getGUID());
                Assert.assertEquals(TestValues.TEST_ACCESS_TOKEN, accountList.get(0).getToken());
                Assert.assertEquals(newName, accountList.get(0).getDisplayName());
                Assert.assertEquals(loginId, accountList.get(0).getUserName());
            }
        });
        // Actual test
        authManager.bootstrap(mContext, dataList, onBootStrapResponseSecond);

        verify(onBootStrapResponseFirst).onResponse(AuthManager.OnBootstrapResponse.SUCCESS);
        verify(onBootStrapResponseSecond).onResponse(AuthManager.OnBootstrapResponse.SUCCESS);

        ArgumentCaptor<Map> customParam = ArgumentCaptor.forClass(Map.class);
        verify(mEventLoggerSpy, times(2)).logUserEvent(Matchers.eq(EVENT_BOOTSTRAP_START), customParam.capture());
        Map<String, Object> realMap = (Map<String, Object>) customParam.getValue();
        assertTrue("Must have the key", realMap.containsKey(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));
        assertEquals(1, realMap.get(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));

        List<IAccount> allAccounts = authManager.getAllAccounts();
        assertEquals("Account should still be accessible", 1, allAccounts.size());
        IAccount account = allAccounts.get(0);
        assertTrue("The account should be in active state", account.isActive());

        verify(mEventLoggerSpy, times(2)).logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_START, null);
        verify(mEventLoggerSpy, times(2)).logUserEvent(EVENT_BOOTSTRAP_FETCH_USER_INFO_START, null);
        verify(mEventLoggerSpy, times(2)).logUserEvent(EVENT_BOOTSTRAP_FETCH_USER_INFO_SUCCESS, null);
        verify(mEventLoggerSpy, times(2)).logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_SUCCESS, null);
        verify(mEventLoggerSpy, times(2)).logUserEvent(EVENT_BOOTSTRAP_SUCCESS, null);
    }

    @Test
    public void bootstrap_WhenRefreshTokenSucceedsAndGetUserInfoSucceeds_ShouldReturnSuccess() throws Exception {
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        AuthManager.BootstrapData bootStrapData = new AuthManager.BootstrapData("test@example.com", TestValues.TEST_REFRESH_TOKEN, "Frank Underwood", "try.myemail@aol.com");
        List<AuthManager.BootstrapData> dataList = new ArrayList<>();
        dataList.add(bootStrapData);

        mockRefreshTokenResponse(invocation -> {
            AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
            asyncCallback.onSuccess(TestValues.getTestAuthCodeExchangeResponse().jsonSerializeString());
            return null;
        });

        mockSingletonExecutor();

        AuthManager.OnBootstrapResponse onBootStrapResponse = spy(new AuthManager.OnBootstrapResponse() {
            @Override
            public void onResponse(int result) {
                Assert.assertEquals("Should succeed", AuthManager.OnBootstrapResponse.SUCCESS, result);
                List<IAccount> accountList = authManager.getAllAccounts();
                Assert.assertEquals("test@example.com", accountList.get(0).getGUID());
                Assert.assertTrue("Account should be logged in", ((Account) accountList.get(0)).isLoggedIn());
            }
        });

        //Already mocked
        AccountNetworkAPI accountNetworkAPI = AccountNetworkAPI.getInstance(mContext);

        doReturn(GET_USER_INFO_VALID_RESPONSE).when(accountNetworkAPI).executeGet(any(Context.class), anyString(), any(Headers.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPI);

        // Test
        authManager.bootstrap(mContext, dataList, onBootStrapResponse);

        verify(onBootStrapResponse).onResponse(AuthManager.OnBootstrapResponse.SUCCESS);

        ArgumentCaptor<Map> customParam = ArgumentCaptor.forClass(Map.class);
        verify(mEventLoggerSpy, times(1)).logUserEvent(Matchers.eq(EVENT_BOOTSTRAP_START), customParam.capture());
        Map<String, Object> realMap = (Map<String, Object>) customParam.getValue();
        assertTrue("Must have the key", realMap.containsKey(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));
        assertEquals(1, realMap.get(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));

        List<IAccount> allAccounts = authManager.getAllAccounts();
        assertEquals("Account should still be accessible", 1, allAccounts.size());
        IAccount account = allAccounts.get(0);
        assertTrue("The account should be in active state", account.isActive());

        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_START, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_FETCH_USER_INFO_START, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_FETCH_USER_INFO_SUCCESS, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_SUCCESS, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_SUCCESS, null);
    }

    @Test
    public void bootstrap_WhenRefreshTokenSucceedsAndGetUserInfoFails_ShouldReturnFailure() throws Exception {
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        AuthManager.BootstrapData bootStrapData = new AuthManager.BootstrapData("ji00000000367215", TestValues.TEST_REFRESH_TOKEN, "Frank Underwood", "try.myemail@aol.com");
        List<AuthManager.BootstrapData> dataList = new ArrayList<>();
        dataList.add(bootStrapData);

        mockRefreshTokenResponse(invocation -> {
            AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
            asyncCallback.onSuccess(TestValues.getTestAuthCodeExchangeResponse().jsonSerializeString());
            return null;
        });

        mockSingletonExecutor();

        AuthManager.OnBootstrapResponse onBootStrapResponse = spy(new AuthManager.OnBootstrapResponse() {
            @Override
            public void onResponse(int result) {
                Assert.assertEquals("Should succeed", AuthManager.OnBootstrapResponse.FAILURE, result);
                List<IAccount> accountList = authManager.getAllAccounts();
                Assert.assertEquals("ji00000000367215", accountList.get(0).getGUID());
                Assert.assertTrue("Account should be logged in", ((Account) accountList.get(0)).isLoggedIn());
            }
        });

        //Already mocked
        AccountNetworkAPI accountNetworkAPI = AccountNetworkAPI.getInstance(mContext);

        doThrow(mock(HttpConnectionException.class)).when(accountNetworkAPI).executeGet(any(Context.class), anyString(), any(Headers.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPI);

        // Test
        authManager.bootstrap(mContext, dataList, onBootStrapResponse);

        verify(onBootStrapResponse).onResponse(AuthManager.OnBootstrapResponse.FAILURE);

        ArgumentCaptor<Map> customParam = ArgumentCaptor.forClass(Map.class);
        verify(mEventLoggerSpy, times(1)).logUserEvent(Matchers.eq(EVENT_BOOTSTRAP_START), customParam.capture());
        Map<String, Object> realMap = (Map<String, Object>) customParam.getValue();
        assertTrue("Must have the key", realMap.containsKey(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));
        assertEquals(1, realMap.get(EventLogger.BootstrapUserEvents.TRACKING_PARAM_ACCOUNT_NUMBERS));

        List<IAccount> allAccounts = authManager.getAllAccounts();
        assertEquals("Account should still be accessible", 1, allAccounts.size());
        IAccount account = allAccounts.get(0);
        assertFalse("The account should be in inactive state", account.isActive());

        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_START, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_FETCH_USER_INFO_START, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_FETCH_USER_INFO_FAILURE, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_REFRESH_TOKEN_SUCCESS, null);
        verify(mEventLoggerSpy, times(1)).logUserEvent(EVENT_BOOTSTRAP_FAILURE, null);
    }

}
