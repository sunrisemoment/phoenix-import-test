package com.oath.mobile.platform.phoenix.core;

import android.net.Uri;

import net.openid.appauth.AuthorizationRequest;
import net.openid.appauth.AuthorizationResponse;
import net.openid.appauth.AuthorizationServiceConfiguration;
import net.openid.appauth.GrantTypeValues;
import net.openid.appauth.ResponseTypeValues;
import net.openid.appauth.TokenRequest;
import net.openid.appauth.TokenResponse;

/**
 * Created by nsoni on 10/13/17.
 * Contains common test values which are useful across all tests.
 */
class TestValues {

    public static final String TEST_ORIGIN = "test_origin";

    public static final String TEST_CLIENT_ID = "test_client_id";
    public static final String TEST_CLIENT_SECRET = "test_client_secret";
    public static final String TEST_STATE = "$TAT3";
    public static final String TEST_APP_SCHEME = "com.test.app";
    public static final Uri TEST_APP_REDIRECT_URI = Uri.parse(TEST_APP_SCHEME + ":/oidc_callback");
    public static final String TEST_SCOPE = "openid email";

    public static final String TEST_AOL_IDP_ISSUER ="https://testidp.example.aol.com";

    public static final Uri TEST_IDP_AUTH_ENDPOINT =
            Uri.parse("https://testidp.yahoo.com/authorize");
    public static final Uri TEST_IDP_TOKEN_ENDPOINT =
            Uri.parse("https://testidp.yahoo.com/token");

    public static final Uri TEST_AOL_IDP_AUTH_ENDPOINT =
            Uri.parse("https://api.screenname.aol.com/auth/authorize");

    public static final Uri TEST_AOL_IDP_TOKEN_ENDPOINT =
            Uri.parse("https://api.screenname.aol.com/auth/access_token");

    public static final String TEST_CODE_VERIFIER = "0123456789_0123456789_0123456789_0123456789";
    public static final String TEST_AUTH_CODE = "zxcvbnmjk";
    public static final String TEST_ACCESS_TOKEN = "aaabbbccc";
    public static final Long TEST_ACCESS_TOKEN_EXPIRATION_TIME = 120000L; // two minutes
    public static final String TEST_REFRESH_TOKEN = "asdfghjkl";
    public static final String TEST_REFRESH_TOKEN2 = "9348fa9uwlje";

    public static final String TEST_JWT_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJKV1QiLCJpYXQiOjE1MDk1NzcxODIsImV4cCI6MTU0MTExMzE4OCwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoidGVzdEBleGFtcGxlLmNvbSIsIkFscGhhIjoiVGVzdCIsIkJldGEiOiJUZXN0MSIsIlRoZXRhIjoiVGVzdDIiLCJnaXZlbl9uYW1lIjoidGVzdG5hbWUiLCJmYW1pbHlfbmFtZSI6InRlc3RmYW1pbHluYW1lIiwibmFtZSI6InRlc3REaXNwbGF5TmFtZSIsImVtYWlsIjoidGVzdEVtYWlsIn0.Aklk7QMvotTELJDbEVjm4jrTY5kzLD-8WUq4y-ft7ms";
    public static final String TEST_JWT_SECRET = "test";
    public static final String TEST_JWT_CLAIM_KEY_ONE = "Alpha";
    public static final String TEST_JWT_CLAIM_KEY_TWO = "Beta";
    public static final String TEST_JWT_CLAIM_KEY_THREE = "Theta";
    public static final String TEST_JWT_CLAIM_VALUE_ONE = "Test";
    public static final String TEST_JWT_CLAIM_VALUE_TWO = "Test1";
    public static final String TEST_JWT_CLAIM_VALUE_THREE = "Test2";
    public static final String TEST_JWT_CLAIM_ISS = "iss";
    public static final String TEST_JWT_CLAIM_ISS_VAL = "JWT";
    public static final String TEST_JWT_CLAIM_IAT = "iat";
    public static final long TEST_JWT_CLAIM_IAT_VAL = 1509577182;
    public static final String TEST_JWT_CLAIM_EXP = "exp";
    public static final long TEST_JWT_CLAIM_EXP_VAL = 1541113188;
    public static final String TEST_JWT_CLAIM_AUD = "aud";
    public static final String TEST_JWT_CLAIM_AUD_VAL = "www.example.com";
    public static final String TEST_JWT_CLAIM_SUB = "sub";
    public static final String TEST_JWT_CLAIM_SUB_VAL = "test@example.com";
    public static final String TEST_JWT_TOKEN_INVALID = "asdf1234";
    public static final String TEST_JWT_TOKEN_BAD_PAYLOAD = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9" +
            ".eyJpc3MiOiJKV1QiLCJpYXQiOjE1MDk1NzcxODIsImV4cCI6MTU0MTExMzE4OCwiYXVkIjoid3d3LmV4YW1" +
            "wbGUuY29tIiwic3ViIjoidGVzdEBleGFtcGxlLmNvbSIsIkFscGhhIjoiVGVzdCIsIkJldGEiOiJUZXN0MSI" +
            "sIlRoZXRhIjoiVGVzdDIifQswq2xaw1dw2.kDIncgwSvTxROtw4iL963Z9ZIoye7tudoEGUUPCMSZA";
    public static final String TEST_JWT_TOKEN_BAD_JSON = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.c3" +
            "MiOiJKV1QiLCJpYXQiOjE1MDk1NzcxODIsImV4cCI6MTU0MTExMzE4OCwiYXVkIjoid3d3LmV4YW1wbGUuY2" +
            "9tIiwic3ViIjoidGVzdEBleGFtcGxlLmNvbSIsIkFscGhhIjoiVGVzdCIsIkJldGEiOiJUZXN0MSIsIlRoZX" +
            "RhIjoiVGVzdDIifQswq2xaw1dw2s.kDIncgwSvTxROtw4iL963Z9ZIoye7tudoEGUUPCMSZA";
    public static final String TEST_ID_TOKEN_JWT = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3Mi" +
            "OiJKV1QiLCJpYXQiOjE1MDk1NzcxODIsImV4cCI6MTU0MTExMzE4OCwiYXVkIjoid3d3LmV4YW1wbGUuY29t" +
            "Iiwic3ViIjoidGVzdEBleGFtcGxlLmNvbSIsIkFscGhhIjoiVGVzdCIsIkJldGEiOiJUZXN0MSIsIlRoZXRh" +
            "IjoiVGVzdDIiLCJub25jZSI6Im5vbmNlVGVzdCIsImF0X2hhc2giOiJoYXNoVGVzdCJ9.wn222Pcstodq-Iq" +
            "YM-ESEMY-ih201FvgexsKO779kOU";
    public static final String TEST_ID_TOKEN_WITH_LOGIN_ID_JWT = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJKV1QiLCJpYXQiOjE1MDk1NzcxODIsImV4cCI6MTU0MTExMzE4OCwibG9naW5faWQiOiJ0ZXN0IiwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoidGVzdEBleGFtcGxlLmNvbSIsIkFscGhhIjoiVGVzdCIsIkJldGEiOiJUZXN0MSIsIlRoZXRhIjoiVGVzdDIiLCJub25jZSI6Im5vbmNlVGVzdCIsImF0X2hhc2giOiJoYXNoVGVzdCJ9.LgAjS4LRULxXeSRofQb5KnReAUwJSkOiERgvugbYxWU";
    public static final String TEST_JWT_NONCE_VALUE = "nonceTest";
    public static final String TEST_JWT_AT_HASH_VALUE = "hashTest";

    public static final String TEST_JWT_TOKEN_YAHOO_WITH_LEGACY_GUID = "eyJhbGciOiJFUzI1NiIsImtpZCI6IjM0NjZkNTFmN2RkMGM3ODA1NjU2ODhjMTgzOTIxODE2YzQ1ODg5YWQifQ.eyJhdF9oYXNoIjoiaGFzaFRlc3QiLCJzdWIiOiJyYW5kb21fY3VycmVudF9ndWlkIiwiYW9sX2xlZ2FjeV9ndWlkIjoidGVzdEBleGFtcGxlLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJiaXJ0aGRhdGUiOiIxOTgwIiwiaXNzIjoiaHR0cHM6Ly9hcGkubG9naW4ueWFob28uY29tIiwicHJvZmlsZV9pbWFnZXMiOnsiaW1hZ2U2NCI6Imh0dHBzOi8vd3MucHJvZ3Jzcy55YWhvby5jb20vcHJvZ3Jzcy92MS91c2VyL3Rlc3QxL3Byb2ZpbGUvcGljdHVyZT8uaW1nc2l6ZT02NHg2NCIsImltYWdlMTkyIjoiaHR0cHM6Ly93cy5wcm9ncnNzLnlhaG9vLmNvbS9wcm9ncnNzL3YxL3VzZXIvdGVzdDEvcHJvZmlsZS9waWN0dXJlPy5pbWdzaXplPTE5MngxOTIiLCJpbWFnZTEyOCI6Imh0dHBzOi8vd3MucHJvZ3Jzcy55YWhvby5jb20vcHJvZ3Jzcy92MS91c2VyL3Rlc3QxL3Byb2ZpbGUvcGljdHVyZT8uaW1nc2l6ZT0xMjh4MTI4IiwiaW1hZ2UzMiI6Imh0dHBzOi8vd3MucHJvZ3Jzcy55YWhvby5jb20vcHJvZ3Jzcy92MS91c2VyL3Rlc3QxL3Byb2ZpbGUvcGljdHVyZT8uaW1nc2l6ZT0zMngzMiJ9LCJsb2NhbGUiOiJlbi1VUyIsImdpdmVuX25hbWUiOiJUZXN0RyIsIm5vbmNlIjoibm9uY2VUZXN0IiwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwiYXV0aF90aW1lIjoxNTEwMjYyNjk3LCJuYW1lIjoiVGVzdEcgVGVzdEYiLCJzZXNzaW9uX2V4cCI6MTUxMTQ3MjI5NywiZXhwIjoxNTEwMjY2Mjk5LCJpYXQiOjE1MTAyNjI2OTksImZhbWlseV9uYW1lIjoiVGVzdEYiLCJlbWFpbCI6InRlc3RAeWFob28uY29tIn0.0Ems4WOUGtK8_PFw-I7L98pIc6JiaE32iTtEQwEoF0I";
    public static final String TEST_JWT_TOKEN_YAHOO = "eyJhbGciOiJFUzI1NiIsImtpZCI6IjM0NjZkNTFmN2RkMGM3ODA1NjU2ODhjMTgzOTIxODE2YzQ1ODg5YWQifQ.eyJhdF9oYXNoIjoiaGFzaFRlc3QiLCJzdWIiOiJ0ZXN0QGV4YW1wbGUuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImJpcnRoZGF0ZSI6IjE5ODAiLCJpc3MiOiJodHRwczovL2FwaS5sb2dpbi55YWhvby5jb20iLCJwcm9maWxlX2ltYWdlcyI6eyJpbWFnZTY0IjoiaHR0cHM6Ly93cy5wcm9ncnNzLnlhaG9vLmNvbS9wcm9ncnNzL3YxL3VzZXIvdGVzdDEvcHJvZmlsZS9waWN0dXJlPy5pbWdzaXplPTY0eDY0IiwiaW1hZ2UxOTIiOiJodHRwczovL3dzLnByb2dyc3MueWFob28uY29tL3Byb2dyc3MvdjEvdXNlci90ZXN0MS9wcm9maWxlL3BpY3R1cmU_LmltZ3NpemU9MTkyeDE5MiIsImltYWdlMTI4IjoiaHR0cHM6Ly93cy5wcm9ncnNzLnlhaG9vLmNvbS9wcm9ncnNzL3YxL3VzZXIvdGVzdDEvcHJvZmlsZS9waWN0dXJlPy5pbWdzaXplPTEyOHgxMjgiLCJpbWFnZTMyIjoiaHR0cHM6Ly93cy5wcm9ncnNzLnlhaG9vLmNvbS9wcm9ncnNzL3YxL3VzZXIvdGVzdDEvcHJvZmlsZS9waWN0dXJlPy5pbWdzaXplPTMyeDMyIn0sImxvY2FsZSI6ImVuLVVTIiwiZ2l2ZW5fbmFtZSI6IlRlc3RHIiwibm9uY2UiOiJub25jZVRlc3QiLCJhdWQiOiJ3d3cuZXhhbXBsZS5jb20iLCJhdXRoX3RpbWUiOjE1MTAyNjI2OTcsIm5hbWUiOiJUZXN0RyBUZXN0RiIsInNlc3Npb25fZXhwIjoxNTExNDcyMjk3LCJleHAiOjE1MTAyNjYyOTksImlhdCI6MTUxMDI2MjY5OSwiZmFtaWx5X25hbWUiOiJUZXN0RiIsImVtYWlsIjoidGVzdEB5YWhvby5jb20ifQ.nnIKlxUYN4bb9IBQAWpuxKiBYUKcw4EGfG48V8iNVhE";
    public static final String TEST_JWT_TOKEN_YAHOO_NONCE = "nonceTest";
    public static final String TEST_JWT_TOKEN_YAHOO_WITH_UPDATED_NANE = "eyJhbGciOiJFUzI1NiIsImtpZCI6IjM0NjZkNTFmN2RkMGM3ODA1NjU2ODhjMTgzOTIxODE2YzQ1ODg5YWQifQ.eyJhdF9oYXNoIjoiaGFzaFRlc3QiLCJzdWIiOiJ0ZXN0QGV4YW1wbGUuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImJpcnRoZGF0ZSI6IjE5ODAiLCJpc3MiOiJodHRwczovL2FwaS5sb2dpbi55YWhvby5jb20iLCJwcm9maWxlX2ltYWdlcyI6eyJpbWFnZTY0IjoiaHR0cHM6Ly93cy5wcm9ncnNzLnlhaG9vLmNvbS9wcm9ncnNzL3YxL3VzZXIvdGVzdDEvcHJvZmlsZS9waWN0dXJlPy5pbWdzaXplPTY0eDY0IiwiaW1hZ2UxOTIiOiJodHRwczovL3dzLnByb2dyc3MueWFob28uY29tL3Byb2dyc3MvdjEvdXNlci90ZXN0MS9wcm9maWxlL3BpY3R1cmU_LmltZ3NpemU9MTkyeDE5MiIsImltYWdlMTI4IjoiaHR0cHM6Ly93cy5wcm9ncnNzLnlhaG9vLmNvbS9wcm9ncnNzL3YxL3VzZXIvdGVzdDEvcHJvZmlsZS9waWN0dXJlPy5pbWdzaXplPTEyOHgxMjgiLCJpbWFnZTMyIjoiaHR0cHM6Ly93cy5wcm9ncnNzLnlhaG9vLmNvbS9wcm9ncnNzL3YxL3VzZXIvdGVzdDEvcHJvZmlsZS9waWN0dXJlPy5pbWdzaXplPTMyeDMyIn0sImxvY2FsZSI6ImVuLVVTIiwiZ2l2ZW5fbmFtZSI6IlRlc3RIIiwibm9uY2UiOiJub25jZVRlc3QiLCJhdWQiOiJ3d3cuZXhhbXBsZS5jb20iLCJhdXRoX3RpbWUiOjE1MTAyNjI2OTcsIm5hbWUiOiJUZXN0SCBUZXN0SSIsInNlc3Npb25fZXhwIjoxNTExNDcyMjk3LCJleHAiOjE1MTAyNjYyOTksImlhdCI6MTUxMDI2MjY5OSwiZmFtaWx5X25hbWUiOiJUZXN0SSIsImVtYWlsIjoidGVzdEB5YWhvby5jb20ifQ.UDG7WyGfc6dBpZfvbOuGq-9HLi9fzuEY8oPzy-TYNJk";
    public static final String TEST_JWT_UPDATED_GIVEN_NAME_VALUE = "TestH";
    public static final String TEST_JWT_TOKEN_YAHOO2 = "eyJhbGciOiJFUzI1NiIsImtpZCI6IjM0NjZkNTFmN2RkMGM3ODA1NjU2ODhjMTgzOTIxODE2YzQ1ODg5YWQifQ.eyJhdF9oYXNoIjoiaGFzaFRlc3QiLCJzdWIiOiJ0ZXN0X2FjY291bnQyQGV4YW1wbGUuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImJpcnRoZGF0ZSI6IjE5ODAiLCJpc3MiOiJodHRwczovL2FwaS5sb2dpbi55YWhvby5jb20iLCJwcm9maWxlX2ltYWdlcyI6eyJpbWFnZTY0IjoiaHR0cHM6Ly93cy5wcm9ncnNzLnlhaG9vLmNvbS9wcm9ncnNzL3YxL3VzZXIvdGVzdDEvcHJvZmlsZS9waWN0dXJlPy5pbWdzaXplPTY0eDY0IiwiaW1hZ2UxOTIiOiJodHRwczovL3dzLnByb2dyc3MueWFob28uY29tL3Byb2dyc3MvdjEvdXNlci90ZXN0MS9wcm9maWxlL3BpY3R1cmU_LmltZ3NpemU9MTkyeDE5MiIsImltYWdlMTI4IjoiaHR0cHM6Ly93cy5wcm9ncnNzLnlhaG9vLmNvbS9wcm9ncnNzL3YxL3VzZXIvdGVzdDEvcHJvZmlsZS9waWN0dXJlPy5pbWdzaXplPTEyOHgxMjgiLCJpbWFnZTMyIjoiaHR0cHM6Ly93cy5wcm9ncnNzLnlhaG9vLmNvbS9wcm9ncnNzL3YxL3VzZXIvdGVzdDEvcHJvZmlsZS9waWN0dXJlPy5pbWdzaXplPTMyeDMyIn0sImxvY2FsZSI6ImVuLVVTIiwiZ2l2ZW5fbmFtZSI6IlRlc3RHMiIsIm5vbmNlIjoibm9uY2VUZXN0IiwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwiYXV0aF90aW1lIjoxNTEwMjYyNjk3LCJuYW1lIjoiVGVzdEcyIFRlc3RGMiIsInNlc3Npb25fZXhwIjoxNTExNDcyMjk3LCJleHAiOjE1MTAyNjYyOTksImlhdCI6MTUxMDI2MjY5OSwiZmFtaWx5X25hbWUiOiJUZXN0RjIiLCJlbWFpbCI6InRlc3QyQHlhaG9vLmNvbSJ9.Ny7EH9gazUkeU6YuIHJu5r_o2GWmLghZrfZHFGPH8ck";
    public static final String TEST_JWT_NAME_VALUE = "TestG TestF";
    public static final String TEST_JWT_GIVEN_NAME_VALUE = "TestG";
    public static final String TEST_JWT_YAHOO_ISS = "https://api.login.yahoo.com";
    public static final String TEST_JWT_FAMILY_NAME_VALUE = "TestF";
    public static final String TEST_JWT_EMAIL_VALUE = "test@yahoo.com";
    public static final String TEST_JWT_EMAIL_VALUE2 = "test_account2@yahoo.com";
    public static final String TEST_JWT_LOGIN_ID_VALUE = "test";
    public static final String TEST_JWT_LOCALE = "en-US";
    public static final boolean TEST_JWT_EMAIL_VERIFIED_VALUE = true;
    public static final long TEST_JWT_YAHOO_EXP = 1510266299;
    public static final long TEST_JWT_YAHOO_IAT = 1510262699;
    public static final String TEST_JWT_IMAGE192 = "https://ws.progrss.yahoo.com/progrss/v1/user/test1/profile/picture?.imgsize=192x192";

    public static final String TEST_VALID_FORM_ENCODED_DATA = "refresh_token=testToken&grant_type=refresh_token&scope=openid%20sdpp-w&nonce=testNonce&client_id=testClientId";
    public static final String TEST_VALID_RESPONSE_BODY = "{\n" +
            "\t\"access_token\": \"testToken\",\n" +
            "\t\"refresh_token\": \"testRefreshToken\",\n" +
            "\t\"expires_in\": 3600,\n" +
            "\t\"token_type\": \"bearer\",\n" +
            "\t\"xoauth_yahoo_guid\": \"testGUID\"\n" +
            "}";
    public static final String TEST_INVALID_RESPONSE_BODY_EMPTY_ACCESS_TOKEN = "{\n" +
            "\t\"access_token\": \"\",\n" +
            "\t\"refresh_token\": \"\",\n" +
            "\t\"expires_in\": 3600,\n" +
            "\t\"token_type\": \"bearer\",\n" +
            "\t\"xoauth_yahoo_guid\": \"testGUID\"\n" +
            "}";
    public static final String TEST_VALID_ERROR_RESPONSE_BODY = "{\n" +
            "\t\"error\": \"Bad Token\"\n" +
            "}";
    public static final String TEST_VALID_INTERNAL_SERVER_ERROR_RESPONSE_BODY = "{\n" +
            "\t\"error\": \"Internal Server Error\"\n" +
            "}";
    public static final String TEST_VALID_OAUTH_INVALID_GRANT_ERROR = "{\n" +
            "\t\"error\": \"invalid_grant\"\n" +
            "}";
    public static final String TEST_VALID_OAUTH_IDP_SWITCH_ERROR = "{\n" +
            "\t\"error\": \"refresh_token_invalid_idp_switched\"\n" +
            "}";
    public static final String TEST_VALID_OAUTH_INVALID_REQUEST_ERROR = "{\n" +
            "\t\"error\": \"invalid_request\"\n" +
            "}";
    public static final String TEST_VALID_OAUTH_INVALID_CLIENT_ERROR = "{\n" +
            "\t\"error\": \"invalid_client\"\n" +
            "}";
    public static final String TEST_VALID_OAUTH_UNAUTHORIZED_CLIENT_ERROR = "{\n" +
            "\t\"error\": \"unauthorized_client\"\n" +
            "}";
    public static final String TEST_VALID_OAUTH_UNSUPPORTED_GRANT_TYPE_ERROR = "{\n" +
            "\t\"error\": \"unsupported_grant_type\"\n" +
            "}";
    public static final String TEST_VALID_OAUTH_INVALID_SCOPE_ERROR = "{\n" +
            "\t\"error\": \"invalid_scope\"\n" +
            "}";
    public static final String TEST_INVALID_OAUTH_ERROR = "{\n" +
            "\t\"error\": \"giant_fiery_chipmunk\"\n" +
            "}";
    public static final String TEST_INVALID_ERROR = "{\n" +
            "\t\"chipmunk\": \"giant_fiery\"\n" +
            "}";


    public static AuthorizationServiceConfiguration getTestServiceConfig() {
        return new AuthorizationServiceConfiguration(
                TEST_IDP_AUTH_ENDPOINT,
                TEST_IDP_TOKEN_ENDPOINT, null);
    }

    public static AuthorizationRequest.Builder getMinimalAuthRequestBuilder(String responseType) {
        return new AuthorizationRequest.Builder(
                getTestServiceConfig(),
                TEST_CLIENT_ID,
                responseType,
                TEST_APP_REDIRECT_URI);
    }

    public static AuthorizationRequest.Builder getTestAuthRequestBuilder() {
        return getMinimalAuthRequestBuilder(ResponseTypeValues.CODE)
                .setScopes(AuthorizationRequest.Scope.OPENID, AuthorizationRequest.Scope.EMAIL)
                .setCodeVerifier(TEST_CODE_VERIFIER);
    }

    public static AuthorizationRequest getTestAuthRequest() {
        return getTestAuthRequestBuilder().build();
    }

    public static AuthorizationResponse.Builder getTestAuthResponseBuilder() {
        AuthorizationRequest req = getTestAuthRequest();
        return new AuthorizationResponse.Builder(req)
                .setState(req.state)
                .setAuthorizationCode(TEST_AUTH_CODE);
    }

    public static AuthorizationResponse getTestAuthResponse() {
        return getTestAuthResponseBuilder().build();
    }

    public static TokenRequest.Builder getMinimalTokenRequestBuilder() {
        return new TokenRequest.Builder(getTestServiceConfig(), TEST_CLIENT_ID);
    }

    public static TokenRequest.Builder getTestAuthCodeExchangeRequestBuilder() {
        return getMinimalTokenRequestBuilder()
                .setAuthorizationCode(TEST_AUTH_CODE)
                .setCodeVerifier(TEST_CODE_VERIFIER)
                .setGrantType(GrantTypeValues.AUTHORIZATION_CODE)
                .setRedirectUri(TEST_APP_REDIRECT_URI);
    }

    public static TokenRequest getTestAuthCodeExchangeRequest() {
        return getTestAuthCodeExchangeRequestBuilder().build();
    }

    public static TokenResponse.Builder getTestAuthCodeExchangeResponseBuilder() {
        return new TokenResponse.Builder(getTestAuthCodeExchangeRequest())
                .setTokenType(TokenResponse.TOKEN_TYPE_BEARER)
                .setRefreshToken(TEST_REFRESH_TOKEN)
                .setAccessToken(TEST_ACCESS_TOKEN)
                .setIdToken(TEST_JWT_TOKEN_YAHOO)
                .setAccessTokenExpirationTime(TEST_ACCESS_TOKEN_EXPIRATION_TIME);
    }

    /**
     * Note that the nonce associated with this is {@link TestValues#TEST_JWT_TOKEN_YAHOO_NONCE}
     * use {@link TestValues#getTestAuthCodeExchangeResponseNonce()} to get it easily
     * @return a simple test auth code exchange response
     */
    public static TokenResponse getTestAuthCodeExchangeResponse() {
        return getTestAuthCodeExchangeResponseBuilder().build();
    }

    public static String getTestAuthCodeExchangeResponseNonce() {
        return TEST_JWT_TOKEN_YAHOO_NONCE;
    }
}
