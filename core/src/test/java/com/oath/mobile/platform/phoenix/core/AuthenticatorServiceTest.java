package com.oath.mobile.platform.phoenix.core;

import android.content.Intent;
import android.os.IBinder;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

/**
 * Created by nsoni on 10/31/17.
 */
@RunWith(RobolectricTestRunner.class)
public class AuthenticatorServiceTest extends BaseTest{
    @Test
    public void onBind_WhenCalled_ShouldReturnIBinder() throws Exception {
        AuthenticatorService service = Robolectric.setupService(AuthenticatorService.class);
        IBinder iBinder = service.onBind(new Intent());
        Assert.assertEquals("android.accounts.IAccountAuthenticator", iBinder.getInterfaceDescriptor());
    }

}
