package com.oath.mobile.platform.phoenix.core;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.lang.reflect.Constructor;

import static junit.framework.Assert.assertEquals;

/**
 * Created by nsoni on 11/3/17.
 */
@RunWith(RobolectricTestRunner.class)
public class ThemeManagerTest extends BaseTest{

    @Test
    public void constructor_WhenInvoked_ShouldThrowException() throws Exception {
        Class<?> themeManager = Class.forName("com.oath.mobile.platform.phoenix.core.ThemeManager");
        try {
            themeManager.newInstance();
        } catch (IllegalAccessException e) {
            assertEquals("class java.lang.IllegalAccessException", e.getClass().toString());
        }
        Constructor<?> constructor = themeManager.getDeclaredConstructor();
        constructor.setAccessible(true); // set it public
        constructor.newInstance(); // invoke it for coverage
    }
}
