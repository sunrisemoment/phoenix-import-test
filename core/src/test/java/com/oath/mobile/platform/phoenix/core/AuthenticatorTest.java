package com.oath.mobile.platform.phoenix.core;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

/**
 * Created by nsoni on 10/31/17.
 */
@RunWith(RobolectricTestRunner.class)
public class AuthenticatorTest extends BaseTest {
    Authenticator mAuthenticator;

    @Before
    public void setup() throws Exception {
        mAuthenticator = new Authenticator(RuntimeEnvironment.application);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void editProperties_WhenCalled_ShouldReturnNull() throws Exception {
        mAuthenticator.editProperties(null, null);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void addAccount() throws Exception {
        mAuthenticator.addAccount(null, null, null, null, null);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void confirmCredentials_WhenCalled_ShouldReturnNull() throws Exception {
        mAuthenticator.confirmCredentials(null, null, null);
    }

    @Test
    public void getAuthToken_WhenCalled_ShouldReturnNull() throws Exception {
        Assert.assertNull(mAuthenticator.getAuthToken(null, null, null, null));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void getAuthTokenLabel_WhenCalled_ShouldReturnNull() throws Exception {
        mAuthenticator.getAuthTokenLabel(null);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void updateCredentials_WhenCalled_ShouldReturnNull() throws Exception {
        mAuthenticator.updateCredentials(null, null, null, null);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void hasFeatures_WhenCalled_ShouldReturnNull() throws Exception {
        mAuthenticator.hasFeatures(null, null, null);
    }

}
