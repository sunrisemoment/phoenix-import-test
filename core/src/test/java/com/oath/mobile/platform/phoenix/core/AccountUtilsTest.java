package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by yuhongli on 10/30/17.
 */
@RunWith(RobolectricTestRunner.class)
public class AccountUtilsTest extends BaseTest {

    @Test
    public void constructor_WhenInvoked_ShouldThrowException() throws Exception {
        Class<?> accountUtils = Class.forName("com.oath.mobile.platform.phoenix.core.AccountUtils");
        try {
            accountUtils.newInstance();
        } catch (IllegalAccessException e) {
            assertEquals("class java.lang.IllegalAccessException", e.getClass().toString());
        }
        Constructor<?> constructor = accountUtils.getDeclaredConstructor();
        constructor.setAccessible(true); // set it public
        constructor.newInstance(); // invoke it for coverage
    }

    @Test
    public void getDisplayName_WhenAccountDoesNotHaveFirstNameAndLastName_ReturnUsername() throws Exception {
        IAccount accountMock = mock(IAccount.class);
        when(accountMock.getUserName()).thenReturn("test@yahoo.com");
        String displayName = AccountUtils.getDisplayName(accountMock);
        assertEquals("Not the correct display name", "test@yahoo.com", displayName);
    }

    @Test
    public void getDisplayName_WhenAccountHasJustLastName_ReturnLastName() throws Exception {
        IAccount accountMock = mock(IAccount.class);
        when(accountMock.getLastName()).thenReturn("LastName");
        String displayName = AccountUtils.getDisplayName(accountMock);
        assertEquals("Not the correct display name", "LastName", displayName);
    }

    @Test
    public void getDisplayName_WhenAccountHasJustFirstName_ReturnFirstName() throws Exception {
        IAccount accountMock = mock(IAccount.class);
        when(accountMock.getFirstName()).thenReturn("FirstName");
        String displayName = AccountUtils.getDisplayName(accountMock);
        assertEquals("Not the correct display name", "FirstName", displayName);
    }

    @Test
    public void getDisplayName_WhenAccountHasBothFirstLastName_ReturnFirstLastName() throws Exception {
        IAccount accountMock = mock(IAccount.class);
        when(accountMock.getFirstName()).thenReturn("FirstName");
        when(accountMock.getLastName()).thenReturn("LastName");
        String displayName = AccountUtils.getDisplayName(accountMock);
        assertEquals("Not the correct display name", "FirstName LastName", displayName);
    }

    @Test
    public void getDisplayName_WhenCJKFirstLastNames_ReturnCorrectNameOrder() throws Exception {
        IAccount accountMock = mock(IAccount.class);
        when(accountMock.getFirstName()).thenReturn("豪");
        when(accountMock.getLastName()).thenReturn("陈");
        String displayName = AccountUtils.getDisplayName(accountMock);
        assertEquals("Not the correct display name", "陈豪", displayName);
    }

    @Test
    public void getDisplayName_WhenHiraganaFirstLastNames_ReturnCorrectNameOrder() throws Exception {
        IAccount accountMock = mock(IAccount.class);
        when(accountMock.getFirstName()).thenReturn("あすか");
        when(accountMock.getLastName()).thenReturn("田中");
        String displayName = AccountUtils.getDisplayName(accountMock);
        assertEquals("Not the correct display name", "田中あすか", displayName);
    }

    @Test
    public void getDisplayName_WhenKatakanaFirstLastNames_ReturnCorrectNameOrder() throws Exception {
        IAccount accountMock = mock(IAccount.class);
        when(accountMock.getFirstName()).thenReturn("ニシャント");
        when(accountMock.getLastName()).thenReturn("ソニ");
        String displayName = AccountUtils.getDisplayName(accountMock);
        assertEquals("Not the correct display name", "ソニニシャント", displayName);
    }

    @Test
    public void getDisplayName_WhenKoreanFirstLastNames_ReturnCorrectNameOrder() throws Exception {
        IAccount accountMock = mock(IAccount.class);
        when(accountMock.getFirstName()).thenReturn("기문");
        when(accountMock.getLastName()).thenReturn("반");
        String displayName = AccountUtils.getDisplayName(accountMock);
        assertEquals("Not the correct display name", "반기문", displayName);
    }

    @Test
    public void getApplicationName_WhenApplicationInfoHasLabelRes_GetResString() throws Exception {
        Context contextMock = mock(Context.class);
        ApplicationInfo applicationInfoMock = mock(ApplicationInfo.class);
        applicationInfoMock.labelRes = 1234;

        when(contextMock.getApplicationInfo()).thenReturn(applicationInfoMock);
        when(contextMock.getString(anyInt())).thenReturn("someString");

        CharSequence string = AccountUtils.getApplicationName(contextMock);
        assertEquals("Application name is not correct", "someString", string);
    }

    @Test
    public void getApplicationName_WhenGetStringThrowsNotFoundException_ReturnEmptyString() throws Exception {
        Context contextMock = mock(Context.class);
        ApplicationInfo applicationInfoMock = mock(ApplicationInfo.class);
        applicationInfoMock.labelRes = 1234;

        when(contextMock.getApplicationInfo()).thenReturn(applicationInfoMock);
        when(contextMock.getString(anyInt())).thenThrow(Resources.NotFoundException.class);

        CharSequence string = AccountUtils.getApplicationName(contextMock);
        assertEquals("Application name is not correct", "", string);
    }

    @Test
    public void getApplicationName_WhenAppInfoLabelResZeroAndValidPackageManagerApplicationLabel_ReturnApplicationLabel() throws Exception {
        Context contextMock = mock(Context.class);
        ApplicationInfo applicationInfoMock = mock(ApplicationInfo.class);

        applicationInfoMock.labelRes = 0;

        when(contextMock.getApplicationInfo()).thenReturn(applicationInfoMock);
        PackageManager packageManagerMock = mock(PackageManager.class);
        when(packageManagerMock.getApplicationLabel(applicationInfoMock)).thenReturn("AppLabel");
        when(contextMock.getPackageManager()).thenReturn(packageManagerMock);

        CharSequence string = AccountUtils.getApplicationName(contextMock);
        assertEquals("Application name is not correct", "AppLabel", string);
    }

    @Test
    public void getApplicationName_WhenPackageManagerIsNull_ReturnEmpty() throws Exception {
        Context contextMock = mock(Context.class);
        ApplicationInfo applicationInfoMock = mock(ApplicationInfo.class);
        applicationInfoMock.labelRes = 0;

        when(contextMock.getApplicationInfo()).thenReturn(applicationInfoMock);

        CharSequence string = AccountUtils.getApplicationName(contextMock);
        assertEquals("Application name is not correct", "", string);
    }

    @Test
    public void getApplicationName_WhenValidPackageManagerAndApplicationLabelIsNull_ReturnEmpty() throws Exception {
        Context contextMock = mock(Context.class);
        ApplicationInfo applicationInfoMock = mock(ApplicationInfo.class);
        applicationInfoMock.labelRes = 0;

        when(contextMock.getApplicationInfo()).thenReturn(applicationInfoMock);
        PackageManager packageManagerMock = mock(PackageManager.class);
        when(packageManagerMock.getApplicationLabel(applicationInfoMock)).thenReturn(null);
        when(contextMock.getPackageManager()).thenReturn(packageManagerMock);

        CharSequence string = AccountUtils.getApplicationName(contextMock);
        assertEquals("Application name is not correct", "", string);
    }

    @Test
    public void sortAccounts_WhenParameterIsNull_ShouldNotThrowException() {
        AccountUtils.sortAccounts(null);
    }

    @Test
    public void sortAccounts_WhenParameterIsEmptyList_ShouldNotThrowException() {
        AccountUtils.sortAccounts(Collections.emptyList());
    }

    @Test
    public void sortAccounts_WhenAccountsListIsNotInAlphabeticalOrder_ShouldSortInAlphabeticalOrder() {
        List accounts = new ArrayList<Account>();
        Account account1 = mock(Account.class);
        when(account1.getDisplayName()).thenReturn("Tom@yahoo.com");
        Account account2 = mock(Account.class);
        when(account2.getDisplayName()).thenReturn("sun@yahoo.com");
        accounts.add(account1);
        accounts.add(account2);
        AccountUtils.sortAccounts(accounts);

        assertEquals("Unexpected account email", "sun@yahoo.com", ((Account) accounts.get(0)).getDisplayName());
        assertEquals("Unexpected account email", "Tom@yahoo.com", ((Account) accounts.get(1)).getDisplayName());
    }

    @Test
    public void sortAccounts_WhenOneAccountIsNull_ShouldSortInAlphabeticalOrder() {
        List accounts = new ArrayList<Account>();
        Account account1 = mock(Account.class);
        when(account1.getDisplayName()).thenReturn("a@yahoo.com");
        Account account2 = mock(Account.class);
        when(account2.getDisplayName()).thenReturn(null);
        Account account3 = mock(Account.class);
        when(account3.getDisplayName()).thenReturn("c@yahoo.com");
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        AccountUtils.sortAccounts(accounts);

        assertEquals("Unexpected account email", null, ((Account) accounts.get(0)).getDisplayName());
        assertEquals("Unexpected account email", "a@yahoo.com", ((Account) accounts.get(1)).getDisplayName());
        assertEquals("Unexpected account email", "c@yahoo.com", ((Account) accounts.get(2)).getDisplayName());
    }

    @Test
    public void sortAccounts_WhenOneAccountIsEmpty_ShouldSortInAlphabeticalOrder() {
        List accounts = new ArrayList<Account>();
        Account account1 = mock(Account.class);
        when(account1.getDisplayName()).thenReturn(null);
        Account account2 = mock(Account.class);
        when(account2.getDisplayName()).thenReturn("");
        Account account3 = mock(Account.class);
        when(account3.getDisplayName()).thenReturn("c@yahoo.com");
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        AccountUtils.sortAccounts(accounts);

        assertEquals("Unexpected account email", null, ((Account) accounts.get(0)).getDisplayName());
        assertEquals("Unexpected account email", "", ((Account) accounts.get(1)).getDisplayName());
        assertEquals("Unexpected account email", "c@yahoo.com", ((Account) accounts.get(2)).getDisplayName());
    }

    @Test
    public void sortAccounts_WhenAllAccountsAreNull_ShouldSortInAlphabeticalOrder() {
        List accounts = new ArrayList<Account>();
        Account account1 = mock(Account.class);
        when(account1.getDisplayName()).thenReturn(null);
        Account account2 = mock(Account.class);
        when(account2.getDisplayName()).thenReturn(null);
        Account account3 = mock(Account.class);
        when(account3.getDisplayName()).thenReturn(null);
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        AccountUtils.sortAccounts(accounts);

        assertEquals("Unexpected account email", null, ((Account) accounts.get(0)).getDisplayName());
        assertEquals("Unexpected account email", null, ((Account) accounts.get(1)).getDisplayName());
        assertEquals("Unexpected account email", null, ((Account) accounts.get(2)).getDisplayName());
    }
}
