package com.oath.mobile.platform.phoenix.core;

import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.widget.TextView;

import net.openid.appauth.TokenResponse;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowAccountManager;
import org.robolectric.shadows.ShadowAlertDialog;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.Map;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static com.oath.mobile.platform.phoenix.core.AccountNetworkAPI.CONTENT_TYPE_JSON;
import static com.oath.mobile.platform.phoenix.core.AccountNetworkAPI.ELEM_HEADER_CONTENT_TYPE;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.OATH_AUTH_CONFIG_KEY;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenInternalError.EVENT_REFRESH_TOKEN_CLIENT_ERROR;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenUserEvents.EVENT_REFRESH_TOKEN_FAILURE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenUserEvents.EVENT_REFRESH_TOKEN_START;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenUserEvents.EVENT_REFRESH_TOKEN_SUCCESS;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_INTERNAL_SERVER_ERROR_RESPONSE_BODY;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_OAUTH_IDP_SWITCH_ERROR;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_OAUTH_INVALID_GRANT_ERROR;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_OAUTH_INVALID_REQUEST_ERROR;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_OAUTH_INVALID_SCOPE_ERROR;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_OAUTH_UNAUTHORIZED_CLIENT_ERROR;
import static com.oath.mobile.platform.phoenix.core.TestValues.getTestAuthCodeExchangeResponseNonce;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by nsoni on 9/21/17.
 */
@RunWith(RobolectricTestRunner.class)
public class AccountTest extends BaseTest {
    private Account mAccount;
    ShadowAccountManager mShadowAccountManager;
    private android.accounts.Account mAndroidAccount;
    private AccountManager mAccountManager;
    private Context mContext;

    @Before
    public void setUp() throws Exception {
        mAccountManager = AccountManager.get(RuntimeEnvironment.application.getApplicationContext());
        mShadowAccountManager = shadowOf(mAccountManager);
        mAndroidAccount = new android.accounts.Account("test_account", "com.some.type");
        mShadowAccountManager.addAccountExplicitly(mAndroidAccount, null, null);
        mAccount = new Account(mAccountManager, mAndroidAccount);
        mContext = RuntimeEnvironment.application;
    }

    @After
    public void tearDown() throws Exception {
        mShadowAccountManager.removeAllAccounts();
        EventLogger.sEventLoggerSingleton = null; // reset to get fresh instance of EventLogger so Roboelectric won't complain on re-spy
    }

    @Test
    public void constructor_WhenCreated_ShouldNotFail() throws Exception {
        Assert.assertNotNull("Should not be null", mAccount);
    }

    @Test
    public void getToken_WhenSet_ShouldReturnSetValue() throws Exception {
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_ACCESS_TOKEN, "accessToken");
        Assert.assertEquals("Should have the correct value", "accessToken", mAccount.getToken());
    }

    @Test
    public void getToken_WhenNotSet_ShouldReturnNull() throws Exception {
        Assert.assertEquals("Should have the correct value", null, mAccount.getToken());
    }

    @Test
    public void getPublicMethods_WhenNotSet_ShouldReturnEmpty() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));

        Activity activity = Robolectric.setupActivity(Activity.class);

        config.saveAuthConfig(activity, OATH_AUTH_CONFIG_KEY);

        Assert.assertEquals("Should have the correct value", null, mAccount.getFirstName());
        Assert.assertEquals("Should have the correct value", null, mAccount.getLastName());
        Assert.assertEquals("Should have the correct value", null, mAccount.getImageUri());
        Assert.assertEquals("Should have the correct value", null, mAccount.getEmail());
        Assert.assertEquals("Should have the correct value", null, mAccount.getUserName());
        Assert.assertEquals("Should have the correct value", false, mAccount.isLoggedIn());
        mAccount.disable(new AccountChangeListener() {
            @Override
            public void onAccountChanged(int result) {

            }
        });
        mAccount.refreshToken(activity, mock(OnRefreshTokenResponse.class));
        Assert.assertEquals("Should have the correct value", false, mAccount.isLoggedIn());
    }

    @Test
    public void refreshToken_WhenAuthConfigIsNull_ShouldNotExecutePost() throws Exception {
        AuthConfig authConfig = new AuthConfig(null, null, null, null, null, null, null);
        authConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doNothing().when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        OnRefreshTokenResponse responseListener = spy(new OnRefreshTokenResponse() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(int errorCode) {
                assertEquals("Error code is not the same", OnRefreshTokenResponse.GENERAL_ERROR, errorCode);
            }
        });

        // Test
        mAccount.refreshToken(mContext, responseListener);

        // Verify
        verify(accountNetworkAPIMock, never()).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));
        verify(eventLogger, times(1)).logErrorInformationEvent(Matchers.eq(EVENT_REFRESH_TOKEN_CLIENT_ERROR), Matchers.eq(EventLogger.RefreshTokenInternalError.AUTH_CONFIG_NULL), Matchers.contains("Auth config null"));
    }

    @Test
    public void refreshToken_WhenValidRequest_ShouldUpdateTokens() throws NoSuchFieldException, IllegalAccessException {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));

        Activity activity = Robolectric.setupActivity(Activity.class);

        config.saveAuthConfig(activity, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                asyncCallback.onSuccess(TestValues.getTestAuthCodeExchangeResponse().jsonSerializeString());
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        OnRefreshTokenResponse onRefreshTokenResponse = spy(new OnRefreshTokenResponse() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(int errorCode) {

            }
        });

        //Reset data
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_ACCESS_TOKEN, "fail");
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_REFRESH_TOKEN, "fail");

        mAccount.refreshToken(activity, onRefreshTokenResponse);

        //Verify data is written to account
        verify(onRefreshTokenResponse).onSuccess();
        verify(onRefreshTokenResponse, never()).onError(anyInt());
        assertEquals(TestValues.TEST_ACCESS_TOKEN, mAccount.getToken());
        assertEquals(TestValues.TEST_REFRESH_TOKEN, mAccount.getRefreshToken());
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_REFRESH_TOKEN_START), isNull(Map.class));
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_REFRESH_TOKEN_SUCCESS), isNull(Map.class));

    }

    @Test
    public void refreshToken_WhenWhenErrorReturned_ShouldNotChangeTokensAndLog() throws NoSuchFieldException, IllegalAccessException {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));

        Activity activity = Robolectric.setupActivity(Activity.class);

        config.saveAuthConfig(activity, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, new HttpConnectionException(400, "Non 200 response from server", TEST_VALID_OAUTH_INVALID_GRANT_ERROR));
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        OnRefreshTokenResponse onRefreshTokenResponse = spy(new OnRefreshTokenResponse() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(int errorCode) {

            }
        });

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        //Reset data
        String testToken = "test";
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_ACCESS_TOKEN, testToken);
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_REFRESH_TOKEN, testToken);
        mAccount.refreshToken(activity, onRefreshTokenResponse);

        //Verify data is written to account
        verify(onRefreshTokenResponse, never()).onSuccess();
        verify(onRefreshTokenResponse, times(1)).onError(anyInt());
        assertEquals("Access token was wrongly updated", testToken, mAccount.getToken());
        assertEquals("Refresh token was wrongly updated", testToken, mAccount.getRefreshToken());
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_REFRESH_TOKEN_START), any());
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_REFRESH_TOKEN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Message is incorrect", EventLogger.RefreshTokenUserEvents.REAUTHORIZE_USER_MSG, realMap.get(EventLogger.TRACKING_KEY_ERROR_MESSAGE));
        assertEquals("Error code is incorrect", EventLogger.RefreshTokenUserEvents.REAUTHORIZE_USER, realMap.get(EventLogger.TRACKING_KEY_ERROR_CODE));
        assertFalse("Account should not be active", mAccount.isActive());
        assertTrue("Account should be logged in", mAccount.isLoggedIn());
    }

    @Test
    public void refreshToken_WhenWhenErrorReturnedInvalidRequest_ShouldNotChangeTokensAndLog() throws NoSuchFieldException, IllegalAccessException {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));

        Activity activity = Robolectric.setupActivity(Activity.class);

        config.saveAuthConfig(activity, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, new HttpConnectionException(400, "Non 200 response from server", TEST_VALID_OAUTH_INVALID_REQUEST_ERROR));
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        OnRefreshTokenResponse onRefreshTokenResponse = spy(new OnRefreshTokenResponse() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(int errorCode) {

            }
        });

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        //Reset data
        String testToken = "test";
        String testTokenTwo = "twoTokenAndABlue";
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_ACCESS_TOKEN, testToken);
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_REFRESH_TOKEN, testTokenTwo);
        mAccount.refreshToken(activity, onRefreshTokenResponse);

        //Verify data is written to account
        verify(onRefreshTokenResponse, never()).onSuccess();
        verify(onRefreshTokenResponse, times(1)).onError(anyInt());
        assertEquals("Access token was wrongly updated", testToken, mAccount.getToken());
        assertEquals("Refresh token was wrongly updated", testTokenTwo, mAccount.getRefreshToken());
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_REFRESH_TOKEN_START), any());
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_REFRESH_TOKEN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Message is incorrect", EventLogger.RefreshTokenUserEvents.INVALID_REQUEST_MSG, realMap.get(EventLogger.TRACKING_KEY_ERROR_MESSAGE));
        assertEquals("Error code is incorrect", EventLogger.RefreshTokenUserEvents.INVALID_REQUEST, realMap.get(EventLogger.TRACKING_KEY_ERROR_CODE));
    }

    @Test
    public void refreshToken_WhenWhenErrorReturnedReauthUser_ShouldNotChangeTokensAndLog() throws NoSuchFieldException, IllegalAccessException {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));

        Activity activity = Robolectric.setupActivity(Activity.class);

        config.saveAuthConfig(activity, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, new HttpConnectionException(400, "Non 200 response from server", TEST_VALID_OAUTH_INVALID_GRANT_ERROR));
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        OnRefreshTokenResponse onRefreshTokenResponse = spy(new OnRefreshTokenResponse() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(int errorCode) {

            }
        });

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        //Reset data
        String testToken = "test";
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_ACCESS_TOKEN, testToken);
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_REFRESH_TOKEN, testToken);
        mAccount.refreshToken(activity, onRefreshTokenResponse);

        //Verify data is written to account
        verify(onRefreshTokenResponse, never()).onSuccess();
        verify(onRefreshTokenResponse, times(1)).onError(anyInt());
        assertEquals("Access token was wrongly updated", testToken, mAccount.getToken());
        assertEquals("Refresh token was wrongly updated", testToken, mAccount.getRefreshToken());
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_REFRESH_TOKEN_START), any());
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_REFRESH_TOKEN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Message is incorrect", EventLogger.RefreshTokenUserEvents.REAUTHORIZE_USER_MSG, realMap.get(EventLogger.TRACKING_KEY_ERROR_MESSAGE));
        assertEquals("Error code is incorrect", EventLogger.RefreshTokenUserEvents.REAUTHORIZE_USER, realMap.get(EventLogger.TRACKING_KEY_ERROR_CODE));
    }

    @Test
    public void refreshToken_WhenErrorReturnedIdpSwitched_ShouldNotChangeTokensAndLog() throws NoSuchFieldException, IllegalAccessException {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));

        Activity activity = Robolectric.setupActivity(Activity.class);

        config.saveAuthConfig(activity, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, new HttpConnectionException(400, "Non 200 response from server", TEST_VALID_OAUTH_IDP_SWITCH_ERROR));
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        OnRefreshTokenResponse onRefreshTokenResponse = spy(new OnRefreshTokenResponse() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(int errorCode) {
                assertEquals("Code is not the same", REAUTHORIZE_USER, errorCode);
            }
        });

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        //Reset data
        String testToken = "test";
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_ACCESS_TOKEN, testToken);
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_REFRESH_TOKEN, testToken);
        mAccount.refreshToken(activity, onRefreshTokenResponse);

        //Verify data is written to account
        verify(onRefreshTokenResponse, never()).onSuccess();
        verify(onRefreshTokenResponse, times(1)).onError(anyInt());
        assertEquals("Access token was wrongly updated", testToken, mAccount.getToken());
        assertEquals("Refresh token was wrongly updated", testToken, mAccount.getRefreshToken());
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_REFRESH_TOKEN_START), any());
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_REFRESH_TOKEN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Message is incorrect", EventLogger.RefreshTokenUserEvents.IDP_SWITCHED_MSG, realMap.get(EventLogger.TRACKING_KEY_ERROR_MESSAGE));
        assertEquals("Error code is incorrect", EventLogger.RefreshTokenUserEvents.IDP_SWITCHED, realMap.get(EventLogger.TRACKING_KEY_ERROR_CODE));
    }

    @Test
    public void refreshToken_WhenWhenErrorReturnedUnauthClient_ShouldNotChangeTokensAndLog() throws NoSuchFieldException, IllegalAccessException {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));

        Activity activity = Robolectric.setupActivity(Activity.class);

        config.saveAuthConfig(activity, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, new HttpConnectionException(400, "Non 200 response from server", TEST_VALID_OAUTH_UNAUTHORIZED_CLIENT_ERROR));
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        OnRefreshTokenResponse onRefreshTokenResponse = spy(new OnRefreshTokenResponse() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(int errorCode) {

            }
        });

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        //Reset data
        String testToken = "test";
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_ACCESS_TOKEN, testToken);
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_REFRESH_TOKEN, testToken);
        mAccount.refreshToken(activity, onRefreshTokenResponse);

        //Verify data is written to account
        verify(onRefreshTokenResponse, never()).onSuccess();
        verify(onRefreshTokenResponse, times(1)).onError(anyInt());
        assertEquals("Access token was wrongly updated", testToken, mAccount.getToken());
        assertEquals("Refresh token was wrongly updated", testToken, mAccount.getRefreshToken());
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_REFRESH_TOKEN_START), any());
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_REFRESH_TOKEN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Message is incorrect", EventLogger.RefreshTokenUserEvents.UNAUTHORIZED_CLIENT_MSG, realMap.get(EventLogger.TRACKING_KEY_ERROR_MESSAGE));
        assertEquals("Error code is incorrect", EventLogger.RefreshTokenUserEvents.UNAUTHORIZED_CLIENT, realMap.get(EventLogger.TRACKING_KEY_ERROR_CODE));
    }


    @Test
    public void refreshToken_WhenWhenErrorReturnedInvalidScope_ShouldNotChangeTokensAndLog() throws NoSuchFieldException, IllegalAccessException {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));

        Activity activity = Robolectric.setupActivity(Activity.class);

        config.saveAuthConfig(activity, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, new HttpConnectionException(400, "Non 200 response from server", TEST_VALID_OAUTH_INVALID_SCOPE_ERROR));
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        OnRefreshTokenResponse onRefreshTokenResponse = spy(new OnRefreshTokenResponse() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(int errorCode) {

            }
        });

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        //Reset data
        String testToken = "test";
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_ACCESS_TOKEN, testToken);
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_REFRESH_TOKEN, testToken);
        mAccount.refreshToken(activity, onRefreshTokenResponse);

        //Verify data is written to account
        verify(onRefreshTokenResponse, never()).onSuccess();
        verify(onRefreshTokenResponse, times(1)).onError(anyInt());
        assertEquals("Access token was wrongly updated", testToken, mAccount.getToken());
        assertEquals("Refresh token was wrongly updated", testToken, mAccount.getRefreshToken());
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_REFRESH_TOKEN_START), any());
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_REFRESH_TOKEN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Message is incorrect", EventLogger.RefreshTokenUserEvents.INVALID_SCOPE_MSG, realMap.get(EventLogger.TRACKING_KEY_ERROR_MESSAGE));
        assertEquals("Error code is incorrect", EventLogger.RefreshTokenUserEvents.INVALID_SCOPE, realMap.get(EventLogger.TRACKING_KEY_ERROR_CODE));
    }


    @Test
    public void refreshToken_WhenWhenErrorReturnedServerError_ShouldNotChangeTokensAndLog() throws NoSuchFieldException, IllegalAccessException {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));

        Activity activity = Robolectric.setupActivity(Activity.class);

        config.saveAuthConfig(activity, OATH_AUTH_CONFIG_KEY);

        AccountNetworkAPI accountNetworkAPIMock = mock(AccountNetworkAPI.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AccountNetworkAPI.FormPostAsyncCallback asyncCallback = (AccountNetworkAPI.FormPostAsyncCallback) invocation.getArguments()[4];
                asyncCallback.onFailure(AccountNetworkAPI.FormPostAsyncCallback.IDP_SPECIFIC_ERROR, new HttpConnectionException(500, "Non 200 response from server", TEST_VALID_INTERNAL_SERVER_ERROR_RESPONSE_BODY));
                return null;
            }
        }).when(accountNetworkAPIMock).executeFormPostAsync(any(Context.class), anyString(), anyMap(), anyString(), any(AccountNetworkAPI.FormPostAsyncCallback.class));

        Field field = AccountNetworkAPI.class.getDeclaredField("sSingletonInstance");
        field.setAccessible(true);
        field.set(null, accountNetworkAPIMock);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        OnRefreshTokenResponse onRefreshTokenResponse = spy(new OnRefreshTokenResponse() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(int errorCode) {

            }
        });

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        //Reset data
        String testToken = "test";
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_ACCESS_TOKEN, testToken);
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_REFRESH_TOKEN, testToken);
        mAccount.refreshToken(activity, onRefreshTokenResponse);

        //Verify data is written to account
        verify(onRefreshTokenResponse, never()).onSuccess();
        verify(onRefreshTokenResponse, times(1)).onError(anyInt());
        assertEquals("Access token was wrongly updated", testToken, mAccount.getToken());
        assertEquals("Refresh token was wrongly updated", testToken, mAccount.getRefreshToken());
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_REFRESH_TOKEN_START), any());
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_REFRESH_TOKEN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Message is incorrect", EventLogger.RefreshTokenUserEvents.SERVER_ERROR_MSG, realMap.get(EventLogger.TRACKING_KEY_ERROR_MESSAGE));
        assertEquals("Error code is incorrect", EventLogger.RefreshTokenUserEvents.SERVER_ERROR, realMap.get(EventLogger.TRACKING_KEY_ERROR_CODE));
    }

    @Test
    public void Remove_WhenContextIsNull_ShouldNotCallGetRefreshTokenAndListener() throws Exception {
        Account account = spy(mAccount);
        AccountChangeListener listener = mock(AccountChangeListener.class);
        account.remove(null, listener);
        verify(account, times(0)).getRefreshToken();
        verify(listener, times(0)).onAccountChanged(anyInt());
    }

    @Test
    public void remove_WhenRemoveFromAndroidAccountManagerThrowException_ShouldNotListenerAndAccountIsNotRemoved() throws Exception {
        final String responseBody = "{ \"error\": \"none\" }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url("https://api.login.yahoo.com").build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_OK).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message("OK")
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);

        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));

        AccountManager accountManagerMock = mock(AccountManager.class);
        AccountManagerFuture futureMocK = mock(AccountManagerFuture.class);
        when(futureMocK.getResult()).thenThrow(IOException.class);
        final boolean[] isListenerCalled = {false};
        doAnswer(invocation -> {
            isListenerCalled[0] = true;
            Object[] args = invocation.getArguments();
            ((AccountManagerCallback) args[1]).run(futureMocK);
            return null;
        }).when(accountManagerMock).removeAccount(any(android.accounts.Account.class), any(AccountManagerCallback.class), eq(null));

        setAndroidAccManager(account, accountManagerMock);

        assertEquals("Should be 1", 1, AuthManager.getInstance(mContext).getAllAccounts().size());
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(),
                Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountChangeListener listener = mock(AccountChangeListener.class);
        Activity activity = Robolectric.buildActivity(ManageAccountsActivity.class).get();
        account.remove(activity, listener);
        Robolectric.flushBackgroundThreadScheduler();
        verify(account).getRefreshToken();
        assertEquals("Should be 1", 1, AuthManager.getInstance(mContext).getAllAccounts().size());
        verify(listener).onAccountChanged(eq(AccountChangeListener.FAIL));

        Dialog dialog = ShadowAlertDialog.getLatestDialog();
        assertNotNull("Dialog should not be null", dialog);
        TextView messageText = dialog.findViewById(R.id.account_custom_dialog_message);
        assertTrue("Alert dialog is not showing", dialog.isShowing());
        assertEquals("Alert dialog message is not correct", mContext.getResources().getString(R.string.phoenix_try_again_error), messageText.getText());
    }

    private void setAndroidAccManager(Account account, AccountManager accountManagerMock) throws NoSuchFieldException, IllegalAccessException {
        Field field = Account.class.getDeclaredField("mAndroidAccManager");
        field.setAccessible(true);
        field.set(account, accountManagerMock);
    }

    @Test
    public void remove_WhenContextIsNotNull_ShouldCallGetRefreshTokenAndRemoveAccount() throws Exception {
        final String responseBody = "{ \"error\": \"none\" }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url("https://api.login.yahoo.com").build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_OK).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message("OK")
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);

        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        assertEquals("Should be 1", 1, AuthManager.getInstance(mContext).getAllAccounts().size());
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        AccountChangeListener listener = mock(AccountChangeListener.class);
        Activity activity = Robolectric.buildActivity(ManageAccountsActivity.class).get();
        account.remove(activity, listener);
        verify(account).getRefreshToken();
        assertEquals("Should be 0", 0, AuthManager.getInstance(mContext).getAllAccounts().size());
        verify(listener).onAccountChanged(anyInt());
    }

    //TODO: keep updating as soon as the valid values come it
    @Test
    public void getPublicMethods_WhenSet_ShouldReturnValue() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));

        Activity activity = Robolectric.setupActivity(Activity.class);

        config.saveAuthConfig(activity, OATH_AUTH_CONFIG_KEY);

        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_FIRST_NAME, "firstname");
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_LAST_NAME, "lastname");
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_EMAIL, "email");

        Assert.assertEquals("Should have the correct value", "firstname", mAccount.getFirstName());
        Assert.assertEquals("Should have the correct value", "lastname", mAccount.getLastName());
        Assert.assertEquals("Should have the correct value", null, mAccount.getImageUri());
        Assert.assertEquals("Should have the correct value", "email", mAccount.getEmail());
        Assert.assertEquals("Should have the correct value", null, mAccount.getUserName());
        Assert.assertEquals("Should have the correct value", false, mAccount.isLoggedIn());
        Assert.assertEquals("Should have the correct value", true, mAccount.isActive());
        mAccount.disable(new AccountChangeListener() {
            @Override
            public void onAccountChanged(int result) {

            }
        });
        mAccount.refreshToken(activity, mock(OnRefreshTokenResponse.class));
        Assert.assertEquals("Should have the correct value", false, mAccount.isLoggedIn());
    }

    @Test
    public void getRefreshToken_WhenSet_ShouldReturnSetValue() throws Exception {
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_REFRESH_TOKEN, "refreshToken");
        Assert.assertEquals("Should have the correct value", "refreshToken", mAccount.getRefreshToken());
    }

    @Test
    public void getRefreshToken_WhenNotSet_ShouldReturnNull() throws Exception {
        Assert.assertEquals("Should have the correct value", null, mAccount.getRefreshToken());
    }

    @Test
    public void remove_WhenContextIsNull_ShouldNotCallGetRefreshTokenAndListener() throws Exception {
        Account account = spy(mAccount);
        AccountChangeListener listener = mock(AccountChangeListener.class);
        account.remove(null, listener);
        verify(account, times(0)).getRefreshToken();
        verify(listener, times(0)).onAccountChanged(anyInt());
    }

    @Test
    public void remove_WhenNetworkUnavailable_ShouldCallGetRefreshTokenAndListener() throws Exception {
        TestUtils.setNetworkConnectivity(false);
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        Account account = spy(mAccount);
        AccountChangeListener listener = mock(AccountChangeListener.class);
        Activity activity = Robolectric.buildActivity(ManageAccountsActivity.class).get();
        account.remove(activity, listener);
        verify(account, times(1)).getRefreshToken();
        verify(listener, times(1)).onAccountChanged(AccountChangeListener.FAIL);
        TestUtils.setNetworkConnectivity(true);
    }

    @Test
    public void remove_WhenContextIsNotNull_ShouldCallGetRefreshToken() throws Exception {
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);
        Account account = spy(mAccount);
        AccountChangeListener listener = mock(AccountChangeListener.class);
        Activity activity = Robolectric.buildActivity(ManageAccountsActivity.class).get();
        account.remove(activity, listener);
        verify(account).getRefreshToken();
        verify(listener).onAccountChanged(anyInt());
    }

    @Test
    public void getImageUri_WhenSet_ShouldReturnImageUri() throws Exception {
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_IMAGE_URI, TestValues.TEST_JWT_IMAGE192);
        Assert.assertEquals("Should have the correct value", TestValues.TEST_JWT_IMAGE192, mAccount.getImageUri());
    }

    @Test
    public void getImageUri_WhenNotSet_ShouldNull() throws Exception {
        Assert.assertEquals("Should have the correct value", null, null);
    }

    @Test
    public void setRefreshToken_WhenSet_ShouldSaveTheRefreshTokenInAndroidAccManager() throws Exception {
        mAccount.setRefreshToken(TestValues.TEST_REFRESH_TOKEN);

        Assert.assertEquals("Should be the same", TestValues.TEST_REFRESH_TOKEN, mAccount.getRefreshToken());
    }

    @Test
    public void getDisplayName_WhenSet_ShouldReturnDisplayName() throws Exception {
        mShadowAccountManager.setUserData(mAndroidAccount, Account.KEY_DISPLAY_NAME, TestValues.TEST_JWT_NAME_VALUE);
        Assert.assertEquals("Should have the correct value", TestValues.TEST_JWT_NAME_VALUE, mAccount.getDisplayName());
    }

    @Test
    public void getDisplayName_WhenNotSet_ShouldReturnNull() throws Exception {
        Assert.assertNull("Should have null value", mAccount.getDisplayName());
    }

    @Test
    public void setEmail_WhenSet_ShouldReturnCorrectValue() throws Exception {
        String email = "new_email@domain.com";
        mAccount.setEmail(email);
        Assert.assertEquals("Should have the correct value", email, mAccount.getEmail());
    }

    @Test
    public void setDisplayName_WhenSet_ShouldReturnCorrectValue() throws Exception {
        String displayName = "display name";
        mAccount.setDisplayName(displayName);
        Assert.assertEquals("Should have the correct value", displayName, mAccount.getDisplayName());
    }

    @Test
    public void setFirstName_WhenSet_ShouldReturnCorrectValue() throws Exception {
        String firstName = "firstName";
        mAccount.setFirstName(firstName);
        Assert.assertEquals("Should have the correct value", firstName, mAccount.getFirstName());
    }

    @Test
    public void setLastName_WhenSet_ShouldReturnCorrectValue() throws Exception {
        String lastName = "lastName";
        mAccount.setLastName(lastName);
        Assert.assertEquals("Should have the correct value", lastName, mAccount.getLastName());
    }

    @Test
    public void setUserName_WhenSet_ShouldReturnCorrectValue() throws Exception {
        String username = "new_email";
        mAccount.setUserName(username);
        Assert.assertEquals("Should have the correct value", username, mAccount.getUserName());
    }

    @Test
    public void setIsActive_WhenSetTrue_ShouldBeActive() throws Exception {
        mAccount.setIsActive(true);
        Assert.assertTrue("Should be active", mAccount.isActive());
    }

    @Test
    public void setIsActive_WhenSetFalse_ShouldNotBeActive() throws Exception {
        mAccount.setIsActive(false);
        Assert.assertFalse("Should not be active", mAccount.isActive());
    }
}
