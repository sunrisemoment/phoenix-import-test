package com.oath.mobile.platform.phoenix.core;

import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.widget.TextView;

import com.oath.mobile.testutils.TestRoboTestRunnerValidRes;

import junit.framework.Assert;

import net.openid.appauth.TokenResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowConnectivityManager;
import org.robolectric.shadows.ShadowDialog;
import org.robolectric.shadows.ShadowNetworkInfo;
import org.robolectric.shadows.ShadowSettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.oath.mobile.platform.phoenix.core.AuthConfig.OATH_AUTH_CONFIG_KEY;
import static com.oath.mobile.platform.phoenix.core.EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_END;
import static com.oath.mobile.platform.phoenix.core.EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_FETCH_USER_INFO_ERROR;
import static com.oath.mobile.platform.phoenix.core.EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_FETCH_USER_INFO_START;
import static com.oath.mobile.platform.phoenix.core.EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_FETCH_USER_INFO_SUCCESS;
import static com.oath.mobile.platform.phoenix.core.EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_ERROR;
import static com.oath.mobile.platform.phoenix.core.EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_START;
import static com.oath.mobile.platform.phoenix.core.EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_SUCCESS;
import static com.oath.mobile.platform.phoenix.core.EventLogger.AccountPickerUserEvents.EVENT_ACCOUNT_PICKER_START;
import static com.oath.mobile.platform.phoenix.core.EventLogger.TRACKING_KEY_ERROR_CODE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.TRACKING_KEY_ERROR_MESSAGE;
import static com.oath.mobile.platform.phoenix.core.OnRefreshTokenResponse.REAUTHORIZE_USER;
import static com.oath.mobile.platform.phoenix.core.TestValues.getTestAuthCodeExchangeResponseNonce;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.robolectric.Robolectric.setupActivity;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by yuhongli on 12/4/17.
 */
@RunWith(TestRoboTestRunnerValidRes.class)
public class AccountPickerActivityTest extends BaseTest {
    private Application mContext;

    @Before
    public void setUp() throws Exception {
        mContext = RuntimeEnvironment.application;
    }

    @Test
    public void onCreate_WhenActivityIsInvoked_ShouldNotBeNullAndLogStart() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        AccountPickerActivity activity = Robolectric.setupActivity(AccountPickerActivity.class);
        Assert.assertNotNull("AccountServiceSelectActivity should not be null", activity);
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_ACCOUNT_PICKER_START), isNull(Map.class));
    }

    @Test
    public void onAccountSelected_WhenAccountNotLoggedIn_ShouldCallRefreshTokenAndLogStartOfExistingAccount() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));

        AccountPickerActivity activity = setupActivity(AccountPickerActivity.class);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        activity.onAccountSelected(0, account);
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_START), isNull(Map.class));

        verify(account, times(1)).refreshToken(any(), any());
    }

    @Test
    public void onAccountSelected_WhenAccountSuccessfullyLoggedInAndFetchUserInfoSucceeds_ShouldSetResultAndFinishActivityAndFetchUserInfoAndLogExistingUserSuccess() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onSuccess();
                return null;
            }
        }).when(account).refreshToken(any(), any());
        String newloginId = "testloginId";
        String newGivenName = "testGivenName";
        String newFamilyName = "testFamilyName";
        String newEmail = "";
        String newDisplayName = "testDisplayName";
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                UserData userData = new UserData(account.getGUID(), newloginId, newGivenName, newFamilyName, newEmail, newDisplayName);
                ((OnUserDataResponseListener) args[1]).onSuccess(userData);
                return null;
            }
        }).when(account).fetchUserInfo(any(), any());

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        activity.onAccountSelected(0, account);
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_SUCCESS), isNull(Map.class));
        Robolectric.flushBackgroundThreadScheduler();

        verify(activity).finish();
        verify(activity).notifyAccountSetChanged();
        verify(account).fetchUserInfo(eq(activity), any());
        assertEquals(newDisplayName, account.getDisplayName());
        assertEquals(newGivenName, account.getFirstName());
        assertEquals(newFamilyName, account.getLastName());
        assertEquals(newEmail, account.getEmail());
        assertTrue("Account should be active", account.isActive());
    }

    @Test
    public void onAccountSelected_WhenAccountSuccessfullyLoggedInAndFetchUserInfoSucceeds_ShouldLogFetchUserSuccess() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onSuccess();
                return null;
            }
        }).when(account).refreshToken(any(), any());
        String newloginId = "testloginId";
        String newGivenName = "testGivenName";
        String newFamilyName = "testFamilyName";
        String newEmail = "";
        String newDisplayName = "testDisplayName";
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                UserData userData = new UserData(account.getGUID(), newloginId, newGivenName, newFamilyName, newEmail, newDisplayName);
                ((OnUserDataResponseListener) args[1]).onSuccess(userData);
                return null;
            }
        }).when(account).fetchUserInfo(any(), any());

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        activity.onAccountSelected(0, account);
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_ACCOUNT_PICKER_FETCH_USER_INFO_SUCCESS), isNull(Map.class));
        assertTrue("Account should be active", account.isActive());
    }

    @Test
    public void onAccountSelected_WhenAccountSuccessfullyLoggedInAndFetchUser_ShouldLogFetchUserStart() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onSuccess();
                return null;
            }
        }).when(account).refreshToken(any(), any());
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnUserDataResponseListener) args[1]).onFailure();
                return null;
            }
        }).when(account).fetchUserInfo(any(), any());

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        activity.onAccountSelected(0, account);
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_ACCOUNT_PICKER_FETCH_USER_INFO_START), isNull(Map.class));
        assertTrue("Account should be active", account.isActive());
    }

    @Test
    public void onAccountSelected_WhenAccountSuccessfullyLoggedInButFetchUserInfoFailed_ShouldSetResultAndFinishActivityAndDoNotFetchUserInfoAndLogFetchUserFailure() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onSuccess();
                return null;
            }
        }).when(account).refreshToken(any(), any());
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnUserDataResponseListener) args[1]).onFailure();
                return null;
            }
        }).when(account).fetchUserInfo(any(), any());

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        activity.onAccountSelected(0, account);
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_ACCOUNT_PICKER_FETCH_USER_INFO_ERROR), isNull(Map.class));
        Robolectric.flushBackgroundThreadScheduler();

        verify(activity).finish();
        verify(activity, never()).notifyAccountSetChanged();
        verify(account).fetchUserInfo(eq(activity), any());
        assertTrue("Account should be active", account.isActive());
    }

    @Test
    public void onAccountSelected_WhenAccountLoginSucceeds_ShouldDoNothing() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onSuccess();
                return null;
            }
        }).when(account).refreshToken(any(), any());

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        activity.onAccountSelected(0, account);

        verify(account, times(1)).refreshToken(any(), any());
        verify(activity, never()).notifyAccountSetChanged();
    }

    @Test
    public void onAccountSelected_WhenAccountLoginFails_ShouldCallNotifyAccountSetChangedAndLogSelectAccountError() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(OnRefreshTokenResponse.GENERAL_ERROR);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        activity.onAccountSelected(0, account);
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_ERROR), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error status is incorrect", EventLogger.RefreshTokenUserEvents.GENERAL_ERROR, realMap.get(TRACKING_KEY_ERROR_CODE));
        assertEquals("Error msg is incorrect", EventLogger.RefreshTokenUserEvents.GENERAL_ERROR_MSG, realMap.get(TRACKING_KEY_ERROR_MESSAGE));

        verify(account, times(1)).refreshToken(any(), any());
        verify(activity, times(1)).notifyAccountSetChanged();
        assertFalse("Account should be disabled", account.isLoggedIn());

        verify(activity, times(1)).showTryAgainLaterDialog();

        Dialog alertDialog = ShadowDialog.getLatestDialog();
        TextView textView = alertDialog.findViewById(R.id.phoenix_custom_dialog_title);
        assertEquals("Unexpected dialog title", activity.getString(R.string.phoenix_unable_to_use_this_account), textView.getText());
        TextView messageTextView = alertDialog.findViewById(R.id.account_custom_dialog_message);
        assertEquals("Unexpected message", activity.getString(R.string.phoenix_try_again_error), messageTextView.getText());

        alertDialog.findViewById(R.id.account_custom_dialog_button).performClick();
        assertFalse("Dialogue should have been dismissed", alertDialog.isShowing());
        assertFalse("Account should still be not logged in", account.isLoggedIn());
    }

    @Test
    public void onAccountSelected_WhenAccountLoginRefreshTokenIsInvalid_ShouldCallNotifyAccountSetChangedAndLogSelectAccountError() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(REAUTHORIZE_USER);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        activity.onAccountSelected(0, account);
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_ERROR), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error status is incorrect", EventLogger.RefreshTokenUserEvents.REAUTHORIZE_USER, realMap.get(TRACKING_KEY_ERROR_CODE));
        assertEquals("Error msg is incorrect", EventLogger.RefreshTokenUserEvents.REAUTHORIZE_USER_MSG, realMap.get(TRACKING_KEY_ERROR_MESSAGE));

        verify(account, times(1)).refreshToken(any(), any());
        verify(activity, times(1)).notifyAccountSetChanged();
        assertFalse("Account should be disabled", account.isLoggedIn());
        verify(activity, times(1)).showLoginAgainDialog();

        Dialog alertDialog = ShadowDialog.getLatestDialog();
        TextView textView = alertDialog.findViewById(R.id.phoenix_custom_dialog_title);
        assertEquals("Unexpected dialog title", activity.getString(R.string.phoenix_unable_to_use_this_account), textView.getText());
        TextView descriptionTextView = alertDialog.findViewById(R.id.account_custom_dialog_description);
        assertEquals("Unexpected description", activity.getString(R.string.phoenix_invalid_refresh_token_error), descriptionTextView.getText());

        alertDialog.findViewById(R.id.account_custom_dialog_button_two).performClick();
        assertFalse("Dialogue should have been dismissed", alertDialog.isShowing());
        assertFalse("Account should still be not logged in", account.isLoggedIn());
    }


    @Test
    public void onAccountSelected_WhenAccountLoginRefreshTokenIdpSwitched_ShouldCallNotifyAccountSetChangedAndLogSelectAccountError() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(REAUTHORIZE_USER);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        activity.onAccountSelected(0, account);
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_ERROR), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error status is incorrect", EventLogger.RefreshTokenUserEvents.REAUTHORIZE_USER, realMap.get(TRACKING_KEY_ERROR_CODE));
        assertEquals("Error msg is incorrect", EventLogger.RefreshTokenUserEvents.REAUTHORIZE_USER_MSG, realMap.get(TRACKING_KEY_ERROR_MESSAGE));

        verify(account, times(1)).refreshToken(any(), any());
        verify(activity, times(1)).notifyAccountSetChanged();
        assertFalse("Account should be disabled", account.isLoggedIn());
        verify(activity, times(1)).showLoginAgainDialog();

        Dialog alertDialog = ShadowDialog.getLatestDialog();
        TextView textView = alertDialog.findViewById(R.id.phoenix_custom_dialog_title);
        assertEquals("Unexpected dialog title", activity.getString(R.string.phoenix_unable_to_use_this_account), textView.getText());
        TextView descriptionTextView = alertDialog.findViewById(R.id.account_custom_dialog_description);
        assertEquals("Unexpected description", activity.getString(R.string.phoenix_invalid_refresh_token_error), descriptionTextView.getText());

        alertDialog.findViewById(R.id.account_custom_dialog_button_two).performClick();
        assertFalse("Dialogue should have been dismissed", alertDialog.isShowing());
        assertFalse("Account should still be not logged in", account.isLoggedIn());
    }

    @Test
    public void onAccountSelected_WhenInvalidRequest_ShouldLogSelectAccountError() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(OnRefreshTokenResponse.INVALID_REQUEST);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        activity.onAccountSelected(0, account);
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_ERROR), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error status is incorrect", EventLogger.RefreshTokenUserEvents.INVALID_REQUEST, realMap.get(TRACKING_KEY_ERROR_CODE));
        assertEquals("Error msg is incorrect", EventLogger.RefreshTokenUserEvents.INVALID_REQUEST_MSG, realMap.get(TRACKING_KEY_ERROR_MESSAGE));
    }

    @Test
    public void onAccountSelected_WhenInvalidScope_ShouldLogSelectAccountError() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(OnRefreshTokenResponse.INVALID_SCOPE);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        activity.onAccountSelected(0, account);
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_ERROR), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error status is incorrect", EventLogger.RefreshTokenUserEvents.INVALID_SCOPE, realMap.get(TRACKING_KEY_ERROR_CODE));
        assertEquals("Error msg is incorrect", EventLogger.RefreshTokenUserEvents.INVALID_SCOPE_MSG, realMap.get(TRACKING_KEY_ERROR_MESSAGE));
    }

    @Test
    public void onAccountSelected_WhenUnauthClient_ShouldLogSelectAccountError() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(OnRefreshTokenResponse.UNAUTHORIZED_CLIENT);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        activity.onAccountSelected(0, account);
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_ERROR), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error status is incorrect", EventLogger.RefreshTokenUserEvents.UNAUTHORIZED_CLIENT, realMap.get(TRACKING_KEY_ERROR_CODE));
        assertEquals("Error msg is incorrect", EventLogger.RefreshTokenUserEvents.UNAUTHORIZED_CLIENT_MSG, realMap.get(TRACKING_KEY_ERROR_MESSAGE));
    }

    @Test
    public void onAccountSelected_WhenAccountLoginRefreshTokenIsInvalidNetworkOutage_ShouldCallNotifyAccountSetChangedAndLogSelectAccountError() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(OnRefreshTokenResponse.NETWORK_ERROR);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        activity.onAccountSelected(0, account);
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_ACCOUNT_PICKER_SELECT_ACCOUNT_ERROR), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error status is incorrect", EventLogger.RefreshTokenUserEvents.NETWORK_ERROR, realMap.get(TRACKING_KEY_ERROR_CODE));
        assertEquals("Error msg is incorrect", EventLogger.RefreshTokenUserEvents.NETWORK_ERROR_MSG, realMap.get(TRACKING_KEY_ERROR_MESSAGE));

        verify(account, times(1)).refreshToken(any(), any());
        verify(activity, times(1)).notifyAccountSetChanged();
        assertFalse("Account should be disabled", account.isLoggedIn());
        verify(activity, times(1)).showNoNetworkDialog();

        Dialog alertDialog = ShadowDialog.getLatestDialog();
        TextView textView = alertDialog.findViewById(R.id.phoenix_custom_dialog_title);
        assertEquals("Unexpected dialog title", activity.getString(R.string.phoenix_unable_to_use_this_account), textView.getText());
        TextView messageTextView = alertDialog.findViewById(R.id.account_custom_dialog_message);
        assertEquals("Unexpected message", activity.getString(R.string.phoenix_no_internet_connection), messageTextView.getText());

        alertDialog.findViewById(R.id.account_custom_dialog_button).performClick();
        assertFalse("Dialogue should have been dismissed", alertDialog.isShowing());
        assertFalse("Account should still be not logged in", account.isLoggedIn());
    }

    @Test
    public void onAccountToggled_WhenAccountLoginRefreshTokenIsInvalidNetworkOutageAirplaneMode_ShouldCallNotifyAccountSetChangedAndShowAirplaneMsg() throws Exception {
        setNetworkConnectivity(false);
        ShadowSettings.setAirplaneMode(true);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(OnRefreshTokenResponse.NETWORK_ERROR);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountSelected(0, account);

        verify(account, times(1)).refreshToken(any(), any());
        verify(activity, times(1)).notifyAccountSetChanged();
        verify(activity, times(1)).showNoNetworkDialog();

        Dialog alertDialog = ShadowDialog.getLatestDialog();
        TextView textView = alertDialog.findViewById(R.id.phoenix_custom_dialog_title);
        assertEquals("Unexpected dialog title", activity.getString(R.string.phoenix_login_airplane_title), textView.getText());
        TextView descriptionTextView = alertDialog.findViewById(R.id.account_custom_dialog_description);
        assertEquals("Unexpected message", activity.getString(R.string.phoenix_login_airplane_mode), descriptionTextView.getText());

        alertDialog.findViewById(R.id.account_custom_dialog_button_one).performClick();
        assertFalse("Dialogue should have been dismissed", alertDialog.isShowing());
        assertFalse("Account should still be not logged in", account.isLoggedIn());
        setNetworkConnectivity(true);
    }

    @Test
    public void onAccountSelected_WhenAccountLoginRefreshTokenIsInvalidServerError_ShouldCallNotifyAccountSetChanged() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(OnRefreshTokenResponse.SERVER_ERROR);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        activity.onAccountSelected(0, account);

        verify(account, times(1)).refreshToken(any(), any());
        verify(activity, times(1)).notifyAccountSetChanged();
        assertFalse("Account should be disabled", account.isLoggedIn());
        verify(activity, times(1)).showTryAgainLaterDialog();

        Dialog alertDialog = ShadowDialog.getLatestDialog();
        TextView textView = alertDialog.findViewById(R.id.phoenix_custom_dialog_title);
        assertEquals("Unexpected dialog title", activity.getString(R.string.phoenix_unable_to_use_this_account), textView.getText());
        TextView messageTextView = alertDialog.findViewById(R.id.account_custom_dialog_message);
        assertEquals("Unexpected message", activity.getString(R.string.phoenix_try_again_error), messageTextView.getText());

        alertDialog.findViewById(R.id.account_custom_dialog_button).performClick();
        assertFalse("Dialogue should have been dismissed", alertDialog.isShowing());
        assertFalse("Account should still be not logged in", account.isLoggedIn());
    }

    @Test
    public void onActivityResult_WhenRequestCodeForAuthAndResultOKAndShouldDismissActivity_ShouldSetAccountChangedAndFinish() {
        AccountPickerActivity activity = Robolectric.buildActivity(AccountPickerActivity.class).create().get();
        AccountPickerActivity activitySpy = spy(activity);
        Intent returnIntent = new Intent();
        String username = "username";
        returnIntent.putExtra(IAuthManager.KEY_USERNAME, username);
        activitySpy.onActivityResult(AccountPickerActivity.REQUEST_CODE_FOR_AUTH, Activity.RESULT_OK, returnIntent);

        verify(activitySpy).finish();
        verify(activitySpy).setResult(eq(Activity.RESULT_OK), any(Intent.class));

        assertTrue("Activity should be finishing", activitySpy.isFinishing());
    }

    @Test
    public void onActivityResult_WhenRequestCodeForAuthAndResultCancelAndNoAccount_ShouldSetAccountChangedAndFinishAndLogFinish() {
        AccountPickerActivity activity = setupActivity(AccountPickerActivity.class);
        AccountPickerActivity activitySpy = spy(activity);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        activitySpy.onActivityResult(AccountPickerActivity.REQUEST_CODE_FOR_AUTH, Activity.RESULT_CANCELED, mock(Intent.class));
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_ACCOUNT_PICKER_END), isNull(Map.class));
        verify(activitySpy).finish();
        assertTrue("Activity should be finishing", activitySpy.isFinishing());
    }

    @Test
    public void onActivityResult_WhenRequestCodeForAuthAndResultCancelAndHasAccount_ShouldSetAccountChangedAndDoNotFinish() {
        AccountPickerActivity activity = Robolectric.setupActivity(AccountPickerActivity.class);
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<Account> accounts = new ArrayList<>();
        Account accountMock1 = mock(Account.class);
        accounts.add(accountMock1);
        doReturn(accounts).when(authManager).getAllDisabledAccounts();
        AccountPickerAdapter.Callback listener = mock(AccountPickerAdapter.Callback.class);

        AccountPickerAdapter adapter = new AccountPickerAdapter(listener, authManager);

        AccountPickerActivity activitySpy = spy(activity);
        activitySpy.mAccountsAdapter = adapter;
        activitySpy.onActivityResult(AccountPickerActivity.REQUEST_CODE_FOR_AUTH, Activity.RESULT_CANCELED, mock(Intent.class));
        verify(activitySpy, never()).finish();
    }

    @Test
    public void onActivityResult_WhenRequestCodeForAuthAndResultError_ShouldShowErrorDialog() {
        AccountPickerActivity activity = setupActivity(AccountPickerActivity.class);
        AccountPickerActivity activitySpy = spy(activity);
        activitySpy.onActivityResult(AccountPickerActivity.REQUEST_CODE_FOR_AUTH, IAuthManager.RESULT_CODE_ERROR, mock(Intent.class));
        assertTrue("Activity should be finishing", activitySpy.isFinishing());
        //TODO Verification
    }

    @Test
    public void onActivityResult_WhenRequestCodeForOthers_ShouldCallSuperOnActivityResult() {
        AccountPickerActivity activity = setupActivity(AccountPickerActivity.class);
        AccountPickerActivity activitySpy = spy(activity);
        activitySpy.onActivityResult(100, Activity.RESULT_OK, mock(Intent.class));

        //TODO Verification
    }

    @Test
    public void onAddAccount_WhenNewAccountIsAdded_ShouldLaunchAuthIntent() throws Exception {
        AuthConfig authConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(),
                Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        authConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountPickerActivity activity = setupActivity(AccountPickerActivity.class);
        activity.onAddAccount();
        ShadowActivity shadowActivity = shadowOf(activity);
        ShadowActivity.IntentForResult intentForResult = shadowActivity.getNextStartedActivityForResult();
        Assert.assertEquals("Must have a request code", AccountPickerActivity.REQUEST_CODE_FOR_AUTH, intentForResult.requestCode);
        Assert.assertEquals(BuildConfig.APPLICATION_ID + ".AuthActivity", intentForResult.intent.getComponent().getClassName());
    }

    @Test
    public void onStart_WhenIsCalledOnKitKat_ShouldCallSetResultForPreLollipop() throws Exception {
        AuthConfig authConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(),
                Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        authConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        activity.onStart();
        verify(activity).setResultForPreLollipop();
    }

    @Test
    @Config(sdk = Build.VERSION_CODES.LOLLIPOP)
    public void onStart_WhenIsCalledOnLollipop_ShouldCallSetResultForPreLollipop() throws Exception {
        AuthConfig authConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(),
                Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        authConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        activity.onStart();
        verify(activity).setResultForPreLollipop();
    }

    @Test
    @Config(sdk = Build.VERSION_CODES.LOLLIPOP)
    public void setResultForPreLollipop_WhenNumOfEnabledAccountsChangedOnLollipop_ShouldNotCallSetResult() throws Exception {
        AuthConfig authConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(),
                Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        authConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        activity.mNumOfEnabledAccounts = 0;
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = TestValues.getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        activity.setResultForPreLollipop();
        verify(activity, never()).setResult(anyInt(), any(Intent.class));
    }

    @Test
    @Config(sdk = Build.VERSION_CODES.KITKAT)
    public void setResultForPreLollipop_WhenNumOfEnabledAccountsChangedOnKitKat_ShouldCallSetResult() throws Exception {
        AuthConfig authConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(),
                Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        authConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        activity.mNumOfEnabledAccounts = 0;
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = TestValues.getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        ArgumentCaptor<Intent> capturedIntent = ArgumentCaptor.forClass(Intent.class);
        activity.setResultForPreLollipop();
        verify(activity).setResult(eq(Activity.RESULT_OK), capturedIntent.capture());
        Intent capturedIntentValue = capturedIntent.getValue();
        assertEquals(capturedIntentValue.getBundleExtra(IAuthManager.KEY_USERNAME), AuthHelper.createResultIntentForAuth(authManager.getAllAccounts().get(0)).getBundleExtra(IAuthManager.KEY_USERNAME));
    }

    @Test
    @Config(sdk = Build.VERSION_CODES.KITKAT)
    public void setResultForPreLollipop_WhenNumOfEnabledAccountsDoesNotChangeOnKitKat_ShouldNotCallSetResult() throws Exception {
        AuthConfig authConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(),
                Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        authConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        activity.setResultForPreLollipop();
        verify(activity, never()).setResult(anyInt(), any(Intent.class));
    }

    @Test
    public void onStop_WhenHasProgressDialog_ShouldDismissDialog() throws Exception {
        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        when(activity.isFinishing()).thenReturn(false);
        activity.showProgressDialog();
        Dialog latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        assertTrue("Should show dialog", latestDialog.isShowing());

        activity.onStop();
        latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        assertFalse("Should not show dialog", latestDialog.isShowing());
    }

    @Test
    public void safeDismissProgressDialog_WhenDialogShowing_ShouldDismiss() {
        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        when(activity.isFinishing()).thenReturn(false);

        activity.showProgressDialog();

        Dialog latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        assertTrue("Should show dialog", latestDialog.isShowing());

        activity.safeDismissProgressDialog();

        latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        assertFalse("Should not show dialog", latestDialog.isShowing());
    }

    @Test
    public void showProgressDialog_WhenProgressDialogIsNull_ShouldCreateNewDialogAndShow() throws Exception {
        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        when(activity.isFinishing()).thenReturn(false);

        activity.showProgressDialog();

        Dialog latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        assertTrue("Should show dialog", latestDialog.isShowing());
    }

    @Test
    public void showProgressDialog_WhenProgressDialogIsNotNull_ShouldShowExistingProgressDialog() throws Exception {
        AccountPickerActivity activity = spy(setupActivity(AccountPickerActivity.class));
        when(activity.isFinishing()).thenReturn(false);

        activity.showProgressDialog(); //progress dialog is created
        Dialog dialogSpy = spy(activity.mProgressDialog);
        activity.mProgressDialog = dialogSpy;
        activity.safeDismissProgressDialog();

        ShadowDialog.reset();
        activity.showProgressDialog();
        verify(dialogSpy, never()).setCanceledOnTouchOutside(anyBoolean());

        Dialog latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        assertTrue("Should show dialog", latestDialog.isShowing());
    }

    private static void setNetworkConnectivity(boolean isConnected) {
        ConnectivityManager cm = (ConnectivityManager)
                RuntimeEnvironment.application.getSystemService("connectivity");

        ShadowConnectivityManager shadowCM = shadowOf(cm);
        ShadowNetworkInfo shadowNetworkInfo = shadowOf(shadowCM.getActiveNetworkInfo());
        shadowNetworkInfo.setConnectionStatus(isConnected);
    }

}
