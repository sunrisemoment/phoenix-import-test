package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import org.junit.After;
import org.junit.Before;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Locale;

import static com.oath.mobile.platform.phoenix.core.AuthConfig.AOL_AUTH_CONFIG_KEY;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.AUTH_CONFIG_EXPIRE_TIME_KEY;
import static com.oath.mobile.platform.phoenix.core.AuthConfig.OATH_AUTH_CONFIG_KEY;

/**
 * Created by nsoni on 10/19/17.
 */

@Config(manifest = Config.NONE, sdk = Build.VERSION_CODES.KITKAT, constants = BuildConfig.class)
public abstract class BaseTest {

    @Before
    public void beforeTest() throws Exception {
        disableDiscoveryRequest();
    }

    @After
    public void afterTest() {
        AuthManager actual = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        actual.removeAccounts();
        AccountNetworkAPI.resetInstance();
        clearSharedPreferences();
        EventLogger.sEventLoggerSingleton = null; // reset to get fresh instance of EventLogger so Roboelectric won't complain on re-spy
        resetAuthConfigInSharedPreference();
        Locale.setDefault(new Locale("en", "US"));
    }

    private void clearSharedPreferences() {
        SharedPreferences prefs =
                RuntimeEnvironment.application.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }

    private void disableDiscoveryRequest() {
        SharedPreferences sharedPreferences = RuntimeEnvironment.application.getApplicationContext().getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(AUTH_CONFIG_EXPIRE_TIME_KEY, System.currentTimeMillis() + 5000);
        editor.apply();
    }

    private void resetAuthConfigInSharedPreference() {
        SharedPreferences sharedPreferences = RuntimeEnvironment.application.getApplicationContext().getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(OATH_AUTH_CONFIG_KEY);
        editor.remove(AOL_AUTH_CONFIG_KEY);
    }
}
