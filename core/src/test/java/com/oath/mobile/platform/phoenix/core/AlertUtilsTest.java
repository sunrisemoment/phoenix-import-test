package com.oath.mobile.platform.phoenix.core;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowToast;
import org.robolectric.shadows.ShadowView;

import java.lang.reflect.Constructor;

import static com.oath.mobile.platform.phoenix.core.AccountNetworkAPI.ERROR_CODE_ACCOUNT_NETWORK_SSL_VERIFICATION_ERROR;
import static com.oath.mobile.platform.phoenix.core.AccountNetworkAPI.ERROR_CODE_ACCOUNT_NETWORK_UNAVAILABLE;
import static com.oath.mobile.platform.phoenix.core.AccountNetworkAPI.ERROR_CODE_NETWORK_UNAVAILABLE_AIRPLANE_MODE;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.robolectric.Robolectric.setupActivity;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by yuhongli on 11/6/17.
 */
@RunWith(RobolectricTestRunner.class)
public class AlertUtilsTest extends BaseTest {

    private Context mContextShadow;

    @Before
    public void setUp() throws Exception {
        mContextShadow = RuntimeEnvironment.application.getApplicationContext();
    }

    @Test
    public void constructor_WhenInvoked_ShouldThrowException() throws Exception {
        Class<?> alertUtils = Class.forName("com.oath.mobile.platform.phoenix.core.AlertUtils");
        try {
            alertUtils.newInstance();
        } catch (IllegalAccessException e) {
            assertEquals("class java.lang.IllegalAccessException", e.getClass().toString());
        }
        Constructor<?> constructor = alertUtils.getDeclaredConstructor();
        constructor.setAccessible(true); // set it public
        constructor.newInstance(); // invoke it for coverage
    }

    @Test
    public void showAirplaneModeDialog_WhenActivityIsNotFinishing_ShouldShowTheDialog() throws Exception {
        Activity activity = setupActivity(Activity.class);

        AlertUtils.showAirplaneModeDialog(activity);

        Dialog dialog = ShadowAlertDialog.getLatestDialog();
        assertNotNull("Dialog should not be null", dialog);
        TextView messageText = (TextView) dialog.findViewById(R.id.account_custom_dialog_description);
        assertTrue("Alert dialog is not showing", dialog.isShowing());
        assertEquals("Alert dialog message is not correct", mContextShadow.getString(R.string.phoenix_login_airplane_mode), messageText.getText());
    }

    @Test
    public void showAirplaneModeDialog_WhenActivityIsFinishing_ShouldNotShowTheDialog() throws Exception {
        Activity activity = setupActivity(Activity.class);
        activity.finish();

        AlertUtils.showAirplaneModeDialog(activity);

        Dialog dialog = ShadowAlertDialog.getLatestDialog();
        assertNull("Dialog should be null", dialog);
    }

    @Test
    public void showErrorDialog_WhenActivityIsNotFinishing_ShouldShowTheDialog() throws Exception {
        Activity activity = setupActivity(Activity.class);
        String expectedMessage = "Some error msg!";

        AlertUtils.showErrorDialog(activity, expectedMessage);

        Dialog dialog = ShadowAlertDialog.getLatestDialog();
        assertNotNull("Dialog should not be null", dialog);
        TextView messageText = dialog.findViewById(R.id.account_custom_dialog_message);
        assertTrue("Alert dialog is not showing", dialog.isShowing());
        assertEquals("Alert dialog message is not correct", expectedMessage, messageText.getText());
    }

    @Test
    public void showErrorDialog_WhenActivityIsFinishing_ShouldNotShowTheDialog() throws Exception {
        Activity activity = setupActivity(Activity.class);
        activity.finish();

        AlertUtils.showErrorDialog(activity, "Some message!");

        Dialog dialog = ShadowAlertDialog.getLatestDialog();
        assertNull("Dialog should be null", dialog);
    }

    @Test
    public void showNetworkAuthenticationDialog_WhenActivityIsNotFinishing_ShouldShowTheDialog() throws Exception {
        Activity activity = setupActivity(Activity.class);

        AlertUtils.showNetworkAuthenticationDialog(activity);

        Dialog dialog = ShadowAlertDialog.getLatestDialog();
        assertNotNull("Dialog should not be null", dialog);
        TextView messageText = dialog.findViewById(R.id.account_custom_dialog_description);
        assertTrue("Alert dialog is not showing", dialog.isShowing());
        assertEquals("Alert dialog message is not correct", mContextShadow.getString(R.string.phoenix_network_authentication_required), messageText.getText());
    }

    @Test
    public void showNetworkAuthenticationDialog_WhenActivityIsFinishing_ShouldNotShowTheDialog() throws Exception {
        Activity activity = setupActivity(Activity.class);
        activity.finish();

        AlertUtils.showNetworkAuthenticationDialog(activity);

        Dialog dialog = ShadowAlertDialog.getLatestDialog();
        assertNull("Dialog should be null", dialog);
    }

    @Test
    public void showToast_WhenActivityIsNotFinishing_ShouldShowTheDialog() throws Exception {
        Activity activity = setupActivity(Activity.class);
        String expectedMessage = "Some error msg!";

        AlertUtils.showToast(activity, expectedMessage);

        Toast dialog = ShadowToast.getLatestToast();
        assertNotNull("Toast should not be null", dialog);
        assertEquals("Toast message is not correct", expectedMessage, ShadowToast.getTextOfLatestToast());
    }

    @Test
    public void showToast_WhenActivityIsFinishing_ShouldNotShowTheDialog() throws Exception {
        Activity activity = setupActivity(Activity.class);
        activity.finish();

        AlertUtils.showToast(activity, "Some message!");

        Dialog dialog = ShadowAlertDialog.getLatestDialog();
        assertNull("Toast should be null", dialog);
    }

    @Test
    public void showNetworkErrorDialog_WhenAirplanModeIsOn_ShouldShowAirplaneModeDialog() throws Exception {
        Activity activity = setupActivity(Activity.class);

        AlertUtils.showNetworkErrorDialog(activity, ERROR_CODE_NETWORK_UNAVAILABLE_AIRPLANE_MODE, "");

        Dialog dialog = ShadowAlertDialog.getLatestDialog();
        assertNotNull("Dialog should not be null", dialog);
        TextView messageText = dialog.findViewById(R.id.account_custom_dialog_description);
        assertTrue("Alert dialog is not showing", dialog.isShowing());
        assertEquals("Alert dialog message is not correct", mContextShadow.getString(R.string.phoenix_login_airplane_mode), messageText.getText());
    }

    @Test
    public void showNetworkErrorDialog_WhenAirplaneModeIsOnAndClickSetting_ShouldShowSettingAction() throws Exception {
        Activity activity = setupActivity(Activity.class);

        AlertUtils.showNetworkErrorDialog(activity, ERROR_CODE_NETWORK_UNAVAILABLE_AIRPLANE_MODE, "");

        Dialog dialog = ShadowAlertDialog.getLatestDialog();
        assertNotNull("Dialog should not be null", dialog);
        TextView messageText = dialog.findViewById(R.id.account_custom_dialog_description);
        assertTrue("Alert dialog is not showing", dialog.isShowing());
        assertEquals("Alert dialog message is not correct", mContextShadow.getString(R.string.phoenix_login_airplane_mode), messageText.getText());
        TextView dialogButton = dialog.findViewById(R.id.account_custom_dialog_button_two);

        ShadowView.clickOn(dialogButton);
        assertFalse("Dialog should have been dismissed", dialog.isShowing());
        ShadowActivity shadowActivity = shadowOf(activity);
        Intent startedIntent = shadowActivity.getNextStartedActivity();
        assertNotNull(startedIntent);
        assertEquals("Unexpected action", android.provider.Settings.ACTION_SETTINGS, startedIntent.getAction());
    }

    @Test
    public void showNetworkErrorDialog_WhenAirplaneModeIsOnAndClickCancel_ShouldDismissDialog() throws Exception {
        Activity activity = setupActivity(Activity.class);

        AlertUtils.showNetworkErrorDialog(activity, ERROR_CODE_NETWORK_UNAVAILABLE_AIRPLANE_MODE, "");

        Dialog dialog = ShadowAlertDialog.getLatestDialog();
        assertNotNull("Dialog should not be null", dialog);
        TextView messageText = dialog.findViewById(R.id.account_custom_dialog_description);
        assertTrue("Alert dialog is not showing", dialog.isShowing());
        assertEquals("Alert dialog message is not correct", mContextShadow.getString(R.string.phoenix_login_airplane_mode), messageText.getText());
        TextView dialogButton = dialog.findViewById(R.id.account_custom_dialog_button_one);

        ShadowView.clickOn(dialogButton);
        assertFalse("Dialog should have been dismissed", dialog.isShowing());
        ShadowActivity shadowActivity = shadowOf(activity);
        Intent startedIntent = shadowActivity.getNextStartedActivity();
        assertNull(startedIntent);
    }

    @Test
    public void showNetworkErrorDialog_WhenNetworkSSLVerificationErrorAndClickCancel_ShouldShowDialog() throws Exception {
        Activity activity = setupActivity(Activity.class);

        AlertUtils.showNetworkErrorDialog(activity, ERROR_CODE_ACCOUNT_NETWORK_SSL_VERIFICATION_ERROR, "");

        Dialog dialog = ShadowAlertDialog.getLatestDialog();
        assertNotNull("Dialog should not be null", dialog);
        TextView messageText = dialog.findViewById(R.id.account_custom_dialog_description);
        assertTrue("Alert dialog is not showing", dialog.isShowing());
        assertEquals("Alert dialog message is not correct", mContextShadow.getString(R.string.phoenix_network_authentication_required), messageText.getText());
        Button dialogButton = dialog.findViewById(R.id.account_custom_dialog_button_one);

        ShadowView.clickOn(dialogButton);
        assertFalse("Dialog should have been dismissed", dialog.isShowing());
    }

    @Test
    public void showNetworkErrorDialog_WhenNetworkSSLVerificationErrorAndClickGoToBrowser_ShouldShowDialogAndLaunchBrowser() throws Exception {
        Activity activity = setupActivity(Activity.class);

        AlertUtils.showNetworkErrorDialog(activity, ERROR_CODE_ACCOUNT_NETWORK_SSL_VERIFICATION_ERROR, "");

        Dialog dialog = ShadowAlertDialog.getLatestDialog();
        assertNotNull("Dialog should not be null", dialog);
        TextView messageText = dialog.findViewById(R.id.account_custom_dialog_description);
        assertTrue("Alert dialog is not showing", dialog.isShowing());
        assertEquals("Alert dialog message is not correct", mContextShadow.getString(R.string.phoenix_network_authentication_required), messageText.getText());
        TextView dialogButton = dialog.findViewById(R.id.account_custom_dialog_button_two);

        ShadowView.clickOn(dialogButton);
        assertFalse("Dialog should have been dismissed", dialog.isShowing());
        ShadowActivity shadowActivity = shadowOf(activity);
        Intent startedIntent = shadowActivity.getNextStartedActivity();
        assertNotNull(startedIntent);
        assertEquals("Unexpected action", Intent.ACTION_VIEW, startedIntent.getAction());
    }

    @Test
    public void showNetworkErrorDialog_WhenNetworkUnavailable_ShouldShowDialog() throws Exception {
        Activity activity = setupActivity(Activity.class);
        String message = "Some message";
        AlertUtils.showNetworkErrorDialog(activity, ERROR_CODE_ACCOUNT_NETWORK_UNAVAILABLE, message);

        Dialog dialog = ShadowAlertDialog.getLatestDialog();
        assertNotNull("Dialog should not be null", dialog);
        TextView messageText = dialog.findViewById(R.id.account_custom_dialog_message);
        assertTrue("Alert dialog is not showing", dialog.isShowing());
        assertEquals("Alert dialog message is not correct", message, messageText.getText());
        Button dialogButton = dialog.findViewById(R.id.account_custom_dialog_button);

        ShadowView.clickOn(dialogButton);
        assertFalse("Dialog should have been dismissed", dialog.isShowing());
    }

    @Test
    public void showNetworkErrorDialog_WhenOtherError_ShouldShowToast() throws Exception {
        Activity activity = setupActivity(Activity.class);
        String message = "Some message";
        AlertUtils.showNetworkErrorDialog(activity, 8000, message);

        Dialog dialog = ShadowAlertDialog.getLatestDialog();
        assertNull("Dialog should not be null", dialog);
        Toast toast = ShadowToast.getLatestToast();
        assertNotNull("Toast should not be null", toast);
    }
}
