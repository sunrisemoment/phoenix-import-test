package com.oath.mobile.platform.phoenix.core;

import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.yahoo.mobile.client.share.util.ThreadPoolExecutorSingleton;

import junit.framework.Assert;

import net.openid.appauth.TokenResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;
import org.robolectric.shadows.ShadowDialog;
import org.robolectric.shadows.ShadowView;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;

import static com.oath.mobile.platform.phoenix.core.AuthConfig.OATH_AUTH_CONFIG_KEY;
import static com.oath.mobile.platform.phoenix.core.AuthHelper.RefreshTokenResponseListener.ERROR_CODE_IDP_SWITCHED;
import static com.oath.mobile.platform.phoenix.core.AuthHelper.RevokeTokenResponseListener.REVOKE_TOKEN_SUCCESS;
import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_END;
import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_START;
import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_END;
import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_REMOVE_CANCEL;
import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_REMOVE_FAILURE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_REMOVE_START;
import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_REMOVE_SUCCESS;
import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_SIGN_IN_CANCEL;
import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_SIGN_IN_ERROR;
import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_SIGN_IN_START;
import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_SIGN_IN_SUCCESS;
import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_START;
import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_FAILURE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_SUCCESS;
import static com.oath.mobile.platform.phoenix.core.EventLogger.TRACKING_KEY_ERROR_CODE;
import static com.oath.mobile.platform.phoenix.core.EventLogger.TRACKING_KEY_ERROR_MESSAGE;
import static com.oath.mobile.platform.phoenix.core.ManageAccountsActivity.KEY_SHOW_MANAGE_ACCOUNTS_ONBOARDING;
import static com.oath.mobile.platform.phoenix.core.OnRefreshTokenResponse.REAUTHORIZE_USER;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_JWT_EMAIL_VALUE;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_JWT_FAMILY_NAME_VALUE;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_JWT_GIVEN_NAME_VALUE;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_JWT_NAME_VALUE;
import static com.oath.mobile.platform.phoenix.core.TestValues.getTestAuthCodeExchangeResponseNonce;
import static com.oath.mobile.platform.phoenix.core.ToolTipWindow.SHARED_PREF_PREFIX;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.robolectric.Robolectric.buildActivity;
import static org.robolectric.Robolectric.setupActivity;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by yuhongli on 10/31/17.
 */
@RunWith(RobolectricTestRunner.class) @Config(sdk = Build.VERSION_CODES.LOLLIPOP)
public class ManageAccountsActivityTest extends BaseTest {
    private Application mContext;
    Intent mManageAccountIntent;

    @Before
    public void setUp() throws Exception {
        mContext = RuntimeEnvironment.application;
        AuthManager actual = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        actual.removeAccounts();
        mManageAccountIntent = new Intent();
        mManageAccountIntent.setClass(mContext, ManageAccountsActivity.class);
        mManageAccountIntent.putExtra(ManageAccountsActivity.INTERNAL_LAUNCH_GATE, true);
        mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE).edit().putBoolean(KEY_SHOW_MANAGE_ACCOUNTS_ONBOARDING, false).apply();
    }

    @Test
    public void onCreate_WhenActivityIsInvoked_ShouldNotBeNull() throws Exception {
        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        Assert.assertNotNull("AccountServiceSelectActivity should not be null", activity);
    }

    @Test
    public void onCreate_WhenActivityIsInvokedWithResultReceiverCallback_ShouldSetValueToViewModel() throws Exception {
        ResultReceiver resultReceiver = new ResultReceiver(null);
        Intent intent = new ManageAccounts.IntentBuilder().setResultReceiver(resultReceiver).build(RuntimeEnvironment.application);

        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class, intent).create().get();
        ManageAccountsActivity.ManageAccountsViewModel viewModel = ViewModelProviders.of(activity).get(ManageAccountsActivity.ManageAccountsViewModel.class);
        Assert.assertNotNull("AccountServiceSelectActivity should not be null", activity);

        assertEquals("Should be same", resultReceiver, viewModel.getResultReceiver());
    }

    @Test
    public void onCreate_WhenActivityIsInvokedWithDismissOnAddAccount_ShouldSetValueToViewModel() throws Exception {
        Intent intent = new ManageAccounts.IntentBuilder().enableDismissOnAddAccount().build(RuntimeEnvironment.application);

        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class, intent).create().get();
        ManageAccountsActivity.ManageAccountsViewModel viewModel = ViewModelProviders.of(activity).get(ManageAccountsActivity.ManageAccountsViewModel.class);
        Assert.assertNotNull("AccountServiceSelectActivity should not be null", activity);
        assertTrue(viewModel.shouldDismissOnAddAccount());
    }

    @Test
    public void onCreate_WhenActivityIsInvokedWithOutDismissOnAddAccount_ShouldNotSetValueToViewModel() throws Exception {
        Intent intent = new ManageAccounts.IntentBuilder().build(RuntimeEnvironment.application);

        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class, intent).create().get();
        ManageAccountsActivity.ManageAccountsViewModel viewModel = ViewModelProviders.of(activity).get(ManageAccountsActivity.ManageAccountsViewModel.class);
        Assert.assertNotNull("AccountServiceSelectActivity should not be null", activity);
        assertFalse(viewModel.shouldDismissOnAddAccount());
    }

    @Test
    public void onCreateOptionMenu_WhenMenuCreated_ShouldHaveEditOption() {
        ActivityController<ManageAccountsActivity> controller = buildActivity(ManageAccountsActivity.class, mManageAccountIntent);
        ManageAccountsActivity activity = controller.create().get();

        controller.start().postCreate(null).resume().visible();

        assertNotNull("Edit Option should not be null", activity.mEditMenuItem);
        assertEquals("Unexpected edit option title", "Edit", activity.mEditMenuItem.getTitle());
        assertTrue("Edit Option should be visible", activity.mEditMenuItem.isVisible());

        PopupWindow popupWindow = ShadowApplication.getInstance().getLatestPopupWindow();
        org.junit.Assert.assertNull("PopupWindow should be null", popupWindow);
    }

    @Test
    public void onOptionsItemSelected_WhenEnableEditMode_ShouldShowRemoveButton() throws Exception {
        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();

        activity.onOptionsItemSelected(activity.mEditMenuItem);
        assertEquals("Unexpected edit option title", "Done", activity.mEditMenuItem.getTitle());
    }

    @Test
    public void onOptionsItemSelected_WhenDisableEditMode_ShouldShowEditButton() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        ActivityController<ManageAccountsActivity> controller = buildActivity(ManageAccountsActivity.class, mManageAccountIntent);

        ManageAccountsActivity activity = controller.create().get();
        ToolTipWindow toolTipWindow = spy(activity.mEditButtonToolTipWindow);
        doReturn(true).when(toolTipWindow).isAnchorViewVisible(any(View.class));
        activity.mEditButtonToolTipWindow = toolTipWindow;
        mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE).edit().putBoolean(KEY_SHOW_MANAGE_ACCOUNTS_ONBOARDING, false).apply();
        resetToolTipDisplayCount();
        controller.start().postCreate(null).resume().visible();
        PopupWindow popupWindow = ShadowApplication.getInstance().getLatestPopupWindow();
        assertNotNull("PopupWindow should not be null", popupWindow);
        TextView textView = (TextView) popupWindow.getContentView().findViewById(R.id.tooltip_text);
        org.junit.Assert.assertEquals("Text is not correct", Html.fromHtml(mContext.getResources().getString(R.string.phoenix_manage_accounts_edit_tooltip)).toString(), textView.getText().toString());

        activity.onOptionsItemSelected(activity.mEditMenuItem);
        assertEquals("Unexpected edit option title", "Done", activity.mEditMenuItem.getTitle());

        activity.onOptionsItemSelected(activity.mEditMenuItem);
        assertEquals("Unexpected edit option title", "Edit", activity.mEditMenuItem.getTitle());
    }

    @Test
    public void onOptionsItemSelected_WhenParameterIsNotEdit_ShouldReturnFalse() throws Exception {
        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        MenuItem menu = mock(MenuItem.class);

        assertFalse("Unexpected value of onOptionsItemSelected ", activity.onOptionsItemSelected(menu));
    }

    @Test
    public void onRemoveAccount_WhenConfirmationDialogCancelled_ShouldRetainAccount() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));

        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        activity.onRemoveAccount(1, account);

        Dialog alertDialog = ShadowDialog.getLatestDialog();

        TextView textView = alertDialog.findViewById(R.id.phoenix_custom_dialog_title);
        assertEquals("Unexpected dialog title", activity.getString(R.string.phoenix_remove_account_dialog_title), textView.getText());

        alertDialog.findViewById(R.id.account_custom_dialog_button_two).performClick();
        assertFalse("Dialog should have been dismissed", alertDialog.isShowing());
        assertFalse("HasAccountChanged should be false", activity.mViewModel.hasAccountChanged());
        assertEquals("List items count does not match", ManageAccountsAdapter.HEADER_OFFSET + 2, ((RecyclerView) activity.findViewById(R.id.phoenix_manage_accounts_list)).getAdapter().getItemCount());

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_REMOVE_CANCEL), Matchers.isNull(Map.class));
    }

    @Test
    public void onRemoveAccount_WhenConfirmationDialogOk_ShouldRemoveAccount() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(RuntimeEnvironment.application.getApplicationContext(), OATH_AUTH_CONFIG_KEY);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        String username = account.getUserName();

        ResultReceiver resultReceiver = spy(new ResultReceiver(null) {
            @Override
            public void onReceiveResult(int resultCode, Bundle resultData) {
                super.onReceiveResult(resultCode, resultData);
            }
        });
        Intent intent = new ManageAccounts.IntentBuilder().setResultReceiver(resultReceiver).build(RuntimeEnvironment.application);

        ManageAccountsActivity activity = spy(Robolectric.buildActivity(ManageAccountsActivity.class, intent).create().get());
        mockSingletonExecutor();
        activity.onRemoveAccount(1, account);

        Dialog dialog = ShadowAlertDialog.getLatestDialog();
        // Assert
        assertTrue("Remove account alert dialog is not showing", dialog.isShowing());

        TextView textView = (TextView) dialog.findViewById(R.id.phoenix_custom_dialog_title);
        assertEquals("Unexpected dialog title", activity.getString(R.string.phoenix_remove_account_dialog_title), textView.getText());

        Button dialogButton = (Button) dialog.findViewById(R.id.account_custom_dialog_button_one);
        assertEquals("Remove account alert dialog remove button doesn't have correct caption", activity.getString(R.string.phoenix_remove_account), dialogButton.getText());

        assertFalse("HasAccountChanged should be false", activity.mViewModel.hasAccountChanged());
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((AccountChangeListener) args[1]).onAccountChanged(REVOKE_TOKEN_SUCCESS);
                return null;
            }
        }).when(account).remove(any(Activity.class), any(AccountChangeListener.class));
        assertEquals("Should have 1 account", 1, activity.mAccountsAdapter.getAccountCount());
        ShadowView.clickOn(dialogButton);
        assertFalse("Dialog should have been dismissed", dialog.isShowing());
        assertTrue("HasAccountChanged should be true", activity.mViewModel.hasAccountChanged());
        assertEquals("Should have 0 account", 0, activity.mAccountsAdapter.getAccountCount());
        verify(activity, times(1)).markAccountRemoved(username);
        verify(activity, never()).markAccountAdded(anyString());
        ArgumentCaptor<Bundle> bundleArgumentCaptor = ArgumentCaptor.forClass(Bundle.class);
        verify(resultReceiver).send(eq(IAuthManager.RESULT_CODE_ACCOUNT_LOGGED_OUT), bundleArgumentCaptor.capture());
        assertEquals("test@yahoo.com", bundleArgumentCaptor.getValue().getString(IAuthManager.KEY_USERNAME));
        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_REMOVE_SUCCESS), isNull(Map.class));
    }

    @Test
    public void onCreate_WhenValidBundleWithMissingRemovedAndAddedLists_ShouldMakeNewLists() {
        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class).get();
        Bundle bundle = new Bundle();
        bundle.putBoolean(ManageAccountsActivity.INTERNAL_LAUNCH_GATE, true);
        activity.onCreate(bundle);

        assertNotNull("Added Accounts List was not created successfully", activity.mAddedAccounts);
        assertNotNull("Removed Accounts List was not created successfully", activity.mRemovedAccounts);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void onCreate_WhenIsNotFromIntentBuilder_ShouldThrowException() {
        ManageAccountsActivity accountsActivity = setupActivity(ManageAccountsActivity.class);
    }

    @Test
    public void onCreate_ShouldLogStart() {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();

        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_START), isNull(Map.class));
    }

    @Test
    public void onFinish_ShouldLogEnd() {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();

        activity.finish();

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_END), isNull(Map.class));
    }

    @Test
    public void onEnableEdit_ShouldLogEditStart() {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        activity.enableEditMode();

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_START), isNull(Map.class));
    }

    @Test
    public void onDisableEdit_ShouldLogEditEnd() {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        activity.disableEditMode();

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_END), isNull(Map.class));
    }

    @Test
    public void onSaveInstanceState_WhenInformationIsStored_ShouldRetainInformationAndGetItAfter() {
        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();

        String username1 = "username1";
        String username2 = "username2";

        activity.markAccountAdded(username1);
        activity.markAccountRemoved(username2);

        Bundle savedInstanceBundle = new Bundle();
        activity.onSaveInstanceState(savedInstanceBundle);

        assertTrue("Saved bundle does not have added account list", savedInstanceBundle.containsKey(ManageAccounts.KEY_ADDED_ACCOUNTS_LIST));
        assertTrue("Saved bundle does not have removed account list", savedInstanceBundle.containsKey(ManageAccounts.KEY_REMOVED_ACCOUNTS_LIST));

        ManageAccountsActivity activity1 = Robolectric.buildActivity(ManageAccountsActivity.class).get();
        activity1.onCreate(savedInstanceBundle);

        assertTrue("Restored data does not contain the right information for added accounts list", activity1.mAddedAccounts.contains(username1));
        assertTrue("Restored data does not contain the right information for removed accounts list", activity1.mRemovedAccounts.contains(username2));
    }

    @Test
    public void crudOperationWithAddedAndRemovedLists_WhenAdding_EnsureNoDuplicate() {
        ManageAccountsActivity accountsActivity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();

        String username1 = "username1";
        String username2 = "username2";
        String username3 = "username3";
        String username4 = "username4";

        accountsActivity.markAccountAdded(username1);
        accountsActivity.markAccountAdded(username1);
        assertTrue("More than 1 item is added", accountsActivity.mAddedAccounts.size() == 1);

        accountsActivity.markAccountRemoved(username1);
        assertTrue("More than 1 item is added", accountsActivity.mRemovedAccounts.size() == 1);
        assertTrue("Guid is not removed from added List", accountsActivity.mAddedAccounts.size() == 0);

        accountsActivity.markAccountAdded(username1);
        assertTrue("More than 1 item is added", accountsActivity.mAddedAccounts.size() == 1);
        assertTrue("Guid is not removed from Removed List", accountsActivity.mRemovedAccounts.size() == 0);

        accountsActivity.markAccountAdded(username2);
        accountsActivity.markAccountAdded(username3);
        accountsActivity.markAccountAdded(username4);
        accountsActivity.markAccountRemoved(username4);

        assertTrue("Not the right amount of guids in list", accountsActivity.mAddedAccounts.size() == 3);
        assertTrue("Not the right amount of guids in list", accountsActivity.mRemovedAccounts.size() == 1);
    }

    @Test
    public void onRemoveAccount_WhenStartingToRemove_ShouldLogRemoveStart() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(),
                Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(RuntimeEnvironment.application.getApplicationContext(), OATH_AUTH_CONFIG_KEY);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));

        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        activity.onRemoveAccount(1, account);

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_REMOVE_START), isNull(Map.class));
    }

    @Test
    public void onRemoveAccount_WhenConfirmationDialogOkButRemoveAccountNotSuccess_ShouldNotRemoveAccountAndLogRemoveFailed() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        AuthConfig config = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(),
                Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        config.saveAuthConfig(RuntimeEnvironment.application.getApplicationContext(), OATH_AUTH_CONFIG_KEY);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));

        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        activity.onRemoveAccount(1, account);

        Dialog dialog = ShadowAlertDialog.getLatestDialog();
        // Assert
        assertTrue("Remove account alert dialog is not showing", dialog.isShowing());

        TextView textView = (TextView) dialog.findViewById(R.id.phoenix_custom_dialog_title);
        assertEquals("Unexpected dialog title", activity.getString(R.string.phoenix_remove_account_dialog_title), textView.getText());

        Button dialogButton = (Button) dialog.findViewById(R.id.account_custom_dialog_button_one);
        assertEquals("Remove account alert dialog remove button doesn't have correct caption", activity.getString(R.string.phoenix_remove_account), dialogButton.getText());

        assertFalse("HasAccountChanged should be false", activity.mViewModel.hasAccountChanged());

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((AccountChangeListener) args[1]).onAccountChanged(100); //any code other than 0
                return null;
            }
        }).when(account).remove(any(Activity.class), any(AccountChangeListener.class));
        ShadowView.clickOn(dialogButton);
        assertFalse("HasAccountChanged should be false", activity.mViewModel.hasAccountChanged());
        assertFalse("Dialog should have been dismissed", dialog.isShowing());

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_EDIT_ACCOUNTS_REMOVE_FAILURE), isNull(Map.class));
    }

    @Test
    public void onAccountToggled_WhenAccountNotLoggedIn_ShouldCallRefreshToken() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));

        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountToggled(0, account, mockListener);

        verify(account, times(1)).refreshToken(any(), any());
    }

    @Test
    public void onAccountToggled_WhenAccountIsLoggedInButNotActive_ShouldCallRefreshToken() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.setIsActive(false);

        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountToggled(0, account, mockListener);

        verify(account, times(1)).refreshToken(any(), any());
    }

    @Test
    public void onAccountToggled_WhenAccountSuccessfullyLoggedInAndFetchUserInfoSucceeds_ShouldAddGuidToAddedListAndFetchUserInfoAndLogSuccess() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        String accountUserName = account.getUserName();
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onSuccess();
                return null;
            }
        }).when(account).refreshToken(any(), any());
        String newloginId = "testloginId";
        String newGivenName = "testGivenName";
        String newFamilyName = "testFamilyName";
        String newEmail = "";
        String newDisplayName = "testDisplayName";
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                UserData userData = new UserData(account.getGUID(), newloginId, newGivenName, newFamilyName, newEmail, newDisplayName);
                ((OnUserDataResponseListener) args[1]).onSuccess(userData);
                return null;
            }
        }).when(account).fetchUserInfo(any(), any());
        ResultReceiver resultReceiver = spy(new ResultReceiver(null) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                super.onReceiveResult(resultCode, resultData);
            }
        });
        Intent intent = new ManageAccounts.IntentBuilder().setResultReceiver(resultReceiver).build(RuntimeEnvironment.application);

        ManageAccountsActivity activity = spy(Robolectric.buildActivity(ManageAccountsActivity.class, intent).create().get());
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountToggled(0, account, mockListener);
        Robolectric.flushBackgroundThreadScheduler();
        verify(activity).markAccountAdded(accountUserName);
        verify(account, times(1)).refreshToken(any(), any());
        verify(activity).notifyAccountSetChanged();
        ArgumentCaptor<Bundle> bundleArgumentCaptor = ArgumentCaptor.forClass(Bundle.class);
        verify(resultReceiver).send(eq(IAuthManager.RESULT_CODE_ACCOUNT_LOGGED_IN), bundleArgumentCaptor.capture());
        assertEquals(newloginId, bundleArgumentCaptor.getValue().getString(IAuthManager.KEY_USERNAME));
        verify(account).fetchUserInfo(eq(activity), any());
        assertEquals(newDisplayName, account.getDisplayName());
        assertEquals(newGivenName, account.getFirstName());
        assertEquals(newFamilyName, account.getLastName());
        assertEquals(newEmail, account.getEmail());
        assertTrue("Account should be active", account.isActive());

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_SUCCESS), isNull(Map.class));
    }

    @Test
    public void onAccountToggled_WhenAccountSuccessfullyLoggedInButFetchUserInfoFailed_ShouldAddGuidToAddedListAndDoNotFetchUserInfo() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        String accountUserName = account.getUserName();
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onSuccess();
                return null;
            }
        }).when(account).refreshToken(any(), any());
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnUserDataResponseListener) args[1]).onFailure();
                return null;
            }
        }).when(account).fetchUserInfo(any(), any());
        ResultReceiver resultReceiver = spy(new ResultReceiver(null) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                super.onReceiveResult(resultCode, resultData);
            }
        });
        Intent intent = new ManageAccounts.IntentBuilder().setResultReceiver(resultReceiver).build(RuntimeEnvironment.application);

        ManageAccountsActivity activity = spy(Robolectric.buildActivity(ManageAccountsActivity.class, intent).create().get());
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountToggled(0, account, mockListener);
        Robolectric.flushBackgroundThreadScheduler();
        verify(account, times(1)).refreshToken(any(), any());
        verify(activity, never()).notifyAccountSetChanged();
        verify(activity).markAccountAdded(accountUserName);
        ArgumentCaptor<Bundle> bundleArgumentCaptor = ArgumentCaptor.forClass(Bundle.class);
        verify(resultReceiver).send(eq(IAuthManager.RESULT_CODE_ACCOUNT_LOGGED_IN), bundleArgumentCaptor.capture());
        assertEquals(TEST_JWT_EMAIL_VALUE, bundleArgumentCaptor.getValue().getString(IAuthManager.KEY_USERNAME));
        verify(account).fetchUserInfo(eq(activity), any());
        assertEquals(TEST_JWT_EMAIL_VALUE, account.getUserName());
        assertEquals(TEST_JWT_NAME_VALUE, account.getDisplayName());
        assertEquals(TEST_JWT_GIVEN_NAME_VALUE, account.getFirstName());
        assertEquals(TEST_JWT_FAMILY_NAME_VALUE, account.getLastName());
        assertEquals(TEST_JWT_EMAIL_VALUE, account.getEmail());
        assertTrue("Account should be active", account.isActive());
    }

    @Test
    public void onAccountToggled_WhenAccountLoginFails_ShouldCallNotifyAccountSetChangedAndDoNotFetchUserInfo() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(OnRefreshTokenResponse.GENERAL_ERROR);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountToggled(0, account, mockListener);

        verify(account, times(1)).refreshToken(any(), any());
        verify(account, never()).fetchUserInfo(any(), any());
        verify(activity, times(1)).notifyAccountSetChanged();
        verify(activity, times(1)).showTryAgainLaterDialog();
        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error status is incorrect", EventLogger.RefreshTokenUserEvents.GENERAL_ERROR, realMap.get(TRACKING_KEY_ERROR_CODE));
        assertEquals("Error msg is incorrect", EventLogger.RefreshTokenUserEvents.GENERAL_ERROR_MSG, realMap.get(TRACKING_KEY_ERROR_MESSAGE));

        Dialog alertDialog = ShadowDialog.getLatestDialog();
        TextView textView = alertDialog.findViewById(R.id.phoenix_custom_dialog_title);
        assertEquals("Unexpected dialog title", activity.getString(R.string.phoenix_unable_to_turn_on_account), textView.getText());
        TextView messageTextView = alertDialog.findViewById(R.id.account_custom_dialog_message);
        assertEquals("Unexpected message", activity.getString(R.string.phoenix_try_again_error), messageTextView.getText());

        alertDialog.findViewById(R.id.account_custom_dialog_button).performClick();
        assertFalse("Dialogue should have been dismissed", alertDialog.isShowing());
        assertFalse("Account should still be not logged in", account.isLoggedIn());
        assertTrue("Account should be active", account.isActive());
    }

    @Test
    public void onAccountToggled_WhenAccountLoginFailsWithInvalidRequest_ShouldLogFailure() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(OnRefreshTokenResponse.INVALID_REQUEST);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountToggled(0, account, mockListener);

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error status is incorrect", EventLogger.RefreshTokenUserEvents.INVALID_REQUEST, realMap.get(TRACKING_KEY_ERROR_CODE));
        assertEquals("Error msg is incorrect", EventLogger.RefreshTokenUserEvents.INVALID_REQUEST_MSG, realMap.get(TRACKING_KEY_ERROR_MESSAGE));
    }

    @Test
    public void onAccountToggled_WhenAccountLoginFailsWithReAuthorizeUser_ShouldLogFailure() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(REAUTHORIZE_USER);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountToggled(0, account, mockListener);

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error status is incorrect", EventLogger.RefreshTokenUserEvents.REAUTHORIZE_USER, realMap.get(TRACKING_KEY_ERROR_CODE));
        assertEquals("Error msg is incorrect", EventLogger.RefreshTokenUserEvents.REAUTHORIZE_USER_MSG, realMap.get(TRACKING_KEY_ERROR_MESSAGE));
    }

    @Test
    public void onAccountToggled_WhenAccountLoginFailsWithIdpSwitch_ShouldLogFailure() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(ERROR_CODE_IDP_SWITCHED);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountToggled(0, account, mockListener);

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error status is incorrect", EventLogger.RefreshTokenUserEvents.IDP_SWITCHED, realMap.get(TRACKING_KEY_ERROR_CODE));
        assertEquals("Error msg is incorrect", EventLogger.RefreshTokenUserEvents.IDP_SWITCHED_MSG, realMap.get(TRACKING_KEY_ERROR_MESSAGE));
    }

    @Test
    public void onAccountToggled_WhenAccountLoginFailsWithUnAuthClient_ShouldLogFailure() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(OnRefreshTokenResponse.UNAUTHORIZED_CLIENT);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountToggled(0, account, mockListener);

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error status is incorrect", EventLogger.RefreshTokenUserEvents.UNAUTHORIZED_CLIENT, realMap.get(TRACKING_KEY_ERROR_CODE));
        assertEquals("Error msg is incorrect", EventLogger.RefreshTokenUserEvents.UNAUTHORIZED_CLIENT_MSG, realMap.get(TRACKING_KEY_ERROR_MESSAGE));
    }

    @Test
    public void onAccountToggled_WhenAccountLoginFailsWithInvalidScope_ShouldLogFailure() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(OnRefreshTokenResponse.INVALID_SCOPE);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountToggled(0, account, mockListener);

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error status is incorrect", EventLogger.RefreshTokenUserEvents.INVALID_SCOPE, realMap.get(TRACKING_KEY_ERROR_CODE));
        assertEquals("Error msg is incorrect", EventLogger.RefreshTokenUserEvents.INVALID_SCOPE_MSG, realMap.get(TRACKING_KEY_ERROR_MESSAGE));
    }

    @Test
    public void onAccountToggled_WhenAccountLoginFailsWithNetworkError_ShouldLogFailure() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(OnRefreshTokenResponse.NETWORK_ERROR);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountToggled(0, account, mockListener);

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error status is incorrect", EventLogger.RefreshTokenUserEvents.NETWORK_ERROR, realMap.get(TRACKING_KEY_ERROR_CODE));
        assertEquals("Error msg is incorrect", EventLogger.RefreshTokenUserEvents.NETWORK_ERROR_MSG, realMap.get(TRACKING_KEY_ERROR_MESSAGE));
    }

    @Test
    public void onAccountToggled_WhenAccountLoginFailsWithServerError_ShouldLogFailure() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(OnRefreshTokenResponse.SERVER_ERROR);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountToggled(0, account, mockListener);

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error status is incorrect", EventLogger.RefreshTokenUserEvents.SERVER_ERROR, realMap.get(TRACKING_KEY_ERROR_CODE));
        assertEquals("Error msg is incorrect", EventLogger.RefreshTokenUserEvents.SERVER_ERROR_MSG, realMap.get(TRACKING_KEY_ERROR_MESSAGE));
    }

    @Test
    public void onAccountToggled_WhenAccountLoginRefreshTokenIsInvalid_ShouldCallNotifyAccountSetChanged() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(REAUTHORIZE_USER);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountToggled(0, account, mockListener);

        verify(account, times(1)).refreshToken(any(), any());
        verify(activity, times(1)).notifyAccountSetChanged();
        verify(activity, times(1)).showLoginAgainDialog();

        Dialog alertDialog = ShadowDialog.getLatestDialog();
        TextView textView = alertDialog.findViewById(R.id.phoenix_custom_dialog_title);
        assertEquals("Unexpected dialog title", activity.getString(R.string.phoenix_unable_to_turn_on_account), textView.getText());
        TextView descriptionTextView = alertDialog.findViewById(R.id.account_custom_dialog_description);
        assertEquals("Unexpected description", activity.getString(R.string.phoenix_invalid_refresh_token_error), descriptionTextView.getText());

        alertDialog.findViewById(R.id.account_custom_dialog_button_two).performClick();
        assertFalse("Dialogue should have been dismissed", alertDialog.isShowing());
        assertFalse("Account should still be not logged in", account.isLoggedIn());
    }


    @Test
    public void onAccountToggled_WhenRefreshTokenIdpSwitched_ShouldCallNotifyAccountSetChanged() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(REAUTHORIZE_USER);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountToggled(0, account, mockListener);

        verify(account, times(1)).refreshToken(any(), any());
        verify(activity, times(1)).notifyAccountSetChanged();
        verify(activity, times(1)).showLoginAgainDialog();

        Dialog alertDialog = ShadowDialog.getLatestDialog();
        TextView textView = alertDialog.findViewById(R.id.phoenix_custom_dialog_title);
        assertEquals("Unexpected dialog title", activity.getString(R.string.phoenix_unable_to_turn_on_account), textView.getText());
        TextView descriptionTextView = alertDialog.findViewById(R.id.account_custom_dialog_description);
        assertEquals("Unexpected description", activity.getString(R.string.phoenix_invalid_refresh_token_error), descriptionTextView.getText());

        alertDialog.findViewById(R.id.account_custom_dialog_button_two).performClick();
        assertFalse("Dialogue should have been dismissed", alertDialog.isShowing());
        assertFalse("Account should still be not logged in", account.isLoggedIn());
    }

    @Test
    public void onAccountToggled_WhenAccountLoginRefreshTokenIsInvalidNetworkOutage_ShouldCallNotifyAccountSetChangedAndShowNoNetworkMsg() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(OnRefreshTokenResponse.NETWORK_ERROR);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountToggled(0, account, mockListener);

        verify(account, times(1)).refreshToken(any(), any());
        verify(activity, times(1)).notifyAccountSetChanged();
        verify(activity, times(1)).showNoNetworkDialog();

        Dialog alertDialog = ShadowDialog.getLatestDialog();
        TextView textView = alertDialog.findViewById(R.id.phoenix_custom_dialog_title);
        assertEquals("Unexpected dialog title", activity.getString(R.string.phoenix_unable_to_turn_on_account), textView.getText());
        TextView messageTextView = alertDialog.findViewById(R.id.account_custom_dialog_message);
        assertEquals("Unexpected message", activity.getString(R.string.phoenix_no_internet_connection), messageTextView.getText());

        alertDialog.findViewById(R.id.account_custom_dialog_button).performClick();
        assertFalse("Dialogue should have been dismissed", alertDialog.isShowing());
        assertFalse("Account should still be not logged in", account.isLoggedIn());
    }

    @Test
    public void onAccountToggled_WhenAccountLoginRefreshTokenIsInvalidNetworkOutageAirplaneMode_ShouldCallNotifyAccountSetChangedAndShowAirplaneMsg() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(OnRefreshTokenResponse.NETWORK_ERROR);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        doReturn(true).when(activity).isAirplaneModeOn();

        activity.onAccountToggled(0, account, mockListener);

        verify(account, times(1)).refreshToken(any(), any());
        verify(activity, times(1)).notifyAccountSetChanged();
        verify(activity, times(1)).showNoNetworkDialog();

        Dialog alertDialog = ShadowDialog.getLatestDialog();
        TextView textView = alertDialog.findViewById(R.id.phoenix_custom_dialog_title);
        assertEquals("Unexpected dialog title", activity.getString(R.string.phoenix_login_airplane_title), textView.getText());
        TextView descriptionTextView = alertDialog.findViewById(R.id.account_custom_dialog_description);
        assertEquals("Unexpected message", activity.getString(R.string.phoenix_login_airplane_mode), descriptionTextView.getText());

        alertDialog.findViewById(R.id.account_custom_dialog_button_one).performClick();
        assertFalse("Dialogue should have been dismissed", alertDialog.isShowing());
        assertFalse("Account should still be not logged in", account.isLoggedIn());
    }

    @Test
    public void onAccountToggled_WhenAccountLoginRefreshTokenIsInvalidServerError_ShouldCallNotifyAccountSetChangedAndShowTryAgainLaterMsg() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        account.disable(mock(AccountChangeListener.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ((OnRefreshTokenResponse) args[1]).onError(OnRefreshTokenResponse.SERVER_ERROR);
                return null;
            }
        }).when(account).refreshToken(any(), any());

        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountToggled(0, account, mockListener);

        verify(account, times(1)).refreshToken(any(), any());
        verify(activity, times(1)).notifyAccountSetChanged();
        verify(activity, times(1)).showTryAgainLaterDialog();

        Dialog alertDialog = ShadowDialog.getLatestDialog();
        TextView textView = alertDialog.findViewById(R.id.phoenix_custom_dialog_title);
        assertEquals("Unexpected dialog title", activity.getString(R.string.phoenix_unable_to_turn_on_account), textView.getText());
        TextView messageTextView = alertDialog.findViewById(R.id.account_custom_dialog_message);
        assertEquals("Unexpected message", activity.getString(R.string.phoenix_try_again_error), messageTextView.getText());

        alertDialog.findViewById(R.id.account_custom_dialog_button).performClick();
        assertFalse("Dialogue should have been dismissed", alertDialog.isShowing());
        assertFalse("Account should still be not logged in", account.isLoggedIn());
    }

    @Test
    public void onAccountToggled_WhenAccountLoggedIn_ShouldCallDisable() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));


        ResultReceiver resultReceiver = spy(new ResultReceiver(null));
        Intent intent = new ManageAccounts.IntentBuilder().setResultReceiver(resultReceiver).build(RuntimeEnvironment.application);

        ManageAccountsActivity activity = spy(Robolectric.buildActivity(ManageAccountsActivity.class, intent).create().get());

        when(activity.isFinishing()).thenReturn(false);

        assertTrue("Account should be logged in", account.isLoggedIn());
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        mockSingletonExecutor();
        activity.onAccountToggled(0, account, mockListener);

        verify(account).disable(any(AccountChangeListener.class));
        ArgumentCaptor<Bundle> bundleArgumentCaptor = ArgumentCaptor.forClass(Bundle.class);
        verify(resultReceiver).send(eq(IAuthManager.RESULT_CODE_ACCOUNT_LOGGED_OUT), bundleArgumentCaptor.capture());
        assertEquals("test@yahoo.com", bundleArgumentCaptor.getValue().getString(IAuthManager.KEY_USERNAME));
    }

    @Test
    public void onAccountToggled_WhenAccountSuccessfullyLogout_ShouldAddGuidToRemovedList() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        Account account = spy((Account) authManager.getAllAccounts().get(0));
        String username = account.getUserName();


        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        when(activity.isFinishing()).thenReturn(false);
        mockSingletonExecutor();
        assertTrue("Account should be logged in", account.isLoggedIn());
        AccountChangeListener mockListener = mock(AccountChangeListener.class);
        activity.onAccountToggled(0, account, mockListener);

        verify(activity).markAccountRemoved(username);
    }

    @Test
    public void onActivityResult_WhenRequestCodeForAuthAndResultOKAndShouldDismissActivity_ShouldSetAccountChangedAndFinishAndLogSignInSuccess() {
        ResultReceiver resultReceiver = spy(new ResultReceiver(null));
        Intent intent = new ManageAccounts
                .IntentBuilder()
                .setResultReceiver(resultReceiver)
                .enableDismissOnAddAccount()
                .build(RuntimeEnvironment.application);
        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class, intent).create().get();
        ManageAccountsActivity manageAccountsActivitySpy = spy(activity);
        ManageAccountsActivity.ManageAccountsViewModel viewModel = ViewModelProviders.of(activity).get(ManageAccountsActivity.ManageAccountsViewModel.class);
        Intent returnIntent = new Intent();
        returnIntent.putExtra(IAuthManager.KEY_USERNAME, "test@yahoo.com");
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        manageAccountsActivitySpy.onActivityResult(ManageAccountsActivity.REQUEST_CODE_FOR_AUTH, Activity.RESULT_OK, returnIntent);

        assertTrue("Accountchanged should be true", viewModel.hasAccountChanged());
        verify(manageAccountsActivitySpy).markAccountAdded("test@yahoo.com");
        verify(manageAccountsActivitySpy).finish();
        ArgumentCaptor<Bundle> bundleArgumentCaptor = ArgumentCaptor.forClass(Bundle.class);
        verify(resultReceiver).send(eq(IAuthManager.RESULT_CODE_ACCOUNT_LOGGED_IN), bundleArgumentCaptor.capture());
        assertEquals("test@yahoo.com", bundleArgumentCaptor.getValue().getString(IAuthManager.KEY_USERNAME));
        assertTrue("Activity should be finishing", manageAccountsActivitySpy.isFinishing());
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_SIGN_IN_SUCCESS), isNull(Map.class));
    }

    @Test
    public void onActivityResult_WhenRequestCodeForAuthAndResultCancel_ShouldSetAccountChangedAndFinishAndLogSignInCancel() {
        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        ManageAccountsActivity manageAccountsActivitySpy = spy(activity);
        ManageAccountsActivity.ManageAccountsViewModel viewModel = mock(ManageAccountsActivity.ManageAccountsViewModel.class);
        manageAccountsActivitySpy.mViewModel = viewModel;
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        manageAccountsActivitySpy.onActivityResult(ManageAccountsActivity.REQUEST_CODE_FOR_AUTH, Activity.RESULT_CANCELED, mock(Intent.class));
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_SIGN_IN_CANCEL), isNull(Map.class));
        verify(viewModel).setAccountChanged(true);
        verify(manageAccountsActivitySpy).finish();
        assertTrue("Activity should be finishing", manageAccountsActivitySpy.isFinishing());
    }

    @Test
    public void onActivityResult_WhenRequestCodeForAuthAndResultNotOkNotCancel_ShouldShowErrorDialogAndLogSignInError() {
        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        ManageAccountsActivity manageAccountsActivitySpy = spy(activity);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        manageAccountsActivitySpy.onActivityResult(ManageAccountsActivity.REQUEST_CODE_FOR_AUTH, IAuthManager.RESULT_CODE_ERROR, mock(Intent.class));
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_SIGN_IN_ERROR), isNull(Map.class));
        //TODO Verification
    }

    @Test
    public void onActivityResult_WhenRequestCodeForOthers_ShouldCallSuperOnActivityResult() {
        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        ManageAccountsActivity manageAccountsActivitySpy = spy(activity);
        manageAccountsActivitySpy.onActivityResult(100, Activity.RESULT_OK, mock(Intent.class));

        //TODO Verification
    }

    @Test
    public void onAddAccount_WhenNewAccountIsAdded_ShouldLaunchAuthIntent() throws Exception {
        AuthConfig authConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(),
                Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        authConfig.saveAuthConfig(mContext, OATH_AUTH_CONFIG_KEY);

        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        activity.onAddAccount();
        verify(eventLogger).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_SIGN_IN_START), isNull(Map.class));
        ShadowActivity shadowActivity = shadowOf(activity);
        ShadowActivity.IntentForResult intentForResult = shadowActivity.getNextStartedActivityForResult();
        Assert.assertEquals("Must have a request code", ManageAccountsActivity.REQUEST_CODE_FOR_AUTH, intentForResult.requestCode);
        Assert.assertEquals(BuildConfig.APPLICATION_ID + ".AuthActivity", intentForResult.intent.getComponent().getClassName());
    }

    @Test
    public void finish_WhenNoAccountChanged_ShouldNotSetResult() {
        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        activity.finish();
        verify(activity, times(0)).setResult(Activity.RESULT_OK);
    }

    @Test
    public void finish_WhenAccountChanged_ShouldSetResult() {
        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        activity.mViewModel.setAccountChanged(true);
        activity.finish();
        verify(activity).setResult(eq(Activity.RESULT_OK), any(Intent.class));
    }

    @Test
    public void onStop_WhenHasProgressDialog_ShouldDismissDialog() throws Exception {
        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        when(activity.isFinishing()).thenReturn(false);
        activity.showProgressDialog();
        Dialog latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        assertTrue("Should show dialog", latestDialog.isShowing());

        activity.onStop();
        latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        assertFalse("Should not show dialog", latestDialog.isShowing());
    }

    @Test
    public void safeDismissProgressDialog_WhenDialogShowing_ShouldDismiss() {
        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        when(activity.isFinishing()).thenReturn(false);

        activity.showProgressDialog();

        Dialog latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        assertTrue("Should show dialog", latestDialog.isShowing());

        activity.safeDismissProgressDialog();

        latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        assertFalse("Should not show dialog", latestDialog.isShowing());
    }

    @Test
    public void showProgressDialog_WhenProgressDialogIsNull_ShouldCreateNewDialogAndShow() throws Exception {
        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        when(activity.isFinishing()).thenReturn(false);

        activity.showProgressDialog();

        Dialog latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        assertTrue("Should show dialog", latestDialog.isShowing());
    }

    @Test
    public void showProgressDialog_WhenProgressDialogIsNotNull_ShouldShowExistingProgressDialog() throws Exception {
        ManageAccountsActivity activity = spy(buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        when(activity.isFinishing()).thenReturn(false);

        activity.showProgressDialog(); //progress dialog is created
        Dialog dialogSpy = spy(activity.mProgressDialog);
        activity.mProgressDialog = dialogSpy;
        activity.safeDismissProgressDialog();

        ShadowDialog.reset();
        activity.showProgressDialog();
        verify(dialogSpy, never()).setCanceledOnTouchOutside(anyBoolean());

        Dialog latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        assertTrue("Should show dialog", latestDialog.isShowing());
    }

    @Test
    public void shouldShowOnboardingFlow_WhenTrue_ShouldShowOnboardingDialogFragment() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE).edit().putBoolean(KEY_SHOW_MANAGE_ACCOUNTS_ONBOARDING, true).apply();
        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        Dialog dialog = ShadowDialog.getLatestDialog();
        TextView title = dialog.findViewById(R.id.toggleAccountOnboardingTitle);
        assertNotNull("Dialog was expected", dialog);
        assertEquals("Unexpected dialog", mContext.getResources().getString(R.string.phoenix_manage_accounts_toggle_acct_onboarding_title), title.getText());
        assertFalse(mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE).getBoolean(KEY_SHOW_MANAGE_ACCOUNTS_ONBOARDING, false));

    }

    @Test
    public void onDismiss_WhenOnboardingDialogIsDismissed_ShouldShowToolTip() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE).edit().putBoolean(KEY_SHOW_MANAGE_ACCOUNTS_ONBOARDING, true).apply();
        resetToolTipDisplayCount();
        ActivityController<ManageAccountsActivity> controller = buildActivity(ManageAccountsActivity.class, mManageAccountIntent);
        ManageAccountsActivity activity = controller.create().get();
        ToolTipWindow toolTipWindow = spy(activity.mEditButtonToolTipWindow);
        doReturn(true).when(toolTipWindow).isAnchorViewVisible(any(View.class));
        activity.mEditButtonToolTipWindow = toolTipWindow;

        controller.start().postCreate(null).resume().visible();
        Dialog dialog = ShadowDialog.getLatestDialog();
        assertNotNull("Dialog was expected", ShadowDialog.getLatestDialog());
        // dismiss onboarding dialog
        dialog.findViewById(R.id.close_action).performClick();
        PopupWindow popupWindow = ShadowApplication.getInstance().getLatestPopupWindow();
        assertNotNull("PopupWindow should not be null", popupWindow);
        TextView textView = (TextView) popupWindow.getContentView().findViewById(R.id.tooltip_text);
        org.junit.Assert.assertEquals("Text is not correct", Html.fromHtml(mContext.getResources().getString(R.string.phoenix_manage_accounts_edit_tooltip)).toString(), textView.getText().toString());

    }

    private void resetToolTipDisplayCount() {
        mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE).edit().remove(SHARED_PREF_PREFIX + ToolTipWindow.EDIT).apply();
        mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE).edit().remove(SHARED_PREF_PREFIX + ToolTipWindow.REMOVE).apply();
    }

    @Test
    public void shouldShowOnboardingFlow_WhenFalse_ShouldNotShowOnboardingDialogFragment() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.mVerificationNonce = getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE).edit().putBoolean(KEY_SHOW_MANAGE_ACCOUNTS_ONBOARDING, false).apply();
        ManageAccountsActivity activity = buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        assertNull("Dialog was not expected", ShadowDialog.getLatestDialog());
    }

    void mockSingletonExecutor() throws NoSuchFieldException, IllegalAccessException {
        ThreadPoolExecutorSingleton mockExecutor = mock(ThreadPoolExecutorSingleton.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Runnable runnable = (Runnable) invocation.getArguments()[0];
                runnable.run();
                return null;
            }
        }).when(mockExecutor).execute(any(Runnable.class));

        Field field = ThreadPoolExecutorSingleton.class.getDeclaredField("sInstance");
        field.setAccessible(true);
        field.set(null, mockExecutor);
    }
}
