package com.oath.mobile.platform.phoenix.core;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import junit.framework.Assert;

import net.openid.appauth.TokenResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;
import org.robolectric.shadows.ShadowDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_TOGGLE_OFF_ACCOUNT_CANCEL;
import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_TOGGLE_OFF_ACCOUNT_START;
import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_TOGGLE_OFF_ACCOUNT_SUCCESS;
import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_START;
import static com.oath.mobile.platform.phoenix.core.ManageAccountsActivity.KEY_SHOW_MANAGE_ACCOUNTS_ONBOARDING;
import static com.oath.mobile.platform.phoenix.core.ToolTipWindow.SHARED_PREF_PREFIX;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by yuhongli on 11/1/17.
 */
@RunWith(RobolectricTestRunner.class)
public class ManageAccountsAdapterTest extends BaseTest {

    private Context mContext;
    Intent mManageAccountIntent;

    @Before
    public void setUp() {
        mContext = RuntimeEnvironment.application;
        AuthManager actual = (AuthManager) AuthManager.getInstance(mContext);
        actual.removeAccounts();
        mManageAccountIntent = new Intent();
        mManageAccountIntent.setClass(mContext, ManageAccountsActivity.class);
        mManageAccountIntent.putExtra(ManageAccountsActivity.INTERNAL_LAUNCH_GATE, true);
        setShowOnboardingFlagToFalseForDefaultBehavior();
        setShowToolTipCountToMoreThanMaxForDefaultBehavior();
    }

    private void setShowOnboardingFlagToFalseForDefaultBehavior() {
        mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE).edit().putBoolean(KEY_SHOW_MANAGE_ACCOUNTS_ONBOARDING, false).apply();
    }

    private void setShowToolTipCountToMoreThanMaxForDefaultBehavior() {
        mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE).edit().putInt(SHARED_PREF_PREFIX + ToolTipWindow.EDIT, ToolTipWindow.MAX_COUNT + 1).apply();
        mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE).edit().putInt(SHARED_PREF_PREFIX + ToolTipWindow.REMOVE, ToolTipWindow.MAX_COUNT + 1).apply();
    }

    private void resetToolTipDisplayCount() {
        mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE).edit().remove(SHARED_PREF_PREFIX + ToolTipWindow.EDIT).apply();
        mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE).edit().remove(SHARED_PREF_PREFIX + ToolTipWindow.REMOVE).apply();
    }

    @After
    public void cleanUp() {
        EventLogger.sEventLoggerSingleton = null; // reset to get fresh instance of EventLogger so Roboelectric won't complain on re-spy
    }


    @Test
    public void constructor_WhenCalled_Success() {
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        doReturn(Collections.emptyList()).when(authManager).getAllAccountsInternal();
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);
        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        verify(listener).onNoAccountsFound();
    }

    @Test
    public void onCreateViewHolder_WhenTypeAccountItem_ShouldReturnAccountItemViewHolder() {
        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponse);

        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);
        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        RecyclerView.ViewHolder viewHolder = adapter.createViewHolder(new LinearLayout(activity), ManageAccountsAdapter.TYPE_ACCOUNT_ITEM);
        assertNotNull("View holder should not be null", viewHolder);
        assertTrue("View Holder type does not match", viewHolder instanceof ManageAccountsAdapter.AccountItemViewHolder);
    }

    @Test
    public void onCreateViewHolder_WhenTypeHeaderItem_ShouldReturnHeaderItemViewHolder() {
        Activity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        doReturn(Collections.emptyList()).when(authManager).getAllAccountsInternal();
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);
        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        RecyclerView.ViewHolder viewHolder = adapter.createViewHolder(new LinearLayout(activity), ManageAccountsAdapter.TYPE_HEADER_ITEM);
        assertNotNull("View holder should not be null", viewHolder);
        assertTrue("View Holder type does not match", viewHolder instanceof ManageAccountsAdapter.HeaderItemViewHolder);
    }

    @Test
    public void onCreateViewHolder_WhenTypeAddAccountItem_ShouldReturnAddAccountItemViewHolder() {
        Activity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        doReturn(Collections.emptyList()).when(authManager).getAllAccountsInternal();
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);

        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        RecyclerView.ViewHolder viewHolder = adapter.createViewHolder(new LinearLayout(activity), ManageAccountsAdapter.TYPE_ADD_ACCOUNT_ITEM);
        assertNotNull("View holder should not be null", viewHolder);
        assertTrue("View Holder type does not match", viewHolder instanceof ManageAccountsAdapter.AddAccountItemViewHolder);
    }

    @Test(expected = IllegalArgumentException.class)
    public void onCreateViewHolder_WhenUnknownType_ShouldThrowAnException() {
        Activity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        doReturn(Collections.emptyList()).when(authManager).getAllAccountsInternal();

        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);

        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        adapter.createViewHolder(new LinearLayout(activity), 3);
        fail("IllegalArgumentException expected for creating an unknown view type");
    }

    @Test
    public void onBindViewHolder_WhenHeaderViewHolderNotInEditMode_ShouldShowManageAccountsHeading() {
        Activity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        doReturn(Collections.emptyList()).when(authManager).getAllAccountsInternal();

        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);
        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        adapter.disableEditMode();
        ManageAccountsAdapter.HeaderItemViewHolder viewHolder = (ManageAccountsAdapter.HeaderItemViewHolder) adapter.createViewHolder(new LinearLayout(activity), ManageAccountsAdapter.TYPE_HEADER_ITEM);
        ManageAccountsAdapter.HeaderItemViewHolder viewHolderSpy = spy(viewHolder);
        adapter.bindViewHolder(viewHolderSpy, ManageAccountsAdapter.HEADER_POSITION);
        verify(viewHolderSpy).bindData(false);

        TextView header = viewHolder.itemView.findViewById(R.id.account_manage_accounts_header);
        assertEquals("Unexpected header title", activity.getString(R.string.phoenix_manage_accounts_header, AccountUtils.getApplicationName(activity)), header.getText());
    }

    @Test
    public void onBindViewHolder_WhenHeaderViewHolderInEditMode_ShouldShowManageAccountsHeading() {
        Activity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        doReturn(Collections.emptyList()).when(authManager).getAllAccountsInternal();
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);
        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        adapter.enableEditMode();
        ManageAccountsAdapter.HeaderItemViewHolder viewHolder = (ManageAccountsAdapter.HeaderItemViewHolder) adapter.createViewHolder(new LinearLayout(activity), ManageAccountsAdapter.TYPE_HEADER_ITEM);
        ManageAccountsAdapter.HeaderItemViewHolder viewHolderSpy = spy(viewHolder);
        adapter.bindViewHolder(viewHolderSpy, ManageAccountsAdapter.HEADER_POSITION);
        verify(viewHolderSpy).bindData(true);

        TextView header = viewHolder.itemView.findViewById(R.id.account_manage_accounts_header);
        assertEquals("Unexpected header title", activity.getString(R.string.phoenix_manage_accounts_edit_mode_header), header.getText());
    }

    @Test
    public void getItemCount_WhenAccountSetIsEmpty_ShouldReturnOne() {
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        doReturn(Collections.emptyList()).when(authManager).getAllAccountsInternal();
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);

        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        assertEquals("Unexpected account count", 0, adapter.getAccountCount());
        assertEquals("Unexpected item count", ManageAccountsAdapter.HEADER_OFFSET + 1, adapter.getItemCount()); // header and add account
    }

    @Test
    public void getItemCount_WhenAccountSetIsNotEmpty_Success() {
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        IAccount accountMock = mock(IAccount.class);
        List<IAccount> accounts = new ArrayList<>();
        accounts.add(accountMock);
        doReturn(accounts).when(authManager).getAllAccountsInternal();
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);
        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);

        assertEquals("Unexpected account count", 1, adapter.getAccountCount());
        assertEquals("Unexpected item count", ManageAccountsAdapter.HEADER_OFFSET + 2, adapter.getItemCount());
    }

    @Test
    public void getItemViewType_WhenHeaderItemViewType_Success() {
        IAuthManager authManager = spy(AuthManager.getInstance(mContext));
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);
        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        int itemViewType = adapter.getItemViewType(ManageAccountsAdapter.HEADER_POSITION);

        assertEquals("Unexpected item view type", ManageAccountsAdapter.TYPE_HEADER_ITEM, itemViewType);
    }

    @Test
    public void getItemViewType_WhenAddAccountItemViewType_Success() {
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<IAccount> accounts = new ArrayList<>();
        IAccount accountMock = mock(IAccount.class);
        accounts.add(accountMock);
        doReturn(accounts).when(authManager).getAllAccountsInternal();
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);
        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        int itemViewType = adapter.getItemViewType(ManageAccountsAdapter.HEADER_OFFSET + 1);

        assertEquals("Unexpected item view type", ManageAccountsAdapter.TYPE_ADD_ACCOUNT_ITEM, itemViewType);
    }

    @Test
    public void getItemViewType_WhenAccountItemViewType_Success() {
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<IAccount> accounts = new ArrayList<>();
        IAccount accountMock = mock(IAccount.class);
        accounts.add(accountMock);
        doReturn(accounts).when(authManager).getAllAccountsInternal();
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);

        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        int itemViewType = adapter.getItemViewType(ManageAccountsAdapter.HEADER_OFFSET);

        assertEquals("Unexpected item view type", ManageAccountsAdapter.TYPE_ACCOUNT_ITEM, itemViewType);
    }

    @Test
    public void notifyAccountSetChanged_WhenAccountAdded_ShouldUpdateAccountSet() {
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<IAccount> accounts = new ArrayList<>();
        IAccount accountMock1 = mock(IAccount.class);
        when(accountMock1.getUserName()).thenReturn("test1@xx.com");
        accounts.add(accountMock1);
        IAccount accountMock2 = mock(IAccount.class);
        when(accountMock2.getUserName()).thenReturn("test2@xx.com");
        doReturn(accounts).when(authManager).getAllAccountsInternal();

        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);
        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        ManageAccountsAdapter adapterSpy = spy(adapter);

        assertEquals("Unexpected item count", ManageAccountsAdapter.HEADER_OFFSET + 2, adapterSpy.getItemCount());

        accounts.add(accountMock2);
        doReturn(accounts).when(authManager).getAllAccountsInternal();

        adapterSpy.notifyAccountSetChanged();

        assertEquals("Unexpected item count", ManageAccountsAdapter.HEADER_OFFSET + 3, adapterSpy.getItemCount());

        verify(adapterSpy).notifyDataSetChanged();
    }

    @Test
    public void addAccountItemViewHolder_OnClick_ShouldNotifyListener() throws Exception {
        Activity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<IAccount> accounts = new ArrayList<>();
        IAccount accountMock1 = mock(IAccount.class);
        accounts.add(accountMock1);
        doReturn(accounts).when(authManager).getAllAccountsInternal();
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);

        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        RecyclerView.ViewHolder viewHolder = adapter.createViewHolder(new LinearLayout(activity), ManageAccountsAdapter.TYPE_ADD_ACCOUNT_ITEM);
        assertNotNull("View holder should not be null", viewHolder);
        viewHolder.itemView.callOnClick();

        verify(listener).onAddAccount();
    }

    @Test
    public void accountItemViewHolder_OnClickRemoveAccount_ShouldNotifyListener() {
        Activity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<Account> accounts = new ArrayList<>();
        Account accountMock1 = mock(Account.class);
        accounts.add(accountMock1);
        doReturn(accounts).when(authManager).getAllAccountsInternal();
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);

        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        RecyclerView.ViewHolder viewHolder = spy(adapter.createViewHolder(new LinearLayout(activity), ManageAccountsAdapter.TYPE_ACCOUNT_ITEM));
        adapter.bindViewHolder(viewHolder, ManageAccountsAdapter.HEADER_OFFSET);
        doReturn(ManageAccountsAdapter.HEADER_OFFSET).when(viewHolder).getAdapterPosition();
        assertNotNull("View holder should not be null", viewHolder);
        viewHolder.itemView.findViewById(R.id.account_remove).callOnClick();

        verify(listener).onRemoveAccount(anyInt(), any(IAccount.class));
    }

    @Test
    public void accountItemViewHolder_BindData_WhenUserNameAndDisplayNameDifferent_ShouldDisplayBoth() {
        Activity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponse);
        Account account = (Account) authManager.getAllAccountsInternal().get(0);

        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);

        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);

        ManageAccountsAdapter.AccountItemViewHolder viewHolder = (ManageAccountsAdapter.AccountItemViewHolder) adapter.createViewHolder(new LinearLayout(activity), ManageAccountsAdapter.TYPE_ACCOUNT_ITEM);
        assertNotNull("View holder should not be null", viewHolder);
        viewHolder.bindData(account, false);
        TextView displayName = (TextView) viewHolder.itemView.findViewById(R.id.account_display_name);
        TextView email = (TextView) viewHolder.itemView.findViewById(R.id.account_email);
        assertEquals("Unexpected value returned", account.getFirstName() + " " + account.getLastName(), displayName.getText().toString());
        assertEquals("Email field shoud be visible", View.VISIBLE, email.getVisibility());
        assertEquals("Unexpected value returned", account.getEmail(), email.getText().toString());
    }

    @Test
    public void accountItemViewHolder_BindData_WhenAccountDisabled_ShouldHaveOffToggle() {
        Activity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponse);
        Account account = (Account) authManager.getAllAccountsInternal().get(0);
        account.disable(mock(AccountChangeListener.class));
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);

        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        ManageAccountsAdapter.AccountItemViewHolder viewHolder = (ManageAccountsAdapter.AccountItemViewHolder) adapter.createViewHolder(new LinearLayout(activity), ManageAccountsAdapter.TYPE_ACCOUNT_ITEM);
        assertNotNull("View holder should not be null", viewHolder);
        viewHolder.bindData(account, false);
        assertFalse("Account should be disabled", ((SwitchCompat) viewHolder.itemView.findViewById(R.id.account_state_toggle)).isChecked());
        assertEquals("Doesn't have correct content description", account.getEmail() + " " + activity.getString(R.string.phoenix_accessibility_account_disabled), viewHolder.itemView.getContentDescription());
    }

    @Test
    public void accountItemViewHolder_BindData_WhenAccountEnabled_ShouldHaveOnToggle() {
        Activity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponse);
        Account account = (Account) authManager.getAllAccountsInternal().get(0);
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);

        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        ManageAccountsAdapter.AccountItemViewHolder viewHolder = (ManageAccountsAdapter.AccountItemViewHolder) adapter.createViewHolder(new LinearLayout(activity), ManageAccountsAdapter.TYPE_ACCOUNT_ITEM);
        assertNotNull("View holder should not be null", viewHolder);
        viewHolder.bindData(account, false);
        assertTrue("Account should be enabled", ((SwitchCompat) viewHolder.itemView.findViewById(R.id.account_state_toggle)).isChecked());
        assertEquals("Doesn't have correct content description", account.getEmail() + " " + activity.getString(R.string.phoenix_accessibility_account_enabled), viewHolder.itemView.getContentDescription());
    }

    @Test
    public void accountItemViewHolder_BindData_WhenAccountIsLoggedInButNotActive_ShouldHaveToggleOff() {
        Activity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponse);
        Account account = (Account) authManager.getAllAccountsInternal().get(0);
        account.setIsActive(false);
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);

        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        ManageAccountsAdapter.AccountItemViewHolder viewHolder = (ManageAccountsAdapter.AccountItemViewHolder) adapter.createViewHolder(new LinearLayout(activity), ManageAccountsAdapter.TYPE_ACCOUNT_ITEM);
        assertNotNull("View holder should not be null", viewHolder);
        viewHolder.bindData(account, false);
        assertFalse("Account should be disabled", ((SwitchCompat) viewHolder.itemView.findViewById(R.id.account_state_toggle)).isChecked());
        assertEquals("Doesn't have correct content description", account.getEmail() + " " + activity.getString(R.string.phoenix_accessibility_account_disabled), viewHolder.itemView.getContentDescription());
    }

    @Test
    @Config(sdk = Build.VERSION_CODES.LOLLIPOP)
    public void accountItemViewHolder_OnClickToggleAccount_ShouldNotifyListener() throws Exception {
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<Account> accounts = new ArrayList<>();
        Account accountMock1 = mock(Account.class);
        when(accountMock1.getUserName()).thenReturn("test1@xx.com");
        when(accountMock1.getFirstName()).thenReturn("Jamine");
        when(accountMock1.getLastName()).thenReturn("Test");
        when(accountMock1.isLoggedIn()).thenReturn(true);
        when(accountMock1.isActive()).thenReturn(true);

        accounts.add(accountMock1);
        doReturn(accounts).when(authManager).getAllAccountsInternal();
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);
        ManageAccountsActivity activity = spy(Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        doReturn(false).when(activity).isFinishing();

        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        ManageAccountsAdapter.AccountItemViewHolder viewHolder = spy((ManageAccountsAdapter.AccountItemViewHolder) adapter.createViewHolder(new LinearLayout(activity), ManageAccountsAdapter.TYPE_ACCOUNT_ITEM));
        viewHolder.mContext = activity;
        assertNotNull("View holder should not be null", viewHolder);
        adapter.bindViewHolder(viewHolder, ManageAccountsAdapter.HEADER_OFFSET);
        TextView displayName = viewHolder.itemView.findViewById(R.id.account_display_name);
        doNothing().when(viewHolder).showAccountDisabledSnackbar();
        final boolean[] isListenerCalled = {false};
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                isListenerCalled[0] = true;
                Object[] args = invocation.getArguments();
                doReturn(false).when(accountMock1).isLoggedIn();
                ((AccountChangeListener) args[2]).onAccountChanged(0);
                return null;
            }
        }).when(listener).onAccountToggled(anyInt(), any(IAccount.class), any(AccountChangeListener.class));
        assertEquals("Doesn't have correct content description", accountMock1.getUserName() + " " + activity.getString(R.string.phoenix_accessibility_account_enabled), viewHolder.itemView.getContentDescription());
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ((SwitchCompat) viewHolder.itemView.findViewById(R.id.account_state_toggle)).toggle();

        Dialog latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        Button confirmButton = latestDialog.findViewById(R.id.account_custom_dialog_button_one);
        confirmButton.performClick();
        assertEquals("Doesn't have correct content description", accountMock1.getUserName() + " " + activity.getString(R.string.phoenix_accessibility_account_disabled), viewHolder.itemView.getContentDescription());
        assertNull("Should not have a right drawable", displayName.getCompoundDrawables()[2]);
        assertTrue("Listener was not called when we expected it to be called", isListenerCalled[0]);
        // verify that we called the method to generate the snackbox
        verify(viewHolder, times(1)).showAccountDisabledSnackbar();

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_TOGGLE_OFF_ACCOUNT_SUCCESS), isNull(Map.class));
    }

    @Test
    @Config(sdk = Build.VERSION_CODES.LOLLIPOP)
    public void accountItemViewHolder_WhenCancelToggleOffAccountDialog_ShouldDismissDialogAndDoNotDisableAccountAndLogStartToggleOnAtTheStartBeforeShowingDialog() throws Exception {
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<Account> accounts = new ArrayList<>();
        Account accountMock1 = mock(Account.class);
        when(accountMock1.getUserName()).thenReturn("test1@xx.com");
        when(accountMock1.getFirstName()).thenReturn("Jamine");
        when(accountMock1.getLastName()).thenReturn("Test");
        when(accountMock1.isLoggedIn()).thenReturn(true);
        when(accountMock1.isActive()).thenReturn(true);

        accounts.add(accountMock1);
        doReturn(accounts).when(authManager).getAllAccountsInternal();
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);

        ManageAccountsActivity activity = spy(Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        doReturn(false).when(activity).isFinishing();
        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        ManageAccountsAdapter.AccountItemViewHolder viewHolder = (ManageAccountsAdapter.AccountItemViewHolder) adapter.createViewHolder(new LinearLayout(activity), ManageAccountsAdapter.TYPE_ACCOUNT_ITEM);
        viewHolder.mContext = activity;
        assertNotNull("View holder should not be null", viewHolder);
        adapter.bindViewHolder(viewHolder, ManageAccountsAdapter.HEADER_OFFSET);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                when(accountMock1.isLoggedIn()).thenReturn(false);
                ((AccountChangeListener) args[2]).onAccountChanged(0);
                return null;
            }
        }).when(listener).onAccountToggled(anyInt(), any(IAccount.class), any(AccountChangeListener.class));

        assertEquals("Doesn't have correct content description", accountMock1.getUserName() + " " + activity.getString(R.string.phoenix_accessibility_account_enabled), viewHolder.itemView.getContentDescription());
        SwitchCompat toggleView = viewHolder.itemView.findViewById(R.id.account_state_toggle);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        toggleView.toggle();

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_TOGGLE_ON_ACCOUNT_START), isNull(Map.class));

        Dialog latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        TextView cancelLink = latestDialog.findViewById(R.id.account_custom_dialog_button_two);
        cancelLink.performClick();
        assertEquals("Doesn't have correct content description", accountMock1.getUserName() + " " + activity.getString(R.string.phoenix_accessibility_account_enabled), viewHolder.itemView.getContentDescription());
        verify(listener, times(0)).onAccountToggled(anyInt(), any(IAccount.class), any(AccountChangeListener.class));
        latestDialog = ShadowDialog.getLatestDialog();
        assertFalse("Unexpected dialog", latestDialog.isShowing());
        assertTrue("Account should be enabled", ((SwitchCompat) viewHolder.itemView.findViewById(R.id.account_state_toggle)).isChecked());
    }

    @Test
    @Config(sdk = Build.VERSION_CODES.LOLLIPOP)
    public void accountItemViewHolder_WhenCancelToggleOffAccountDialog_ShouldDismissDialogAndDoNotDisableAccountAndLogCancelToggleOff() throws Exception {

        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<Account> accounts = new ArrayList<>();
        Account accountMock1 = mock(Account.class);
        when(accountMock1.getUserName()).thenReturn("test1@xx.com");
        when(accountMock1.getFirstName()).thenReturn("Jamine");
        when(accountMock1.getLastName()).thenReturn("Test");
        when(accountMock1.isLoggedIn()).thenReturn(true);
        when(accountMock1.isActive()).thenReturn(true);

        accounts.add(accountMock1);
        doReturn(accounts).when(authManager).getAllAccountsInternal();
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);

        ManageAccountsActivity activity = spy(Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        doReturn(false).when(activity).isFinishing();
        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        ManageAccountsAdapter.AccountItemViewHolder viewHolder = (ManageAccountsAdapter.AccountItemViewHolder) adapter.createViewHolder(new LinearLayout(activity), ManageAccountsAdapter.TYPE_ACCOUNT_ITEM);
        viewHolder.mContext = activity;
        assertNotNull("View holder should not be null", viewHolder);
        adapter.bindViewHolder(viewHolder, ManageAccountsAdapter.HEADER_OFFSET);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                when(accountMock1.isLoggedIn()).thenReturn(false);
                ((AccountChangeListener) args[2]).onAccountChanged(0);
                return null;
            }
        }).when(listener).onAccountToggled(anyInt(), any(IAccount.class), any(AccountChangeListener.class));

        assertEquals("Doesn't have correct content description", accountMock1.getUserName() + " " + activity.getString(R.string.phoenix_accessibility_account_enabled), viewHolder.itemView.getContentDescription());
        SwitchCompat toggleView = viewHolder.itemView.findViewById(R.id.account_state_toggle);
        toggleView.toggle();

        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        Dialog latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        TextView cancelLink = latestDialog.findViewById(R.id.account_custom_dialog_button_two);
        cancelLink.performClick();
        assertEquals("Doesn't have correct content description", accountMock1.getUserName() + " " + activity.getString(R.string.phoenix_accessibility_account_enabled), viewHolder.itemView.getContentDescription());
        verify(listener, times(0)).onAccountToggled(anyInt(), any(IAccount.class), any(AccountChangeListener.class));
        latestDialog = ShadowDialog.getLatestDialog();
        assertFalse("Unexpected dialog", latestDialog.isShowing());
        assertTrue("Account should be enabled", ((SwitchCompat) viewHolder.itemView.findViewById(R.id.account_state_toggle)).isChecked());

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_TOGGLE_OFF_ACCOUNT_CANCEL), isNull(Map.class));
    }

    @Test
    @Config(sdk = Build.VERSION_CODES.LOLLIPOP)
    public void accountItemViewHolder_WhenToggleOff_ShouldLogStartToggleOff() throws Exception {

        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<Account> accounts = new ArrayList<>();
        Account accountMock1 = mock(Account.class);
        when(accountMock1.getEmail()).thenReturn("test1@xx.com");
        when(accountMock1.getFirstName()).thenReturn("Jamine");
        when(accountMock1.getLastName()).thenReturn("Test");
        when(accountMock1.isLoggedIn()).thenReturn(false);

        accounts.add(accountMock1);
        doReturn(accounts).when(authManager).getAllAccountsInternal();
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);

        ManageAccountsActivity activity = spy(Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        doReturn(false).when(activity).isFinishing();
        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        ManageAccountsAdapter.AccountItemViewHolder viewHolder = (ManageAccountsAdapter.AccountItemViewHolder) adapter.createViewHolder(new LinearLayout(activity), ManageAccountsAdapter.TYPE_ACCOUNT_ITEM);
        viewHolder.mContext = activity;
        assertNotNull("View holder should not be null", viewHolder);
        adapter.bindViewHolder(viewHolder, ManageAccountsAdapter.HEADER_OFFSET);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                when(accountMock1.isLoggedIn()).thenReturn(false);
                ((AccountChangeListener) args[2]).onAccountChanged(0);
                return null;
            }
        }).when(listener).onAccountToggled(anyInt(), any(IAccount.class), any(AccountChangeListener.class));

        SwitchCompat toggleView = viewHolder.itemView.findViewById(R.id.account_state_toggle);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);
        toggleView.setChecked(false);
        toggleView.toggle();

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_TOGGLE_OFF_ACCOUNT_START), isNull(Map.class));
    }

    @Test
    @Config(sdk = Build.VERSION_CODES.LOLLIPOP)
    public void accountItemViewHolder_WhenAccountToggledOffAndCounterGreaterThanMax_ShouldNotShowDialog() {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponse);
        Account account = (Account) authManager.getAllAccountsInternal().get(0);

        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);

        ManageAccountsActivity activity = spy(Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        activity.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE).edit().putInt(ManageAccountsAdapter.KEY_TOGGLE_ACCOUNT_CONFIRMATION_DIALOG_COUNTER, ManageAccountsAdapter.TOGGLE_ACCOUNT_CONFIRMATION_DIALOG_MAX_COUNT + 1).apply();
        doReturn(false).when(activity).isFinishing();
        ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);

        ManageAccountsAdapter.AccountItemViewHolder viewHolder = (ManageAccountsAdapter.AccountItemViewHolder) adapter.createViewHolder(new LinearLayout(activity), ManageAccountsAdapter.TYPE_ACCOUNT_ITEM);
        assertNotNull("View holder should not be null", viewHolder);
        viewHolder.bindData(account, false);
        assertTrue("Account should be enabled", ((SwitchCompat) viewHolder.itemView.findViewById(R.id.account_state_toggle)).isChecked());
        ((SwitchCompat) viewHolder.itemView.findViewById(R.id.account_state_toggle)).toggle();

        assertNull("Should not show a confirmation dialog", ShadowDialog.getLatestDialog());
        verify(listener).onAccountToggled(anyInt(), any(IAccount.class), any(AccountChangeListener.class));

        activity.getSharedPreferences((AuthHelper.SHARED_PREF_PHOENIX), Context.MODE_PRIVATE).edit().remove(ManageAccountsAdapter.KEY_TOGGLE_ACCOUNT_CONFIRMATION_DIALOG_COUNTER).apply();
    }

    @Test
    public void onRemoveAccount_WhenAdapterPositionIsNoPosition_ShouldNotRemoveAccount() throws Exception {
        List<Account> accounts = new ArrayList<>();
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponse);
        Account account = (Account) authManager.getAllAccountsInternal().get(0);

        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class).get();

        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);
        final ManageAccountsAdapter adapter = new ManageAccountsAdapter(listener, authManager);
        final RecyclerView.ViewHolder viewHolder = spy(adapter.createViewHolder(new LinearLayout(activity), ManageAccountsAdapter.TYPE_ACCOUNT_ITEM));
        assertNotNull("View holder should not be null", viewHolder);
        adapter.bindViewHolder(viewHolder, ManageAccountsAdapter.HEADER_OFFSET);
        doReturn(RecyclerView.NO_POSITION).when(viewHolder).getAdapterPosition();
        viewHolder.itemView.findViewById(R.id.account_remove).performClick();

        verify(listener, never()).onRemoveAccount(anyInt(), eq(account));
    }

    @Test
    public void removeAccountAtPosition_WhenMultipleAccounts_ShouldUpdateAccountsSet() throws Exception {
        AuthManager authManager = spy((AuthManager) AuthManager.getInstance(mContext));
        List<Account> accounts = new ArrayList<>();
        Account accountMock1 = mock(Account.class);
        when(accountMock1.getUserName()).thenReturn("test1@xx.com");
        when(accountMock1.getFirstName()).thenReturn("Jamine");
        when(accountMock1.getLastName()).thenReturn("Test");
        when(accountMock1.isLoggedIn()).thenReturn(true);

        Account accountMock2 = mock(Account.class);
        when(accountMock2.getUserName()).thenReturn("test2@xx.com");
        when(accountMock2.getFirstName()).thenReturn("Jamine");
        when(accountMock2.getLastName()).thenReturn("Test");
        when(accountMock2.isLoggedIn()).thenReturn(true);

        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class).get();
        accounts.add(accountMock1);
        accounts.add(accountMock2);
        doReturn(accounts).when(authManager).getAllAccountsInternal();

        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);
        final ManageAccountsAdapter adapter = spy(new ManageAccountsAdapter(listener, authManager));

        assertEquals("Unexpected account set size", ManageAccountsAdapter.HEADER_OFFSET + 3, adapter.getItemCount()); // Two accounts, and Add Account items

        adapter.removeAccountAtPosition(ManageAccountsAdapter.HEADER_OFFSET);

        assertEquals("Unexpected account set size", ManageAccountsAdapter.HEADER_OFFSET + 2, adapter.getItemCount());
        verify(adapter).notifyItemRemoved(ManageAccountsAdapter.HEADER_OFFSET);
        verify(listener, never()).onNoAccountsFound();
    }

    @Test
    public void removeAccountAtPosition_WhenOnlyOneAccount_ShouldNotifyListener() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponse);

        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);
        final ManageAccountsAdapter adapter = spy(new ManageAccountsAdapter(listener, authManager));

        assertEquals("Unexpected account set size", ManageAccountsAdapter.HEADER_OFFSET + 2, adapter.getItemCount()); // One account, and Add Account items

        adapter.removeAccountAtPosition(ManageAccountsAdapter.HEADER_OFFSET);

        assertEquals("Unexpected account set size", ManageAccountsAdapter.HEADER_OFFSET + 1, adapter.getItemCount());
        verify(adapter, never()).notifyItemRemoved(ManageAccountsAdapter.HEADER_OFFSET);
        verify(listener).onNoAccountsFound();
    }

    @Test
    public void removeAccountAtPosition_WhenRemovePositionInvalid_ShouldNotNotify() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponse);

        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);
        final ManageAccountsAdapter adapter = spy(new ManageAccountsAdapter(listener, authManager));

        assertEquals("Unexpected account set size", ManageAccountsAdapter.HEADER_OFFSET + 2, adapter.getItemCount()); // Two accounts, and Add Account items

        // Invalid position for the account to be removed
        adapter.removeAccountAtPosition(ManageAccountsAdapter.HEADER_OFFSET + 1);

        assertEquals("Unexpected account set size", ManageAccountsAdapter.HEADER_OFFSET + 2, adapter.getItemCount());
        verify(adapter, never()).notifyItemRemoved(0);
        verify(listener, never()).onNoAccountsFound();
    }

    @Test
    public void removeAccountAtPosition_WhenRemovePositionIsNoPosition_ShouldNotNotifyItemRemoved() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponse);

        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);
        final ManageAccountsAdapter adapter = spy(new ManageAccountsAdapter(listener, authManager));

        assertEquals("Unexpected account set size", ManageAccountsAdapter.HEADER_OFFSET + 2, adapter.getItemCount()); // Two accounts, and Add Account items

        // Invalid position for the account to be removed
        adapter.removeAccountAtPosition(RecyclerView.NO_POSITION);

        assertEquals("Unexpected account set size", ManageAccountsAdapter.HEADER_OFFSET + 2, adapter.getItemCount());
        verify(adapter, never()).notifyItemRemoved(0);
        verify(listener, never()).onNoAccountsFound();
    }

    @Test
    public void removeAccountAtPosition_WhenRemovePositionInvalidAccountAlreadyRemoved_ShouldNotNotify() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponse);
        ManageAccountsAdapter.Callback listener = mock(ManageAccountsAdapter.Callback.class);
        final ManageAccountsAdapter adapter = spy(new ManageAccountsAdapter(listener, authManager));

        assertEquals("Unexpected account set size", ManageAccountsAdapter.HEADER_OFFSET + 2, adapter.getItemCount()); // One account, and Add Account items

        adapter.removeAccountAtPosition(ManageAccountsAdapter.HEADER_OFFSET);

        // Test invalid position for the account to be removed
        adapter.removeAccountAtPosition(ManageAccountsAdapter.HEADER_OFFSET + 2);

        assertEquals("Unexpected account set size", ManageAccountsAdapter.HEADER_OFFSET + 1, adapter.getItemCount());
        verify(adapter, never()).notifyItemRemoved(ManageAccountsAdapter.HEADER_OFFSET);
        verify(listener, times(1)).onNoAccountsFound();
    }

    @Test
    public void disableEditMode_WhenCalled_ShouldHideRemoveAccountButton() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponse);
        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        RecyclerView accountsList = activity.findViewById(R.id.phoenix_manage_accounts_list);
        ManageAccountsAdapter adapter = (ManageAccountsAdapter) accountsList.getAdapter();
        adapter.onCreateViewHolder(activity.findViewById(android.R.id.content), ManageAccountsAdapter.TYPE_ACCOUNT_ITEM);
        adapter.enableEditMode();
        adapter.disableEditMode();

        // need to call these two methods to force recyclerview to manually lay out and measure its views
        // or findViewHolderForAdapterPosition will return null
        accountsList.measure(0, 0);
        accountsList.layout(0, 0, 100, 10000);

        View accountItem = accountsList.findViewHolderForAdapterPosition(ManageAccountsAdapter.HEADER_OFFSET).itemView;
        assertTrue("Remove button should be invisible", accountItem.findViewById(R.id.account_remove).getVisibility() == View.INVISIBLE);
        assertTrue("Toggle button should be visible", accountItem.findViewById(R.id.account_state_toggle).getVisibility() == View.VISIBLE);
        assertEquals("Item count doesn't match", ManageAccountsAdapter.HEADER_OFFSET + 2, adapter.getItemCount());
    }

    @Test
    public void disableEditMode_WhenAlreadyDisabled_ShouldReturnSilently() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponse);
        ManageAccountsActivity activity = spy(Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get());
        RecyclerView accountsList = activity.findViewById(R.id.phoenix_manage_accounts_list);
        ManageAccountsAdapter adapter = (ManageAccountsAdapter) accountsList.getAdapter();
        ManageAccountsAdapter adapterSpy = spy(adapter);
        adapterSpy.disableEditMode();
        verify(adapterSpy, times(0)).notifyDataSetChanged();
        assertEquals("Item count doesn't match", ManageAccountsAdapter.HEADER_OFFSET + 2, adapterSpy.getItemCount());
    }

    @Test
    public void enableEditMode_WhenCalled_ShouldShowRemoveAccountButton() throws Exception {
        resetToolTipDisplayCount();
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponse);
        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().get();
        RecyclerView accountsList = activity.findViewById(R.id.phoenix_manage_accounts_list);
        ManageAccountsAdapter adapter = (ManageAccountsAdapter) accountsList.getAdapter();
        adapter.onCreateViewHolder((ViewGroup) activity.findViewById(android.R.id.content), ManageAccountsAdapter.TYPE_ACCOUNT_ITEM);
        adapter.enableEditMode();
        ToolTipWindow toolTipWindow = spy(adapter.mRemoveButtonToolTipWindow);
        doReturn(true).when(toolTipWindow).isAnchorViewVisible(any(View.class));
        adapter.mRemoveButtonToolTipWindow = toolTipWindow;
        // need to call these two methods to force recyclerview to manually lay out and measure its views
        // or findViewHolderForAdapterPosition will return null
        accountsList.measure(0, 0);
        accountsList.layout(0, 0, 100, 10000);

        View accountItem = accountsList.findViewHolderForAdapterPosition(ManageAccountsAdapter.HEADER_OFFSET).itemView;
        assertTrue("Remove button should be visible", accountItem.findViewById(R.id.account_remove).getVisibility() == View.VISIBLE);
        assertTrue("Toggle button should be invisible", accountItem.findViewById(R.id.account_state_toggle).getVisibility() == View.INVISIBLE);
        assertEquals("Item count doesn't match", ManageAccountsAdapter.HEADER_OFFSET + 1, adapter.getItemCount());
        PopupWindow popupWindow = ShadowApplication.getInstance().getLatestPopupWindow();
        Assert.assertNotNull("PopupWindow should not be null", popupWindow);
        TextView textView = (TextView) popupWindow.getContentView().findViewById(R.id.tooltip_text);
        Assert.assertEquals("Text is not correct", Html.fromHtml(mContext.getResources().getString(R.string.phoenix_manage_accounts_remove_tooltip)).toString(), textView.getText().toString());

    }

    @Test
    public void enableEditMode_WhenAlreadyEnabled_ShouldReturnSilently() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.mVerificationNonce = TestValues.getTestAuthCodeExchangeResponseNonce();
        authManager.addAccount(tokenResponse);
        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        RecyclerView accountsList = activity.findViewById(R.id.phoenix_manage_accounts_list);
        ManageAccountsAdapter adapter = (ManageAccountsAdapter) accountsList.getAdapter();
        adapter.enableEditMode();
        adapter.enableEditMode();

        // need to call these two methods to force recyclerview to manually lay out and measure its views
        // or findViewHolderForAdapterPosition will return null
        accountsList.measure(0, 0);
        accountsList.layout(0, 0, 100, 10000);

        View accountItem = accountsList.findViewHolderForAdapterPosition(ManageAccountsAdapter.HEADER_OFFSET).itemView;
        assertTrue("Remove button should be visible", accountItem.findViewById(R.id.account_remove).getVisibility() == View.VISIBLE);
        assertTrue("Toggle button should be invisible", accountItem.findViewById(R.id.account_state_toggle).getVisibility() == View.INVISIBLE);

        ManageAccountsAdapter adapterSpy = spy(adapter);
        adapterSpy.enableEditMode();
        verify(adapterSpy, times(0)).notifyDataSetChanged();
        assertEquals("Item count doesn't match", ManageAccountsAdapter.HEADER_OFFSET + 1, adapterSpy.getItemCount());
    }

    @Test
    @Config(sdk = Build.VERSION_CODES.LOLLIPOP, shadows = ShadowSnackbar.class)
    public void showAccountDisabledMessage_WhenInvoked_ShowsSnackbarWithMessage() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        authManager.addAccount(tokenResponse);
        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();

        RecyclerView accountsList = activity.findViewById(R.id.phoenix_manage_accounts_list);
        // need to call these two methods to force recyclerview to manually lay out and measure its views
        // or findViewHolderForAdapterPosition will return null
        accountsList.measure(0, 0);
        accountsList.layout(0, 0, 100, 10000);
        ManageAccountsAdapter.AccountItemViewHolder viewHolder = (ManageAccountsAdapter.AccountItemViewHolder) accountsList.findViewHolderForAdapterPosition(ManageAccountsAdapter.TYPE_ACCOUNT_ITEM);
        assertNotNull("View holder should not be null", viewHolder);

        viewHolder.showAccountDisabledSnackbar();

        Snackbar latestSnackbar = ShadowSnackbar.getLatestSnackbar();
        assertNotNull("latestSnackbar is unexpectedly null", latestSnackbar);
        assertEquals("snackbar has unexpected content", activity.getString(R.string.phoenix_manage_accounts_disable_message, AccountUtils.getApplicationName(activity)),
                ShadowSnackbar.getTextOfLatestSnackbar());
    }
}
