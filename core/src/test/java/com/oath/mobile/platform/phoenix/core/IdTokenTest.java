package com.oath.mobile.platform.phoenix.core;

import android.os.Build;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Mockito.spy;

/**
 * Created by billhuang on 11/2/17.
 */
@RunWith(RobolectricTestRunner.class)
public class IdTokenTest extends BaseTest {
    @Test @Config(sdk = Build.VERSION_CODES.JELLY_BEAN)
    public void fromJwt_WhenValidJwtIsPresentedAPI16_ShouldReturnValidIdToken() throws JSONException {
        IdToken idToken = spy(IdToken.fromJwt(TestValues.TEST_ID_TOKEN_JWT));

        Assert.assertTrue("Issuer is not the same", idToken.getIssuer().equals(TestValues.TEST_JWT_CLAIM_ISS_VAL));
        Assert.assertTrue("Issued at time is not the same", idToken.getIssuedAtTime() == TestValues.TEST_JWT_CLAIM_IAT_VAL);
        Assert.assertTrue("Expiry time is not the same", idToken.getExpirationTime() == TestValues.TEST_JWT_CLAIM_EXP_VAL);
        Assert.assertTrue("Audience is not the same", idToken.getAudience().equals(TestValues.TEST_JWT_CLAIM_AUD_VAL));
        Assert.assertEquals("Subject is not the same", "test@example.com", idToken.getSubscriber());
        Assert.assertEquals("Email is not the same", "", idToken.getEmail());
        Assert.assertEquals("Login id is not the same", "", idToken.getLoginId());

        String claimOne = (String) idToken.getAdditionalClaim(TestValues.TEST_JWT_CLAIM_KEY_ONE);
        Assert.assertTrue("Key Value One is not the same", claimOne.equals(TestValues.TEST_JWT_CLAIM_VALUE_ONE));

        String claimTwo = (String) idToken.getAdditionalClaim(TestValues.TEST_JWT_CLAIM_KEY_TWO);
        Assert.assertTrue("Key Value Two is not the same", claimTwo.equals(TestValues.TEST_JWT_CLAIM_VALUE_TWO));

        String claimThree = (String) idToken.getAdditionalClaim(TestValues.TEST_JWT_CLAIM_KEY_THREE);
        Assert.assertTrue("Key Value Three is not the same", claimThree.equals(TestValues.TEST_JWT_CLAIM_VALUE_THREE));

        Assert.assertTrue("Nonce is not the same", idToken.getNonce().equals(TestValues.TEST_JWT_NONCE_VALUE));
        Assert.assertTrue("AT_Hash is not the same", idToken.getAccessTokenHash().equals(TestValues.TEST_JWT_AT_HASH_VALUE));

        Assert.assertNull("Random object is not null", idToken.getAdditionalClaim("Random"));

    }

    @Test @Config(sdk = Build.VERSION_CODES.KITKAT)
    public void fromJwt_WhenValidJwtIsPresentedAPI19_ShouldReturnValidIdToken() throws JSONException {
        IdToken idToken = spy(IdToken.fromJwt(TestValues.TEST_ID_TOKEN_JWT));

        Assert.assertTrue("Issuer is not the same", idToken.getIssuer().equals(TestValues.TEST_JWT_CLAIM_ISS_VAL));
        Assert.assertTrue("Issued at time is not the same", idToken.getIssuedAtTime() == TestValues.TEST_JWT_CLAIM_IAT_VAL);
        Assert.assertTrue("Expiry time is not the same", idToken.getExpirationTime() == TestValues.TEST_JWT_CLAIM_EXP_VAL);
        Assert.assertTrue("Audience is not the same", idToken.getAudience().equals(TestValues.TEST_JWT_CLAIM_AUD_VAL));
        Assert.assertEquals("Subject is not the same", "test@example.com", idToken.getSubscriber());
        Assert.assertEquals("Email is not the same", "", idToken.getEmail());
        Assert.assertEquals("Login id is not the same", "", idToken.getLoginId());

        String claimOne = (String) idToken.getAdditionalClaim(TestValues.TEST_JWT_CLAIM_KEY_ONE);
        Assert.assertTrue("Key Value One is not the same", claimOne.equals(TestValues.TEST_JWT_CLAIM_VALUE_ONE));

        String claimTwo = (String) idToken.getAdditionalClaim(TestValues.TEST_JWT_CLAIM_KEY_TWO);
        Assert.assertTrue("Key Value Two is not the same", claimTwo.equals(TestValues.TEST_JWT_CLAIM_VALUE_TWO));

        String claimThree = (String) idToken.getAdditionalClaim(TestValues.TEST_JWT_CLAIM_KEY_THREE);
        Assert.assertTrue("Key Value Three is not the same", claimThree.equals(TestValues.TEST_JWT_CLAIM_VALUE_THREE));

        Assert.assertTrue("Nonce is not the same", idToken.getNonce().equals(TestValues.TEST_JWT_NONCE_VALUE));
        Assert.assertTrue("AT_Hash is not the same", idToken.getAccessTokenHash().equals(TestValues.TEST_JWT_AT_HASH_VALUE));

        Assert.assertNull("Random object is not null", idToken.getAdditionalClaim("Random"));

    }

    @Test
    public void fromJwt_WhenValidJwtIsPresentedWithLoginId_ShouldReturnValidIdToken() throws JSONException {
        IdToken idToken = spy(IdToken.fromJwt(TestValues.TEST_ID_TOKEN_WITH_LOGIN_ID_JWT));

        Assert.assertTrue("Issuer is not the same", idToken.getIssuer().equals(TestValues.TEST_JWT_CLAIM_ISS_VAL));
        Assert.assertTrue("Issued at time is not the same", idToken.getIssuedAtTime() == TestValues.TEST_JWT_CLAIM_IAT_VAL);
        Assert.assertTrue("Expiry time is not the same", idToken.getExpirationTime() == TestValues.TEST_JWT_CLAIM_EXP_VAL);
        Assert.assertTrue("Audience is not the same", idToken.getAudience().equals(TestValues.TEST_JWT_CLAIM_AUD_VAL));
        Assert.assertEquals("Subject is not the same", "test@example.com", idToken.getSubscriber());
        Assert.assertEquals("Email is not the same", "test", idToken.getLoginId());

        String claimOne = (String) idToken.getAdditionalClaim(TestValues.TEST_JWT_CLAIM_KEY_ONE);
        Assert.assertTrue("Key Value One is not the same", claimOne.equals(TestValues.TEST_JWT_CLAIM_VALUE_ONE));

        String claimTwo = (String) idToken.getAdditionalClaim(TestValues.TEST_JWT_CLAIM_KEY_TWO);
        Assert.assertTrue("Key Value Two is not the same", claimTwo.equals(TestValues.TEST_JWT_CLAIM_VALUE_TWO));

        String claimThree = (String) idToken.getAdditionalClaim(TestValues.TEST_JWT_CLAIM_KEY_THREE);
        Assert.assertTrue("Key Value Three is not the same", claimThree.equals(TestValues.TEST_JWT_CLAIM_VALUE_THREE));

        Assert.assertTrue("Nonce is not the same", idToken.getNonce().equals(TestValues.TEST_JWT_NONCE_VALUE));
        Assert.assertTrue("AT_Hash is not the same", idToken.getAccessTokenHash().equals(TestValues.TEST_JWT_AT_HASH_VALUE));

        Assert.assertNull("Random object is not null", idToken.getAdditionalClaim("Random"));

    }

    @Test(expected = IllegalArgumentException.class)
    public void fromJwt_BadInput_ShouldReturnInvalidIdToken() throws JSONException {
        IdToken idToken = IdToken.fromJwt("");
    }

    @Test
    public void getClaimPayloadAsJsonFromJwt_WhenValidJwtIsPresented_ShouldReturnValidJSONObject() throws Exception {
        IdToken idToken = IdToken.fromJwt(TestValues.TEST_JWT_TOKEN);

        JSONObject jsonObject = idToken.getClaimPayloadAsJsonFromJwt(TestValues.TEST_JWT_TOKEN);

        Assert.assertTrue("Issuer is not the same", jsonObject.getString(TestValues.TEST_JWT_CLAIM_ISS).equals(TestValues.TEST_JWT_CLAIM_ISS_VAL));
        Assert.assertTrue("Issued at time is not the same", jsonObject.getLong(TestValues.TEST_JWT_CLAIM_IAT) == TestValues.TEST_JWT_CLAIM_IAT_VAL);
        Assert.assertTrue("Expiry time is not the same", jsonObject.getLong(TestValues.TEST_JWT_CLAIM_EXP) == TestValues.TEST_JWT_CLAIM_EXP_VAL);
        Assert.assertTrue("Audience is not the same", jsonObject.getString(TestValues.TEST_JWT_CLAIM_AUD).equals(TestValues.TEST_JWT_CLAIM_AUD_VAL));
        Assert.assertTrue("Subject is not the same", jsonObject.getString(TestValues.TEST_JWT_CLAIM_SUB).equals(TestValues.TEST_JWT_CLAIM_SUB_VAL));
        Assert.assertTrue("Key Value One is not the same", jsonObject.getString(TestValues.TEST_JWT_CLAIM_KEY_ONE).equals(TestValues.TEST_JWT_CLAIM_VALUE_ONE));
        Assert.assertTrue("Key Value Two is not the same", jsonObject.getString(TestValues.TEST_JWT_CLAIM_KEY_TWO).equals(TestValues.TEST_JWT_CLAIM_VALUE_TWO));
        Assert.assertTrue("Key Value Three is not the same", jsonObject.getString(TestValues.TEST_JWT_CLAIM_KEY_THREE).equals(TestValues.TEST_JWT_CLAIM_VALUE_THREE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getClaimPayloadAsJsonFromJwt_WhenInvalidJwtIsPresented_ShouldReturnNull() throws Exception {
        IdToken idToken = IdToken.fromJwt(TestValues.TEST_JWT_TOKEN);

        JSONObject jsonObject = idToken.getClaimPayloadAsJsonFromJwt(TestValues.TEST_JWT_TOKEN_INVALID);

        Assert.assertNull(jsonObject);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getClaimPayloadAsJsonFromJwt_WhenValidJwtWithBadPayloadIsPresented_ShouldReturnNull() throws Exception {
        IdToken idToken = IdToken.fromJwt(TestValues.TEST_JWT_TOKEN);

        //Triggers bad base 64 error (IllegalArgumentException)
        JSONObject jsonObject = idToken.getClaimPayloadAsJsonFromJwt(TestValues.TEST_JWT_TOKEN_BAD_PAYLOAD);

        Assert.assertNull(jsonObject);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getClaimPayloadAsJsonFromJwt_WhenNull_ShouldReturnNull() throws Exception {
        IdToken idToken = IdToken.fromJwt(TestValues.TEST_JWT_TOKEN);

        JSONObject jsonObject = idToken.getClaimPayloadAsJsonFromJwt(null);

        Assert.assertNull(jsonObject);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getClaimPayloadAsJsonFromJwt_WhenEmpty_ShouldThrowException() throws Exception {
        IdToken idToken = IdToken.fromJwt("");

        JSONObject jsonObject = idToken.getClaimPayloadAsJsonFromJwt("");

        Assert.assertNull(jsonObject);
    }

    @Test(expected = JSONException.class)
    public void getClaimPayloadAsJsonFromJwt_WhenValidJwtWithBadPayloadJSONIsPresented_ShouldReturnNull() throws Exception {
        IdToken idToken = IdToken.fromJwt(TestValues.TEST_JWT_TOKEN);

        //Triggers bad json object error (JSONException)
        JSONObject jsonObject = idToken.getClaimPayloadAsJsonFromJwt(TestValues.TEST_JWT_TOKEN_BAD_JSON);
    }

    @Test
    public void getAdditionalClaim_whenJsonObjectIsNull_ShouldReturnNull() throws JSONException {
        IdToken idToken = IdToken.fromJwt(TestValues.TEST_JWT_TOKEN);

        idToken.mJsonObject = null;

        String email = (String) idToken.getAdditionalClaim(TestValues.TEST_JWT_CLAIM_KEY_ONE);

        Assert.assertNull("The email object is not null", email);
    }

}
