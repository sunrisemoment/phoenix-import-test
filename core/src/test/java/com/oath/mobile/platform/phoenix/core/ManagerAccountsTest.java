package com.oath.mobile.platform.phoenix.core;

import android.content.Intent;
import android.os.ResultReceiver;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.lang.reflect.Constructor;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

/**
 * Created by yuhongli on 11/6/17.
 */
@RunWith(RobolectricTestRunner.class)
public class ManagerAccountsTest extends BaseTest {

    @Test
    public void constructor_WhenInvoked_ShouldThrowException() throws Exception {
        Class<?> manageAccounts = Class.forName("com.oath.mobile.platform.phoenix.core.ManageAccounts");
        try {
            manageAccounts.newInstance();
        } catch (IllegalAccessException e) {
            assertEquals("class java.lang.IllegalAccessException", e.getClass().toString());
        }
        Constructor<?> constructor = manageAccounts.getDeclaredConstructor();
        constructor.setAccessible(true); // set it public
        constructor.newInstance(); // invoke it for coverage
    }

    @Test
    public void constructor_WhenIsCalled_ShouldCreateInstance() throws Exception {
        Intent intent = new ManageAccounts.IntentBuilder().build(RuntimeEnvironment.application);
        assertNotNull("Should not be null", intent);
    }

    @Test
    public void build_WhenEnableDismissOnAddAccount_ShouldConstructIntentWithDismissActivityOption() throws Exception {
        Intent intent = new ManageAccounts.IntentBuilder().enableDismissOnAddAccount().build(RuntimeEnvironment.application);
        assertTrue("Should be true", intent.getBooleanExtra(ManageAccountsActivity.ARG_DISMISS_ON_ADD_ACCOUNT, true));
    }

    @Test
    public void build_WhenDoesNotEnableDismissOnAddAccount_ShouldConstructIntentWithOutDismissActivityOption() throws Exception {
        Intent intent = new ManageAccounts.IntentBuilder().build(RuntimeEnvironment.application);
        assertFalse("Should be false", intent.getBooleanExtra(ManageAccountsActivity.ARG_DISMISS_ON_ADD_ACCOUNT, false));
    }

    @Test
    public void build_WhenSetResultReceiver_ShouldBePresentInTheBundle() throws Exception {
        ResultReceiver resultReceiver = new ResultReceiver(null);
        Intent intent = new ManageAccounts.IntentBuilder().setResultReceiver(resultReceiver).build(RuntimeEnvironment.application);
        assertEquals("Should be same", resultReceiver, intent.getParcelableExtra(ManageAccountsActivity.RESULT_RECEIVER_EXTRA));
    }
}
