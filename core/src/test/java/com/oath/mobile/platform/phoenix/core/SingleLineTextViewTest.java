package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.text.Layout;
import android.util.AttributeSet;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by yuhongli on 11/1/17.
 */
@RunWith(RobolectricTestRunner.class)
public class SingleLineTextViewTest extends BaseTest {
    private Context mContextMocked;

    @Before
    public void setUp() throws Exception {
        mContextMocked = RuntimeEnvironment.application.getApplicationContext();
    }

    @Test
    public void SingleLineTextView_WhenCallingConstructorWith1Argument_Success() {
        SingleLineTextView singleLineTextView = new SingleLineTextView(mContextMocked);
        Assert.assertNotNull("SingleLineTextView should not be null", singleLineTextView);
    }

    @Test
    public void SingleLineTextView_WhenCallingConstructorWith2ndArgumentNull_Success() {
        SingleLineTextView singleLineTextView = new SingleLineTextView(mContextMocked, null);
        Assert.assertNotNull("SingleLineTextView should not be null", singleLineTextView);
    }

    @Test
    public void SingleLineTextView_WhenCallingConstructorWith2ndArgumentNonNull_Success() {
        AttributeSet roboAttributeSet = Robolectric.buildAttributeSet().build();
        Assert.assertNotNull("RoboAttributeSet should not be null", roboAttributeSet);
        SingleLineTextView singleLineTextView = new SingleLineTextView(mContextMocked, roboAttributeSet);
        Assert.assertNotNull("SingleLineTextView should not be null", singleLineTextView);
    }

    @Test
    public void ReduceSize_WhenMinSizeNotSet_DoNotReduceTextViewSize() {
        AttributeSet roboAttributeSet = Robolectric.buildAttributeSet().build();
        SingleLineTextView singleLineTextView = new SingleLineTextView(mContextMocked, roboAttributeSet);
        float oldSize = singleLineTextView.getTextSize();
        singleLineTextView.reduceSize();
        float newSize = singleLineTextView.getTextSize();
        Assert.assertEquals("Text size should not change", oldSize, newSize);
    }

    @Test
    public void ReduceSize_WhenMinSizeSet_ReduceTextViewSize() {
        AttributeSet roboAttributeSet = Robolectric.buildAttributeSet()
                .addAttribute(R.attr.minTextSize, "16sp")
                .addAttribute(android.R.attr.textSize, "24sp")
                .build();
        SingleLineTextView singleLineTextView = new SingleLineTextView(mContextMocked, roboAttributeSet);
        singleLineTextView.reduceSize();
        float newSize = singleLineTextView.getTextSize();
        float expectedSize = 16;
        Assert.assertEquals("Text size not change", expectedSize, newSize);
    }

    @Test
    public void Initialize_WhenMinSizeIsGreaterThanTextSize_SetTextViewSizeToMinSize() {
        AttributeSet roboAttributeSet = Robolectric.buildAttributeSet()
                .addAttribute(R.attr.minTextSize, "16sp")
                .addAttribute(android.R.attr.textSize, "10sp")
                .build();
        SingleLineTextView singleLineTextView = new SingleLineTextView(mContextMocked, roboAttributeSet);
        float actualSize = singleLineTextView.getTextSize();
        float expectedSize = 16;
        Assert.assertEquals("Text size should change", expectedSize, actualSize);
    }

    @Test
    public void Initialize_WhenTextSizeIsGreaterThanMinSize_DoNotChangeTextSize() {
        AttributeSet roboAttributeSet = Robolectric.buildAttributeSet()
                .addAttribute(R.attr.minTextSize, "16sp")
                .addAttribute(android.R.attr.textSize, "24sp")
                .build();
        SingleLineTextView singleLineTextView = new SingleLineTextView(mContextMocked, roboAttributeSet);
        float actualSize = singleLineTextView.getTextSize();
        float expectedSize = 24;
        Assert.assertEquals("Text size should not change", expectedSize, actualSize);
    }

    @Test
    public void OnMeasure_WhenNotEllipsized_DoNotReduceSize() {
        AttributeSet roboAttributeSet = Robolectric.buildAttributeSet()
                .addAttribute(R.attr.minTextSize, "16sp")
                .addAttribute(android.R.attr.textSize, "24sp")
                .addAttribute(android.R.attr.text, "NotALong Name")
                .build();
        SingleLineTextView singleLineTextView = new SingleLineTextView(mContextMocked, roboAttributeSet);
        float oldSize = singleLineTextView.getTextSize();
        // doesn't matter what values go in measure, this just makes sure onMeasure gets called
        singleLineTextView.measure(100, 100);
        float newSize = singleLineTextView.getTextSize();
        Assert.assertEquals("Text size should not change", oldSize, newSize);
    }

    @Test
    public void OnMeasure_WhenEcllipsisCountGreateThanZero_DoReduceSize() {
        AttributeSet roboAttributeSet = Robolectric.buildAttributeSet()
                .addAttribute(R.attr.minTextSize, "16sp")
                .addAttribute(android.R.attr.textSize, "24sp")
                .addAttribute(android.R.attr.text, "Is longlonglonglonglonglonglonglonglong Name")
                .build();
        SingleLineTextView singleLineTextView = spy(new SingleLineTextView(mContextMocked, roboAttributeSet));
        Layout layout = mock(Layout.class);
        when(layout.getEllipsisCount(0)).thenReturn(3);
        doReturn(layout).when(singleLineTextView).getLayout();

        float oldSize = singleLineTextView.getTextSize();
        // doesn't matter what values go in measure, this just makes sure onMeasure gets called
        singleLineTextView.measure(100, 100);
        float newSize = singleLineTextView.getTextSize();
        verify(singleLineTextView).reduceSize();
        Assert.assertFalse("Text size should change", oldSize == newSize);
    }
}
