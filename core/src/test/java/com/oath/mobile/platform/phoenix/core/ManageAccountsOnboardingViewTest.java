package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;
import android.widget.LinearLayout;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import java.util.Map;

import static com.oath.mobile.platform.phoenix.core.EventLogger.ManageAccountsUserEvents.EVENT_MANAGE_ACCOUNTS_TUTORIAL_SCREEN;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

/**
 * Created by yuhongli on 1/3/18.
 */
@RunWith(RobolectricTestRunner.class) @Config(sdk = Build.VERSION_CODES.LOLLIPOP)
public class ManageAccountsOnboardingViewTest extends BaseTest {


    @Test
    public void newInstance() {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;
        ManageAccountsOnboardingView view = new ManageAccountsOnboardingView();
        SupportFragmentTestUtilExtended.startFragment(view);

        assertNotNull("Button should not be null", view.mButton);
        assertEquals("Unexpected text", view.getString(R.string.phoenix_manage_accounts_toggle_acct_onboarding_button_text), view.mButton.getText());
        assertEquals("Unexpected item selected", ManageAccountsOnboardingView.TOGGLE_ACCOUNT_VIEW_POSITION, view.mViewPager.getCurrentItem());
        assertTrue("Unexpected viewpager indicator selected", view.mPageOneIndicator.isSelected());
        assertFalse("Unexpected viewpager indicator selected", view.mPageTwoIndicator.isSelected());

        verify(eventLogger, atLeastOnce()).logUserEvent(Matchers.eq(EVENT_MANAGE_ACCOUNTS_TUTORIAL_SCREEN), isNull(Map.class));
    }

    @Test(expected = ClassCastException.class)
    public void onAttach_WhenContextIsNotInstanceOfOnDismissListener_ShouldThrowException() throws Exception {
        ManageAccountsOnboardingView view = new ManageAccountsOnboardingView();
        view.onAttach(mock(Context.class));
    }

    @Test
    public void onCancel_WhenDialogIsCanceled_ShouldDismissView() throws Exception {
        ManageAccountsOnboardingView view = spy(new ManageAccountsOnboardingView());
        SupportFragmentTestUtilExtended.startFragment(view);
        assertTrue(view.isVisible());
        DialogInterface.OnDismissListener dismissListenerSpy = spy(view.mDismissListener);
        view.mDismissListener = dismissListenerSpy;
        view.onCancel(mock(DialogInterface.class));
        verify(dismissListenerSpy).onDismiss(any(DialogInterface.class));
    }

    @Test
    public void onPageSelected_WhenSecondPageSelected_ShouldUpdateView() {
        ManageAccountsOnboardingView view = new ManageAccountsOnboardingView();
        SupportFragmentTestUtilExtended.startFragment(view);
        view.mViewPager.setCurrentItem(ManageAccountsOnboardingView.REMOVE_ACCOUNT_VIEW_POSITION);
        assertNotNull("Button should not be null", view.mButton);
        assertEquals("Unexpected text", view.getString(R.string.phoenix_manage_accounts_remove_acct_onboarding_button_text), view.mButton.getText());
        assertEquals("Unexpected item selected", ManageAccountsOnboardingView.REMOVE_ACCOUNT_VIEW_POSITION, view.mViewPager.getCurrentItem());
        assertFalse("Unexpected viewpager indicator selected", view.mPageOneIndicator.isSelected());
        assertTrue("Unexpected viewpager indicator selected", view.mPageTwoIndicator.isSelected());
        assertEquals("Close button should be invisible", View.INVISIBLE, view.mCloseButton.getVisibility());
    }

    @Test
    public void onPageSelected_WhenFirstPageSelected_ShouldUpdateView() {
        ManageAccountsOnboardingView view = new ManageAccountsOnboardingView();
        SupportFragmentTestUtilExtended.startFragment(view);
        view.mViewPager.setCurrentItem(ManageAccountsOnboardingView.REMOVE_ACCOUNT_VIEW_POSITION);
        view.mViewPager.setCurrentItem(ManageAccountsOnboardingView.TOGGLE_ACCOUNT_VIEW_POSITION);
        assertNotNull("Button should not be null", view.mButton);
        assertEquals("Unexpected text", view.getString(R.string.phoenix_manage_accounts_toggle_acct_onboarding_button_text), view.mButton.getText());
        assertEquals("Unexpected item selected", ManageAccountsOnboardingView.TOGGLE_ACCOUNT_VIEW_POSITION, view.mViewPager.getCurrentItem());
        assertTrue("Unexpected viewpager indicator selected", view.mPageOneIndicator.isSelected());
        assertFalse("Unexpected viewpager indicator selected", view.mPageTwoIndicator.isSelected());
        assertEquals("Close button should be visible", View.VISIBLE, view.mCloseButton.getVisibility());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getItem_WhenPositionDoesNotExist_ShouldThrowException() throws Exception {
        ManageAccountsOnboardingView view = new ManageAccountsOnboardingView();
        SupportFragmentTestUtilExtended.startFragment(view);
        view.mViewPager.setCurrentItem(ManageAccountsOnboardingView.REMOVE_ACCOUNT_VIEW_POSITION);
        view.mViewPager.setCurrentItem(ManageAccountsOnboardingView.TOGGLE_ACCOUNT_VIEW_POSITION);
        ((FragmentPagerAdapter) view.mViewPager.getAdapter()).getItem(2);
    }

    @Test
    public void onClick_WhenButtonOnToggleViewClicked_ShouldGoToRemoveView() {
        ManageAccountsOnboardingView view = new ManageAccountsOnboardingView();
        SupportFragmentTestUtilExtended.startFragment(view);
        assertNotNull("Button should not be null", view.mButton);
        assertEquals("Unexpected text", view.getString(R.string.phoenix_manage_accounts_toggle_acct_onboarding_button_text), view.mButton.getText());

        view.mButton.callOnClick();

        assertEquals("Unexpected text", view.getString(R.string.phoenix_manage_accounts_remove_acct_onboarding_button_text), view.mButton.getText());
        assertEquals("Unexpected item selected", ManageAccountsOnboardingView.REMOVE_ACCOUNT_VIEW_POSITION, view.mViewPager.getCurrentItem());
        assertFalse("Unexpected viewpager indicator selected", view.mPageOneIndicator.isSelected());
        assertTrue("Unexpected viewpager indicator selected", view.mPageTwoIndicator.isSelected());
    }

    @Test
    public void onClick_WhenButtonOnRemoveViewClicked_ShouldDismissView() {
        ManageAccountsOnboardingView view = spy(new ManageAccountsOnboardingView());
        SupportFragmentTestUtilExtended.startFragment(view);
        view.mViewPager.setCurrentItem(ManageAccountsOnboardingView.REMOVE_ACCOUNT_VIEW_POSITION);
        assertNotNull("Button should not be null", view.mButton);
        view.mButton.callOnClick();

        verify(view).dismiss();
    }

    @Test
    public void onClick_WhenCloseButtonClicked_ShouldDismissView() {
        ManageAccountsOnboardingView view = spy(new ManageAccountsOnboardingView());
        SupportFragmentTestUtilExtended.startFragment(view);
        assertNotNull("Button should not be null", view.mCloseButton);
        view.mCloseButton.callOnClick();

        verify(view).dismiss();
    }

    private static class SupportFragmentTestUtilExtended extends SupportFragmentTestUtil {
        public static void startFragment(Fragment fragment) {
            buildSupportFragmentManager(FragmentUtilActivity.class)
                    .beginTransaction().add(fragment, null).commit();
        }

        private static FragmentManager buildSupportFragmentManager(Class<? extends FragmentActivity> fragmentActivityClass) {
            FragmentActivity activity = Robolectric.setupActivity(fragmentActivityClass);
            return activity.getSupportFragmentManager();
        }
    }

    private static class FragmentUtilActivity extends FragmentActivity implements DialogInterface.OnDismissListener {
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            LinearLayout view = new LinearLayout(this);
            view.setId(1);

            setContentView(view);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {

        }

    }
}
