package com.oath.mobile.platform.phoenix.core;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsServiceConnection;
import android.widget.Button;
import android.widget.TextView;

import com.oath.mobile.testutils.TestRoboTestRunnerValidRes;

import junit.framework.Assert;

import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowDialog;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_APP_SCHEME;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_AUTH_CODE;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_STATE;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by nsoni on 10/13/17.
 */
@RunWith(TestRoboTestRunnerValidRes.class)
public class AuthActivityTest extends BaseTest {

    final String KEY_AUTHORIZATION_CODE = "code";
    final String KEY_STATE = "state";

    private Uri mCodeUri;

    private Intent mCodeIntent;

    private Uri mExceptionUri;

    private Intent mExceptionIntent;
    private AuthConfig mAuthConfig;
    private Intent mAuthConfigIntent;

    @Before
    public void setUp() {
        // for a clean start
        AuthManager actual = (AuthManager) AuthManager.getInstance(RuntimeEnvironment.application);
        actual.removeAccounts();

        mCodeUri = new Uri.Builder()
                .scheme(TEST_APP_SCHEME)
                .appendQueryParameter(KEY_STATE, TEST_STATE)
                .appendQueryParameter(KEY_AUTHORIZATION_CODE, TEST_AUTH_CODE)
                .build();
        mCodeIntent = new Intent();
        mCodeIntent.setData(mCodeUri);

        mAuthConfig = new AuthConfig(TestValues.TEST_IDP_AUTH_ENDPOINT.getAuthority(),
                TestValues.TEST_IDP_AUTH_ENDPOINT.getPath(),
                TestValues.TEST_IDP_TOKEN_ENDPOINT.getPath(),
                TestValues.TEST_CLIENT_ID,
                TestValues.TEST_CLIENT_SECRET,
                TestValues.TEST_APP_REDIRECT_URI.toString(), Arrays.asList(TestValues.TEST_SCOPE.split(" ")));
        mAuthConfigIntent = new Intent();
        mAuthConfigIntent.putExtra(AuthConfig.OATH_AUTH_CONFIG_KEY, mAuthConfig);
    }

    @Test
    public void onCreate_WhenActivityLaunched_ShouldLaunchTheCustomTabIntentAndLogStart() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Intent AppLaunchedIntent = new Intent();
        AppLaunchedIntent.putExtra(AuthConfig.OATH_AUTH_CONFIG_KEY, mAuthConfig);
        AppLaunchedIntent.putExtra(EventLogger.APP_INTERNAL_ORIGIN_DATA_KEY, EventLogger.TRACKING_PARAM_EVENT_ORIGIN_APP);

        ActivityController<AuthActivity> controller =
                Robolectric.buildActivity(AuthActivity.class, AppLaunchedIntent);
        AuthActivity activity = spy(controller.get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        activity.onCreate(null);
        ShadowActivity shadowActivity = shadowOf(activity);
        Intent startedIntent = shadowActivity.getNextStartedActivity();
        assertEquals(startedIntent.getAction(), "android.intent.action.VIEW");
        AuthHelper authService = new AuthHelper(activity);
        authService.initAuthService(activity);
        Intent expectedIntent = authService.createAuthIntent(activity);

        assertEquals("Should be present", expectedIntent.getData().getAuthority(), startedIntent.getData().getAuthority());
        assertEquals("Should be present", expectedIntent.getData().getPath(), startedIntent.getData().getPath());
        assertEquals("Should be present", expectedIntent.getData().getQueryParameter("redirect_uri"), startedIntent.getData().getQueryParameter("redirect_uri"));
        assertEquals("Should be present", expectedIntent.getData().getQueryParameter("client_id"), startedIntent.getData().getQueryParameter("client_id"));
        assertEquals("Should be present", expectedIntent.getData().getQueryParameter("prompt"), startedIntent.getData().getQueryParameter("prompt"));
        assertEquals("Should be present", "code", startedIntent.getData().getQueryParameter("response_type"));
        assertFalse("Should be present", startedIntent.getData().getQueryParameters("state").isEmpty());
        assertEquals("Should be present", "openid email sdpp-w", startedIntent.getData().getQueryParameter("scope"));


        verify(eventLogger).logUserEvent(Matchers.eq(EventLogger.SignInUserEvents.EVENT_SIGN_IN_START), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Origin is incorrect", realMap.get(EventLogger.TRACKING_KEY_EVENT_ORIGIN), EventLogger.TRACKING_PARAM_EVENT_ORIGIN_APP);
    }

    @Test
    public void onCreate_WhenActivityLaunchedFromManageAccounts_ShouldLaunchTheCustomTabIntentAndLogStart() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Intent manageAccountLaunchedIntent = new Intent();
        manageAccountLaunchedIntent.putExtra(AuthConfig.OATH_AUTH_CONFIG_KEY, mAuthConfig);
        manageAccountLaunchedIntent.putExtra(EventLogger.APP_INTERNAL_ORIGIN_DATA_KEY, EventLogger.TRACKING_PARAM_EVENT_ORIGIN_MANAGE_ACCOUNT);

        ActivityController<AuthActivity> controller =
                Robolectric.buildActivity(AuthActivity.class, manageAccountLaunchedIntent);
        AuthActivity activity = spy(controller.get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        activity.onCreate(null);

        verify(eventLogger).logUserEvent(Matchers.eq(EventLogger.SignInUserEvents.EVENT_SIGN_IN_START), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Origin is incorrect", EventLogger.TRACKING_PARAM_EVENT_ORIGIN_MANAGE_ACCOUNT, realMap.get(EventLogger.TRACKING_KEY_EVENT_ORIGIN));
    }

    @Test
    public void onCreate_WhenActivityLaunchedFromAccountPicker_ShouldLaunchTheCustomTabIntentAndLogStart() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Intent manageAccountLaunchedIntent = new Intent();
        manageAccountLaunchedIntent.putExtra(AuthConfig.OATH_AUTH_CONFIG_KEY, mAuthConfig);
        manageAccountLaunchedIntent.putExtra(EventLogger.APP_INTERNAL_ORIGIN_DATA_KEY, EventLogger.TRACKING_PARAM_EVENT_ORIGIN_ACCOUNT_PICKER);

        ActivityController<AuthActivity> controller =
                Robolectric.buildActivity(AuthActivity.class, manageAccountLaunchedIntent);
        AuthActivity activity = spy(controller.get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        activity.onCreate(null);

        verify(eventLogger).logUserEvent(Matchers.eq(EventLogger.SignInUserEvents.EVENT_SIGN_IN_START), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Origin is incorrect", realMap.get(EventLogger.TRACKING_KEY_EVENT_ORIGIN), EventLogger.TRACKING_PARAM_EVENT_ORIGIN_ACCOUNT_PICKER);
    }

    @Test
    public void onCreate_WhenSavedBundleIsPassed_ShouldNotStartIntent() throws Exception {
        ActivityController<AuthActivity> controller =
                Robolectric.buildActivity(AuthActivity.class, mAuthConfigIntent);
        AuthActivity activity = spy(controller.get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());


        Bundle outState = spy(new Bundle());

        activity.onCreate(null);

        ShadowActivity shadowActivity = shadowOf(activity);
        Intent startedIntent = shadowActivity.getNextStartedActivity();
        assertNotNull(startedIntent);

        activity.onStop();
        activity.onSaveInstanceState(outState);
        activity.onDestroy();
        activity.onCreate(outState);

        verify(outState).getString("SAVED_AUTH_REQUEST_KEY");
    }

    @Test
    public void onSaveInstanceState_WhenStopping_ShouldSaveOriginInformation() {
        Intent accountPickerLaunchedIntent = new Intent();
        accountPickerLaunchedIntent.putExtra(AuthConfig.OATH_AUTH_CONFIG_KEY, mAuthConfig);
        accountPickerLaunchedIntent.putExtra(EventLogger.APP_INTERNAL_ORIGIN_DATA_KEY, EventLogger.TRACKING_PARAM_EVENT_ORIGIN_ACCOUNT_PICKER);

        ActivityController<AuthActivity> controller =
                Robolectric.buildActivity(AuthActivity.class, accountPickerLaunchedIntent);
        AuthActivity activity = spy(controller.get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());

        activity.onCreate(null);

        Bundle outState = new Bundle();
        activity.onSaveInstanceState(outState);

        assertEquals("Origin information is not saved", EventLogger.TRACKING_PARAM_EVENT_ORIGIN_ACCOUNT_PICKER, outState.getString(EventLogger.APP_INTERNAL_ORIGIN_DATA_KEY));
    }

    @Test
    public void onCreate_WhenSavedBundleIsPassedWithWrongJson_ShouldFinishActivityWithErrorResultSet() throws Exception {
        ActivityController<AuthActivity> controller =
                Robolectric.buildActivity(AuthActivity.class, mAuthConfigIntent);
        AuthActivity activity = spy(controller.get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());

        Bundle outState = spy(new Bundle());
        outState.putString("SAVED_AUTH_REQUEST_KEY", "{wrong json}");

        activity.onCreate(outState);

        ShadowActivity shadowActivity = shadowOf(activity);
        Intent startedIntent = shadowActivity.getNextStartedActivity();

        assertNull(startedIntent);

        verify(outState).getString("SAVED_AUTH_REQUEST_KEY");
        Assert.assertNotSame(IAuthManager.RESULT_CODE_ERROR, shadowActivity.getResultCode());
        //dismiss error dialog
        Dialog latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        Button confirmButton = latestDialog.findViewById(R.id.account_custom_dialog_button);
        confirmButton.performClick();
        //verify result code

        Assert.assertEquals(IAuthManager.RESULT_CODE_ERROR, shadowActivity.getResultCode());
        Assert.assertEquals(null, shadowActivity.getResultIntent());
        Assert.assertTrue(activity.isFinishing());
    }

    @Test
    public void onResume_WhenCalledAfterOnStopAndNotOnNewIntent_ShouldFinishedTheActivityWithCanceledResultSet() throws Exception {
        ActivityController<AuthActivity> controller =
                Robolectric.buildActivity(AuthActivity.class, mAuthConfigIntent);
        AuthActivity activity = spy(controller.get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());

        activity.onCreate(null);
        activity.mAuthHelper = spy(activity.mAuthHelper);
        activity.onStop();
        activity.onResume();

        ShadowActivity shadowActivity = shadowOf(activity);
        Intent startedIntent = shadowActivity.getNextStartedActivity();

        assertNotNull(startedIntent);

        Assert.assertEquals(Activity.RESULT_CANCELED, shadowActivity.getResultCode());
        Assert.assertEquals(null, shadowActivity.getResultIntent());
        Assert.assertTrue(activity.isFinishing());
    }

    @Test
    public void onDestroy_WhenCalled_ShouldDisposeCustomTabService() throws Exception {
        ActivityController<AuthActivity> controller =
                Robolectric.buildActivity(AuthActivity.class, mAuthConfigIntent);
        AuthActivity activity = spy(controller.get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());

        activity.onCreate(null);
        activity.mAuthHelper = spy(activity.mAuthHelper);

        activity.onDestroy();

        verify(activity.mAuthHelper).disposeCustomTabService();
    }

    @Test
    public void onDestroy_WhenCalledAndAuthHelperIsNull_ShouldNotThrowException() {
        ActivityController<AuthActivity> controller =
                Robolectric.buildActivity(AuthActivity.class, mAuthConfigIntent);
        AuthActivity activity = spy(controller.get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());

        activity.onCreate(null);
        activity.mAuthHelper = null;
        activity.onDestroy();
    }

    @Test
    public void onCreateThemeTest_WhenThemeMangerIsSet_ShouldApplyTheme() {
        ThemeManager.setThemeResId(R.style.Theme_Phoenix_Default);
        ActivityController<AuthActivity> controller =
                Robolectric.buildActivity(AuthActivity.class, mAuthConfigIntent);
        AuthActivity activity = spy(controller.get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);

        verify(activity).setTheme(R.style.Theme_Phoenix_Default);

        ThemeManager.setThemeResId(0);
    }


    @Test
    public void onCreateThemeTest_WhenThemeMangerIsNotSet_ShouldNotApplyTheme() {
        ThemeManager.setThemeResId(0);
        ActivityController<AuthActivity> controller =
                Robolectric.buildActivity(AuthActivity.class, mAuthConfigIntent);
        AuthActivity activity = spy(controller.get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);

        verify(activity, never()).setTheme(anyInt());
    }

    public void onNewIntent_WhenIntentDataIsNull_ShouldFinishTheActivityWithNullIntent() throws Exception {
        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onNewIntent(new Intent());
        ShadowActivity shadowActivity = shadowOf(activity);
        Assert.assertEquals(IAuthManager.RESULT_CODE_ERROR, shadowActivity.getResultCode());
        Assert.assertEquals(null, shadowActivity.getResultIntent());
    }

    @Test
    public void onNewIntent_WhenIntentHasErrorData_ShouldFinishTheActivityWithErrorIntent() throws Exception {
        mExceptionUri = new Uri.Builder()
                .scheme(TEST_APP_SCHEME)
                .appendQueryParameter(KEY_STATE, TEST_STATE)
                .appendQueryParameter(AuthorizationException.PARAM_ERROR, "1002")
                .appendQueryParameter(AuthorizationException.PARAM_ERROR_DESCRIPTION, "access denied")
                .build();

        mExceptionIntent = new Intent();
        mExceptionIntent.setData(mExceptionUri);

        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);
        activity.onNewIntent(mExceptionIntent);
        ShadowActivity shadowActivity = shadowOf(activity);
        Assert.assertNotSame(IAuthManager.RESULT_CODE_ERROR, shadowActivity.getResultCode());
        //dismiss error dialog
        Dialog latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        Button confirmButton = latestDialog.findViewById(R.id.account_custom_dialog_button);
        confirmButton.performClick();
        //verify result code
        Assert.assertEquals(IAuthManager.RESULT_CODE_ERROR, shadowActivity.getResultCode());
        Assert.assertNull("Intent should be null", shadowActivity.getResultIntent());
    }

    @Test
    public void onNewIntent_WhenExchangeCodeFails_ShouldSetErrorResultSet() throws Exception {
        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);
        activity.mAuthHelper = spy(new AuthHelper(activity));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AuthHelper.ResponseListener callback = (AuthHelper.ResponseListener) invocation.getArguments()[2];
                callback.onComplete(IAuthManager.RESULT_CODE_ERROR, null, new AuthorizationException(AuthorizationException.TYPE_GENERAL_ERROR, 1002, "general error", "general error desc", Uri.parse("https://error.uri"), null));
                return null;
            }
        }).when(activity.mAuthHelper).exchangeCodeForTokens(any(Context.class), any(AuthorizationResponse.class), any(AuthHelper.ResponseListener.class));


        activity.onNewIntent(mCodeIntent);
        ShadowActivity shadowActivity = shadowOf(activity);
        Assert.assertNotSame(IAuthManager.RESULT_CODE_ERROR, shadowActivity.getResultCode());
        //dismiss error dialog
        Dialog latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        Button confirmButton = latestDialog.findViewById(R.id.account_custom_dialog_button);
        confirmButton.performClick();
        //verify result code
        Assert.assertEquals(IAuthManager.RESULT_CODE_ERROR, shadowActivity.getResultCode());
        Intent resultIntent = shadowActivity.getResultIntent();
        Assert.assertNull("Should have a null intent", resultIntent);

        List<IAccount> accountList = AuthManager.getInstance(activity).getAllAccounts();
        assertEquals(0, accountList.size());
        assertTrue(activity.isFinishing());
    }

    @Test
    public void onNewIntent_WhenAuthHelperIsNull_ShouldFinishActivityWithErrorResultSet() throws Exception {
        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);
        activity.mAuthHelper = null;
        activity.onNewIntent(mCodeIntent);
        ShadowActivity shadowActivity = shadowOf(activity);

        Assert.assertNotSame(IAuthManager.RESULT_CODE_ERROR, shadowActivity.getResultCode());
        //dismiss error dialog
        Dialog latestDialog = ShadowDialog.getLatestDialog();
        assertNotNull(latestDialog);
        Button confirmButton = latestDialog.findViewById(R.id.account_custom_dialog_button);
        confirmButton.performClick();
        //verify result code
        Assert.assertEquals(IAuthManager.RESULT_CODE_ERROR, shadowActivity.getResultCode());
        Assert.assertEquals(null, shadowActivity.getResultIntent());
        Assert.assertTrue(activity.isFinishing());
    }

    @Test
    public void onAuthResponse_WhenResultCodeIsErrorAndAuthExceptionIsNull_ShouldShowDialogWithUnknownError() throws Exception {
        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);

        activity.onAuthResponse(IAuthManager.RESULT_CODE_ERROR, null, null);

        Dialog dialog = ShadowDialog.getLatestDialog();
        TextView textView = dialog.findViewById(R.id.account_custom_dialog_message);
        Assert.assertEquals("", RuntimeEnvironment.application.getString(R.string.phoenix_try_again_error), textView.getText());
    }

    @Test
    public void onAuthResponse_WhenResultCodeIsErrorAndAuthExceptionIsNetworkError_ShouldShowErrorDialog() throws Exception {
        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);

        AuthorizationException ex = AuthorizationException.GeneralErrors.NETWORK_ERROR;
        activity.onAuthResponse(IAuthManager.RESULT_CODE_ERROR, null, ex);

        Dialog dialog = ShadowDialog.getLatestDialog();
        TextView textView = dialog.findViewById(R.id.account_custom_dialog_message);
        Assert.assertEquals("", RuntimeEnvironment.application.getString(R.string.phoenix_no_internet_connection), textView.getText());
    }

    @Test
    public void onAuthResponse_WhenResultCodeIsErrorAndAuthExceptionIsServerError_ShouldShowErrorDialog() throws Exception {
        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);

        AuthorizationException ex = AuthorizationException.AuthorizationRequestErrors.SERVER_ERROR;
        activity.onAuthResponse(IAuthManager.RESULT_CODE_ERROR, null, ex);

        Dialog dialog = ShadowDialog.getLatestDialog();
        TextView textView = dialog.findViewById(R.id.account_custom_dialog_message);
        Assert.assertEquals("", RuntimeEnvironment.application.getString(R.string.phoenix_try_again_error), textView.getText());
    }

    @Test
    public void onAuthResponse_WhenResultCodeIsErrorAndAuthExceptionIsServerTemporaryUnavailableError_ShouldShowErrorDialog() throws Exception {
        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);

        AuthorizationException ex = AuthorizationException.AuthorizationRequestErrors.TEMPORARILY_UNAVAILABLE;
        activity.onAuthResponse(IAuthManager.RESULT_CODE_ERROR, null, ex);

        Dialog dialog = ShadowDialog.getLatestDialog();
        TextView textView = dialog.findViewById(R.id.account_custom_dialog_message);
        Assert.assertEquals("", RuntimeEnvironment.application.getString(R.string.phoenix_try_again_error), textView.getText());
    }


    @Test
    public void onAuthResponse_WhenResultCodeIsErrorAndAuthExceptionIsUnknownError_ShouldShowErrorDialogAndLogFailureEvent() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);
        activity.onResume();

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        AuthorizationException ex = AuthorizationException.AuthorizationRequestErrors.INVALID_SCOPE;
        activity.onAuthResponse(IAuthManager.RESULT_CODE_ERROR, null, ex);

        Dialog dialog = ShadowDialog.getLatestDialog();
        TextView textView = dialog.findViewById(R.id.account_custom_dialog_message);
        Assert.assertEquals("", RuntimeEnvironment.application.getString(R.string.phoenix_try_again_error), textView.getText());
        verify(eventLogger).logUserEvent(Matchers.eq(EventLogger.SignInUserEvents.EVENT_SIGN_IN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Message is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_MESSAGE), EventLogger.SignInUserEvents.INVALID_SCOPE_MSG);
        assertEquals("Error code is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_CODE), EventLogger.SignInUserEvents.INVALID_SCOPE);
    }

    @Test
    public void onAuthResponse_WhenExceptionInvalidRequest_ShouldLogFailureEvent() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);
        activity.onResume();

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        AuthorizationException ex = AuthorizationException.AuthorizationRequestErrors.INVALID_REQUEST;
        activity.onAuthResponse(IAuthManager.RESULT_CODE_ERROR, null, ex);

        verify(eventLogger).logUserEvent(Matchers.eq(EventLogger.SignInUserEvents.EVENT_SIGN_IN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error code is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_CODE), EventLogger.SignInUserEvents.INVALID_REQUEST);
        assertEquals("Message is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_MESSAGE), EventLogger.SignInUserEvents.INVALID_REQUEST_MSG);
    }

    @Test
    public void onAuthResponse_WhenExceptionUnAuthClient_ShouldLogFailureEvent() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);
        activity.onResume();

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        AuthorizationException ex = AuthorizationException.AuthorizationRequestErrors.UNAUTHORIZED_CLIENT;
        activity.onAuthResponse(IAuthManager.RESULT_CODE_ERROR, null, ex);

        verify(eventLogger).logUserEvent(Matchers.eq(EventLogger.SignInUserEvents.EVENT_SIGN_IN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error code is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_CODE), EventLogger.SignInUserEvents.UNAUTHORIZED_CLIENT);
        assertEquals("Message is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_MESSAGE), EventLogger.SignInUserEvents.UNAUTHORIZED_CLIENT_MSG);
    }

    @Test
    public void onAuthResponse_WhenExceptionAccessDenied_ShouldLogFailureEvent() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);
        activity.onResume();

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        AuthorizationException ex = AuthorizationException.AuthorizationRequestErrors.ACCESS_DENIED;
        activity.onAuthResponse(IAuthManager.RESULT_CODE_ERROR, null, ex);

        verify(eventLogger).logUserEvent(Matchers.eq(EventLogger.SignInUserEvents.EVENT_SIGN_IN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error code is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_CODE), EventLogger.SignInUserEvents.ACCESS_DENIED);
        assertEquals("Message is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_MESSAGE), EventLogger.SignInUserEvents.ACCESS_DENIED_MSG);
    }

    @Test
    public void onAuthResponse_WhenExceptionUnSupportedResponseType_ShouldLogFailureEvent() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);
        activity.onResume();

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        AuthorizationException ex = AuthorizationException.AuthorizationRequestErrors.UNSUPPORTED_RESPONSE_TYPE;
        activity.onAuthResponse(IAuthManager.RESULT_CODE_ERROR, null, ex);

        verify(eventLogger).logUserEvent(Matchers.eq(EventLogger.SignInUserEvents.EVENT_SIGN_IN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error code is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_CODE), EventLogger.SignInUserEvents.UNSUPPORTED_RESPONSE_TYPE);
        assertEquals("Message is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_MESSAGE), EventLogger.SignInUserEvents.UNSUPPORTED_RESPONSE_TYPE_MSG);
    }

    @Test
    public void onAuthResponse_WhenExceptionInvalidScope_ShouldLogFailureEvent() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);
        activity.onResume();

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        AuthorizationException ex = AuthorizationException.AuthorizationRequestErrors.INVALID_SCOPE;
        activity.onAuthResponse(IAuthManager.RESULT_CODE_ERROR, null, ex);

        verify(eventLogger).logUserEvent(Matchers.eq(EventLogger.SignInUserEvents.EVENT_SIGN_IN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error code is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_CODE), EventLogger.SignInUserEvents.INVALID_SCOPE);
        assertEquals("Message is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_MESSAGE), EventLogger.SignInUserEvents.INVALID_SCOPE_MSG);
    }

    @Test
    public void onAuthResponse_WhenExceptionServerError_ShouldLogFailureEvent() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);
        activity.onResume();

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        AuthorizationException ex = AuthorizationException.AuthorizationRequestErrors.SERVER_ERROR;
        activity.onAuthResponse(IAuthManager.RESULT_CODE_ERROR, null, ex);

        verify(eventLogger).logUserEvent(Matchers.eq(EventLogger.SignInUserEvents.EVENT_SIGN_IN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error code is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_CODE), EventLogger.SignInUserEvents.SERVER_ERROR);
        assertEquals("Message is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_MESSAGE), EventLogger.SignInUserEvents.SERVER_ERROR_MSG);
    }

    @Test
    public void onAuthResponse_WhenExceptionTempUnAvail_ShouldLogFailureEvent() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);
        activity.onResume();

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        AuthorizationException ex = AuthorizationException.AuthorizationRequestErrors.TEMPORARILY_UNAVAILABLE;
        activity.onAuthResponse(IAuthManager.RESULT_CODE_ERROR, null, ex);

        verify(eventLogger).logUserEvent(Matchers.eq(EventLogger.SignInUserEvents.EVENT_SIGN_IN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error code is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_CODE), EventLogger.SignInUserEvents.TEMPORARILY_UNAVAILABLE);
        assertEquals("Message is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_MESSAGE), EventLogger.SignInUserEvents.TEMPORARILY_UNAVAILABLE_MSG);
    }

    @Test
    public void onAuthResponse_WhenExceptionClientError_ShouldLogFailureEvent() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);
        activity.onResume();

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        AuthorizationException ex = AuthorizationException.AuthorizationRequestErrors.CLIENT_ERROR;
        activity.onAuthResponse(IAuthManager.RESULT_CODE_ERROR, null, ex);

        verify(eventLogger).logUserEvent(Matchers.eq(EventLogger.SignInUserEvents.EVENT_SIGN_IN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error code is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_CODE), EventLogger.SignInUserEvents.CLIENT_ERROR);
        assertEquals("Message is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_MESSAGE), EventLogger.SignInUserEvents.CLIENT_ERROR_MSG);
    }


    @Test
    public void onAuthResponse_WhenExceptionOther_ShouldLogFailureEvent() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, mAuthConfigIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);
        activity.onResume();

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        AuthorizationException ex = AuthorizationException.AuthorizationRequestErrors.OTHER;
        activity.onAuthResponse(IAuthManager.RESULT_CODE_ERROR, null, ex);

        verify(eventLogger).logUserEvent(Matchers.eq(EventLogger.SignInUserEvents.EVENT_SIGN_IN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Error code is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_CODE), EventLogger.SignInUserEvents.GENERAL_ERROR);
        assertEquals("Message is incorrect", realMap.get(EventLogger.TRACKING_KEY_ERROR_MESSAGE), EventLogger.SignInUserEvents.GENERAL_ERROR_MSG);
    }


    @Test
    public void onAuthResponse_WhenResultCodeIsCanceled_ShouldLogUserCancelledEvent() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Intent manageAccountLaunchedIntent = new Intent();
        manageAccountLaunchedIntent.putExtra(AuthConfig.OATH_AUTH_CONFIG_KEY, mAuthConfig);
        manageAccountLaunchedIntent.putExtra(EventLogger.APP_INTERNAL_ORIGIN_DATA_KEY, EventLogger.TRACKING_PARAM_EVENT_ORIGIN_MANAGE_ACCOUNT);

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, manageAccountLaunchedIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);

        activity.onAuthResponse(Activity.RESULT_CANCELED, null, null);

        verify(eventLogger).logUserEvent(eq(EventLogger.SignInUserEvents.EVENT_SIGN_IN_USER_CANCELED), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Origin is incorrect", realMap.get(EventLogger.TRACKING_KEY_EVENT_ORIGIN), EventLogger.TRACKING_PARAM_EVENT_ORIGIN_MANAGE_ACCOUNT);
    }

    @Test
    public void onAuthResponse_WhenResultCodeIsOk_ShouldLogUserSuccess() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Intent accountPickerLaunchedIntent = new Intent();
        accountPickerLaunchedIntent.putExtra(AuthConfig.OATH_AUTH_CONFIG_KEY, mAuthConfig);
        accountPickerLaunchedIntent.putExtra(EventLogger.APP_INTERNAL_ORIGIN_DATA_KEY, EventLogger.TRACKING_PARAM_EVENT_ORIGIN_ACCOUNT_PICKER);

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, accountPickerLaunchedIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);

        activity.onAuthResponse(Activity.RESULT_OK, null, null);

        verify(eventLogger).logUserEvent(eq(EventLogger.SignInUserEvents.EVENT_SIGN_IN_SUCCESS), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Origin is incorrect", realMap.get(EventLogger.TRACKING_KEY_EVENT_ORIGIN), EventLogger.TRACKING_PARAM_EVENT_ORIGIN_ACCOUNT_PICKER);
    }

    @Test
    public void onAuthResponse_WhenResultCodeIsNotCanceledAndNotError_ShouldSetResult() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Intent appLaunchedIntent = new Intent();
        appLaunchedIntent.putExtra(AuthConfig.OATH_AUTH_CONFIG_KEY, mAuthConfig);
        appLaunchedIntent.putExtra(EventLogger.APP_INTERNAL_ORIGIN_DATA_KEY, EventLogger.TRACKING_PARAM_EVENT_ORIGIN_APP);

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, appLaunchedIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);

        int otherResult = 1000;
        activity.onAuthResponse(otherResult, null, null);
        ShadowActivity shadowActivity = shadowOf(activity);

        Assert.assertEquals(otherResult, shadowActivity.getResultCode());
        Assert.assertTrue(activity.isFinishing());

        verify(eventLogger).logUserEvent(eq(EventLogger.SignInUserEvents.EVENT_SIGN_IN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Origin is incorrect", realMap.get(EventLogger.TRACKING_KEY_EVENT_ORIGIN), EventLogger.TRACKING_PARAM_EVENT_ORIGIN_APP);
    }


    @Test
    public void onNewIntent_WhenResultCodeIsNotCanceledAndNotError_ShouldSetResult() throws Exception {
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        Intent appLaunchedIntent = new Intent();
        appLaunchedIntent.putExtra(AuthConfig.OATH_AUTH_CONFIG_KEY, mAuthConfig);
        appLaunchedIntent.putExtra(EventLogger.APP_INTERNAL_ORIGIN_DATA_KEY, EventLogger.TRACKING_PARAM_EVENT_ORIGIN_APP);

        ArgumentCaptor<Map> additionalParamCaptor = ArgumentCaptor.forClass(Map.class);

        AuthActivity activity = spy(Robolectric
                .buildActivity(AuthActivity.class, appLaunchedIntent)
                .get());
        doReturn(true).when(((Context) activity)).bindService(any(Intent.class), any(CustomTabsServiceConnection.class),
                anyInt());
        activity.onCreate(null);

        Intent intent = new Intent();
        intent.setData(mCodeUri);

        activity.onNewIntent(intent);

        verify(eventLogger).logUserEvent(eq(EventLogger.SignInUserEvents.EVENT_SIGN_IN_FAILURE), additionalParamCaptor.capture());
        Map<String, Object> realMap = (Map<String, Object>) additionalParamCaptor.getValue();
        assertEquals("Origin is incorrect", realMap.get(EventLogger.TRACKING_KEY_EVENT_ORIGIN), EventLogger.TRACKING_PARAM_EVENT_ORIGIN_APP);
    }
}
