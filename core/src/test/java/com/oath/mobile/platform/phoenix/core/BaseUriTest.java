package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;

import com.oath.mobile.testutils.TestRoboTestRunnerValidRes;

import net.openid.appauth.AuthorizationRequest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RuntimeEnvironment;

import java.io.IOException;
import java.util.Locale;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;

/**
 * Created by nsoni on 1/18/18.
 */
@RunWith(TestRoboTestRunnerValidRes.class)
public class BaseUriTest extends BaseTest {


    private Context mContext;

    @Before
    public void setUp() throws Exception {
        mContext = RuntimeEnvironment.application.getApplicationContext();
    }

    @Test
    public void builder_WhenCalled_ShouldAppendMandatoryQueryParams() throws Exception {
        AuthorizationRequest request = spy(TestValues.getTestAuthRequest());
        Uri requestUri = new BaseUri(request.toUri().buildUpon())
                .Builder(mContext)
                .build();

        Assert.assertEquals(BuildConfig.APPLICATION_ID, requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_APP_ID));
        Assert.assertEquals(BuildConfig.VERSION_NAME, requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_APP_VERSION));
        Assert.assertEquals(BaseUri.QUERY_PARAM_VAL_ASDK_EMBEDDED_1, requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_ASDK_EMBEDDED));
        Assert.assertEquals("us", requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_INTL));
        Assert.assertEquals("en-US", requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_LANG));
        Assert.assertEquals(BaseUri.QUERY_PARAM_VAL_SRC_SDK_ANDROID_PHNX, requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_SRC_SDK));
        Assert.assertEquals(BuildConfig.VERSION_NAME, requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_SRC_SDK_VERSION));
    }

    @Test
    public void builder_WhenGetPackageNameThrowsNameNotFoundException_ShouldHaveEmptyAppId() throws Exception {
        AuthorizationRequest request = spy(TestValues.getTestAuthRequest());
        mContext = spy(mContext);
        doThrow(PackageManager.NameNotFoundException.class).when(mContext).getPackageManager();

        Uri requestUri = new BaseUri(request.toUri().buildUpon())
                .Builder(mContext)
                .build();

        Assert.assertEquals(BuildConfig.APPLICATION_ID, requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_APP_ID));
        // app version is empty
        Assert.assertEquals("", requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_APP_VERSION));
        Assert.assertEquals(BaseUri.QUERY_PARAM_VAL_ASDK_EMBEDDED_1, requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_ASDK_EMBEDDED));
        Assert.assertEquals("us", requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_INTL));
        Assert.assertEquals("en-US", requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_LANG));
        Assert.assertEquals(BaseUri.QUERY_PARAM_VAL_SRC_SDK_ANDROID_PHNX, requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_SRC_SDK));
        Assert.assertEquals(BuildConfig.VERSION_NAME, requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_SRC_SDK_VERSION));
    }

    @Test
    public void builder_WhenGetPackageNameThrowsOtherException_ShouldHaveEmptyAppId() throws Exception {
        AuthorizationRequest request = spy(TestValues.getTestAuthRequest());
        mContext = spy(mContext);
        doThrow(IOException.class).when(mContext).getPackageManager();

        Uri requestUri = new BaseUri(request.toUri().buildUpon())
                .Builder(mContext)
                .build();

        Assert.assertEquals(BuildConfig.APPLICATION_ID, requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_APP_ID));
        // app version is empty
        Assert.assertEquals("", requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_APP_VERSION));
        Assert.assertEquals(BaseUri.QUERY_PARAM_VAL_ASDK_EMBEDDED_1, requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_ASDK_EMBEDDED));
        Assert.assertEquals("us", requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_INTL));
        Assert.assertEquals("en-US", requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_LANG));
        Assert.assertEquals(BaseUri.QUERY_PARAM_VAL_SRC_SDK_ANDROID_PHNX, requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_SRC_SDK));
        Assert.assertEquals(BuildConfig.VERSION_NAME, requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_SRC_SDK_VERSION));
    }

    @Test
    public void builder_WhenLocaleIW_ShouldAppendCorrectLocale() throws Exception {
        AuthorizationRequest request = spy(TestValues.getTestAuthRequest());

        setLocale("iw", "US");
        Uri requestUri = new BaseUri(request.toUri().buildUpon())
                .Builder(mContext)
                .build();

        Assert.assertEquals("us", requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_INTL));
        Assert.assertEquals("he-US", requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_LANG));
    }

    @Test
    public void builder_WhenLocaleJI_ShouldAppendCorrectLocale() throws Exception {
        AuthorizationRequest request = spy(TestValues.getTestAuthRequest());

        setLocale("ji", "US");
        Uri requestUri = new BaseUri(request.toUri().buildUpon())
                .Builder(mContext)
                .build();

        Assert.assertEquals("us", requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_INTL));
        Assert.assertEquals("yi-US", requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_LANG));
    }

    @Test
    public void builder_WhenLocaleIN_ShouldAppendCorrectLocale() throws Exception {
        AuthorizationRequest request = spy(TestValues.getTestAuthRequest());

        setLocale("in", "US");
        Uri requestUri = new BaseUri(request.toUri().buildUpon())
                .Builder(mContext)
                .build();

        Assert.assertEquals("us", requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_INTL));
        Assert.assertEquals("id-US", requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_LANG));
    }

    @Test
    public void builder_WhenLocaleNonConventional_ShouldAppendCorrectLocale() throws Exception {
        AuthorizationRequest request = spy(TestValues.getTestAuthRequest());

        setLocale("zh", "CN");
        Uri requestUriCN = new BaseUri(request.toUri().buildUpon())
                .Builder(mContext)
                .build();

        Assert.assertEquals("us", requestUriCN.getQueryParameter(BaseUri.QUERY_PARAM_KEY_INTL));
        Assert.assertEquals("zh-Hans-CN", requestUriCN.getQueryParameter(BaseUri.QUERY_PARAM_KEY_LANG));

        setLocale("zh", "TW");
        Uri requestUriTW = new BaseUri(request.toUri().buildUpon())
                .Builder(mContext)
                .build();

        Assert.assertEquals("tw", requestUriTW.getQueryParameter(BaseUri.QUERY_PARAM_KEY_INTL));
        Assert.assertEquals("zh-Hant-TW", requestUriTW.getQueryParameter(BaseUri.QUERY_PARAM_KEY_LANG));

        setLocale("zh", "HK");
        Uri requestUriHK = new BaseUri(request.toUri().buildUpon())
                .Builder(mContext)
                .build();

        Assert.assertEquals("hk", requestUriHK.getQueryParameter(BaseUri.QUERY_PARAM_KEY_INTL));
        Assert.assertEquals("zh-Hant-HK", requestUriHK.getQueryParameter(BaseUri.QUERY_PARAM_KEY_LANG));
    }

    @Test
    public void builder_WhenLocaleHasSpecialIntl_ShouldAppendCorrectLocale() throws Exception {
        AuthorizationRequest request = spy(TestValues.getTestAuthRequest());

        setLocale("ar", "JO");
        Uri requestUri = new BaseUri(request.toUri().buildUpon())
                .Builder(mContext)
                .build();

        Assert.assertEquals("xa", requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_INTL));
        Assert.assertEquals("ar-JO", requestUri.getQueryParameter(BaseUri.QUERY_PARAM_KEY_LANG));
    }

    private void setLocale(String language, String country) {
        Locale locale = new Locale(language, country);
        Locale.setDefault(locale);
    }

}
