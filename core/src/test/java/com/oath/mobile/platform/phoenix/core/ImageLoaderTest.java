package com.oath.mobile.platform.phoenix.core;

import android.graphics.Bitmap;
import android.widget.ImageView;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.io.File;
import java.net.HttpURLConnection;

import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by yuhongli on 10/30/17.
 */
@RunWith(RobolectricTestRunner.class)
public class ImageLoaderTest extends BaseTest {

    private static final String sImageUri = "https://s.yimg.com/wv/images/6c91e2c4cf195533bbe122f85ee8d33b_192.png";

    @Test(expected = NullPointerException.class)
    public void loadImageIntoView_WhenImageViewIsNull_ShouldThrowNPException() throws Exception {
        OkHttpClient client = mock(OkHttpClient.class);
        ImageLoader.loadImageIntoView(client, sImageUri, null);
        Assert.fail("Should throw NullPointerException");
    }

    @Test
    public void loadImageIntoView_WhenUrlIsNull_ShouldDoNothing() throws Exception {
        ImageView imageView = mock(ImageView.class);
        OkHttpClient okHttpClient = mock(OkHttpClient.class);

        ImageLoader.loadImageIntoView(okHttpClient, null, imageView);

        verify(imageView, never()).setImageBitmap(any(Bitmap.class));
    }

    @Test
    public void loadImageIntoView_WhenUrlIsNotValid_ShouldNotCrash() throws Exception {
        ImageView imageView = mock(ImageView.class);
        OkHttpClient okHttpClient = mock(OkHttpClient.class);

        ImageLoader.loadImageIntoView(okHttpClient, "test.png", imageView);

        verify(imageView, never()).setImageBitmap(any(Bitmap.class));
    }

    @Test
    public void loadImageIntoView_WhenUrlIsEmpty_ShouldDoNothing() throws Exception {
        ImageView imageView = mock(ImageView.class);
        OkHttpClient okHttpClient = mock(OkHttpClient.class);

        ImageLoader.loadImageIntoView(okHttpClient, "", imageView);

        verify(imageView, never()).setImageBitmap(any(Bitmap.class));
    }

    @Test
    public void loadImageIntoView_WhenValidUrlOnFailure_ShouldDoNothing() throws Exception {
        ImageView imageView = mock(ImageView.class);
        OkHttpClient okHttpClient = mock(OkHttpClient.class);

        final Call call = mock(Call.class);
        when(okHttpClient.newCall(any(Request.class))).thenReturn(call);
        doAnswer(invocation -> {
            okhttp3.Callback callback = (okhttp3.Callback) invocation.getArguments()[0];
            callback.onFailure(call, null);
            return null;
        }).when(call).enqueue(any(okhttp3.Callback.class));

        ImageLoader.loadImageIntoView(okHttpClient, sImageUri, imageView);

        verify(imageView, never()).setImageBitmap(any(Bitmap.class));
    }

    @Test
    public void loadImageIntoView_WhenValidUrlOnResponseNull_ShouldDoNothing() throws Exception {
        ImageView imageView = mock(ImageView.class);
        OkHttpClient okHttpClient = mock(OkHttpClient.class);
        File cacheDir = new File("cache");
        Cache cache = new Cache(cacheDir, 10 * 1024 * 1024);
        when(okHttpClient.cache()).thenReturn(cache);

        final Call call = mock(Call.class);
        when(okHttpClient.newCall(any(Request.class))).thenReturn(call);
        doAnswer(invocation -> {
            okhttp3.Callback callback = (okhttp3.Callback) invocation.getArguments()[0];
            callback.onResponse(call, null);
            return null;
        }).when(call).enqueue(any(okhttp3.Callback.class));

        ImageLoader.loadImageIntoView(okHttpClient, sImageUri, imageView);

        verify(imageView, never()).setImageBitmap(any(Bitmap.class));
    }

    @Test
    public void loadImageIntoView_WhenValidUrlOnResponseValid_ShouldSetTheBitmap() throws Exception {
        ImageView imageView = mock(ImageView.class);
        OkHttpClient okHttpClient = mock(OkHttpClient.class);

        File cacheDir = new File("cache");
        Cache cache = new Cache(cacheDir, 10 * 1024 * 1024);
        doReturn(cache).when(okHttpClient).cache();

        final Call call = mock(Call.class);
        when(okHttpClient.newCall(any(Request.class))).thenReturn(call);
        doAnswer(invocation -> {
            okhttp3.Callback callback = (okhttp3.Callback) invocation.getArguments()[0];
            Request request = new Request.Builder()
                    .url(sImageUri)
                    .build();
            Response.Builder builder = new Response.Builder()
                    .request(request)
                    .code(HttpURLConnection.HTTP_OK)
                    .message("OK")
                    .protocol(Protocol.HTTP_1_0)
                    .body(ResponseBody.create(MediaType.parse("application/json"), new byte[100]));
            callback.onResponse(call, builder.build());
            return null;
        }).when(call).enqueue(any(okhttp3.Callback.class));

        ImageLoader.loadImageIntoView(okHttpClient, sImageUri, imageView);

        verify(imageView).setImageBitmap(any(Bitmap.class));
    }

    @Test
    public void loadImage_WhenValidUrlOnFailure_ShouldDoNothing() throws Exception {
        OkHttpClient okHttpClient = mock(OkHttpClient.class);
        File cacheDir = new File("cache");
        Cache cache = new Cache(cacheDir, 10 * 1024 * 1024);
        doReturn(cache).when(okHttpClient).cache();

        final Call call = mock(Call.class);
        when(okHttpClient.newCall(any(Request.class))).thenReturn(call);
        doAnswer(invocation -> {
            okhttp3.Callback callback = (okhttp3.Callback) invocation.getArguments()[0];
            callback.onFailure(call, null);
            return null;
        }).when(call).enqueue(any(okhttp3.Callback.class));

        ImageLoader.IAccountImageLoaderListener loaderListener = spy(new ImageLoader.IAccountImageLoaderListener() {
            @Override
            public void complete(Bitmap bm) {
                Assert.assertNull("Should be null", bm);
            }
        });

        ImageLoader.loadImage(okHttpClient, sImageUri, loaderListener);

        verify(loaderListener).complete(null);
    }

    @Test
    public void loadImage_WhenValidUrlOnResponseNull_ShouldDoNothing() throws Exception {
        OkHttpClient okHttpClient = mock(OkHttpClient.class);
        File cacheDir = new File("cache");
        Cache cache = new Cache(cacheDir, 10 * 1024 * 1024);
        doReturn(cache).when(okHttpClient).cache();

        final Call call = mock(Call.class);
        when(okHttpClient.newCall(any(Request.class))).thenReturn(call);
        doAnswer(invocation -> {
            okhttp3.Callback callback = (okhttp3.Callback) invocation.getArguments()[0];
            callback.onResponse(call, null);
            return null;
        }).when(call).enqueue(any(okhttp3.Callback.class));

        ImageLoader.IAccountImageLoaderListener loaderListener = spy(new ImageLoader.IAccountImageLoaderListener() {
            @Override
            public void complete(Bitmap bm) {
                Assert.assertNull("Should be null", bm);
            }
        });

        ImageLoader.loadImage(okHttpClient, sImageUri, loaderListener);

        verify(loaderListener).complete(null);
    }

    @Test
    public void loadImage_WhenValidUrlOnResponseValid_ShouldCallListener() throws Exception {
        OkHttpClient okHttpClient = mock(OkHttpClient.class);
        File cacheDir = new File("cache");
        Cache cache = new Cache(cacheDir, 10 * 1024 * 1024);
        doReturn(cache).when(okHttpClient).cache();

        final Call call = mock(Call.class);
        when(okHttpClient.newCall(any(Request.class))).thenReturn(call);
        doAnswer(invocation -> {
            okhttp3.Callback callback = (okhttp3.Callback) invocation.getArguments()[0];
            Request request = new Request.Builder()
                    .url(sImageUri)
                    .build();
            Response.Builder builder = new Response.Builder()
                    .request(request)
                    .code(HttpURLConnection.HTTP_OK)
                    .message("OK")
                    .protocol(Protocol.HTTP_1_0)
                    .body(ResponseBody.create(MediaType.parse("application/json"), new byte[100]));
            callback.onResponse(call, builder.build());
            return null;
        }).when(call).enqueue(any(okhttp3.Callback.class));

        ImageLoader.IAccountImageLoaderListener loaderListener = spy(new ImageLoader.IAccountImageLoaderListener() {
            @Override
            public void complete(Bitmap bm) {
                Assert.assertNotNull("Should not be null", bm);
            }
        });

        ImageLoader.loadImage(okHttpClient, sImageUri, loaderListener);

        verify(loaderListener).complete(any(Bitmap.class));
    }

    @Test
    public void loadImage_WhenOnResponseAndResponseFail_ShouldCallListener() throws Exception {
        OkHttpClient okHttpClient = mock(OkHttpClient.class);
        File cacheDir = new File("cache");
        Cache cache = new Cache(cacheDir, 10 * 1024 * 1024);
        doReturn(cache).when(okHttpClient).cache();

        final Call call = mock(Call.class);
        when(okHttpClient.newCall(any(Request.class))).thenReturn(call);
        doAnswer(invocation -> {
            okhttp3.Callback callback = (okhttp3.Callback) invocation.getArguments()[0];
            Request request = new Request.Builder()
                    .url(sImageUri)
                    .build();
            Response.Builder builder = new Response.Builder()
                    .request(request)
                    .code(HttpURLConnection.HTTP_BAD_GATEWAY)
                    .message("Bad gateway")
                    .protocol(Protocol.HTTP_1_0)
                    .body(ResponseBody.create(null, ""));

            callback.onResponse(call, builder.build());
            return null;
        }).when(call).enqueue(any(okhttp3.Callback.class));

        ImageLoader.IAccountImageLoaderListener loaderListener = spy(new ImageLoader.IAccountImageLoaderListener() {
            @Override
            public void complete(Bitmap bm) {
                Assert.assertNull("Should be null", bm);
            }
        });

        ImageLoader.loadImage(okHttpClient, sImageUri, loaderListener);

        verify(loaderListener).complete(any(Bitmap.class));
    }

    @Test
    public void loadImage_WhenValidUrlOnResponseValidAndOnCompleteWithException_ShouldCallListener() {
        new ImageLoader(); //TODO Remove this when we are using jacoco 0.8, and use private constructor,refer to https://github.com/jacoco/jacoco/pull/529.
        OkHttpClient okHttpClient = mock(OkHttpClient.class);
        File cacheDir = new File("cache");
        Cache cache = new Cache(cacheDir, 10 * 1024 * 1024);
        doReturn(cache).when(okHttpClient).cache();

        final Call call = mock(Call.class);
        when(okHttpClient.newCall(any(Request.class))).thenReturn(call);
        doAnswer(invocation -> {
            okhttp3.Callback callback = (okhttp3.Callback) invocation.getArguments()[0];
            Request request = new Request.Builder()
                    .url(sImageUri)
                    .build();
            Response.Builder builder = new Response.Builder()
                    .request(request)
                    .code(HttpURLConnection.HTTP_OK)
                    .message("OK")
                    .protocol(Protocol.HTTP_1_0)
                    .body(ResponseBody.create(MediaType.parse("application/json"), new byte[100]));
            callback.onResponse(call, builder.build());
            return null;
        }).when(call).enqueue(any(okhttp3.Callback.class));

        final boolean[] isExceptionThrown = {false};
        ImageLoader.IAccountImageLoaderListener loaderListener = spy(new ImageLoader.IAccountImageLoaderListener() {
            @Override
            public void complete(Bitmap bm) {
                if (bm != null) {
                    isExceptionThrown[0] = true;
                    throw new NullPointerException(); //throw Exception when
                } else {
                    Assert.assertNull("Should be null", bm);
                }
            }
        });

        ImageLoader.loadImage(okHttpClient, sImageUri, loaderListener);
        assertTrue("", isExceptionThrown[0]);
        verify(loaderListener, times(2)).complete(any(Bitmap.class));
    }
}
