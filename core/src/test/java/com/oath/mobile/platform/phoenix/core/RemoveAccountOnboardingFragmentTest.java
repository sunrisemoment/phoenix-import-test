package com.oath.mobile.platform.phoenix.core;

import android.os.Build;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.robolectric.shadows.support.v4.SupportFragmentTestUtil.startFragment;

/**
 * Created by yuhongli on 1/3/18.
 */
@RunWith(RobolectricTestRunner.class) @Config(sdk = Build.VERSION_CODES.LOLLIPOP)
public class RemoveAccountOnboardingFragmentTest extends BaseTest {

    @Test
    public void getInstance_WhenCalled_ShouldHaveRemoveRowWithScaleOne() {
        RemoveAccountOnboardingFragment fragment = RemoveAccountOnboardingFragment.newInstance();

        assertNotNull("fragment should not be null", fragment);

        startFragment(fragment);

        assertEquals("remove row should have scaleX 1", 1f, fragment.mRemoveRow.getScaleX());
        assertEquals("remove row should have scaleY 1", 1f, fragment.mRemoveRow.getScaleY());
    }

    @Test
    public void setUserVisibleHint_WhenVisibleToUser_ShouldStartAnimation() {
        RemoveAccountOnboardingFragment fragment = spy(RemoveAccountOnboardingFragment.newInstance());
        startFragment(fragment);

        fragment.setUserVisibleHint(true);

        verify(fragment).startAnimation();
    }

    @Test
    public void setUserVisibleHint_WhenNotVisibleToUser_ShouldResetViews() {
        RemoveAccountOnboardingFragment fragment = spy(RemoveAccountOnboardingFragment.newInstance());
        startFragment(fragment);

        fragment.setUserVisibleHint(false);

        verify(fragment, times(0)).startAnimation();

        assertEquals("remove row should have scaleX 1", 1f, fragment.mRemoveRow.getScaleX());
        assertEquals("remove row should have scaleY 1", 1f, fragment.mRemoveRow.getScaleY());
    }


}
