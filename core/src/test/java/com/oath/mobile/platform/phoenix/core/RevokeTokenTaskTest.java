package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.net.Uri;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static com.oath.mobile.platform.phoenix.core.AccountNetworkAPI.CONTENT_TYPE_JSON;
import static com.oath.mobile.platform.phoenix.core.AccountNetworkAPI.ELEM_HEADER_CONTENT_TYPE;
import static com.oath.mobile.platform.phoenix.core.AuthHelper.RevokeTokenResponseListener.REVOKE_TOKEN_SUCCESS;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

/**
 * Created by yuhongli on 11/5/17.
 */
@RunWith(RobolectricTestRunner.class)
public class RevokeTokenTaskTest extends BaseTest {

    private Context mContext;

    @Before
    public void setUp() {
        mContext = RuntimeEnvironment.application.getApplicationContext();
    }

    @Test
    public void constructor_whenNewTask_shouldReturnValidTaskObject() throws Exception {
        RevokeTokenTask task = new RevokeTokenTask(mock(AuthHelper.RevokeTokenResponseListener.class));
        assertNotNull("RevokeTokenTask should not be null", task);
    }

    @Test
    public void doInBackground_WhenRevokeTokenRequestSuccess_ShouldSendRequestToRevokeTokenAndCallListener() throws Exception {
        AccountNetworkAPI networkAPI = mock(AccountNetworkAPI.class);
        String refreshToken = "refreshToken";
        String clientId = "test";
        Map<String, String> data = new HashMap<>();
        data.put("token_type_hint", "refresh_token");
        data.put("client_id", clientId);
        data.put("token", refreshToken);
        doReturn("").when(networkAPI).executeFormPost(eq(mContext), anyString(), anyMap(), anyMap());
        AuthHelper.RevokeTokenResponseListener listener = mock(AuthHelper.RevokeTokenResponseListener.class);
        RevokeTokenTask task = new RevokeTokenTask(listener);
        task.execute(mContext, networkAPI, clientId, refreshToken);
        ArgumentCaptor<String> urlRevoke = ArgumentCaptor.forClass(String.class);
        verify(networkAPI).executeFormPost(eq(mContext), urlRevoke.capture(), anyMap(), eq(data));
        assertEquals("api.login.yahoo.com", Uri.parse(urlRevoke.getValue()).getAuthority());
        assertEquals("/oauth2/revoke", Uri.parse(urlRevoke.getValue()).getPath());
        verify(listener).onComplete(0, null);
    }

    @Test
    public void doInBackground_WhenRevokeTokenRequestFailWith400Error_ShouldSendRequestToRevokeTokenAndReturnErrorToListener() throws Exception {
        AccountNetworkAPI networkAPI = mock(AccountNetworkAPI.class);
        String refreshToken = "refreshToken";
        String clientId = "test";
        Map<String, String> data = new HashMap<>();
        data.put("token_type_hint", "refresh_token");
        data.put("client_id", clientId);
        data.put("token", refreshToken);
        HttpConnectionException exception = new HttpConnectionException(400, "");
        doThrow(exception).when(networkAPI).executeFormPost(eq(mContext), anyString(), anyMap(), anyMap());
        AuthHelper.RevokeTokenResponseListener listener = mock(AuthHelper.RevokeTokenResponseListener.class);
        RevokeTokenTask task = new RevokeTokenTask(listener);
        task.execute(mContext, networkAPI, clientId, refreshToken);
        ArgumentCaptor<String> urlRevoke = ArgumentCaptor.forClass(String.class);
        verify(networkAPI).executeFormPost(eq(mContext), urlRevoke.capture(), anyMap(), eq(data));
        assertEquals("api.login.yahoo.com", Uri.parse(urlRevoke.getValue()).getAuthority());
        assertEquals("/oauth2/revoke", Uri.parse(urlRevoke.getValue()).getPath());
        verify(listener).onComplete(400, "");
    }

    @Test
    public void doInBackground_WhenRevokeTokenRequestFailWithOtherError_ShouldSendRequestToRevokeTokenAndReturnErrorToListener() throws Exception {
        AccountNetworkAPI networkAPI = mock(AccountNetworkAPI.class);
        String refreshToken = "refreshToken";
        String clientId = "test";
        Map<String, String> data = new HashMap<>();
        data.put("token_type_hint", "refresh_token");
        data.put("client_id", clientId);
        data.put("token", refreshToken);
        HttpConnectionException exception = new HttpConnectionException(408, "Client timeout");
        doThrow(exception).when(networkAPI).executeFormPost(eq(mContext), anyString(), anyMap(), anyMap());
        AuthHelper.RevokeTokenResponseListener listener = mock(AuthHelper.RevokeTokenResponseListener.class);
        RevokeTokenTask task = new RevokeTokenTask(listener);
        task.execute(mContext, networkAPI, clientId, refreshToken);
        ArgumentCaptor<String> urlRevoke = ArgumentCaptor.forClass(String.class);
        verify(networkAPI).executeFormPost(eq(mContext), urlRevoke.capture(), anyMap(), eq(data));
        assertEquals("api.login.yahoo.com", Uri.parse(urlRevoke.getValue()).getAuthority());
        assertEquals("/oauth2/revoke", Uri.parse(urlRevoke.getValue()).getPath());
        verify(listener).onComplete(408, "Client timeout");
    }

    @Test
    public void onPostExecute_WhenListenerIsNull_ShouldNotThrowException() {
        RevokeTokenTask task = new RevokeTokenTask(null);
        task.onPostExecute(null);
    }

    @Test
    public void onPostExecute_WhenListenerIsNotNullAndSuccess_ShouldCallListenerWithSuccessCode() throws Exception {
        final String responseBody = "{ \"error\": \"none\" }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url("https://api.login.yahoo.com").build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_OK).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message("OK")
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = spy(AccountNetworkAPI.getInstance(mContext));
        networkApi.setOkHttpClient(httpClient);
        AuthHelper.RevokeTokenResponseListener listener = mock(AuthHelper.RevokeTokenResponseListener.class);
        RevokeTokenTask task = new RevokeTokenTask(listener);
        task.execute(mContext, networkApi, "", "");
        verify(listener).onComplete(REVOKE_TOKEN_SUCCESS, null);
        ArgumentCaptor<String> url = ArgumentCaptor.forClass(String.class);
        verify(networkApi).executeFormPost(eq(mContext), url.capture(), anyMap(), anyMap());

        Uri uriRevokeToken = Uri.parse(url.getValue());
        assertEquals(BuildConfig.APPLICATION_ID, uriRevokeToken.getQueryParameter(BaseUri.QUERY_PARAM_KEY_APP_ID));
        assertEquals(BuildConfig.VERSION_NAME, uriRevokeToken.getQueryParameter(BaseUri.QUERY_PARAM_KEY_APP_VERSION));
        assertEquals(BaseUri.QUERY_PARAM_VAL_ASDK_EMBEDDED_1, uriRevokeToken.getQueryParameter(BaseUri.QUERY_PARAM_KEY_ASDK_EMBEDDED));
        assertEquals("us", uriRevokeToken.getQueryParameter(BaseUri.QUERY_PARAM_KEY_INTL));
        assertEquals("en-US", uriRevokeToken.getQueryParameter(BaseUri.QUERY_PARAM_KEY_LANG));
        assertEquals(BaseUri.QUERY_PARAM_VAL_SRC_SDK_ANDROID_PHNX, uriRevokeToken.getQueryParameter(BaseUri.QUERY_PARAM_KEY_SRC_SDK));
        assertEquals(BuildConfig.VERSION_NAME, uriRevokeToken.getQueryParameter(BaseUri.QUERY_PARAM_KEY_SRC_SDK_VERSION));
    }

    @Test
    public void onPostExecute_WhenListenerIsNotNullAndFail_ShouldCallListenerWithErrorCode() throws Exception {
        final String responseBody = "{ \"error\": \"unavailable\" }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url("https://api.login.yahoo.com").build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_CLIENT_TIMEOUT).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message("error")
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);
        AuthHelper.RevokeTokenResponseListener listener = mock(AuthHelper.RevokeTokenResponseListener.class);
        RevokeTokenTask task = new RevokeTokenTask(listener);
        task.execute(mContext, networkApi, "", "");
        verify(listener).onComplete(HttpURLConnection.HTTP_CLIENT_TIMEOUT, mContext
                .getString(R.string.phoenix_no_internet_connection));
        task.onPostExecute(null);
    }
}
