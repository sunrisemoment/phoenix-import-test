package com.oath.mobile.platform.phoenix.core;

import android.content.Context;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowSettings;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSource;

import static com.oath.mobile.platform.phoenix.core.AccountNetworkAPI.CONTENT_TYPE_JSON;
import static com.oath.mobile.platform.phoenix.core.AccountNetworkAPI.ELEM_HEADER_CONTENT_TYPE;
import static com.oath.mobile.platform.phoenix.core.AccountNetworkAPI.ERROR_CODE_ACCOUNT_DATA_TRANSPORT_ERROR;
import static com.oath.mobile.platform.phoenix.core.AccountNetworkAPI.ERROR_CODE_ACCOUNT_NETWORK_UNAVAILABLE;
import static com.oath.mobile.platform.phoenix.core.AccountNetworkAPI.ERROR_CODE_HTTP_VALIDATION_ERROR;
import static com.oath.mobile.platform.phoenix.core.AccountNetworkAPI.ERROR_CODE_NETWORK_UNAVAILABLE_AIRPLANE_MODE;
import static com.oath.mobile.platform.phoenix.core.AccountNetworkAPI.validateUrl;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenInternalError.EVENT_REFRESH_TOKEN_CLIENT_ERROR;
import static com.oath.mobile.platform.phoenix.core.EventLogger.RefreshTokenServerError.EVENT_REFRESH_TOKEN_SERVER_ERROR;
import static com.oath.mobile.platform.phoenix.core.TestUtils.setNetworkConnectivity;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_ERROR_RESPONSE_BODY;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_FORM_ENCODED_DATA;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_INTERNAL_SERVER_ERROR_RESPONSE_BODY;
import static com.oath.mobile.platform.phoenix.core.TestValues.TEST_VALID_RESPONSE_BODY;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by yuhongli on 11/3/17.
 */
@RunWith(RobolectricTestRunner.class)
public class AccountNetworkAPITest extends BaseTest {

    public static final String HTTP_BAD_REQUEST_MESSAGE = "Bad Request"; //400
    public static final String HTTP_FORBIDDEN_MESSAGE = "Forbidden"; //403
    public static final String HTTP_REQUEST_TIMEOUT_MESSAGE = " Request Timeout"; //408
    public static final String HTTP_INTERNAL_ERROR_MESSAGE = "Internal Error"; //500
    public static final String HTTP_SERVICE_UNAVAILABLE_MESSAGE = "Service Unavailable"; //503
    public static final String HTTP_GATEWAY_TIMEOUT_MESSAGE = "Gateway Timeout"; //504
    private static final String VALID_URL = "https://api.login.yahoo.com";
    private static final String INVALID_URL = "woeinfijbbijsdfbfisjfdsnj";
    private static final String HTTP_OK_MESSAGE = "OK";
    private Context mContext;

    @Before
    public void setUp() {
        mContext = RuntimeEnvironment.application;
    }

    @After
    public void tearDown() {
        EventLogger.sEventLoggerSingleton = null; // reset to get fresh instance of EventLogger so Roboelectric won't complain on re-spy
    }


    @Test
    public void accountNetworkAPI_WhenValidArgs_Success() throws Exception {
        AccountNetworkAPI accountNetworkAPI = AccountNetworkAPI.getInstance(mContext);
        assertNotNull("AccountNetworkAPI constructor failed.", accountNetworkAPI);
    }

    @Test(expected = HttpConnectionException.class)
    public void executeJsonPost_WhenInvalidUrl_ThrowsHttpConnectionException() throws Exception {
        AccountNetworkAPI networkApi = spy(AccountNetworkAPI.getInstance(mContext));

        Map<String, String> headers = new HashMap<>();
        String url = "http://badurl.com";
        try {
            networkApi.executeJSONPost(mContext, url, headers, "data");
        } catch (HttpConnectionException e) {
            verify(networkApi, never()).executeRequest(eq(mContext), any(Request.class));
            assertEquals("Unexpected error code", ERROR_CODE_HTTP_VALIDATION_ERROR, e.getRespCode());
            throw e;
        }
    }

    @Test
    public void executeJsonPost_WhenHasHeaders_ShouldSetHeadersInRequest() throws Exception {
        final String responseBody = "{ \"response\": 0 }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_OK).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_OK_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = spy(AccountNetworkAPI.getInstance(mContext));
        networkApi.setOkHttpClient(httpClient);
        Map<String, String> headers = new HashMap<>();
        headers.put("bucket", "value");

        networkApi.executeJSONPost(mContext, VALID_URL, headers, "{\"data\":\"\"}");
        ArgumentCaptor<Request> requestCaptor = ArgumentCaptor.forClass(Request.class);
        verify(networkApi, times(1)).executeRequest(eq(mContext), requestCaptor.capture());
        Request request = requestCaptor.getValue();
        Headers headers1 = request.headers();
        assertEquals("Unexpected header value", "value", headers1.get("bucket"));

    }

    @Test(expected = HttpConnectionException.class)
    public void executeJsonPost_WhenResponseHeaderContextTypeIsEmpty_ThrowsException() throws Exception {
        final String responseBody = "{ \"response\": 0 }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_OK).header(ELEM_HEADER_CONTENT_TYPE, "").message(HTTP_OK_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = spy(AccountNetworkAPI.getInstance(mContext));
        networkApi.setOkHttpClient(httpClient);
        try {
            networkApi.executeJSONPost(mContext, VALID_URL, null, "data");
        } catch (HttpConnectionException e) {
            verify(networkApi, times(1)).executeRequest(eq(mContext), any(Request.class));
            Assert.assertEquals("Unexpected error code", AccountNetworkAPI.ERROR_CODE_ACCOUNT_DATA_TRANSPORT_ERROR, e.getRespCode());
            throw e;
        }
    }

    @Test(expected = HttpConnectionException.class)
    public void executeJsonPost_WhenResponseHeaderContentTypeIsNotJson_ThrowsException() throws Exception {
        final String responseBody = "{ \"response\": 0 }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_OK).header(ELEM_HEADER_CONTENT_TYPE, "not json").message(HTTP_OK_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = spy(AccountNetworkAPI.getInstance(mContext));
        networkApi.setOkHttpClient(httpClient);
        try {
            networkApi.executeJSONPost(mContext, VALID_URL, null, "data");
        } catch (HttpConnectionException e) {

            verify(networkApi, times(1)).executeRequest(eq(mContext), any(Request.class));
            assertEquals("Unexpected error code", AccountNetworkAPI.ERROR_CODE_ACCOUNT_DATA_TRANSPORT_ERROR, e.getRespCode());
            throw e;
        }
    }

    @Test
    public void executeJsonPost_WhenResponseIsJson_ShouldGetResponse() throws Exception {
        final String responseBody = "{ \"error\": \"none\" }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_OK).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_OK_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = spy(AccountNetworkAPI.getInstance(mContext));
        networkApi.setOkHttpClient(httpClient);

        String result = networkApi.executeJSONPost(mContext, VALID_URL, null, "data");
        verify(networkApi, times(1)).executeRequest(eq(mContext), any(Request.class));
        assertEquals("Unexpected response body", responseBody, result);
    }

    @Test(expected = HttpConnectionException.class)
    public void executeJsonPost_WhenReadStringFromResponseBodyThrowException_ShouldThrowException() throws Exception {
        BufferedSource source = mock(BufferedSource.class);
        when(source.readString(any(Charset.class))).thenThrow(IOException.class);
        ResponseBody body = new ResponseBody() {
            @Override
            public MediaType contentType() {
                return null;
            }

            @Override
            public long contentLength() {
                return Integer.MAX_VALUE;
            }

            @Override
            public BufferedSource source() {
                return source;
            }
        };
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_OK).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_OK_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(body).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = spy(AccountNetworkAPI.getInstance(mContext));
        networkApi.setOkHttpClient(httpClient);

        try {
            networkApi.executeJSONPost(mContext, VALID_URL, null, "data");
        } catch (HttpConnectionException e) {
            assertEquals("Unexpected error code", AccountNetworkAPI.ERROR_CODE_ACCOUNT_DATA_TRANSPORT_ERROR, e.getRespCode());
            throw e;
        }
    }

    @Test
    public void executeRequest_WhenResponseCodeOK_ReturnsResponseBody() throws Exception {
        final String responseBody = "{ \"error\": \"none\" }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_OK).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_OK_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);
        Response result = networkApi.executeRequest(mContext, testRequest);
        assertEquals("Unexpected response body", responseBody, result.body().string());
    }

    @Test
    public void executeRequest_WhenResponseCodeOKAndMultipleHeaders_ReturnsResponseBody() throws Exception {
        final String responseBody = "{ \"error\": \"none\" }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_OK).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_OK_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);
        Response result = networkApi.executeRequest(mContext, testRequest);
        assertEquals("Unexpected response body", responseBody, result.body().string());
    }

    @Test
    public void executeRequest_WhenResponseCodeBadRequest_ReturnsResponseBody() throws Exception {
        final String responseBody = "{ \"error\": \"bad request\" }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_BAD_REQUEST).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_BAD_REQUEST_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);
        Response result = networkApi.executeRequest(mContext, testRequest);
        assertEquals("Unexpected response body", responseBody, result.body().string());
    }

    @Test(expected = HttpConnectionException.class)
    public void executeRequest_WhenResponseCodeUnavailable_ReturnsResponseBody() throws Exception {
        final String responseBody = "{ \"error\": \"unavailable\" }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_UNAVAILABLE).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_SERVICE_UNAVAILABLE_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);
        try {
            networkApi.executeRequest(mContext, testRequest);
        } catch (HttpConnectionException e) {
            assertEquals("Unexpected error code", AccountNetworkAPI.ERROR_CODE_ACCOUNT_DATA_TRANSPORT_ERROR, e.getRespCode());
            throw e;
        }
    }

    @Test(expected = HttpConnectionException.class)
    public void executeRequest_WhenResponseCodeForbidden_ReturnsResponseBody() throws Exception {
        final String responseBody = "{ \"error\": \"forbidden\" }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_FORBIDDEN).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_FORBIDDEN_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);
        try {
            networkApi.executeRequest(mContext, testRequest);
        } catch (HttpConnectionException e) {
            assertEquals("Unexpected error code", AccountNetworkAPI.ERROR_CODE_ACCOUNT_DATA_TRANSPORT_ERROR, e.getRespCode());
            throw e;
        }
    }

    @Test(expected = HttpConnectionException.class)
    public void executeRequest_WhenNetworkUnavailable_ThrowsHttpConnectionException() throws Exception {
        setNetworkConnectivity(false);
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        try {
            networkApi.executeRequest(mContext, testRequest);
        } catch (HttpConnectionException e) {
            assertEquals("Unexpected error code", ERROR_CODE_ACCOUNT_NETWORK_UNAVAILABLE, e.getRespCode());
            assertEquals("Unexpected error message", mContext.getString(R.string.phoenix_no_internet_connection), e.getMessage());
            throw e;
        }
    }

    @Test(expected = HttpConnectionException.class)
    public void executeRequest_WhenNetworkUnavailableAirplaneMode_ThrowsHttpConnectionException() throws Exception {
        setNetworkConnectivity(false);
        ShadowSettings.setAirplaneMode(true);
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);

        Map<String, String> headers = new HashMap<>();
        headers.put("cookies", "B=bCookie");
        try {
            networkApi.executeRequest(mContext, testRequest);
        } catch (HttpConnectionException e) {

            assertEquals("Unexpected error code", ERROR_CODE_NETWORK_UNAVAILABLE_AIRPLANE_MODE, e.getRespCode());
            assertEquals("Unexpected error message", mContext.getString(R.string.phoenix_login_airplane_mode), e.getMessage());
            throw e;
        }
    }

    @Test(expected = HttpConnectionException.class)
    public void executeRequest_WhenSocketTimeoutException_ThrowsHttpConnectionException() throws Exception {
        OkHttpClient httpClient = spy(new OkHttpClient());
        Call call = mock(Call.class);
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doThrow(new SocketTimeoutException("reason")).when(call).execute();

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);
        try {
            networkApi.executeRequest(mContext, testRequest);
        } catch (HttpConnectionException e) {
            assertEquals("Unexpected error code", AccountNetworkAPI.ERROR_CODE_NETWORK_TIME_OUT, e.getRespCode());
            assertEquals("Unexpected error message", mContext.getString(R.string.phoenix_no_internet_connection), e.getMessage());
            throw e;
        }
    }

    @Test(expected = HttpConnectionException.class)
    public void executeRequest_WhenSocketException_ThrowsHttpConnectionException() throws Exception {
        OkHttpClient httpClient = spy(new OkHttpClient());
        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doThrow(new SocketException("reason")).when(call).execute();

        Request testRequest = new Request.Builder().url(VALID_URL).build();
        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);
        try {
            networkApi.executeRequest(mContext, testRequest);
        } catch (HttpConnectionException e) {
            assertEquals("Unexpected error code", AccountNetworkAPI.ERROR_CODE_NETWORK_TIME_OUT, e.getRespCode());
            assertEquals("Unexpected error message", mContext.getString(R.string.phoenix_no_internet_connection), e.getMessage());
            throw e;
        }
    }

    @Test(expected = HttpConnectionException.class)
    public void executeRequest_WhenSSLHandshakeException_ThrowsException() throws Exception {
        OkHttpClient httpClient = spy(new OkHttpClient());
        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doThrow(new SSLHandshakeException("reason")).when(call).execute();

        Request testRequest = new Request.Builder().url(VALID_URL).build();
        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);
        try {
            networkApi.executeRequest(mContext, testRequest);
        } catch (HttpConnectionException e) {
            assertEquals("Unexpected error code", AccountNetworkAPI.ERROR_CODE_ACCOUNT_NETWORK_SSL_VERIFICATION_ERROR_DUE_TO_DATE_TIME, e.getRespCode());
            throw e;
        }
    }

    @Test(expected = HttpConnectionException.class)
    public void executeRequest_WhenSSLPeerUnverifiedException_ThrowsException() throws Exception {
        OkHttpClient httpClient = spy(new OkHttpClient());
        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doThrow(new SSLPeerUnverifiedException("reason")).when(call).execute();

        Request testRequest = new Request.Builder().url(VALID_URL).build();
        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);
        try {
            networkApi.executeRequest(mContext, testRequest);
        } catch (HttpConnectionException e) {
            assertEquals("Unexpected error code", AccountNetworkAPI.ERROR_CODE_ACCOUNT_NETWORK_SSL_VERIFICATION_ERROR, e.getRespCode());
            throw e;
        }
    }

    @Test(expected = HttpConnectionException.class)
    public void executeRequest_WhenIOException_ThrowsException() throws Exception {
        OkHttpClient httpClient = spy(new OkHttpClient());
        Call call = mock(Call.class);
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doThrow(new IOException("reason")).when(call).execute();

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);
        try {
            networkApi.executeRequest(mContext, testRequest);
        } catch (HttpConnectionException e) {
            assertEquals("Unexpected error code", AccountNetworkAPI.ERROR_CODE_ACCOUNT_DATA_TRANSPORT_ERROR, e.getRespCode());
            throw e;
        }
    }

    @Test(expected = HttpConnectionException.class)
    public void executeRequest_WhenResponseCodeClientTimeout_ThrowsException() throws Exception {
        final String responseBody = "{ \"error\": \"bad\" }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_CLIENT_TIMEOUT).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_REQUEST_TIMEOUT_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);
        try {
            networkApi.executeRequest(mContext, testRequest);
        } catch (HttpConnectionException e) {
            assertEquals("Unexpected error code", HttpURLConnection.HTTP_CLIENT_TIMEOUT, e.getRespCode());
            assertEquals("Unexpected message", mContext.getString(R.string.phoenix_no_internet_connection), e.getMessage());
            throw e;
        }
    }

    @Test(expected = HttpConnectionException.class)
    public void executeRequest_WhenResponseCodeGatewayTimeout_ThrowsException() throws Exception {
        final String responseBody = "{ \"error\": \"bad\" }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_GATEWAY_TIMEOUT).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_GATEWAY_TIMEOUT_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);
        try {
            networkApi.executeRequest(mContext, testRequest);
        } catch (HttpConnectionException e) {
            assertEquals("Unexpected error code", HttpURLConnection.HTTP_GATEWAY_TIMEOUT, e.getRespCode());
            assertEquals("Unexpected message", mContext.getString(R.string.phoenix_no_internet_connection), e.getMessage());
            throw e;
        }
    }

    @Test(expected = HttpConnectionException.class)
    public void executeRequest_WhenResponseCodeInternalError_ThrowsException() throws Exception {
        final String responseBody = "{ \"error\": \"bad\" }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_INTERNAL_ERROR).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_INTERNAL_ERROR_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);
        try {
            networkApi.executeRequest(mContext, testRequest);
        } catch (HttpConnectionException e) {
            assertEquals("Unexpected error code", AccountNetworkAPI.ERROR_CODE_ACCOUNT_DATA_TRANSPORT_ERROR, e.getRespCode());
            throw e;
        }
    }

    @Test
    public void executeFormPostAsync_WhenResponseSuccess_CallSuccessCallback() throws Exception {
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_OK).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_OK_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), TEST_VALID_RESPONSE_BODY)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            ((Callback) args[0]).onResponse(call, response);
            return null;
        }).when(call).enqueue(any(Callback.class));

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);

        HashMap<String, String> headers = new HashMap<>();
        headers.put(AccountNetworkAPI.ACCEPT, AccountNetworkAPI.CONTENT_TYPE_JSON);

        AccountNetworkAPI.FormPostAsyncCallback callback = spy(new AccountNetworkAPI.FormPostAsyncCallback() {
            @Override
            public void onSuccess(String response) {
                assertEquals("response body does not match", response, TEST_VALID_RESPONSE_BODY);
            }

            @Override
            public void onFailure(int code, HttpConnectionException exception) {

            }
        });

        networkApi.executeFormPostAsync(mContext, VALID_URL, headers, TEST_VALID_FORM_ENCODED_DATA, callback);

        verify(callback).onSuccess(any());
        verify(call).enqueue(any());
        verify(callback, never()).onFailure(anyInt(), any());
    }

    @Test
    public void executeFormPostAsync_WhenResponseSuccessCode500_CallFailCallback() throws Exception {
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_INTERNAL_ERROR).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_INTERNAL_ERROR_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), TEST_VALID_INTERNAL_SERVER_ERROR_RESPONSE_BODY)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            ((Callback) args[0]).onResponse(call, response);
            return null;
        }).when(call).enqueue(any(Callback.class));

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);

        HashMap<String, String> headers = new HashMap<>();
        headers.put(AccountNetworkAPI.ACCEPT, AccountNetworkAPI.CONTENT_TYPE_JSON);

        AccountNetworkAPI.FormPostAsyncCallback callback = spy(new AccountNetworkAPI.FormPostAsyncCallback() {
            @Override
            public void onSuccess(String response) {

            }

            @Override
            public void onFailure(int code, HttpConnectionException exception) {
                assertEquals("Failure code is the same", IDP_SPECIFIC_ERROR, code);
                assertEquals("Http Status code not the same", HttpURLConnection.HTTP_INTERNAL_ERROR, exception.getRespCode());
                assertTrue("Response body not the same", TEST_VALID_INTERNAL_SERVER_ERROR_RESPONSE_BODY.equals(exception.getRespBody()));
            }
        });

        networkApi.executeFormPostAsync(mContext, VALID_URL, headers, TEST_VALID_FORM_ENCODED_DATA, callback);

        verify(callback, never()).onSuccess(any());
        verify(call).enqueue(any());
        verify(callback).onFailure(anyInt(), any());
    }

    @Test
    public void executeFormPostAsync_WhenResponseSuccessCode400_CallFailCallback() throws Exception {
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_BAD_REQUEST).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_BAD_REQUEST_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), TEST_VALID_ERROR_RESPONSE_BODY)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            ((Callback) args[0]).onResponse(call, response);
            return null;
        }).when(call).enqueue(any(Callback.class));

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);

        HashMap<String, String> headers = new HashMap<>();
        headers.put(AccountNetworkAPI.ACCEPT, AccountNetworkAPI.CONTENT_TYPE_JSON);

        AccountNetworkAPI.FormPostAsyncCallback callback = spy(new AccountNetworkAPI.FormPostAsyncCallback() {
            @Override
            public void onSuccess(String response) {

            }

            @Override
            public void onFailure(int code, HttpConnectionException exception) {
                assertEquals("Failure code is the same", IDP_SPECIFIC_ERROR, code);
                assertEquals("Http Status code not the same", HttpURLConnection.HTTP_BAD_REQUEST, exception.getRespCode());
                assertTrue("Response body not the same", TEST_VALID_ERROR_RESPONSE_BODY.equals(exception.getRespBody()));
            }
        });

        networkApi.executeFormPostAsync(mContext, VALID_URL, headers, TEST_VALID_FORM_ENCODED_DATA, callback);

        verify(callback, never()).onSuccess(any());
        verify(call).enqueue(any());
        verify(callback).onFailure(anyInt(), any());
    }

    @Test
    public void executeFormPostAsync_WhenResponseSuccessCodeUnknown_CallFailCallback() throws Exception {
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(9001).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message("Not a legitimate error")
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), TEST_VALID_ERROR_RESPONSE_BODY)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            ((Callback) args[0]).onResponse(call, response);
            return null;
        }).when(call).enqueue(any(Callback.class));

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);

        HashMap<String, String> headers = new HashMap<>();
        headers.put(AccountNetworkAPI.ACCEPT, AccountNetworkAPI.CONTENT_TYPE_JSON);

        AccountNetworkAPI.FormPostAsyncCallback callback = spy(new AccountNetworkAPI.FormPostAsyncCallback() {
            @Override
            public void onSuccess(String response) {

            }

            @Override
            public void onFailure(int code, HttpConnectionException exception) {
                assertEquals("Failure code is the same", IDP_SPECIFIC_ERROR, code);
                assertEquals("Http Status code not the same", 9001, exception.getRespCode());
                assertTrue("Response body not the same", TEST_VALID_ERROR_RESPONSE_BODY.equals(exception.getRespBody()));
            }
        });

        networkApi.executeFormPostAsync(mContext, VALID_URL, headers, TEST_VALID_FORM_ENCODED_DATA, callback);

        verify(callback, never()).onSuccess(any());
        verify(call).enqueue(any());
        verify(callback).onFailure(anyInt(), any());
    }

    @Test
    public void executeFormPostAsync_WhenResponseSuccessCode200NullBody_CallFailCallback() throws Exception {
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_OK).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_OK_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            ((Callback) args[0]).onResponse(call, response);
            return null;
        }).when(call).enqueue(any(Callback.class));

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        HashMap<String, String> headers = new HashMap<>();
        headers.put(AccountNetworkAPI.ACCEPT, AccountNetworkAPI.CONTENT_TYPE_JSON);

        AccountNetworkAPI.FormPostAsyncCallback callback = spy(new AccountNetworkAPI.FormPostAsyncCallback() {
            @Override
            public void onSuccess(String response) {

            }

            @Override
            public void onFailure(int code, HttpConnectionException exception) {
                assertEquals("Failure code is the same", OnRefreshTokenResponse.GENERAL_ERROR, code);
                assertNull(exception);
            }
        });

        networkApi.executeFormPostAsync(mContext, VALID_URL, headers, TEST_VALID_FORM_ENCODED_DATA, callback);

        verify(callback, never()).onSuccess(any());
        verify(call).enqueue(any());
        verify(callback).onFailure(anyInt(), any());
        verify(eventLogger, times(1)).logErrorInformationEvent(Matchers.eq(EVENT_REFRESH_TOKEN_SERVER_ERROR), Matchers.eq(EventLogger.RefreshTokenServerError.SERVER_EMPTY_RESPONSE), Matchers.contains("empty response body"));
    }

    @Test
    public void executeFormPostAsync_WhenNetworkIsOffAirPlaneIsOff_CallFailCallback() throws Exception {
        setNetworkConnectivity(false);
        ShadowSettings.setAirplaneMode(false);
        OkHttpClient httpClient = spy(new OkHttpClient());

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);

        HashMap<String, String> headers = new HashMap<>();
        headers.put(AccountNetworkAPI.ACCEPT, AccountNetworkAPI.CONTENT_TYPE_JSON);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        AccountNetworkAPI.FormPostAsyncCallback callback = spy(new AccountNetworkAPI.FormPostAsyncCallback() {
            @Override
            public void onSuccess(String response) {

            }

            @Override
            public void onFailure(int code, HttpConnectionException exception) {
                assertEquals("Failure code is the same", OnRefreshTokenResponse.NETWORK_ERROR, code);
                assertNull(exception);
            }
        });

        networkApi.executeFormPostAsync(mContext, VALID_URL, headers, TEST_VALID_FORM_ENCODED_DATA, callback);

        verify(callback, never()).onSuccess(any());
        verify(callback).onFailure(anyInt(), any());
        verify(eventLogger, times(1)).logErrorInformationEvent(Matchers.eq(EVENT_REFRESH_TOKEN_CLIENT_ERROR), Matchers.eq(EventLogger.RefreshTokenInternalError.NETWORK_ISSUE), Matchers.contains("No network"));
    }

    @Test
    public void executeFormPostAsync_WhenBadEndPoint_CallFailCallback() throws Exception {
        OkHttpClient httpClient = spy(new OkHttpClient());

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);

        HashMap<String, String> headers = new HashMap<>();
        headers.put(AccountNetworkAPI.ACCEPT, AccountNetworkAPI.CONTENT_TYPE_JSON);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        AccountNetworkAPI.FormPostAsyncCallback callback = spy(new AccountNetworkAPI.FormPostAsyncCallback() {
            @Override
            public void onSuccess(String response) {

            }

            @Override
            public void onFailure(int code, HttpConnectionException exception) {
                assertEquals("Failure code is the same", OnRefreshTokenResponse.GENERAL_ERROR, code);
                assertNull(exception);
            }
        });

        networkApi.executeFormPostAsync(mContext, INVALID_URL, headers, TEST_VALID_FORM_ENCODED_DATA, callback);

        verify(callback, never()).onSuccess(any());
        verify(callback).onFailure(anyInt(), any());
        verify(eventLogger, times(1)).logErrorInformationEvent(Matchers.eq(EVENT_REFRESH_TOKEN_CLIENT_ERROR), Matchers.eq(EventLogger.RefreshTokenInternalError.AUTH_CONFIG_ERROR), Matchers.contains("Invalid url"));
    }

    @Test
    public void executeFormPostAsync_WhenResponseFailNetworkOutageAirplaneMode_CallFailCallback() throws Exception {
        OkHttpClient httpClient = spy(new OkHttpClient());

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doAnswer(invocation -> {
            setNetworkConnectivity(false);
            ShadowSettings.setAirplaneMode(true);
            Object[] args = invocation.getArguments();
            ((Callback) args[0]).onFailure(call, new IOException());
            return null;
        }).when(call).enqueue(any(Callback.class));

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        HashMap<String, String> headers = new HashMap<>();
        headers.put(AccountNetworkAPI.ACCEPT, AccountNetworkAPI.CONTENT_TYPE_JSON);

        AccountNetworkAPI.FormPostAsyncCallback callback = spy(new AccountNetworkAPI.FormPostAsyncCallback() {
            @Override
            public void onSuccess(String response) {

            }

            @Override
            public void onFailure(int code, HttpConnectionException exception) {
                assertEquals("Code returned is not the same", code, OnRefreshTokenResponse.NETWORK_ERROR);
                assertNull(exception);
            }
        });

        networkApi.executeFormPostAsync(mContext, VALID_URL, headers, TEST_VALID_FORM_ENCODED_DATA, callback);

        verify(callback, never()).onSuccess(any());
        verify(callback).onFailure(anyInt(), any());
        verify(eventLogger, times(1)).logErrorInformationEvent(Matchers.eq(EVENT_REFRESH_TOKEN_CLIENT_ERROR), Matchers.eq(EventLogger.RefreshTokenInternalError.NETWORK_ISSUE), Matchers.contains("No network"));
    }

    @Test
    public void executeFormPostAsync_WhenResponseFailEmptyException_CallFailCallback() throws Exception {
        OkHttpClient httpClient = spy(new OkHttpClient());

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doAnswer(invocation -> {
            setNetworkConnectivity(true);
            ShadowSettings.setAirplaneMode(false);
            Object[] args = invocation.getArguments();
            ((Callback) args[0]).onFailure(call, null);
            return null;
        }).when(call).enqueue(any(Callback.class));

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        HashMap<String, String> headers = new HashMap<>();
        headers.put(AccountNetworkAPI.ACCEPT, AccountNetworkAPI.CONTENT_TYPE_JSON);

        AccountNetworkAPI.FormPostAsyncCallback callback = spy(new AccountNetworkAPI.FormPostAsyncCallback() {
            @Override
            public void onSuccess(String response) {

            }

            @Override
            public void onFailure(int code, HttpConnectionException exception) {
                assertEquals("Code returned is not the same", code, OnRefreshTokenResponse.NETWORK_ERROR);
                assertNull(exception);
            }
        });

        networkApi.executeFormPostAsync(mContext, VALID_URL, headers, TEST_VALID_FORM_ENCODED_DATA, callback);

        verify(callback, never()).onSuccess(any());
        verify(callback).onFailure(anyInt(), any());
        verify(eventLogger, times(1)).logErrorInformationEvent(Matchers.eq(EVENT_REFRESH_TOKEN_CLIENT_ERROR), Matchers.eq(EventLogger.RefreshTokenInternalError.OKHTTP_ON_ERROR_WITH_NETWORK_CONNECTION), Matchers.contains("Cannot connect"));
    }

    @Test
    public void executeFormPostAsync_WhenResponseFailNetworkOutage_CallFailCallback() throws Exception {
        OkHttpClient httpClient = spy(new OkHttpClient());

        String exceptionMsgTest = "IOExceptionTest";
        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doAnswer(invocation -> {
            setNetworkConnectivity(true);
            ShadowSettings.setAirplaneMode(false);
            Object[] args = invocation.getArguments();
            ((Callback) args[0]).onFailure(call, new IOException(exceptionMsgTest));
            return null;
        }).when(call).enqueue(any(Callback.class));
        EventLogger eventLogger = spy(EventLogger.getInstance());
        EventLogger.sEventLoggerSingleton = eventLogger;

        AccountNetworkAPI networkApi = AccountNetworkAPI.getInstance(mContext);
        networkApi.setOkHttpClient(httpClient);

        HashMap<String, String> headers = new HashMap<>();
        headers.put(AccountNetworkAPI.ACCEPT, AccountNetworkAPI.CONTENT_TYPE_JSON);

        AccountNetworkAPI.FormPostAsyncCallback callback = spy(new AccountNetworkAPI.FormPostAsyncCallback() {
            @Override
            public void onSuccess(String response) {

            }

            @Override
            public void onFailure(int code, HttpConnectionException exception) {
                assertEquals("Failure code is not the same", OnRefreshTokenResponse.NETWORK_ERROR, code);
                assertNull(exception);
            }
        });

        networkApi.executeFormPostAsync(mContext, VALID_URL, headers, TEST_VALID_FORM_ENCODED_DATA, callback);

        verify(callback, never()).onSuccess(any());
        verify(callback).onFailure(anyInt(), any());
        verify(eventLogger, times(1)).logErrorInformationEvent(Matchers.eq(EVENT_REFRESH_TOKEN_CLIENT_ERROR), Matchers.eq(EventLogger.RefreshTokenInternalError.OKHTTP_ON_ERROR_WITH_NETWORK_CONNECTION), Matchers.contains(exceptionMsgTest));
    }

    @Test(expected = HttpConnectionException.class)
    public void executeFormPost_WhenInvalidUrl_ThrowsHttpConnectionException() throws Exception {
        AccountNetworkAPI networkApi = spy(AccountNetworkAPI.getInstance(mContext));

        Map<String, String> headers = new HashMap<>();
        String url = "http://badurl.com";
        try {
            networkApi.executeFormPost(mContext, url, headers, new HashMap<>());
        } catch (HttpConnectionException e) {
            verify(networkApi, never()).executeRequest(eq(mContext), any(Request.class));
            assertEquals("Unexpected error code", ERROR_CODE_HTTP_VALIDATION_ERROR, e.getRespCode());
            throw e;
        }
    }

    @Test(expected = HttpConnectionException.class)
    public void executeFormPost_WhenRequestBodayIsNull_ThrowsHttpConnectionException() throws Exception {
        final String responseBody = "{ \"response\": 0 }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_OK).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_OK_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = spy(AccountNetworkAPI.getInstance(mContext));
        networkApi.setOkHttpClient(httpClient);

        Map<String, String> headers = new HashMap<>();
        headers.put("bucket", "value");

        try {
            networkApi.executeFormPost(mContext, VALID_URL, headers, new HashMap<>());
        } catch (HttpConnectionException e) {
            verify(networkApi, never()).executeRequest(eq(mContext), any(Request.class));
            assertEquals("Unexpected error code", ERROR_CODE_ACCOUNT_DATA_TRANSPORT_ERROR, e.getRespCode());
            throw e;
        }
    }

    @Test
    public void executeFormPost_WhenHasHeaders_ShouldSetHeadersInRequest() throws Exception {
        final String responseBody = "{ \"response\": 0 }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_OK).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_OK_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = spy(AccountNetworkAPI.getInstance(mContext));
        networkApi.setOkHttpClient(httpClient);
        Map<String, String> headers = new HashMap<>();
        headers.put("bucket", "value");

        Map<String, String> data = new HashMap<>();
        data.put("client_id", "test");

        networkApi.executeFormPost(mContext, VALID_URL, headers, data);
        ArgumentCaptor<Request> requestCaptor = ArgumentCaptor.forClass(Request.class);
        verify(networkApi, times(1)).executeRequest(eq(mContext), requestCaptor.capture());
        Request request = requestCaptor.getValue();
        Headers headers1 = request.headers();
        assertEquals("Unexpected header value", "value", headers1.get("bucket"));

    }

    @Test
    public void executeFormPost_WhenResponseIsJson_ShouldGetResponse() throws Exception {
        final String responseBody = "{ \"error\": \"none\" }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_OK).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_OK_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = mock(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();

        AccountNetworkAPI networkApi = spy(AccountNetworkAPI.getInstance(mContext));
        networkApi.setOkHttpClient(httpClient);

        Map<String, String> data = new HashMap<>();
        data.put("client_id", "test");
        String result = networkApi.executeFormPost(mContext, VALID_URL, null, data);
        verify(networkApi, times(1)).executeRequest(eq(mContext), any(Request.class));
        assertEquals("Unexpected response body", responseBody, result);
    }

    @Test
    public void validateUrl_WhenUrlNull_FailsValidation() throws Exception {
        assertFalse("Expected validateUrl to return false", validateUrl(null));
    }

    @Test
    public void validateUrl_WhenUrlIsEmpty_FailsValidation() throws Exception {
        assertFalse("Expected validateUrl to return false", validateUrl(""));
    }

    @Test
    public void validateUrl_WhenUrlIsInvalidYahoo_FailsValidation() throws Exception {
        assertFalse("Expected validateUrl to return false", validateUrl("https//login.yahoo.com"));
    }

    @Test
    public void validateUrl_WhenUrlIsValidYahoo_PassesValidation() throws Exception {
        assertTrue("Expected validateUrl to return true", validateUrl("https://login.yahoo.com/"));
    }

    @Test
    public void validateUrl_WhenUrlIsNotHttpsYahoo_FailsValidation() throws Exception {
        assertFalse("Expected validateUrl to return false", validateUrl("http://login.yahoo.com/"));
    }

    @Test
    public void validateUrl_WhenUrlDoesNotHaveHost_FailsValidation() throws Exception {
        assertFalse("Expected validateUrl to return false", validateUrl("https://@#/test"));
    }

    @Test
    public void validateUrl_WhenURISynctaxException_FailsValidation() throws Exception {
        assertFalse("Expected validateUrl to return false", validateUrl("https://"));
    }

    @Test
    public void isNetworkAvailable_WhenConnectivityManagerIsNull_ShouldReturnFalse() throws Exception {
        Context mockContext = spy(mContext);
        doReturn(null).when(mockContext).getSystemService(Context.CONNECTIVITY_SERVICE);
        AccountNetworkAPI.isNetworkAvailable(mockContext);
    }

    @Test
    public void getOkHttpClient_WhenIsCalled_ShouldReturnOkHttpClient() throws Exception {
        OkHttpClient client = AccountNetworkAPI.getInstance(mContext).getOkHttpClient();
        assertNotNull("Should return okHttp client", client);
    }

    @Test(expected = HttpConnectionException.class)
    public void executeGet_WhenInvalidUrl_ShouldThrowHttpConnException() throws Exception {
        Headers headers = new Headers.Builder().add("attribute", "guid").build();
        try {
            String response = AccountNetworkAPI.getInstance(mContext).executeGet(mContext, INVALID_URL, headers);
        } catch (Exception e) {
            assertTrue("Should be instance of HttpConnectionException", e instanceof HttpConnectionException);
            throw e;
        }
    }

    @Test
    public void executeGet_WhenValidUrl_ShouldReturnValidResponse() throws Exception {
        final String responseBody = "{ \"response\": 0 }";
        OkHttpClient httpClient = spy(new OkHttpClient());
        Request testRequest = new Request.Builder().url(VALID_URL).addHeader("attribute", "guid").build();
        Response response = new Response.Builder().code(HttpURLConnection.HTTP_OK).header(ELEM_HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).message(HTTP_OK_MESSAGE)
                .protocol(Protocol.HTTP_1_1).request(testRequest).body(ResponseBody.create(MediaType.parse(CONTENT_TYPE_JSON), responseBody)).build();

        Call call = spy(Call.class);
        doReturn(call).when(httpClient).newCall(any(Request.class));
        doReturn(response).when(call).execute();
        AccountNetworkAPI networkApi = spy(AccountNetworkAPI.getInstance(mContext));
        networkApi.setOkHttpClient(httpClient);

        Headers headers = new Headers.Builder().add("attribute", "guid").build();
        String responseJson = networkApi.executeGet(mContext, VALID_URL, headers);

        Assert.assertEquals("Not an expected response", responseBody, responseJson);
        verify(httpClient).newCall(any(Request.class));
    }
}
