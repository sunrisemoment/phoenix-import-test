package com.oath.mobile.platform.phoenix.core;

import android.os.Build;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.robolectric.shadows.support.v4.SupportFragmentTestUtil.startFragment;

/**
 * Created by yuhongli on 1/3/18.
 */
@RunWith(RobolectricTestRunner.class) @Config(sdk = Build.VERSION_CODES.LOLLIPOP)
public class ToggleAccountOnboardingFragmentTest extends BaseTest {


    @Test
    public void getInstance_WhenCreated_ShouldHaveTransparentDisableRow() {
        ToggleAccountOnboardingFragment fragment = ToggleAccountOnboardingFragment.newInstance();

        assertNotNull("fragment should not be null", fragment);

        startFragment(fragment);
        assertEquals("disable row should have 0 alpha", 0f, fragment.mDisabledRow.getAlpha());
        assertEquals("enabled row should have 1 alpha", 1f, fragment.mEnabledRow.getAlpha());
    }

    @Test
    public void onCreateView_WhenCalled_ShouldStartAnimation() {
        ToggleAccountOnboardingFragment fragment = spy(ToggleAccountOnboardingFragment.newInstance());
        startFragment(fragment);

        verify(fragment).startAnimation();
    }

    @Test
    public void setUserVisibleHint_WhenVisibleToUser_ShouldStartAnimation() {
        ToggleAccountOnboardingFragment fragment = spy(ToggleAccountOnboardingFragment.newInstance());
        startFragment(fragment);

        fragment.setUserVisibleHint(true);

        // Called once in start fragment and again in setUserVisibleHint
        verify(fragment, times(2)).startAnimation();
    }

    @Test
    public void setUserVisibleHint_WhenNotVisibleToUser_ShouldResetViews() {
        ToggleAccountOnboardingFragment fragment = spy(ToggleAccountOnboardingFragment.newInstance());
        startFragment(fragment);

        fragment.setUserVisibleHint(false);

        // One time called when the fragment was started
        verify(fragment, times(1)).startAnimation();

        assertEquals("disable row should have 0 alpha", 0f, fragment.mDisabledRow.getAlpha());
        assertEquals("enabled row should have 1 alpha", 1f, fragment.mEnabledRow.getAlpha());
        assertEquals("enabled row should have scaleX 1", 1f, fragment.mEnabledRow.getScaleX());
        assertEquals("enabled row should have scaleY 1", 1f, fragment.mEnabledRow.getScaleY());
    }

}
