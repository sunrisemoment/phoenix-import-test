package com.oath.mobile.platform.phoenix.core;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannedString;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;

import junit.framework.Assert;

import net.openid.appauth.TokenResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

/**
 * Created by yuhongli on 12/18/17.
 */
@RunWith(RobolectricTestRunner.class) @Config(sdk = Build.VERSION_CODES.LOLLIPOP)
public class ToolTipWindowTest {

    private Context mContext;
    Intent mManageAccountIntent;

    @Before
    public void setUp() throws Exception {
        mContext = RuntimeEnvironment.application;
        mManageAccountIntent = new Intent();
        mManageAccountIntent.setClass(mContext, ManageAccountsActivity.class);
        mManageAccountIntent.putExtra(ManageAccountsActivity.INTERNAL_LAUNCH_GATE, true);
    }

    @Test
    public void Constructor_WhenInvoked_ShouldNotBeNull() throws Exception {
        ToolTipWindow instance1 = new ToolTipWindow (mContext);
        Assert.assertNotNull("Should not be null", instance1);
    }

    @Test
    public void ShowToolTip_WhenNoOffsetAndValidInputs_ShowPopUpWindow() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        RecyclerView accountsList = activity.findViewById(R.id.phoenix_manage_accounts_list);
        ManageAccountsAdapter adapter = (ManageAccountsAdapter) accountsList.getAdapter();
        adapter.enableEditMode();
        // need to call these two methods to force recyclerview to manually lay out and measure its views
        // or findViewHolderForAdapterPosition will return null
        accountsList.measure(0, 0);
        accountsList.layout(0, 0, 100, 10000);

        View accountItem = accountsList.findViewHolderForAdapterPosition(0).itemView;

        ToolTipWindow instance1 = spy(new ToolTipWindow(mContext));
        doReturn(true).when(instance1).isAnchorViewVisible(any(View.class));
        instance1.showToolTip(accountItem, ToolTipWindow.REMOVE, new SpannedString("tooltiptext"));

        PopupWindow popupWindow = ShadowApplication.getInstance().getLatestPopupWindow();
        Assert.assertNotNull("PopupWindow should not be null", popupWindow);
        TextView textView = (TextView)popupWindow.getContentView().findViewById(R.id.tooltip_text);
        Assert.assertEquals("Text is not correct", "tooltiptext", textView.getText().toString());
        Assert.assertTrue("Should show the popup window", instance1.mPopupWindow.isShowing());

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        int count =  sharedPreferences.getInt(ToolTipWindow.SHARED_PREF_PREFIX + ToolTipWindow.REMOVE, 0);

        Assert.assertEquals("Count is not correct", 0, count);

    }

    @Test
    public void ShowToolTip_WhenOffsetAndValidInputs_ShowPopUpWindow() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        RecyclerView accountsList = (RecyclerView) activity.findViewById(R.id.phoenix_manage_accounts_list);
        ManageAccountsAdapter adapter = (ManageAccountsAdapter) accountsList.getAdapter();
        adapter.enableEditMode();
        // need to call these two methods to force recyclerview to manually lay out and measure its views
        // or findViewHolderForAdapterPosition will return null
        accountsList.measure(0, 0);
        accountsList.layout(0, 0, 100, 10000);

        View accountItem = accountsList.findViewHolderForAdapterPosition(0).itemView;

        ToolTipWindow instance1 = spy(new ToolTipWindow(mContext));
        doReturn(true).when(instance1).isAnchorViewVisible(any(View.class));
        instance1.showToolTip(accountItem, ToolTipWindow.EDIT, new SpannedString("tooltiptext2"), 24);

        PopupWindow popupWindow = ShadowApplication.getInstance().getLatestPopupWindow();
        Assert.assertNotNull("PopupWindow should not be null", popupWindow);
        TextView textView = (TextView)popupWindow.getContentView().findViewById(R.id.tooltip_text);
        Assert.assertEquals("Text is not correct", "tooltiptext2", textView.getText().toString());
        Assert.assertTrue("Should show the popup window", instance1.mPopupWindow.isShowing());

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        int count =  sharedPreferences.getInt(ToolTipWindow.SHARED_PREF_PREFIX + ToolTipWindow.EDIT, 0);

        Assert.assertEquals("Count is not correct", 0, count);
    }

    @Test
    public void ShowToolTip_WhenCalledMoreThanOnceDismissedAndIncremented_ShouldIncrementCount() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);

        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        RecyclerView accountsList = (RecyclerView) activity.findViewById(R.id.phoenix_manage_accounts_list);
        ManageAccountsAdapter adapter = (ManageAccountsAdapter) accountsList.getAdapter();
        adapter.enableEditMode();
        // need to call these two methods to force recyclerview to manually lay out and measure its views
        // or findViewHolderForAdapterPosition will return null
        accountsList.measure(0, 0);
        accountsList.layout(0, 0, 100, 10000);

        View accountItem = accountsList.findViewHolderForAdapterPosition(0).itemView;
        // Call 1
        ToolTipWindow instance1 = spy(new ToolTipWindow(mContext));
        doReturn(true).when(instance1).isAnchorViewVisible(any(View.class));
        instance1.showToolTip(accountItem, ToolTipWindow.REMOVE, new SpannedString("tooltiptext"));

        PopupWindow popupWindow = ShadowApplication.getInstance().getLatestPopupWindow();
        Assert.assertNotNull("PopupWindow should not be null", popupWindow);
        TextView textView = (TextView) popupWindow.getContentView().findViewById(R.id.tooltip_text);
        Assert.assertEquals("Text is not correct", new String("tooltiptext"), textView.getText().toString());

        instance1.onDismiss();
        instance1.dismiss();
        //Call 2
        instance1 = spy(new ToolTipWindow(mContext));
        doReturn(true).when(instance1).isAnchorViewVisible(any(View.class));
        instance1.showToolTip(accountItem, ToolTipWindow.REMOVE, new SpannedString("tooltiptext"));

        popupWindow = ShadowApplication.getInstance().getLatestPopupWindow();
        Assert.assertNotNull("PopupWindow should not be null", popupWindow);
        textView = (TextView) popupWindow.getContentView().findViewById(R.id.tooltip_text);
        Assert.assertEquals("Text is not correct", "tooltiptext", textView.getText().toString());
        Assert.assertFalse("Should not show the popup window", instance1.mPopupWindow.isShowing());

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        int count =  sharedPreferences.getInt(ToolTipWindow.SHARED_PREF_PREFIX + ToolTipWindow.REMOVE, 0);

        Assert.assertEquals("Count is not correct", 1, count);
    }


    @Test
    public void ShowToolTip_WhenCalledMoreThanMaxCountDismissedAndIncremented_ShouldNotShowPopupAfterMaxCount() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);

        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        RecyclerView accountsList = (RecyclerView) activity.findViewById(R.id.phoenix_manage_accounts_list);
        ManageAccountsAdapter adapter = (ManageAccountsAdapter) accountsList.getAdapter();
        adapter.enableEditMode();
        // need to call these two methods to force recyclerview to manually lay out and measure its views
        // or findViewHolderForAdapterPosition will return null
        accountsList.measure(0, 0);
        accountsList.layout(0, 0, 100, 10000);

        View accountItem = accountsList.findViewHolderForAdapterPosition(0).itemView;
        ToolTipWindow instance1;
        String tooltiptext = "tooltiptext2";
        for (int i = 0; i < ToolTipWindow.MAX_COUNT; i++) {
            instance1 = spy(new ToolTipWindow(mContext));
            doReturn(true).when(instance1).isAnchorViewVisible(any(View.class));
            instance1.showToolTip(accountItem, ToolTipWindow.REMOVE, new SpannedString(tooltiptext));

            PopupWindow popupWindow = ShadowApplication.getInstance().getLatestPopupWindow();
            Assert.assertNotNull("PopupWindow should not be null", popupWindow);
            TextView textView = (TextView) popupWindow.getContentView().findViewById(R.id.tooltip_text);
            Assert.assertEquals("Text is not correct", "tooltiptext2", textView.getText().toString());
            Assert.assertTrue("Should show the popup window", instance1.mPopupWindow.isShowing());

            instance1.onDismiss();
            instance1.dismiss();
        }

        instance1 = spy(new ToolTipWindow(mContext));
        doReturn(true).when(instance1).isAnchorViewVisible(any(View.class));
        instance1.showToolTip(accountItem, ToolTipWindow.REMOVE, new SpannedString("tooltip2"));

        PopupWindow popupWindow = ShadowApplication.getInstance().getLatestPopupWindow();
        Assert.assertNotNull("PopupWindow should not be null", popupWindow);
        TextView textView = (TextView) popupWindow.getContentView().findViewById(R.id.tooltip_text);
        Assert.assertEquals("Text is not correct", tooltiptext, textView.getText().toString());
        Assert.assertFalse("Should not show the popup window", instance1.mPopupWindow.isShowing());

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        int count =  sharedPreferences.getInt(ToolTipWindow.SHARED_PREF_PREFIX + ToolTipWindow.REMOVE, 0);

        Assert.assertEquals("Count is not correct", ToolTipWindow.MAX_COUNT, count);
    }

    @Test
    public void OnClick_WhenCalledOnPopupWindow_ShouldRemoveThePopupAndIncrementTheCount() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().get();
        RecyclerView accountsList = (RecyclerView) activity.findViewById(R.id.phoenix_manage_accounts_list);
        ManageAccountsAdapter adapter = (ManageAccountsAdapter) accountsList.getAdapter();
        adapter.onCreateViewHolder((ViewGroup) activity.findViewById(android.R.id.content), 1); // ManageAccountsAdapter.TYPE_ACCOUNT_ITEM
        adapter.enableEditMode();
        ToolTipWindow toolTipWindow = spy(adapter.mRemoveButtonToolTipWindow);
        doReturn(true).when(toolTipWindow).isAnchorViewVisible(any(View.class));
        adapter.mRemoveButtonToolTipWindow = toolTipWindow;
        // need to call these two methods to force recyclerview to manually lay out and measure its views
        // or findViewHolderForAdapterPosition will return null
        accountsList.measure(0, 0);
        accountsList.layout(0, 0, 100, 10000);

        PopupWindow popupWindow = ShadowApplication.getInstance().getLatestPopupWindow();
        Assert.assertNotNull("PopupWindow should not be null", popupWindow);
        TextView textView = (TextView)popupWindow.getContentView().findViewById(R.id.tooltip_text);
        Assert.assertEquals("Text is not correct", Html.fromHtml(mContext.getResources().getString(R.string.phoenix_manage_accounts_remove_tooltip)).toString(), textView.getText().toString());
        Assert.assertTrue("Should show the popup window", popupWindow.isShowing());
        ImageButton dismissButton = (ImageButton) popupWindow.getContentView().findViewById(R.id.tooltip_dismiss);

        //Test
        adapter.mRemoveButtonToolTipWindow.onClick(dismissButton);
        adapter.mRemoveButtonToolTipWindow.onDismiss();

        Assert.assertFalse("Should not show the popup window", popupWindow.isShowing());

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        int count =  sharedPreferences.getInt(ToolTipWindow.SHARED_PREF_PREFIX + ToolTipWindow.REMOVE, 0);

        Assert.assertEquals("Count is not correct", 1, count);
    }

    @Test
    public void OnDismiss_WhenCalled_ShouldRemoveThePopupAndIncrementTheCount() throws Exception {
        TokenResponse tokenResponse = TestValues.getTestAuthCodeExchangeResponse();
        AuthManager authManager = (AuthManager) AuthManager.getInstance(mContext);
        authManager.addAccount(tokenResponse);
        ManageAccountsActivity activity = Robolectric.buildActivity(ManageAccountsActivity.class, mManageAccountIntent).create().start().postCreate(null).resume().visible().get();
        RecyclerView accountsList = (RecyclerView) activity.findViewById(R.id.phoenix_manage_accounts_list);
        ManageAccountsAdapter adapter = (ManageAccountsAdapter) accountsList.getAdapter();
        adapter.enableEditMode();
        // need to call these two methods to force recyclerview to manually lay out and measure its views
        // or findViewHolderForAdapterPosition will return null
        accountsList.measure(0, 0);
        accountsList.layout(0, 0, 100, 10000);

        View accountItem = accountsList.findViewHolderForAdapterPosition(0).itemView;

        ToolTipWindow instance1 = spy(new ToolTipWindow(mContext));
        doReturn(true).when(instance1).isAnchorViewVisible(any(View.class));
        instance1.showToolTip(accountItem, ToolTipWindow.EDIT, new SpannedString("tooltiptext1"), 24);

        PopupWindow popupWindow = ShadowApplication.getInstance().getLatestPopupWindow();
        Assert.assertNotNull("PopupWindow should not be null", popupWindow);
        TextView textView = (TextView)popupWindow.getContentView().findViewById(R.id.tooltip_text);
        Assert.assertEquals("Text is not correct", "tooltiptext1", textView.getText().toString());
        Assert.assertTrue("Should show the popup window", instance1.mPopupWindow.isShowing());

        instance1.onDismiss();

        //Call 2
        instance1 = new ToolTipWindow(mContext);
        instance1.showToolTip(accountItem, ToolTipWindow.EDIT, new SpannedString("tooltiptext"));

        popupWindow = ShadowApplication.getInstance().getLatestPopupWindow();
        Assert.assertNotNull("PopupWindow should not be null", popupWindow);
        textView = (TextView) popupWindow.getContentView().findViewById(R.id.tooltip_text);
        Assert.assertEquals("Text is not correct", "tooltiptext1", textView.getText().toString());
        Assert.assertFalse("Should not show the popup window", instance1.mPopupWindow.isShowing());

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(AuthHelper.SHARED_PREF_PHOENIX, Context.MODE_PRIVATE);
        int count =  sharedPreferences.getInt(ToolTipWindow.SHARED_PREF_PREFIX + ToolTipWindow.EDIT, 0);

        Assert.assertEquals("Count is not correct", 1, count);
    }

}
