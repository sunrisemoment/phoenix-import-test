package com.oath.mobile.platform.phoenix.core;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static junit.framework.Assert.assertEquals;

/**
 * Created by yuhongli on 11/3/17.
 */
@RunWith(RobolectricTestRunner.class)
public class HttpConnectionExceptionTest  extends BaseTest{

    @Test
    public void HttpConnectionException_WhenNullMessage_OutputsCorrectString() throws Exception {
        final int code = 200;
        final String message = null;
        final String responseBody = "{ \"response\" : \"body\"}";
        HttpConnectionException e = new HttpConnectionException(code, message, responseBody);
        final String errorString = "response code: 200, response body: { \"response\" : \"body\"}";
        assertEquals("Unexpected message", message, e.getMessage());
        assertEquals("Unexpected response body", responseBody, e.getRespBody());
        assertEquals("Unexpected response code", code, e.getRespCode());
        assertEquals("Unexpected error string", errorString, e.toString());
    }

    @Test
    public void HttpConnectionException_WhenNullResponseBody_OutputsCorrectString() throws Exception {
        final int code = 200;
        final String message = "message";
        HttpConnectionException e = new HttpConnectionException(code, message);
        final String errorString = "message, response code: 200, response body: null";
        assertEquals("Unexpected message", message, e.getMessage());
        assertEquals("Unexpected response body", null, e.getRespBody());
        assertEquals("Unexpected response code", code, e.getRespCode());
        assertEquals("Unexpected error string", errorString, e.toString());
    }

    @Test
    public void HttpConnectionException_WhenAllParamsSpecified_OutputsCorrectString() throws Exception {
        final int code = 200;
        final String message = "message";
        final String responseBody = "{ \"response\" : \"body\"}";
        HttpConnectionException e = new HttpConnectionException(code, message, responseBody);
        final String errorString = "message, response code: 200, response body: { \"response\" : \"body\"}";
        assertEquals("Unexpected message", message, e.getMessage());
        assertEquals("Unexpected response body", responseBody, e.getRespBody());
        assertEquals("Unexpected response code", code, e.getRespCode());
        assertEquals("Unexpected error string", errorString, e.toString());
    }
}
